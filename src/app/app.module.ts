import { CommonModule } from '@angular/common';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import {
  BrowserModule,
  HammerModule,
  HAMMER_GESTURE_CONFIG,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { MyHammerGestureConfig } from './core/classes/hammer-gesture-config';
import { HttpLoaderFactory } from './core/i18n/constants/translate.constants';
import { AppInterceptorService } from './core/interceptors/app-interceptor.service';
import { AppMockInterceptorService } from './core/interceptors/app-mock-interceptor.service';
import { AppTrackInterceptorService } from './core/interceptors/app-track-interceptor.service';
import { AppTranslateInterceptor } from './core/interceptors/app-translate.interceptor';
import { AppInitializer } from './domain/services/app-initializer.service';
import { appEffects, appReducers } from './domain/store';
import { AppComponent } from './presentation/layout/components/app/app.component';
import { LayoutModule } from './presentation/layout/layout.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HammerModule,
    LayoutModule,
    AppRoutingModule,
    NgIdleKeepaliveModule.forRoot(),
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot(appEffects),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      useDefaultLang: true,
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppTranslateInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppMockInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppTrackInterceptorService,
      multi: true,
    },
    { provide: HAMMER_GESTURE_CONFIG, useClass: MyHammerGestureConfig },
    { provide: APP_INITIALIZER, useFactory: AppInitializer, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
