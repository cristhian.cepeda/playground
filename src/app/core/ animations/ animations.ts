import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const inOutAnimation = [
  trigger('inOutAnimation', [
    transition(':enter', [
      style({ opacity: 0 }),
      animate('300ms 300ms ease-in-out', style({ opacity: 1 })),
    ]),
    transition(':leave', [
      style({ opacity: 1 }),
      animate('300ms ease-in-out', style({ opacity: 0 })),
    ]),
  ]),
];

export const expandCollapseAnimation = [
  trigger('expandCollapseAnimation', [
    state('ex', style({ maxHeight: '*', overflowY: 'auto' })),
    state('coll', style({ height: '0px', overflow: 'hidden' })),
    transition('ex <=> coll', animate('400ms ease-in')),
  ]),
];
