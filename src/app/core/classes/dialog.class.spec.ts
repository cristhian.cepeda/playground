import { TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogComponent } from '@app/presentation/layout/components/dialog/dialog.component';
import { DialogCallback } from '../models/dialog.model';
import { Dialog } from './dialog.class';

describe('Dialog', () => {
  let dialog: Dialog;
  let matDialogRefSpy: jasmine.SpyObj<MatDialogRef<DialogComponent>>;

  beforeEach(() => {
    matDialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);

    TestBed.configureTestingModule({
      providers: [
        Dialog,
        { provide: MatDialogRef, useValue: matDialogRefSpy },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    });
    dialog = TestBed.inject(Dialog);
    matDialogRefSpy = TestBed.inject(MatDialogRef) as jasmine.SpyObj<
      MatDialogRef<DialogComponent>
    >;
  });

  it('should be created', () => {
    expect(dialog).toBeTruthy();
  });

  it('should call close and callback on cancel click', () => {
    dialog.callbacks = { close: () => {} } as DialogCallback;
    const closeSpy = spyOn(dialog.callbacks, 'close');
    dialog.onCancelClick();
    expect(matDialogRefSpy.close).toHaveBeenCalledTimes(1);
    expect(closeSpy).toHaveBeenCalledTimes(1);
  });

  it('should call callback on ok click', () => {
    dialog.callbacks = { ok: () => {} } as DialogCallback;
    const okSpy = spyOn(dialog.callbacks, 'ok');
    dialog.onOkClick();
    expect(okSpy).toHaveBeenCalledTimes(1);
  });

  it('should call close and callback on close click', () => {
    dialog.callbacks = { cancel: () => {} } as DialogCallback;
    const cancelSpy = spyOn(dialog.callbacks, 'cancel');
    dialog.onCloseClick();
    expect(matDialogRefSpy.close).toHaveBeenCalledTimes(1);
    expect(cancelSpy).toHaveBeenCalledTimes(1);
  });
});
