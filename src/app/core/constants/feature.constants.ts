export const FEATURES = {
  AUTH: {
    NAME: 'Auth',
    PATH: 'auth',
    PAGES: {
      LOGIN: {
        NAME: 'Login',
        PATH: 'login',
      },
      SET_PASS: {
        NAME: 'Set Pass',
        PATH: 'set-pass',
        SUB_PAGES: {
          CREATE_PASS: {
            NAME: 'Create your password',
            PATH: 'create-your-password',
          },
          SUCCESS: {
            NAME: 'Password created successfully',
            PATH: 'success',
          },
        },
      },
      FORGOT_PASSWORD: {
        NAME: 'Forgot Password',
        PATH: 'forgot-password',
        SUB_PAGES: {
          EMAIL: {
            NAME: 'Email',
            PATH: 'email',
          },
          VERIFICATION_CODE: {
            NAME: 'Verification code',
            PATH: 'verification-code',
          },
          CREATE_PASS: {
            NAME: 'Create your password',
            PATH: 'create-your-password',
          },
          SUCCESS: {
            NAME: 'Password created successfully',
            PATH: 'success',
          },
        },
      },
    },
  },
  MY_ACCOUNT: {
    NAME: 'My Account',
    PATH: 'my-account',
  },
};
