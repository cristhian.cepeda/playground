import { PAGE_SIZE_DEFAULT } from '@app/presentation/layout/mo-tables/constants/page-size.constant';
import { Filters } from '../models/filters.model';
import { GROUPS } from './groups.constants';

const NUMBER_REGEX = '[0-9]+';
const ID_REGEX = '[0-9a-z-]+';

/**
 * @description Constant for default filters
 */
export const FILTERS: Filters = {
  limit: PAGE_SIZE_DEFAULT,
  offset: 0,
};

export const BACK_FILTERS_DATE_FORMAT = 'YYYY-MM-dd';
export const BACK_DATE_FORMAT_LANGUAJE = 'en';

export const DEFAULT_USERS_FILTERS = {
  group_name: GROUPS.ADMINS,
  limit: 10,
  offset: 0,
};

export const MOCK_TABLE_FILTER = {
  offset: NUMBER_REGEX,
  limit: NUMBER_REGEX,
  ordering: ID_REGEX,
};
