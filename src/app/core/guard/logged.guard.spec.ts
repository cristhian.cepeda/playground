import { TestBed } from '@angular/core/testing';
import { UrlTree } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { LoggedGuard } from './logged.guard';

describe('AuthGuard', () => {
  let guard: LoggedGuard;
  let store: MockStore;
  const isAuthenticatedTrueState = { auth: { isAuthenticated: true } };
  const isAuthenticatedFalseState = { auth: { isAuthenticated: false } };
  const initialState = isAuthenticatedTrueState;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [provideMockStore({ initialState })],
    });
    guard = TestBed.inject(LoggedGuard);
    store = TestBed.inject(MockStore);
    store.refreshState();
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  describe('canActivate', () => {
    it('should be can activate return true', (doneFn) => {
      store.setState(isAuthenticatedTrueState);
      guard.canActivate().subscribe((result) => {
        expect(result).toEqual(true);
        doneFn();
      });
    });

    it('should be can activate return UrlTree', (doneFn) => {
      store.setState(isAuthenticatedFalseState);
      guard.canActivate().subscribe((result) => {
        expect(result).toBeInstanceOf(UrlTree);
        doneFn();
      });
    });
  });

  describe('canActivateChild', () => {
    it('should be can activate child return true', (doneFn) => {
      store.setState(isAuthenticatedTrueState);
      guard.canActivateChild().subscribe((result) => {
        expect(result).toEqual(true);
        doneFn();
      });
    });

    it('should be can activate child return UrlTree', (doneFn) => {
      store.setState(isAuthenticatedFalseState);
      guard.canActivateChild().subscribe((result) => {
        expect(result).toBeInstanceOf(UrlTree);
        doneFn();
      });
    });
  });

  describe('canLoad', () => {
    it('should be can load return true', (doneFn) => {
      store.setState(isAuthenticatedTrueState);
      guard.canLoad().subscribe((result) => {
        expect(result).toEqual(true);
        doneFn();
      });
    });

    it('should be can load return UrlTree', (doneFn) => {
      store.setState(isAuthenticatedFalseState);
      guard.canLoad().subscribe((result) => {
        expect(result).toBeInstanceOf(UrlTree);
        doneFn();
      });
    });
  });
});
