import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Router,
  UrlTree,
} from '@angular/router';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import { combineLatest, map, Observable } from 'rxjs';
import { APP_CONSTANTS } from '../constants/app.constants';
import { FEATURES } from '../constants/feature.constants';

@Injectable({
  providedIn: 'root',
})
export class LoggedGuard implements CanActivate, CanLoad, CanActivateChild {
  constructor(private _authFacade: AuthFacade, private _router: Router) {}

  canActivateChild(): Observable<UrlTree | boolean> {
    return this._validSession();
  }

  canActivate(): Observable<UrlTree | boolean> {
    return this._validSession();
  }

  canLoad(): Observable<UrlTree | boolean> {
    return this._validSession();
  }

  private _validSession(): Observable<UrlTree | boolean> {
    return combineLatest([
      this._authFacade.isAuthenticated$,
      this._authFacade.isFirstLogin$,
    ]).pipe(
      map(([isAuthenticated, isFirstLogin]) => {
        if (isAuthenticated && !isFirstLogin) return true;
        const url: string = isFirstLogin
          ? APP_CONSTANTS.ROUTES.AUTH.SET_PASS.CREATE_PASSWORD
          : FEATURES.AUTH.PATH;
        return this._router.createUrlTree([url]);
      })
    );
  }
}
