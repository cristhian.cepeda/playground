import { TestBed } from '@angular/core/testing';
import { UrlTree } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { UnloggedGuard } from './unlogged.guard';

describe('UnloggedGuard', () => {
  let guard: UnloggedGuard;
  let store: MockStore;
  const isAuthenticatedTrueState = { auth: { isAuthenticated: true } };
  const isAuthenticatedFalseState = { auth: { isAuthenticated: false } };
  const initialState = isAuthenticatedTrueState;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [provideMockStore({ initialState })],
    });
    guard = TestBed.inject(UnloggedGuard);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  describe('canActivate', () => {
    it('should return true if the user is not authenticated', (doneFn) => {
      store.setState(isAuthenticatedFalseState);
      guard.canActivate().subscribe((result) => {
        expect(result).toEqual(true);
        doneFn();
      });
    });

    it('should return false if the user is authenticated', (doneFn) => {
      store.setState(isAuthenticatedTrueState);
      guard.canActivate().subscribe((result) => {
        expect(result).toBeInstanceOf(UrlTree);
        doneFn();
      });
    });
  });

  describe('canLoad', () => {
    it('should return true if the user is not authenticated', (doneFn) => {
      store.setState(isAuthenticatedFalseState);
      guard.canLoad().subscribe((result) => {
        expect(result).toEqual(true);
        doneFn();
      });
    });

    it('should return false if the user is authenticated', (doneFn) => {
      store.setState(isAuthenticatedTrueState);
      guard.canLoad().subscribe((result) => {
        expect(result).toBeInstanceOf(UrlTree);
        doneFn();
      });
    });
  });

  describe('canDeactivate', () => {
    it('should return true if the user is not authenticated', (doneFn) => {
      store.setState(isAuthenticatedTrueState);
      guard.canDeactivate().subscribe((result) => {
        expect(result).toEqual(true);
        doneFn();
      });
    });

    it('should return false if the user is not authenticated', (doneFn) => {
      store.setState(isAuthenticatedFalseState);
      guard.canDeactivate().subscribe((result) => {
        expect(result).toEqual(false);
        doneFn();
      });
    });
  });
});
