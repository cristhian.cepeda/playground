import {
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable } from 'rxjs';
import { HttpLoaderFactory } from './translate.constants';

class httpHandler extends HttpHandler {
  handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    throw new Error('Method implemented only for test.');
  }
}

describe('translateConstants', () => {
  describe('HttpLoaderFactory', () => {
    it('HttpLoaderFactory should be defined', () => {
      const httpClient = new HttpClient(new httpHandler());
      const result = HttpLoaderFactory(httpClient);
      expect(result).toBeInstanceOf(TranslateHttpLoader);
    });
  });
});
