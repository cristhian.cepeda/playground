import ENGLISH_OVERRIDE_BNPL_TRANSLATE from '@app/override/i18n/lang-bnpl/en';
import { BASE_ENGLISH_TRASNLATE } from '../../lang-default/en';

const ENGLISH_BNPL = {
  ...BASE_ENGLISH_TRASNLATE,
  ...ENGLISH_OVERRIDE_BNPL_TRANSLATE,
};
export default ENGLISH_BNPL;
