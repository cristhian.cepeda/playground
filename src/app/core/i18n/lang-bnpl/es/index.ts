import SPANISH_OVERRIDE_BNPL_TRANSLATE from '@app/override/i18n/lang-bnpl/es';
import { BASE_SPANISH_TRASNLATE } from '../../lang-default/es';

const SPANISH_BNPL = {
  ...BASE_SPANISH_TRASNLATE,
  ...SPANISH_OVERRIDE_BNPL_TRANSLATE,
};
export default SPANISH_BNPL;
