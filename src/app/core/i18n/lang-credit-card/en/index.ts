import ENGLISH_OVERRIDE_CREDIT_CARD_TRANSLATE from '@app/override/i18n/lang-credit-card/en';
import { BASE_ENGLISH_TRASNLATE } from '../../lang-default/en';

const ENGLISH_CREDIT_CARD = {
  ...BASE_ENGLISH_TRASNLATE,
  ...ENGLISH_OVERRIDE_CREDIT_CARD_TRANSLATE,
};
export default ENGLISH_CREDIT_CARD;
