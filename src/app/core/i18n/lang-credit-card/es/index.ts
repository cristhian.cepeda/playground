import SPANISH_OVERRIDE_CREDIT_CARD_TRANSLATE from '@app/override/i18n/lang-credit-card/es';
import { BASE_SPANISH_TRASNLATE } from '../../lang-default/es';

const SPANISH_CREDIT_CARD = {
  ...BASE_SPANISH_TRASNLATE,
  ...SPANISH_OVERRIDE_CREDIT_CARD_TRANSLATE,
};
export default SPANISH_CREDIT_CARD;
