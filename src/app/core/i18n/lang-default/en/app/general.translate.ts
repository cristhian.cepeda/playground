import { GeneralTranslate } from '@app/core/i18n/models/app/general-translate';

const GENERAL_TRANSLATE: GeneralTranslate = {
  YES: 'Yes',
  NO: 'No',
  EDIT: 'Edit',
};
export default GENERAL_TRANSLATE;
