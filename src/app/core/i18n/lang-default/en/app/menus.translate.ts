import { MenusTranslate } from '@app/core/i18n/models/app/menu-translate';

const MENUS_TRANSLATE: MenusTranslate = {
  GO_BACK: 'Back',
  SIDE_MENU: {
    MY_ACCOUNT: 'My Account',
  },
  NAVBAR: {
    LOGOUT: 'Log Out',
    NEED_SUPPORT: 'Need support?',
  },
};
export default MENUS_TRANSLATE;
