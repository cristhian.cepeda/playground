import { TableTranslate } from '@app/core/i18n/models/app/table-translate';

const TABLE_TRANSLATE: TableTranslate = {
  EMPTY_STATE: {
    TITLE: 'Ups!.. No results found',
  },
  OPTIONS: {
    DOWNLOAD: 'Download',
  },
};
export default TABLE_TRANSLATE;
