import { ForgotPasswordPageTranslate } from '@app/core/i18n/models/auth/forgot-password-translate';

const FORGOT_PASSWORD_TRANSLATE: ForgotPasswordPageTranslate = {
  EMAIL: {
    TITLE: 'Forgot my password',
    DESCRIPTION:
      'Enter the email with which you registered so that we <br />can recover your password',
    PLACEHOLDER_EMAIL: 'Email',
    NOTE: 'By clicking "Send code" I authorize MO to send me a verification code to <br />the email registered.',
    BTN_SEND_CODE: 'Send code',
  },
  VERIFICATION_CODE: {
    TITLE: 'Verification code',
    DESCRIPTION: 'Enter your verification code that was sent to your email.',
    ENTER_CODE: 'Enter code',
    RESEND_CODE: 'Resend code',
    ERROR: {
      TITLE: '¡Ooops!',
      DESCRIPTION: 'You have reached the maximum limit <br />of attempts',
      NOTE: 'Please try again later.',
      BTN_BACK_TO_LOGIN: 'Back to login',
    },
  },
  CREATE_PASS: {
    TITLE: 'Create your password',
    DESCRIPTION:
      'Write a password, you will use it to enter whenever <br />you want.',
    PLACEHOLDER_PASSWORD: 'Enter a password',
    PLACEHOLDER_CONFIRM_PASSWORD: 'Confirm password',
    PASSWORD_CONDITIONS: {
      INCLUDE_A_LETTER: 'Include a letter',
      INCLUDE_A_NUMBER: 'Include a number',
      HAVE_CHARACTERS_BETWEEN_LETTERS_AND_NUMBERS:
        'Have 8 characters between letters and numbers',
    },
    BTN_SAVE: 'Save',
  },
  SUCCESS: {
    TITLE: 'Password created <br />successfully',
    EMAIL_CONFIRMATION:
      'You will shortly receive a confirmation email to <br />{{email}}.',
    DESCRIPTION:
      'Now you can manage your payments and carry <br />out procedures in complete safety.',
    BTN_BACK_TO_LOGIN: 'Back to login',
  },
};
export default FORGOT_PASSWORD_TRANSLATE;
