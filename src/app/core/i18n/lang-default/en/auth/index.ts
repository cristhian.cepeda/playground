import FORGOT_PASSWORD_TRANSLATE from './forgot-password.translate';
import LOGIN_TRANSLATE from './login.translate';
import SET_PASS_TRANSLATE from './set-password.traslate';

const AUTH_TRANSLATE = {
  ...LOGIN_TRANSLATE,
  SET_PASS: { ...SET_PASS_TRANSLATE },
  FORGOT_PASSWORD: { ...FORGOT_PASSWORD_TRANSLATE },
};
export default AUTH_TRANSLATE;
