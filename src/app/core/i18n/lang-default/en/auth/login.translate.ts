import { LoginPageTranslate } from '@app/core/i18n/models/auth/login-translate';

const LOGIN_TRANSLATE: LoginPageTranslate = {
  LOGIN: {
    SLIDE: {
      TITLE: 'Welcome',
      DESCRIPTION:
        'Welcome to MO Manage, our E2E credit lifecycle management platform. Here you will find the necessary modules and capabilities to manage your credit operation and portfolio. ',
    },
    TITLE: 'Login',
    DESCRIPTION: 'Enter your credentials to access your account',
    EMAIL: 'Enter your email',
    PASSWORD: 'Enter your password',
    FORGOT_PASSWORD: 'Forgot your password?',
    LOGIN: 'Login',
  },
};
export default LOGIN_TRANSLATE;
