import { SetPassPagesTranslate } from '@app/core/i18n/models/auth/set-password-traslate';

const SET_PASS_TRANSLATE: SetPassPagesTranslate = {
  CREATE_PASSWORD: {
    TITLE: 'Create your password',
    DESCRIPTION:
      'Write a password, you will use it to enter whenever you want.',
    PLACEHOLDER_PASSWORD: 'Enter a password',
    PLACEHOLDER_CONFIRM_PASSWORD: 'Confirm password',
    PASSWORD_CONDITIONS: {
      INCLUDE_A_LETTER: 'Include a letter',
      INCLUDE_A_NUMBER: 'Include a number',
      HAVE_CHARACTERS_BETWEEN_LETTERS_AND_NUMBERS:
        'Have 8 characters between letters and numbers',
    },
    BTN_SAVE: 'Save',
  },
  CREATE_PASSWORD_SUCCESS: {
    TITLE: 'Password created <br />successfully',
    EMAIL_CONFIRMATION:
      'You will shortly receive a confirmation email to <br />{{email}}.',
    DESCRIPTION:
      'Now you can manage your payments and carry <br />out procedures in complete safety.',
    BTN_BACK_TO_LOGIN: 'Back to login',
  },
};

export default SET_PASS_TRANSLATE;
