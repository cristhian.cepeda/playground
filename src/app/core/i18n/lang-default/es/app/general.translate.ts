import { GeneralTranslate } from '@app/core/i18n/models/app/general-translate';

const GENERAL_TRANSLATE: GeneralTranslate = {
  YES: 'Si',
  NO: 'No',
  EDIT: 'Editar',
};
export default GENERAL_TRANSLATE;
