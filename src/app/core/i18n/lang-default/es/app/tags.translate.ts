import { TagsTranslate } from '@app/core/i18n/models/app/tags-translate';

const TAGS_TRANSLATE: TagsTranslate = {
  NEW: 'Nuevo',
};

export default TAGS_TRANSLATE;
