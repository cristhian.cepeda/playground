import { ForgotPasswordPageTranslate } from '@app/core/i18n/models/auth/forgot-password-translate';

const FORGOT_PASSWORD_TRANSLATE: ForgotPasswordPageTranslate = {
  EMAIL: {
    TITLE: 'Olvidé mi contraseña',
    DESCRIPTION:
      'Introduce el correo electrónico con el que te registraste <br />para que podamos recuperar tu contraseña',
    PLACEHOLDER_EMAIL: 'Correo electrónico',
    NOTE: 'Al hacer clic en "Enviar código" autorizo a MO a enviarme un código de <br />verificación al correo electrónico registrado.',
    BTN_SEND_CODE: 'Enviar código',
  },
  VERIFICATION_CODE: {
    TITLE: 'Código de verificación',
    DESCRIPTION:
      'Ingrese su código de verificación que fue enviado a su <br />correo electrónico.',
    ENTER_CODE: 'Introduzca el código',
    RESEND_CODE: 'Reenviar codigo',
    ERROR: {
      TITLE: '¡Ooops!',
      DESCRIPTION: 'Has alcanzado el límite máximo <br />de intentos',
      NOTE: 'Por favor, inténtelo de nuevo más tarde.',
      BTN_BACK_TO_LOGIN: 'Volver para iniciar sesión',
    },
  },
  CREATE_PASS: {
    TITLE: 'Crea tu contraseña',
    DESCRIPTION:
      'Escribe una contraseña, la usarás para entrar <br />cuando quieras.',
    PLACEHOLDER_PASSWORD: 'Ingrese una contraseña',
    PLACEHOLDER_CONFIRM_PASSWORD: 'Confirmar contraseña',
    PASSWORD_CONDITIONS: {
      INCLUDE_A_LETTER: 'Incluir una letra',
      INCLUDE_A_NUMBER: 'Incluir un número',
      HAVE_CHARACTERS_BETWEEN_LETTERS_AND_NUMBERS:
        'Tener 8 caracteres entre letras y números',
    },
    BTN_SAVE: 'Guardar y continuar',
  },
  SUCCESS: {
    TITLE: 'Contraseña creada <br />con éxito',
    EMAIL_CONFIRMATION:
      'En breve recibirá un correo electrónico de confirmación a <br />{{email}}.',
    DESCRIPTION:
      'Ahora puedes gestionar tus pagos y realizar <br />trámites con total seguridad.',
    BTN_BACK_TO_LOGIN: 'Volver para iniciar sesión',
  },
};
export default FORGOT_PASSWORD_TRANSLATE;
