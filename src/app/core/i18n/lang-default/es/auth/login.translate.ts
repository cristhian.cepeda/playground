import { LoginPageTranslate } from '@app/core/i18n/models/auth/login-translate';

const LOGIN_TRANSLATE: LoginPageTranslate = {
  LOGIN: {
    SLIDE: {
      TITLE: 'Bienvenidos',
      DESCRIPTION:
        'Bienvenidos a MO Manage, nuestra plataforma de gestión del ciclo de vida del crédito E2E. Aquí encontrarás los módulos y capacidades necesarias para administrar tu operación y cartera de crédito.',
    },
    TITLE: 'Iniciar sesión',
    DESCRIPTION: 'Ingresa tus credenciales para acceder a tu cuenta',
    EMAIL: 'Ingresa tu correo electrónico',
    PASSWORD: 'Ingresa tu contraseña',
    FORGOT_PASSWORD: '¿Olvidaste tu contraseña?',
    LOGIN: 'Iniciar sesión',
  },
};
export default LOGIN_TRANSLATE;
