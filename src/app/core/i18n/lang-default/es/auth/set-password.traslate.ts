import { SetPassPagesTranslate } from '@app/core/i18n/models/auth/set-password-traslate';

const SET_PASS_TRANSLATE: SetPassPagesTranslate = {
  CREATE_PASSWORD: {
    TITLE: 'Crea tu contraseña',
    DESCRIPTION:
      'Escribe una contraseña, la usarás para entrar cuando quieras.',
    PLACEHOLDER_PASSWORD: 'Ingresa una contraseña',
    PLACEHOLDER_CONFIRM_PASSWORD: 'Confirma contraseña',
    PASSWORD_CONDITIONS: {
      INCLUDE_A_LETTER: 'Incluir una letra',
      INCLUDE_A_NUMBER: 'Incluir un número',
      HAVE_CHARACTERS_BETWEEN_LETTERS_AND_NUMBERS:
        'Tener 8 caracteres entre letras y números',
    },
    BTN_SAVE: 'Guardar',
  },
  CREATE_PASSWORD_SUCCESS: {
    TITLE: 'Contraseña creada <br />con éxito',
    EMAIL_CONFIRMATION:
      'En breve recibirá un correo electrónico de confirmación a <br />{{email}}.',
    DESCRIPTION:
      'Ahora puedes gestionar tus pagos y realizar <br />trámites con total seguridad.',
    BTN_BACK_TO_LOGIN: 'Volver para iniciar sesión',
  },
};

export default SET_PASS_TRANSLATE;
