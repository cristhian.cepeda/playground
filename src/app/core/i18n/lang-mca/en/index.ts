import ENGLISH_OVERRIDE_MCA_TRANSLATE from '@app/override/i18n/lang-mca/en';
import { BASE_ENGLISH_TRASNLATE } from '../../lang-default/en';

const ENGLISH_MCA = {
  ...BASE_ENGLISH_TRASNLATE,
  ...ENGLISH_OVERRIDE_MCA_TRANSLATE,
};
export default ENGLISH_MCA;
