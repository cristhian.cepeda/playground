import ENGLISH_OVERRIDE_MICRO_CREDIT_TRANSLATE from '@app/override/i18n/lang-micro-credit/en';
import { BASE_ENGLISH_TRASNLATE } from '../../lang-default/en';

const ENGLISH_MICRO_CREDIT = {
  ...BASE_ENGLISH_TRASNLATE,
  ...ENGLISH_OVERRIDE_MICRO_CREDIT_TRANSLATE,
};
export default ENGLISH_MICRO_CREDIT;
