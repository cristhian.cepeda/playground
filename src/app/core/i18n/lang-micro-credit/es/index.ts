import SPANISH_OVERRIDE_MICRO_CREDIT_TRANSLATE from '@app/override/i18n/lang-micro-credit/es';
import { BASE_SPANISH_TRASNLATE } from '../../lang-default/es';

const SPANISH_MICRO_CREDIT = {
  ...BASE_SPANISH_TRASNLATE,
  ...SPANISH_OVERRIDE_MICRO_CREDIT_TRANSLATE,
};
export default SPANISH_MICRO_CREDIT;
