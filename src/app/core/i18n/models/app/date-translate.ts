export interface DateTranslate {
  DATE_OF_WEEK_SHORT_LIST: string[];
  MONTH_NAMES_SHORT_LIST: string[];
  RANGE_OPTIONS: DateRangeOptionsTranslate;
  BUTTONS: DateButtonsTranslate;
  DATE_OF_WEEK: DateOfWeekTranslate;
  DAYLY: string;
  WEEKLY: string;
  MONTHLY: string;
  DAYS: string;
  WEEKS: string;
  MONTHS: string;
}

export interface DateRangeOptionsTranslate {
  LAST_7_DAYS: string;
  LAST_14_DAYS: string;
  LAST_30_DAYS: string;
  LAST_3_MOUNTS: string;
  LAST_12_MOUNTS: string;
  MONTH_TO_DATE: string;
  QUARTER_TO_DATE: string;
  ALL_TIME: string;
  CUSTMOM: string;
}

export interface DateButtonsTranslate {
  CANCEL: string;
  SET_DATE: string;
}
export interface DateOfWeekTranslate {
  MONDAY: string;
  TUESDAY: string;
  WEDNESDAY: string;
  THURSDAY: string;
  FRIDAY: string;
  SATURDAY: string;
  SUNDAY: string;
}
