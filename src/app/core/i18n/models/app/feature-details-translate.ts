export interface FeatureDetailsTranslate {
  _NAME: string;
  _MENU: FeatureDetailsTranslateMenu;
}

export type FeatureDetailsTranslateMenu = {
  [key: string]: { TITLE: string; TOOLTIP_MESSAGE?: string };
};
