export interface FiltersTranslate {
  TITLE: string;
  DESCRIPTION: string;
}
export interface FiltersToolsTranslate {
  RESULTS: string;
  BTN_SEARCH: string;
  BTN_CLEAN_FILTERS: string;
  ADVANCED_FILTERS?: string;
  RECORDS_PER_PAGE?: string;
}
