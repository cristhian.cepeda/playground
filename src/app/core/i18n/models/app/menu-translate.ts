export interface MenusTranslate {
  GO_BACK: string;
  SIDE_MENU: SideMenuTranslate;
  NAVBAR: NavbarMenuTranslate;
}
export interface SideMenuTranslate {
  MY_ACCOUNT: string;
}
export interface NavbarMenuTranslate {
  LOGOUT: string;
  NEED_SUPPORT: string;
}
