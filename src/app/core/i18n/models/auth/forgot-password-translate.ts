export interface ForgotPasswordPageTranslate {
  EMAIL: ForgotPasswordEmailPageTranslate;
  VERIFICATION_CODE: ForgotPasswordVerificationCodePageTranslate;
  CREATE_PASS: ForgotPasswordCreatePasswordPageTranslate;
  SUCCESS: ForgotPasswordSuccessPageTranslate;
}
export interface ForgotPasswordEmailPageTranslate {
  TITLE: string;
  DESCRIPTION: string;
  PLACEHOLDER_EMAIL: string;
  NOTE: string;
  BTN_SEND_CODE: string;
}

export interface ForgotPasswordVerificationCodePageTranslate {
  TITLE: string;
  DESCRIPTION: string;
  ENTER_CODE: string;
  RESEND_CODE: string;
  ERROR: ForgotPasswordVerificationCodeErrorPageTranslate;
}

export interface ForgotPasswordVerificationCodeErrorPageTranslate {
  TITLE: string;
  DESCRIPTION: string;
  NOTE: string;
  BTN_BACK_TO_LOGIN: string;
}

export interface ForgotPasswordCreatePasswordPageTranslate {
  TITLE: string;
  DESCRIPTION: string;
  PLACEHOLDER_PASSWORD: string;
  PLACEHOLDER_CONFIRM_PASSWORD: string;
  PASSWORD_CONDITIONS: ForgotPasswordCreatePasswordConditionsTranslate;
  BTN_SAVE: string;
}

export interface ForgotPasswordCreatePasswordConditionsTranslate {
  INCLUDE_A_LETTER: string;
  INCLUDE_A_NUMBER: string;
  HAVE_CHARACTERS_BETWEEN_LETTERS_AND_NUMBERS: string;
}

export interface ForgotPasswordSuccessPageTranslate {
  TITLE: string;
  EMAIL_CONFIRMATION: string;
  DESCRIPTION: string;
  BTN_BACK_TO_LOGIN: string;
}
