export interface LoginPageTranslate {
  LOGIN: LoginTranslate;
}
export interface LoginTranslate {
  SLIDE: LoginSliteTranslate;
  TITLE: string;
  DESCRIPTION: string;
  EMAIL: string;
  PASSWORD: string;
  FORGOT_PASSWORD: string;
  LOGIN: string;
}

export interface LoginSliteTranslate {
  TITLE: string;
  DESCRIPTION: string;
}
