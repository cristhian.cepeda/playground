export interface SetPassPagesTranslate {
  CREATE_PASSWORD: SetPassCreatePasswordPageTranslate;
  CREATE_PASSWORD_SUCCESS: SetPassSuccessPageTranslate;
}

export interface SetPassCreatePasswordPageTranslate {
  TITLE: string;
  DESCRIPTION: string;
  PLACEHOLDER_PASSWORD: string;
  PLACEHOLDER_CONFIRM_PASSWORD: string;
  PASSWORD_CONDITIONS: SetPassCreatePasswordConditionsTranslate;
  BTN_SAVE: string;
}
export interface SetPassCreatePasswordConditionsTranslate {
  INCLUDE_A_LETTER: string;
  INCLUDE_A_NUMBER: string;
  HAVE_CHARACTERS_BETWEEN_LETTERS_AND_NUMBERS: string;
}

export interface SetPassSuccessPageTranslate {
  TITLE: string;
  EMAIL_CONFIRMATION: string;
  DESCRIPTION: string;
  BTN_BACK_TO_LOGIN: string;
}
