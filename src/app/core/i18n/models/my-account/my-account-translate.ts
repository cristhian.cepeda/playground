export interface MyAccountPageTranslate {
  ACCOUNT: AccountTranslate;
}

export interface AccountTranslate {
  TITLE: string;
  DESCRIPTION: string;
  YOUR_DETAILS: AccountYourDetailsTranslate;
  ACCOUNT_DETAILS: AccountDetailsTranslate;
  TABLE_CARD: AccountUsersTableCardTranslate;
}

export interface AccountYourDetailsTranslate {
  TITLE: string;
  NAME: string;
  EMAIL: string;
  ROL_TITLE: string;
}

export interface AccountDetailsTranslate {
  TITLE: string;
  NAME: string;
  DESCRIPTION: string;
  MO_MANAGE_FAMILY: string;
  ADD_IN_MO_PRODUCTS: string;
  COUNTRY: string;
  CURRENCY: string;
  TOP_LIMIT_TITLE: string;
  TOP_LIMIT_DESCRIPTION: string;
}

export interface AccountUsersTableCardTranslate {
  TITLE: string;
  TAB_USERS: string;
  TAB_INVITATIONS: string;
  TABLE: AccountUsersTableTranslate;
}

export interface AccountUsersTableTranslate {
  NAME: string;
  STATUS: string;
  ROL: string;
}
