import {
  HttpClient,
  HttpClientModule,
  HttpRequest,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { PRODUCT_FAMILY } from '../constants/product.constants';
import {
  DEFAULT_FAMILY,
  DEFAULT_LANGUAGE,
  LANGUAGES,
  LANGUAGE_PATH,
  LANGUAGE_SUFFIX,
  TRANSLATION_BY_PRODUCT_FAMILY,
} from '../i18n/constants/translate.constants';

import { AppTranslateInterceptor } from './app-translate.interceptor';

describe('AppTranslateInterceptor', () => {
  let http: HttpClient;
  let next: any;
  let requestMock: HttpRequest<any>;
  let interceptor: AppTranslateInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        AppTranslateInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AppTranslateInterceptor,
          multi: true,
        },
      ],
    });
    next = {
      handle: () => {
        return new Observable((subscriber) => {
          subscriber.complete();
        });
      },
    };
    http = TestBed.inject(HttpClient);
    interceptor = TestBed.inject(AppTranslateInterceptor);
  });

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });

  it('should be intercept http req and return EN', (doneFn) => {
    const family = PRODUCT_FAMILY.BNPL;
    const language = LANGUAGES.EN;
    const url = LANGUAGE_PATH + family + '/' + language + LANGUAGE_SUFFIX;
    const translationsArray = TRANSLATION_BY_PRODUCT_FAMILY[family][language];
    requestMock = new HttpRequest('GET', url);
    interceptor.intercept(requestMock, next).subscribe((result: any) => {
      expect(result?.body).toEqual(translationsArray);
      doneFn();
    });
  });

  it('should be intercept http req and return ES', (doneFn) => {
    const family = PRODUCT_FAMILY.BNPL;
    const language = LANGUAGES.ES;
    const url = LANGUAGE_PATH + family + '/' + language + LANGUAGE_SUFFIX;
    const translationsArray = TRANSLATION_BY_PRODUCT_FAMILY[family][language];
    requestMock = new HttpRequest('GET', url);
    interceptor.intercept(requestMock, next).subscribe((result: any) => {
      expect(result?.body).toEqual(translationsArray);
      doneFn();
    });
  });

  it('should be intercept http req and return default language without language', (doneFn) => {
    const family = PRODUCT_FAMILY.BNPL;
    const language = '';
    const url = LANGUAGE_PATH + family + '/' + language + LANGUAGE_SUFFIX;
    const translationsArray =
      TRANSLATION_BY_PRODUCT_FAMILY[DEFAULT_FAMILY][DEFAULT_LANGUAGE];
    requestMock = new HttpRequest('GET', url);
    interceptor.intercept(requestMock, next).subscribe((result: any) => {
      expect(result?.body).toEqual(translationsArray);
      doneFn();
    });
  });

  it('should be intercept http req and return default language without language and family', (doneFn) => {
    const family = '';
    const language = '';
    const url = LANGUAGE_PATH + family + '/' + language + LANGUAGE_SUFFIX;
    const translationsArray =
      TRANSLATION_BY_PRODUCT_FAMILY[DEFAULT_FAMILY][DEFAULT_LANGUAGE];
    requestMock = new HttpRequest('GET', url);
    interceptor.intercept(requestMock, next).subscribe((result: any) => {
      expect(result?.body).toEqual(translationsArray);
      doneFn();
    });
  });

  it('should be intercept http req and return req object mock', (doneFn) => {
    const url = '';
    requestMock = new HttpRequest('GET', url);
    const reqObjectMock = {};
    const spy = spyOn(next, 'handle').and.returnValue(of(reqObjectMock));
    interceptor.intercept(requestMock, next).subscribe((result: any) => {
      expect(spy).toHaveBeenCalled();
      expect(result).toEqual(reqObjectMock);
      doneFn();
    });
  });
});
