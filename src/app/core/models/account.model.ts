export type AccountItem = { [key: string]: AccountDetailsItem };

export interface AccountDetailsItem {
  icon?: string;
  name?: string;
}
