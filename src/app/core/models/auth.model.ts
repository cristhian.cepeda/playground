export interface Credencials {
  username: string;
  password: string;
}

export interface RefreshToken {
  refresh_token: string;
}

export interface Token {
  access_token: string;
  refresh_token: string;
  expires: number;
}

export interface TokenPayload {
  user_id: string;
  groups: string[];
  first_login: boolean;
  exp: number;
}

export interface ChangePassword {
  password: string;
  confirm_password: string;
}

export interface CreateOTP {
  email: string;
  channel: OTP_CHANNEL;
  category: OTP_CATEGORY;
}

export interface CheckOTP {
  otp_value?: string;
  email?: string;
  category?: OTP_CATEGORY;
}

export interface ForgotPassword {
  email?: string;
  otp_value?: string;
  password?: string;
  confirm_password?: string;
  category?: OTP_CATEGORY;
}

export enum OTP_CATEGORY {
  FORGOT_PASSWORD = 'forgot_password',
  LOGIN = 'login',
}

export enum OTP_CHANNEL {
  EMAIL = 'email',
  PHONE_NUMBER = 'phone_number',
}
