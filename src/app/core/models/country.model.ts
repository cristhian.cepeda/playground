export interface CountryConfig {
  countryCode?: string;
  currencyCode?: string;
  decimals?: number;
  phoneMask?: string;
  language?: string;
  phoneLength?: number;
  flag?: string;
  name?: string;
}
