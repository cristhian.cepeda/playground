export interface iDialog {
  title: string;
  message: string;
  callbacks: DialogCallback;
  onCancelClick(): void;
  onOkClick(): void;
  onCancelClick(): void;
}

export interface DialogCallback {
  close?: Function;
  ok?: Function;
  cancel?: Function;
}

export interface DialogParams {
  message: string;
  title?: string;
  size?: string;
  isHTMLMessage?: boolean;
  textAlign?: string;
  className?: string;
  callbacks?: DialogCallback;
  infoButton?: DialogButtons;
}

export interface DialogButtons {
  showButtons?: boolean;
  showButtonOk?: boolean;
  showButtonClose?: boolean;
  showButtonCancel?: boolean;
  classNameOk?: string;
  textCancel?: string;
  textOk: string;
}
