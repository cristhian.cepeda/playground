export interface Filters {
  offset?: number;
  limit?: number;
  ordering?: string;
}
export interface FiltersOptions {
  isFiltred?: boolean; // It's for validate is has any custom filter
  isNativationFilter?: boolean; // Used for paginator and page size components/filters
}
export interface FiltersValidationsOptions {
  filters?: any;
  isFiltred?: boolean;
}
