export enum IDLE_STATE {
  NOT_STARTED = 'NOT_STARTED',
  ACTIVE = 'ACTIVE',
  IDLE = 'IDLE',
  TIME_OUT = 'TIME_OUT',
}
