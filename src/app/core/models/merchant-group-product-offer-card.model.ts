import { MerchantGroupOffer } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';

export interface MerchantGroupProductOfferListItem
  extends MerchantGroupProductOfferCard {
  commission?: string;
}
export interface MerchantGroupProductOfferCard {
  isNew?: boolean;
  productName: string;
  offerId: string;
  offerName: string;
  amountMin: string;
  amountMax: string;
  numberOfInstallments: number;
  offer?: MerchantGroupOffer;
}
