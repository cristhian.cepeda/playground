export interface RoutesMock {
  url: string;
  data: any;
  method: string;
  validateParam?: boolean;
  statusCodeResponse?: number;
}
