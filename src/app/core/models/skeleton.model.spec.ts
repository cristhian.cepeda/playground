import { TestBed } from '@angular/core/testing';
import { getSkeleton, Skeleton } from './skeleton.model';

describe('SkeletonModel', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be get skeleton', () => {
    const result = getSkeleton(1, 1, {});
    expect(result).toBeInstanceOf(Array<Skeleton>);
  });
});
