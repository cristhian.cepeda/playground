import { IKeyValue } from './utils.model';

export interface SummaryGroup {
  type?: string;
  key?: string;
  value?: string | number;
  extraData?: IKeyValue[];
}
export interface SummaryDropItem {
  title?: string;
  items?: SummaryGroup[];
}
