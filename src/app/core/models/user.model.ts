import { GROUPS } from '../constants/groups.constants';
import { Filters } from './filters.model';

export interface User {
  id: string;
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  is_active: boolean;
  is_staff: boolean;
  is_superuser: boolean;
  group_name: GROUPS;
}

export enum USER_STATUS {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
}

export interface UserFilters extends Filters {
  group_name: string;
}
