import { TestBed } from '@angular/core/testing';
import { MOCK_OFFERS_BY_PRODUCT_FILTER } from '@app/plugins/modules/product-catalogue/core/constants/filters.constant';
import { PRODUCTS } from '@app/plugins/modules/product-catalogue/data/mocks/products.mock';
import {
  deepCopy,
  deepMergeWithConserveStructure,
  generateQueryParamsFromObject,
  random,
} from './utils.model';

describe('utils', () => {
  let generateQueryParamsFromObjectFunction: Function;
  let deepCopyFunction: Function;
  let randomFunction: Function;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    generateQueryParamsFromObjectFunction = generateQueryParamsFromObject;
    deepCopyFunction = deepCopy;
    randomFunction = random;
  });

  describe('generateQueryParamsFromObject', () => {
    it('should be created', () => {
      expect(generateQueryParamsFromObjectFunction).toBeTruthy();
    });

    it('should be return string', () => {
      const result = generateQueryParamsFromObjectFunction(
        MOCK_OFFERS_BY_PRODUCT_FILTER
      );
      expect(result).toBeInstanceOf(String);
    });
  });

  describe('randomFunction', () => {
    it('should be created', () => {
      expect(randomFunction).toBeTruthy();
    });

    it('should be return random without params', () => {
      const result = randomFunction();
      expect(result).toBeLessThan(100);
      expect(result).toBeGreaterThan(59);
    });

    it('should be return random with params', () => {
      const max = 200;
      const min = 100;
      const result = randomFunction(min, max);
      expect(result).toBeLessThan(max);
      expect(result).toBeGreaterThan(min);
    });
  });

  describe('deepCopy', () => {
    it('should be created', () => {
      expect(deepCopyFunction).toBeTruthy();
    });

    it('should be return products', () => {
      const products = PRODUCTS;
      const result = deepCopyFunction(products);
      expect(result).toBeInstanceOf(Array);
      expect(result).toEqual(products);
    });

    it('should be return products with preservePropertyDescriptor', () => {
      const products = PRODUCTS;
      const result = deepCopyFunction(products, true);
      expect(result).toBeInstanceOf(Array);
      expect(result).toEqual(products);
    });

    it('should be return object with source Date', () => {
      const products = [
        {
          item: new Date(),
        },
      ];
      const result = deepCopyFunction(products);
      expect(result).toBeInstanceOf(Array);
      expect(result).toEqual(products);
    });

    it('should be return object without excludedProperties as string', () => {
      const products = [
        {
          item: {
            removeProp: 'removeProp',
          },
        },
      ];
      const excludedProperties = 'removeProp';
      const result = deepCopyFunction(products, false, excludedProperties);
      expect(result).toBeInstanceOf(Array);
      expect(result).toEqual([{ item: {} }]);
    });

    it('should be return object without excludedProperties as array', () => {
      const products = [
        {
          item: {
            removeProp: 'removeProp',
          },
        },
      ];
      const excludedProperties = ['removeProp'];
      const result = deepCopyFunction(products, false, excludedProperties);
      expect(result).toBeInstanceOf(Array);
      expect(result).toEqual([{ item: {} }]);
    });
  });

  describe('deepMergeWithConserveStructure', () => {
    it('should be return object merge equal to mock whit 2 levels', () => {
      const obj1 = {
        a: 1,
        b: 2,
        c: {
          d: 3,
          e: 4,
        },
      };
      const obj2 = {
        a: 1,
        b: 2,
        c: {
          f: 5,
          g: 6,
        },
        h: 7,
        i: {
          j: 8,
          k: 9,
        },
      };
      const expectResult = {
        a: 1,
        b: 2,
        c: {
          d: 3,
          e: 4,
          f: 5,
          g: 6,
        },
        h: 7,
        i: {
          j: 8,
          k: 9,
        },
      };
      const result = deepMergeWithConserveStructure(obj1, obj2);
      expect(expectResult).toEqual(result);
    });

    it('should be return object merge equal to mock whit 3 leves', () => {
      const obj1 = {
        a: 1,
        b: 2,
        c: {
          d: 3,
          e: 4,
          l: {
            m: 5,
            n: 6,
          },
        },
      };
      const obj2 = {
        a: 1,
        b: 2,
        c: {
          f: 5,
          g: 6,
          l: {
            n: 7,
          },
        },
        h: 7,
        i: {
          j: 8,
          k: 9,
        },
      };
      const expectResult = {
        a: 1,
        b: 2,
        c: {
          d: 3,
          e: 4,
          f: 5,
          g: 6,
          l: {
            m: 5,
            n: 7,
          },
        },
        h: 7,
        i: {
          j: 8,
          k: 9,
        },
      };
      const result = deepMergeWithConserveStructure(obj1, obj2);
      expect(expectResult).toEqual(result);
    });
  });
});
