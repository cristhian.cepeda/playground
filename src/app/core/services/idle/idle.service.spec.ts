import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Keepalive, NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { IdleService } from './idle.service';

describe('IdleService', () => {
  let service: IdleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgIdleKeepaliveModule, HttpClientModule],
      providers: [Keepalive],
    });
    service = TestBed.inject(IdleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
