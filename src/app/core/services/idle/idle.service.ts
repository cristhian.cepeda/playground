import { Injectable } from '@angular/core';
import { IDLE_STATE } from '@app/core/models/idle.model';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';

@Injectable({
  providedIn: 'root',
})
export class IdleService {
  private secondsToInactive: number = 5;
  private secondsToTimeOut: number = 5;
  private intervalToRefreshToken: number = 5;
  public idleState: IDLE_STATE = IDLE_STATE.NOT_STARTED;

  constructor(private _idle: Idle, private _keepalive: Keepalive) {
    this._initIdle();
  }

  public reset(): void {
    this.idleState = IDLE_STATE.ACTIVE;
    this._idle.stop();
    this._idle.watch();
  }

  private _initIdle(): void {
    this._setIdleConfigurations();
  }

  private _setIdleConfigurations(): void {
    this._idle.setIdle(this.secondsToInactive);
    this._idle.setTimeout(this.secondsToTimeOut);
    this._idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this._setIdleListeners();
    this.reset();
  }

  private _setIdleListeners(): void {
    // Hace algo cuando el usuario se vuelve inactivo
    // this._idle.onIdleStart.subscribe(() => (this.idleState = IDLE_STATE.IDLE));

    // Hace algo cuando el usuario ya no está inactivo
    // this._idle.onIdleEnd.subscribe(() => (this.idleState = IDLE_STATE.ACTIVE));

    // Hace algo cuando el usuario ya agoto el tiempo de la sesión
    // this._idle.onTimeout.subscribe(
    //   () => (this.idleState = IDLE_STATE.TIME_OUT)
    // );

    // Hace algo mientras la cuenta regresiva del tiempo de espera cuenta
    // this._idle.onTimeoutWarning.subscribe(
    //   (seconds) => (this.countdown = seconds)
    // );

    // set keepalive parameters, omit if not using keepalive
    this._keepalive.interval(this.intervalToRefreshToken);
    // this._keepalive.onPing.subscribe(() => {
    //   // call service to refresh token
    // });
  }
}
