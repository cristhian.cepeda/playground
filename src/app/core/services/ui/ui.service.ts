import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { Dialog } from '@app/core/classes/dialog.class';
import { DialogParams } from '@app/core/models/dialog.model';
import { DialogComponent } from '@app/presentation/layout/components/dialog/dialog.component';

@Injectable({ providedIn: 'root' })
export class UIService {
  private _snackRef: MatSnackBarRef<any>;

  constructor(private _dialog?: MatDialog, private _snackBar?: MatSnackBar) {}

  public showDialog(
    data: DialogParams,
    component: ComponentType<Dialog> = DialogComponent
  ) {
    const SIZE_DIALOG = '30rem';
    const className = data.className ?? 'mat-modal';

    if (!data.isHTMLMessage) {
      const align = data.textAlign ? 'text-center' : '';
      data.message = `<p class="modal-body__message ${align}"> ${data.message} </p> `;
    }

    const confDialog = {
      width: SIZE_DIALOG,
      data,
      disableClose: true,
      panelClass: className,
    };
    this._dialog.open(component, confDialog);
  }

  public openDialog(
    component: ComponentType<Dialog> = DialogComponent,
    config: MatDialogConfig
  ): MatDialogRef<any> {
    return this._dialog.open(component, config);
  }

  public showSnackbar(msg, time = 5000, className = 'snackbar-error') {
    this._snackRef = this._snackBar.open(msg, 'x', {
      duration: time,
      panelClass: [className],
    });
    this._snackRef.onAction().subscribe(() => {
      this._snackRef.dismiss();
    });
  }
}
