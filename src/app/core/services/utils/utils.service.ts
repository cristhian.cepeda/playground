import { formatDate, Location } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { APP_CONSTANTS } from '@app/core/constants/app.constants';
import { BACK_DATE_FORMAT_LANGUAJE } from '@app/core/constants/filters.constant';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { deepCopy } from '@app/core/models/utils.model';
import { FILE_TYPE } from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor(
    private _skaneToCapital: SnakeToCapitalPipe,
    private _location: Location,
    private _router: Router
  ) {}

  public getSummayGroupByObject(obj: any): SummaryGroup[] {
    let summaryGroup: SummaryGroup[] = [];
    if (obj && Object.keys(obj).length > 0)
      summaryGroup = Object.entries(obj).map(([key, value] = obj) => {
        const isAllowedType: boolean =
          typeof value === 'string' || typeof value === 'number';

        return {
          key: `${this._skaneToCapital.transform(key)}:`,
          value: isAllowedType ? String(value) : JSON.stringify(value),
        };
      });

    return summaryGroup;
  }

  public removeQueryParams() {
    this._location.replaceState(this._router.url.split('?')[0]);
  }

  public removePaginationFilter(filters) {
    const paginationFilter: string[] = ['offset', 'limit', 'ordering'];
    filters = deepCopy(filters);
    paginationFilter.forEach((paginationFilters) => {
      delete filters[paginationFilters];
    });
    return filters;
  }

  public downloadFile(
    fileResponse: HttpResponse<string>,
    fileName: string
  ): void {
    if (fileResponse) {
      const blob = new Blob([fileResponse?.body], { type: FILE_TYPE.CSV });
      const url = window.URL.createObjectURL(blob);
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.style.display = 'none';
      a.href = url;

      a.download = `${formatDate(
        new Date(),
        APP_CONSTANTS.FILENAME_DATE_FORMAT,
        BACK_DATE_FORMAT_LANGUAJE
      )}_${fileName?.replaceAll(new RegExp(/[-\s]/, 'g'), '')}`;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    }
  }
}
