import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  ChangePassword,
  CheckOTP,
  CreateOTP,
  Credencials,
  ForgotPassword,
  RefreshToken,
  Token,
} from '@app/core/models/auth.model';
import { API_URLS } from '@app/data/constants/api.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthApi {
  constructor(private _http: HttpClient) {}

  public login(credencials: Credencials): Observable<Token> {
    return this._http.post<Token>(API_URLS.AUTH.LOGIN, credencials);
  }

  public refreshToken(refreshToken: RefreshToken): Observable<Token> {
    return this._http.post<Token>(API_URLS.AUTH.REFRESH_TOKEN, refreshToken);
  }

  public changePassword(changePassword: ChangePassword): Observable<any> {
    return this._http.post<any>(API_URLS.AUTH.CHANGE_PASSWORD, changePassword);
  }

  public createOTP(createOTP: CreateOTP): Observable<any> {
    return this._http.post<any>(API_URLS.AUTH.CREATE_OTP, createOTP);
  }

  public checkOTP(checkOTP: CheckOTP): Observable<any> {
    return this._http.post<any>(API_URLS.AUTH.CHECK_OTP, checkOTP);
  }

  public setForgotPassword(forgotPassword: ForgotPassword): Observable<any> {
    return this._http.post<any>(API_URLS.AUTH.FORGOT_PASSWORD, forgotPassword);
  }
}
