import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  CONTENT_TYPE,
  HTTP_HEADERS,
  RESPONSE_OBSERVE,
  RESPONSE_TYPE,
} from '@app/core/constants/http.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FilesApi {
  constructor(private _http: HttpClient) {}

  public downloadFile(
    url: string,
    filters,
    contentType: CONTENT_TYPE = CONTENT_TYPE.TEXT_CSV
  ): Observable<HttpResponse<string>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });

    let headers = new HttpHeaders();
    headers = headers.set(HTTP_HEADERS.CONTENT_TYPE, contentType);

    return this._http.get(url, {
      responseType: RESPONSE_TYPE.TEXT,
      observe: RESPONSE_OBSERVE.RESPONSE,
      headers,
      params,
    });
  }
}
