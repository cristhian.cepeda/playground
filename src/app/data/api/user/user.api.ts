import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User, UserFilters } from '@app/core/models/user.model';
import { API_URLS } from '@app/data/constants/api.constants';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserApi {
  constructor(private _http: HttpClient) {}

  public getUserInfo(): Observable<User> {
    return this._http.get<User>(API_URLS.USER.GET_INFO);
  }

  public getUsersList(filters: UserFilters): Observable<TableResponse<User>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<User>>(API_URLS.USER.GET_USERS_LIST, {
      params,
    });
  }
}
