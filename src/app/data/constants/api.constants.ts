export const API_URLS = {
  AUTH: {
    LOGIN: '/authentication/token',
    REFRESH_TOKEN: '/refresh_token',
    CHANGE_PASSWORD: '/authentication/change-password',
    CREATE_OTP: '/authentication/otp',
    CHECK_OTP: '/authentication/check-otp',
    FORGOT_PASSWORD: '/authentication/forgot-password',
  },
  USER: {
    GET_INFO: '/user',
    GET_USERS_LIST: '/projects/users',
  },
  PROJECT: {
    GET_PROJECT: '/projects',
  },
};
