import { TestBed } from '@angular/core/testing';
import { CookiesService } from './cookies.service';

describe('CookiesService', () => {
  let service: CookiesService;
  let expiredDate: Date | number;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CookiesService);
    expiredDate = new Date(
      new Date().setMinutes(new Date().getMinutes() + 30)
    ).getTime();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('setCookie', () => {
    it('should be set cookie', () => {
      const cookie: string = 'my-cookie=cookievalue;';
      spyOnProperty(document, 'cookie').and.returnValue(cookie);
      service.setCookie('my-cookie', 'cookievalue', 1);
      expect(document.cookie).toContain(cookie);
    });

    it('should be set cookie with path', () => {
      const cookie: string = 'my-cookie=cookievalue;';
      spyOnProperty(document, 'cookie').and.returnValue(cookie);
      service.setCookie('my-cookie', 'cookievalue', 1, '/');
      expect(document.cookie).toContain(cookie);
    });
  });

  describe('getCookie', () => {
    it('should be get cookie', () => {
      service.setCookie('my-cookie', 'cookievalue', expiredDate, '/');
      const result = service.getCookie('my-cookie');
      expect(result).toContain('cookievalue');
    });

    it('should be get cookie undefined', () => {
      const result = service.getCookie('undefined-cookie');
      expect(result).toBeUndefined();
    });
  });

  describe('deleteCookie', () => {
    it('should be delete cookie', () => {
      service.setCookie('my-cookie', 'cookievalue', expiredDate, '/');
      service.deleteCookie('my-cookie');
      const result = service.getCookie('my-cookie');
      expect(result).toBeUndefined();
    });
  });
});
