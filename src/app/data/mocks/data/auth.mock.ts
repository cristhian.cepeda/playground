import { Token } from '@app/core/models/auth.model';

export const SESSION_TOKEN: Token = {
  access_token:
    'eyJhbGciOiJIUzI1NiIsImtpZCI6Ijk1Zjg0OGEwODM0YjQwOGY4NjdmNWNmYjU1MGYzYjc3IiwidHlwIjoiYWNjZXNzIn0.eyJncm91cHMiOlsiQURNSU5TIiwiTUVSQ0hBTlRTIl0sInVzZXJfaWQiOjIsImZpcnN0X2xvZ2luIjpmYWxzZSwiZXhwIjoxNjczOTgwOTY2fQ.QF0xOQ5yfYqHsZdxg850pBPrTWC3b8_PCNguPLhOCHw',
  refresh_token: 'eyJ1c2VyX2lkIjoxfQ.YXg1bQ.H6LM3aWEbpo99J5O3UVvI6-pib8',
  expires: new Date(
    new Date().setMinutes(new Date().getMinutes() + 120)
  ).getTime(),
};
