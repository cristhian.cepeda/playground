import { GROUPS } from '@app/core/constants/groups.constants';
import { User } from '@app/core/models/user.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const USER_INFO: User = {
  id: '1',
  username: 'anuser@manage.com',
  email: 'anuser@manage.com',
  first_name: 'Mover',
  last_name: 'Front',
  is_active: true,
  is_staff: false,
  is_superuser: false,
  group_name: GROUPS.ADMINS,
};

export const USERS_LIST: TableResponse<User> = {
  filtered: 50,
  count: 10,
  results: [
    {
      id: '1',
      username: 'mover@manage.com',
      email: 'mover@manage.com',
      first_name: 'Mover',
      last_name: 'Front',
      is_active: true,
      is_staff: false,
      is_superuser: false,
      group_name: GROUPS.ADMINS,
    },
    {
      id: '2',
      username: 'twoanuser@manage.com',
      email: 'twoanuser@manage.com',
      first_name: 'Mover two',
      last_name: 'Front two',
      is_active: false,
      is_staff: false,
      is_superuser: false,
      group_name: GROUPS.ADMINS,
    },
  ],
};
