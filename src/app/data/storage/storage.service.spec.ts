import { TestBed } from '@angular/core/testing';
import { StorageService } from './storage.service';

describe('StorageService', () => {
  let service: StorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('setItem', () => {
    it('should be set item key => hello, value => world ', () => {
      const key = 'hello';
      const value = 'world';
      service.setItem(key, value);
      expect(localStorage.getItem(key)).toEqual(value);
    });

    it('should be set item key => object, value => {hello: world} ', () => {
      const key = 'object';
      const value = { hello: 'world' };
      service.setItem(key, value);
      expect(localStorage.getItem(key)).toEqual(JSON.stringify(value));
    });
  });

  describe('getItem', () => {
    it('should be get item by key => hello is equal to value => world', (doneFn) => {
      const key = 'hello';
      const value = 'world';
      localStorage.setItem(key, value);
      service.getItem(key).subscribe((data) => {
        expect(data).toEqual(value);
        doneFn();
      });
    });

    it('should be get item by key => hello is equal to value => null', (doneFn) => {
      const key = 'empty';
      localStorage.setItem(key, '');
      service.getItem(key).subscribe((data) => {
        expect(data).toEqual(null);
        doneFn();
      });
    });
  });

  describe('removeItem', () => {
    it('should be remove item by key => hello', () => {
      const key = 'hello';
      const value = 'world';
      localStorage.setItem(key, value);
      service.removeItem(key);
      expect(localStorage.getItem(key)).toEqual(null);
    });
  });
});
