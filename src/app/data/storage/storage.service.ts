import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}

  public setItem(key: string, value: any): void {
    if (!(typeof value == 'string')) value = JSON.stringify(value);
    localStorage.setItem(key, value);
  }

  public getItem(key: string): Observable<any> {
    let value = localStorage.getItem(key);
    if (!value) value = null;
    try {
      value = JSON.parse(value);
    } catch (error) {}
    return of(value ? value : null);
  }

  public removeItem(key: string) {
    localStorage.removeItem(key);
  }
}
