import { TestBed } from '@angular/core/testing';
import { Token } from '@app/core/models/auth.model';
import { CookiesService } from '@app/data/cookies/cookies.service';
import { SESSION_TOKEN } from '@app/data/mocks/data/auth.mock';

import * as authActions from '@app/domain/store/auth/auth.actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })],
    });
    service = TestBed.inject(AuthenticationService);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getSessionToken', () => {
    it('should return the session token undefined', () => {
      service.logout();
      const tokenSession = service.getSessionToken();
      expect(tokenSession).toBeUndefined();
    });

    it('should return the session token defined', () => {
      const tokenMock: Token = SESSION_TOKEN;
      service.updateSessionToken(tokenMock);
      const tokenSession = service.getSessionToken();
      expect(tokenSession).toEqual(tokenMock);
    });
  });

  describe('checkSession', () => {
    it('should logout check session no have token', () => {
      const _cookieService = TestBed.inject(CookiesService);
      const cookieServiceSpy = spyOn(_cookieService, 'getCookie');
      cookieServiceSpy.and.returnValue(undefined);
      const spy = spyOn(store, 'dispatch');
      service.checkSession();
      expect(spy).toHaveBeenCalledWith(authActions.logout());
    });

    it('should check session when have token', () => {
      const tokenMock: Token = SESSION_TOKEN;
      service.updateSessionToken(tokenMock);
      const spy = spyOn(store, 'dispatch');
      service.checkSession();
      expect(spy).toHaveBeenCalledWith(
        authActions.updateIsAuthenticated({ isAuthenticated: true })
      );
    });
  });

  describe('getRefreshToken', () => {
    it('should return the refresh token undefined', () => {
      const _cookieService = TestBed.inject(CookiesService);
      const spy = spyOn(_cookieService, 'getCookie');
      spy.and.returnValue(undefined);
      const result = service.getRefreshToken();
      expect(result).toBeUndefined();
    });

    it('should return the refresh token SESSION_TOKEN', () => {
      const _cookieService = TestBed.inject(CookiesService);
      const spy = spyOn(_cookieService, 'getCookie');
      spy.and.returnValue(SESSION_TOKEN);
      const result = service.getRefreshToken();
      expect(result.refresh_token).toEqual(SESSION_TOKEN.refresh_token);
    });
  });
});
