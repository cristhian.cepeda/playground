import { Injectable } from '@angular/core';
import { RefreshToken, Token } from '@app/core/models/auth.model';
import { COOKIES } from '@app/data/constants/cookies.constants';
import { CookiesService } from '@app/data/cookies/cookies.service';
import * as authActions from '@app/domain/store/auth/auth.actions';
import * as projectActions from '@app/domain/store/project/project.actions';
import * as userActions from '@store/user/user.actions';

import { Router } from '@angular/router';
import { APP_CONSTANTS } from '@app/core/constants/app.constants';
import { Store } from '@ngrx/store';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private _store: Store,
    private _cookiesService: CookiesService,
    private _router: Router,
    private _authorizationService: AuthorizationService
  ) {}

  public getSessionToken(): Token {
    return <Token>this._cookiesService.getCookie(COOKIES.TOKEN);
  }
  public getSessionEmail(): string {
    return <string>this._cookiesService.getCookie(COOKIES.EMAIL);
  }

  public checkSession() {
    const token = this._cookiesService.getCookie(COOKIES.TOKEN);
    if (!token) {
      this._validateForgotPassPath();
      return;
    }
    this._validateFirstLogin(token);
  }

  public updateSessionToken(token: Token): void {
    this._cookiesService.setCookie(COOKIES.TOKEN, token, token?.expires);
  }

  public getRefreshToken(): RefreshToken {
    const sessionToken: Token = this.getSessionToken();
    let refreshToken: RefreshToken;
    if (sessionToken) {
      refreshToken = {
        refresh_token: sessionToken?.refresh_token,
      };
    }
    return refreshToken;
  }

  public logout() {
    this._cookiesService.deleteCookie(COOKIES.TOKEN);
    this._cookiesService.deleteCookie(COOKIES.EMAIL);
  }

  public getLoggedInfo() {
    this._store.dispatch(
      authActions.updateIsAuthenticated({ isAuthenticated: true })
    );
    this._store.dispatch(projectActions.getProject());
    this._store.dispatch(userActions.getUserInfo());
  }

  private _validateForgotPassPath() {
    const FORGOT_PASS_ROUTES: string[] = [
      APP_CONSTANTS.ROUTES.AUTH.FORGOT_PASSWORD.EMAIL,
    ];
    const isForgotPassRoute: boolean = FORGOT_PASS_ROUTES.some((url) =>
      window?.location?.pathname.includes(url)
    );
    if (!isForgotPassRoute) this._store.dispatch(authActions.logout());
  }

  private _validateFirstLogin(token) {
    const isFirstLogin: boolean =
      this._authorizationService.validateFirstLogin(token);
    isFirstLogin
      ? this._router.navigateByUrl(
          APP_CONSTANTS.ROUTES.AUTH.SET_PASS.CREATE_PASSWORD
        )
      : this.getLoggedInfo();
  }
}
