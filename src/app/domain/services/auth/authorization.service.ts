import { Injectable } from '@angular/core';
import { GROUPS_ALLOWED } from '@app/core/constants/groups.constants';
import { Token, TokenPayload } from '@app/core/models/auth.model';

@Injectable({
  providedIn: 'root',
})
export class AuthorizationService {
  public validateTokenLogin(token: Token): boolean {
    const payload = token?.access_token.split('.')[1];
    const tokenPayload: TokenPayload = JSON.parse(atob(payload));
    return GROUPS_ALLOWED.some((value) =>
      tokenPayload?.groups?.includes(value)
    );
  }

  public validateFirstLogin(token: Token): boolean {
    const payload = token?.access_token.split('.')[1];
    const tokenPayload: TokenPayload = JSON.parse(atob(payload));
    return tokenPayload?.first_login;
  }
}
