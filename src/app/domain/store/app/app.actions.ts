import { LANGUAGES } from '@app/core/i18n/constants/translate.constants';
import { createAction, props } from '@ngrx/store';

export const updateLanguage = createAction(
  '[App Language] Update Language',
  props<{ language: LANGUAGES }>()
);

export const toggleModal = createAction('[App Global] Toggle Modal');
