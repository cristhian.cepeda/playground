import { LANGUAGES } from '@app/core/i18n/constants/translate.constants';
import { Menu } from '@app/core/models/menu.model';

export interface AppState {
  menu: Menu;
  language: LANGUAGES;
  showModal: boolean;
}
