import {
  ChangePassword,
  CheckOTP,
  CreateOTP,
  Credencials,
  ForgotPassword,
  RefreshToken,
  Token,
} from '@app/core/models/auth.model';
import { createAction, props } from '@ngrx/store';

export const login = createAction(
  '[Login Page] Login',
  props<{ credencials: Credencials }>()
);
export const validateTokenLogin = createAction(
  '[Auth Effects] Validate Token Login',
  props<{ token: Token; email?: string }>()
);
export const validateFirstLogin = createAction(
  '[Auth Effects] Validate First Login',
  props<{ token: Token; email?: string }>()
);
export const successLogin = createAction(
  '[Auth Effects] Success Login',
  props<{ isFirstLogin: boolean; email?: string }>()
);
export const failedLogin = createAction('[Auth Effects] Failed Login');

export const refreshToken = createAction(
  '[Idle Service] Refresh Token',
  props<{ refreshToken: RefreshToken }>()
);
export const successRefreshToken = createAction(
  '[Auth Effects] Success Refresh Token',
  props<{ token: Token }>()
);
export const failedRefreshToken = createAction(
  '[Auth Effects] Failed Refresh Token'
);

export const updateIsAuthenticated = createAction(
  '[Auth Facade] Update Is Authenticated',
  props<{ isAuthenticated: boolean }>()
);

export const logout = createAction('[Auth Effects] Logout');

export const setPassChangePassword = createAction(
  '[Auth Facade] Set Pass - Change Password',
  props<{ changePassword: ChangePassword }>()
);
export const successSetPassChangePassword = createAction(
  '[Auth Facade] Success Set Pass - Change Password'
);
export const failedSetPassChangePassword = createAction(
  '[Auth Facade] Failed Set Pass - Change Password'
);
export const createOTP = createAction(
  '[Auth Facade] Forgot Password - Create OTP',
  props<{ createOTP: CreateOTP }>()
);
export const successCreateOTP = createAction(
  '[Auth Facade] Success Forgot Password - Create OTP',
  props<{ email: string }>()
);
export const failedCreateOTP = createAction(
  '[Auth Facade] Failed Forgot Password - Create OTP'
);
export const checkOTP = createAction(
  '[Auth Facade] Forgot Password - Check OTP',
  props<{ checkOTP: CheckOTP }>()
);
export const successCheckOTP = createAction(
  '[Auth Facade] Success Forgot Password - Check OTP',
  props<{ otp_value: string }>()
);
export const failedCheckOTP = createAction(
  '[Auth Facade] Failed Forgot Password - Check OTP'
);
export const resendOTP = createAction(
  '[Auth Facade] Forgot Password - Resend OTP'
);
export const successResendOTP = createAction(
  '[Auth Facade] Success Forgot Password - Resend OTP'
);
export const failedResendOTP = createAction(
  '[Auth Facade] Failed Forgot Password - Resend OTP'
);

export const setForgotPassword = createAction(
  '[Auth Facade] Forgot Password - Create Password',
  props<{ forgotPassword: ForgotPassword }>()
);
export const successSetForgotPassword = createAction(
  '[Auth Facade] Success Forgot Password - Create Password'
);
export const failedSetForgotPassword = createAction(
  '[Auth Facade] Failed Forgot Password - Create Password'
);
export const cleanForgotPasswordData = createAction(
  '[Auth Facade] Forgot Password  - Clean Data'
);
