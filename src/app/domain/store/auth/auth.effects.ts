import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { APP_CONSTANTS } from '@app/core/constants/app.constants';
import {
  CreateOTP,
  ForgotPassword,
  OTP_CATEGORY,
  OTP_CHANNEL,
} from '@app/core/models/auth.model';
import { AuthApi } from '@app/data/api/auth/auth.api';
import { COOKIES } from '@app/data/constants/cookies.constants';
import { CookiesService } from '@app/data/cookies/cookies.service';
import { AuthenticationService } from '@app/domain/services/auth/authentication.service';
import { AuthorizationService } from '@app/domain/services/auth/authorization.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, tap, withLatestFrom } from 'rxjs';
import * as authActions from './auth.actions';
import * as authSelectors from './auth.selectors';

@Injectable()
export class AuthEffects {
  constructor(
    private _actions$: Actions,
    private _authApi: AuthApi,
    private _router: Router,
    private _store: Store,
    private _authenticationService: AuthenticationService,
    private _authorizationService: AuthorizationService,
    private _cookiesService: CookiesService
  ) {}

  login$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.login),
      switchMap(({ credencials }) =>
        this._authApi.login(credencials).pipe(
          map((token) =>
            authActions.validateTokenLogin({
              token,
              email: credencials?.username,
            })
          ),
          catchError((_) => of(authActions.failedLogin()))
        )
      )
    )
  );

  validateTokenLogin$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.validateTokenLogin),
      switchMap(({ token, email }) => {
        const isAuthorized: boolean =
          this._authorizationService.validateTokenLogin(token);
        return isAuthorized
          ? of(authActions.validateFirstLogin({ token, email }))
          : of(authActions.failedLogin());
      })
    )
  );

  validateFirstLogin$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.validateFirstLogin),
      switchMap(({ token, email }) => {
        this._authenticationService.updateSessionToken(token);
        const isFirstLogin: boolean =
          this._authorizationService.validateFirstLogin(token);
        return of(authActions.successLogin({ isFirstLogin, email }));
      })
    )
  );

  successLogin$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(authActions.successLogin),
        tap(({ isFirstLogin, email }) => {
          isFirstLogin
            ? this._cookiesService.setCookie(COOKIES.EMAIL, email, null)
            : this._authenticationService.getLoggedInfo();

          const url: string = isFirstLogin
            ? APP_CONSTANTS.ROUTES.AUTH.SET_PASS.CREATE_PASSWORD
            : APP_CONSTANTS.ROUTES.MY_ACCOUNT;
          this._router.navigateByUrl(url);
        })
      ),
    { dispatch: false }
  );

  refreshToken$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.refreshToken),
      switchMap(({ refreshToken }) =>
        this._authApi.refreshToken(refreshToken).pipe(
          map((token) => authActions.successRefreshToken({ token })),
          catchError((_) => of(authActions.failedRefreshToken()))
        )
      )
    )
  );

  logout$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(authActions.logout),
        tap(() => {
          this._authenticationService.logout();
          this._router.navigateByUrl(APP_CONSTANTS.ROUTES.AUTH.LOGIN);
        })
      ),
    { dispatch: false }
  );

  setPassChangePassword$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.setPassChangePassword),
      switchMap(({ changePassword }) =>
        this._authApi.changePassword(changePassword).pipe(
          map(() => authActions.successSetPassChangePassword()),
          catchError((_) => of(authActions.failedSetPassChangePassword()))
        )
      )
    )
  );

  successSetPassChangePassword$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(authActions.successSetPassChangePassword),
        tap(() =>
          this._router.navigateByUrl(APP_CONSTANTS.ROUTES.AUTH.SET_PASS.SUCCESS)
        )
      ),
    { dispatch: false }
  );

  createOTP$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.createOTP),
      switchMap(({ createOTP }) =>
        this._authApi.createOTP(createOTP).pipe(
          map(() => authActions.successCreateOTP({ email: createOTP?.email })),
          catchError((_) => of(authActions.failedCreateOTP()))
        )
      )
    )
  );

  successCreateOTP$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(authActions.successCreateOTP),
        tap(() =>
          this._router.navigateByUrl(
            APP_CONSTANTS.ROUTES.AUTH.FORGOT_PASSWORD.CODE
          )
        )
      ),
    { dispatch: false }
  );

  checkOTP$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.checkOTP),
      withLatestFrom(
        this._store.select(authSelectors.selectForgotPasswordEmail)
      ),
      switchMap(([{ checkOTP }, email]) => {
        checkOTP = { ...checkOTP, email };
        return this._authApi.checkOTP(checkOTP).pipe(
          map(() =>
            authActions.successCheckOTP({ otp_value: checkOTP?.otp_value })
          ),
          catchError((_) => of(authActions.failedCheckOTP()))
        );
      })
    )
  );

  successCheckOTP$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(authActions.successCheckOTP),
        tap(() =>
          this._router.navigateByUrl(
            APP_CONSTANTS.ROUTES.AUTH.FORGOT_PASSWORD.CREATE_PASS
          )
        )
      ),
    { dispatch: false }
  );

  resendOTP$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.resendOTP),
      withLatestFrom(
        this._store.select(authSelectors.selectForgotPasswordEmail)
      ),
      switchMap(([_, email]) => {
        const createOTP: CreateOTP = {
          email,
          channel: OTP_CHANNEL.EMAIL,
          category: OTP_CATEGORY.FORGOT_PASSWORD,
        };

        return this._authApi.createOTP(createOTP).pipe(
          map(() => authActions.successResendOTP()),
          catchError((_) => of(authActions.failedResendOTP()))
        );
      })
    )
  );

  setForgotPassword$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authActions.setForgotPassword),
      withLatestFrom(this._store.select(authSelectors.selectForgotPassword)),
      switchMap(([{ forgotPassword }, forgotPass]) => {
        const forgotPasswordInfo: ForgotPassword = {
          ...forgotPassword,
          ...forgotPass,
        };
        return this._authApi.setForgotPassword(forgotPasswordInfo).pipe(
          map(() => authActions.successSetForgotPassword()),
          catchError((_) => of(authActions.failedSetForgotPassword()))
        );
      })
    )
  );

  successSetForgotPassword$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(authActions.successSetForgotPassword),
        tap(() =>
          this._router.navigateByUrl(
            APP_CONSTANTS.ROUTES.AUTH.FORGOT_PASSWORD.SUCCESS
          )
        )
      ),
    { dispatch: false }
  );

  cleanForgotPasswordData$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(authActions.cleanForgotPasswordData),
        tap(() => this._router.navigateByUrl(APP_CONSTANTS.ROUTES.AUTH.LOGIN))
      ),
    { dispatch: false }
  );
}
