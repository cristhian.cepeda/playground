import { Action, createReducer, on } from '@ngrx/store';
import * as authActions from './auth.actions';
import { AuthState } from './auth.state';

export const initialAuthState: AuthState = {
  isAuthenticated: false,
};

const _authReducer = createReducer(
  initialAuthState,
  on(authActions.login, (state) => {
    return { ...state, isLoadingLogin: true };
  }),
  on(authActions.successLogin, (state, { isFirstLogin }) => {
    return {
      ...state,
      isAuthenticated: true,
      isLoadingLogin: false,
      isFirstLogin,
    };
  }),
  on(authActions.failedLogin, (state) => {
    return { ...state, isAuthenticated: false, isLoadingLogin: false };
  }),
  on(authActions.updateIsAuthenticated, (state, { isAuthenticated }) => {
    return { ...state, isAuthenticated };
  }),
  on(authActions.logout, (state) => {
    return { ...state, isAuthenticated: false };
  }),
  on(authActions.setPassChangePassword, (state) => {
    return { ...state, isLoadingChangePassword: true };
  }),
  on(authActions.successSetPassChangePassword, (state) => {
    return { ...state, isLoadingChangePassword: false };
  }),
  on(authActions.failedSetPassChangePassword, (state) => {
    return { ...state, isLoadingChangePassword: false };
  }),
  on(authActions.createOTP, authActions.resendOTP, (state) => {
    return { ...state, isLoadingCreateOTP: true };
  }),
  on(authActions.successCreateOTP, (state, { email }) => {
    return {
      ...state,
      isLoadingCreateOTP: false,
      otpErrorCounter: 0,
      forgotPassword: {
        email,
      },
    };
  }),
  on(
    authActions.failedCreateOTP,
    authActions.failedResendOTP,
    authActions.successResendOTP,
    (state) => {
      return { ...state, isLoadingCreateOTP: false };
    }
  ),
  on(authActions.checkOTP, (state) => {
    return { ...state, isLoadingCheckingOTP: true };
  }),
  on(authActions.successCheckOTP, (state, { otp_value }) => {
    return {
      ...state,
      isLoadingCheckingOTP: false,
      forgotPassword: {
        ...state.forgotPassword,
        otp_value,
      },
    };
  }),
  on(authActions.failedCheckOTP, (state) => {
    return {
      ...state,
      isLoadingCheckingOTP: false,
      otpErrorCounter: state.otpErrorCounter + 1,
    };
  }),
  on(authActions.setForgotPassword, (state) => {
    return { ...state, isLoadingForgotPassword: true };
  }),
  on(authActions.successSetForgotPassword, (state) => {
    return { ...state, isLoadingForgotPassword: false };
  }),
  on(authActions.failedSetForgotPassword, (state) => {
    return { ...state, isLoadingForgotPassword: false };
  }),
  on(authActions.cleanForgotPasswordData, (state) => {
    return {
      ...state,
      otpErrorCounter: 0,
      forgotPassword: null,
      isLoadingCreateOTP: false,
      isLoadingCheckingOTP: false,
      isLoadingForgotPassword: false,
    };
  })
);

export function AuthReducers(state: AuthState | undefined, action: Action) {
  return _authReducer(state, action);
}
