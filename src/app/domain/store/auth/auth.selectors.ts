import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from './auth.state';

export const getAuthFeatureState = createFeatureSelector<AuthState>('auth');

export const selectIsLoadingLogin = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.isLoadingLogin
);

export const selectIsAuthenticated = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.isAuthenticated
);

export const selectIsFirstLogin = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.isFirstLogin
);

export const selectIsLoadingChangePassword = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.isLoadingChangePassword
);

export const selectIsLoadingCreateOTP = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.isLoadingCreateOTP
);

export const selectForgotPassword = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.forgotPassword
);

export const selectForgotPasswordEmail = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.forgotPassword?.email
);
export const selectIsLoadingCheckingOTP = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.isLoadingCheckingOTP
);
export const selectIsLoadingForgotPassword = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.isLoadingForgotPassword
);
export const selectOTPErrorCounter = createSelector(
  getAuthFeatureState,
  (state: AuthState) => state?.otpErrorCounter
);
