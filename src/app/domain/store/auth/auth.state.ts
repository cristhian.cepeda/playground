import { ForgotPassword } from '@app/core/models/auth.model';

export interface AuthState {
  isLoadingLogin?: boolean;
  isAuthenticated: boolean;
  isFirstLogin?: boolean;
  isLoadingChangePassword?: boolean;
  isLoadingCreateOTP?: boolean;
  otpErrorCounter?: number;
  isLoadingCheckingOTP?: boolean;
  isLoadingForgotPassword?: boolean;
  forgotPassword?: ForgotPassword;
}
