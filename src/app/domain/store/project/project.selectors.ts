import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProjectState } from './project.state';

export const getProjectFeatureState =
  createFeatureSelector<ProjectState>('project');

export const selectData = createSelector(
  getProjectFeatureState,
  (state: ProjectState) => state?.data
);

export const selectPorjectTaxId = createSelector(
  getProjectFeatureState,
  (state: ProjectState) => state?.data?.default_tax_id
);

export const selectPorjectTaxValue = createSelector(
  getProjectFeatureState,
  (state: ProjectState) => state?.data?.default_tax_value
);

export const selectProjectFamily = createSelector(
  getProjectFeatureState,
  (state: ProjectState) => state?.data?.family_name
);

export const selectIsLoadingProject = createSelector(
  getProjectFeatureState,
  (state: ProjectState) => state?.isLoadingProject
);
