import { Project } from '@app/core/models/project.model';

export interface ProjectState {
  data?: Project;
  isLoadingProject?: boolean;
}
