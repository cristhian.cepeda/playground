import { User, UserFilters } from '@app/core/models/user.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const getUserInfo = createAction('[User Effects] Get User Info ');
export const successGetUserInfo = createAction(
  '[User Effects] Success Get User Info ',
  props<{ data: User }>()
);
export const failedGetUserInfo = createAction(
  '[User Effects] Failed Get User Info '
);

export const initUsersList = createAction('[User Effects] Init Users List');
export const updateFiltersUserList = createAction(
  '[User Effects] Update Filters Users List',
  props<{ filters: UserFilters }>()
);
export const successGetUsersList = createAction(
  '[User Effects] Success Get Users List',
  props<{ usersList: TableResponse<User> }>()
);
export const failedGetUsersList = createAction(
  '[User Effects] Failed Get Users List'
);
