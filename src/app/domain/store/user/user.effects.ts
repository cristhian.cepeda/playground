import { Injectable } from '@angular/core';
import { DEFAULT_USERS_FILTERS } from '@app/core/constants/filters.constant';
import { User } from '@app/core/models/user.model';
import { UserApi } from '@app/data/api/user/user.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as userActions from '@store/user/user.actions';
import * as userSelectors from '@store/user/user.selectors';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';

@Injectable()
export class UserEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _userApi: UserApi
  ) {}

  getUserInfo$ = createEffect(() =>
    this._actions$.pipe(
      ofType(userActions.getUserInfo),
      switchMap(() =>
        this._userApi.getUserInfo().pipe(
          map((data: User) => userActions.successGetUserInfo({ data })),
          catchError((_) => of(userActions.failedGetUserInfo()))
        )
      )
    )
  );

  initUsersList$ = createEffect(() =>
    this._actions$.pipe(
      ofType(userActions.initUsersList),
      map(() =>
        userActions.updateFiltersUserList({
          filters: DEFAULT_USERS_FILTERS,
        })
      )
    )
  );

  getUsersList$ = createEffect(() =>
    this._actions$.pipe(
      ofType(userActions.updateFiltersUserList),
      withLatestFrom(this._store.select(userSelectors.selectUsersListFilters)),
      switchMap(([_, filters]) =>
        this._userApi.getUsersList(filters).pipe(
          map((usersList: TableResponse<User>) =>
            userActions.successGetUsersList({ usersList })
          ),
          catchError((_) => of(userActions.failedGetUsersList()))
        )
      )
    )
  );
}
