import { Action, createReducer, on } from '@ngrx/store';
import * as userActions from '@store/user/user.actions';
import { UserState } from './user.state';

export const initialUserState: UserState = {
  user: null,
};

const _userReducer = createReducer(
  initialUserState,

  on(userActions.getUserInfo, (state) => ({
    ...state,
    isLoadingUser: true,
  })),
  on(userActions.successGetUserInfo, (state, { data }) => ({
    ...state,
    user: data,
    isLoadingUser: false,
  })),
  on(userActions.failedGetUserInfo, (state) => ({
    ...state,
    user: null,
    isLoadingUser: false,
  })),
  on(userActions.initUsersList, (state) => ({
    ...state,
    isLoadingUsersList: true,
  })),
  on(userActions.updateFiltersUserList, (state, { filters }) => ({
    ...state,
    usersListFilters: filters,
    isLoadingUsersList: true,
  })),
  on(userActions.successGetUsersList, (state, { usersList }) => ({
    ...state,
    usersList,
    isLoadingUsersList: false,
  })),
  on(userActions.failedGetUsersList, (state) => ({
    ...state,
    usersList: null,
    isLoadingUsersList: false,
  }))
);

export function UserReducers(state: UserState | undefined, action: Action) {
  return _userReducer(state, action);
}
