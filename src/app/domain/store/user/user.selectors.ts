import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState } from './user.state';

export const getUserFeatureState = createFeatureSelector<UserState>('user');

export const selectUser = createSelector(
  getUserFeatureState,
  (state: UserState) => state?.user
);
export const selectIsLoadingUser = createSelector(
  getUserFeatureState,
  (state: UserState) => state?.isLoadingUser
);
export const selectUsersList = createSelector(
  getUserFeatureState,
  (state: UserState) => state?.usersList
);
export const selectUsersListFilters = createSelector(
  getUserFeatureState,
  (state: UserState) => state?.usersListFilters
);
export const selectIsLoadingUsersList = createSelector(
  getUserFeatureState,
  (state: UserState) => state?.isLoadingUsersList
);
