import { User, UserFilters } from '@app/core/models/user.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface UserState {
  user: User;
  isLoadingUser?: boolean;
  usersList?: TableResponse<User>;
  usersListFilters?: UserFilters;
  isLoadingUsersList?: boolean;
}
