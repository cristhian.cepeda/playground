import { Injectable } from '@angular/core';
import { Project } from '@app/core/models/project.model';
import { User } from '@app/core/models/user.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import { Observable } from 'rxjs';
import { UserFacade } from '../user/user.facade';

@Injectable({
  providedIn: 'root',
})
export class MyAccountFacade {
  public user$: Observable<User>;
  public project$: Observable<Project>;
  public isLoadingProject$: Observable<boolean>;

  constructor(
    private _projectFacade: ProjectFacade,
    private _userFacade: UserFacade
  ) {
    this.user$ = this._userFacade.user$;
    this.project$ = this._projectFacade.project$;
    this.isLoadingProject$ = this._projectFacade.isLoadingProject$;
  }
}
