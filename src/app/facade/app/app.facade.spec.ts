import { TestBed } from '@angular/core/testing';
import { APP_MENU } from '@app/core/constants/menus.constants';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AppFacade } from './app.facade';

describe('AppFacade', () => {
  let facade: AppFacade;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })],
    });
    facade = TestBed.inject(AppFacade);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be created menu', (doneFn) => {
    store.setState({ app: { menu: APP_MENU } });
    facade.menu$.subscribe((menu) => {
      expect(menu).toEqual(APP_MENU);
      doneFn();
    });
  });

  it('should show Global Message return undefined', () => {
    const result = facade.showGlobalMessage('message', () => {}, 'title');
    expect(result).toBeUndefined();
  });

  it('should show Global Error return undefined', () => {
    const result = facade.showGlobalError('message');
    expect(result).toBeUndefined();
  });
});
