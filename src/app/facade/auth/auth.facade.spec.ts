import { TestBed } from '@angular/core/testing';
import { Credencials, RefreshToken, Token } from '@app/core/models/auth.model';
import { AuthenticationService } from '@app/domain/services/auth/authentication.service';
import * as authActions from '@app/domain/store/auth/auth.actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AuthFacade } from './auth.facade';

describe('AuthFacade', () => {
  let facade: AuthFacade;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })],
    });
    facade = TestBed.inject(AuthFacade);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be get Session Token', (doneFn) => {
    const _authenticationService = TestBed.inject(AuthenticationService);
    const mockToken: Token = {
      access_token: 'string',
      refresh_token: 'string',
      expires: 123,
    };
    spyOn(_authenticationService, 'getSessionToken').and.returnValue(mockToken);
    facade.getSessionToken().subscribe((token: Token) => {
      expect(token).toEqual(mockToken);
      doneFn();
    });
  });

  it('should be call logout action', () => {
    const spy = spyOn(store, 'dispatch');
    facade.logout();
    expect(spy).toHaveBeenCalledWith(authActions.logout());
  });

  it('should be call logout action', () => {
    const spy = spyOn(store, 'dispatch');
    const credencialsMock: Credencials = {
      username: 'test',
      password: 'test',
    };
    facade.login(credencialsMock);
    expect(spy).toHaveBeenCalledWith(
      authActions.login({ credencials: credencialsMock })
    );
  });

  it('should be a call refres token action', () => {
    const authService = TestBed.inject(AuthenticationService);
    const authServiceSpy = spyOn(authService, 'getRefreshToken');
    const refresTokenMock: RefreshToken = {
      refresh_token: 'test',
    };
    authServiceSpy.and.returnValue(refresTokenMock);
    const storeSpy = spyOn(store, 'dispatch');
    facade.refreshToken();
    expect(storeSpy).toHaveBeenCalledWith(
      authActions.refreshToken({ refreshToken: refresTokenMock })
    );
  });
});
