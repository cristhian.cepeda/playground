import { Injectable } from '@angular/core';
import {
  ChangePassword,
  CheckOTP,
  CreateOTP,
  Credencials,
  ForgotPassword,
  RefreshToken,
  Token,
} from '@app/core/models/auth.model';
import { AuthenticationService } from '@app/domain/services/auth/authentication.service';
import * as authActions from '@app/domain/store/auth/auth.actions';
import * as authSelectors from '@app/domain/store/auth/auth.selectors';
import { Store } from '@ngrx/store';
import { map, Observable, of } from 'rxjs';
import { APP_CONSTANTS } from '../../core/constants/app.constants';

@Injectable({ providedIn: 'root' })
export class AuthFacade {
  public isLoadingLogin$: Observable<boolean>;
  public isAuthenticated$: Observable<boolean>;
  public isFirstLogin$: Observable<boolean>;
  public isLoadingChangePassword$: Observable<boolean>;
  public isLoadingCreateOTP$: Observable<boolean>;
  public showOTPError$: Observable<boolean>;
  public isLoadingCheckingOTP$: Observable<boolean>;
  public isLoadingForgotPassword$: Observable<boolean>;
  public forgotPasswordEmail$: Observable<string>;

  constructor(
    private _store: Store,
    private _authenticationService: AuthenticationService
  ) {
    this._setSelector();
  }

  public getSessionToken(): Observable<Token> {
    return of(this._authenticationService.getSessionToken());
  }

  public getSessionEmail(): Observable<string> {
    return of(this._authenticationService.getSessionEmail());
  }

  public login(credencials: Credencials) {
    this._store.dispatch(authActions.login({ credencials }));
  }

  public refreshToken() {
    const refreshToken: RefreshToken =
      this._authenticationService.getRefreshToken();
    if (refreshToken) {
      this._store.dispatch(authActions.refreshToken({ refreshToken }));
    }
  }

  public logout() {
    this._store.dispatch(authActions.logout());
  }

  public setPassChangePassword(changePassword: ChangePassword) {
    this._store.dispatch(authActions.setPassChangePassword({ changePassword }));
  }

  public createOTP(createOTP: CreateOTP) {
    this._store.dispatch(authActions.createOTP({ createOTP }));
  }

  public checkOTP(checkOTP: CheckOTP) {
    this._store.dispatch(authActions.checkOTP({ checkOTP }));
  }

  public resendOTP() {
    this._store.dispatch(authActions.resendOTP());
  }

  public setForgotPassword(forgotPassword: ForgotPassword) {
    this._store.dispatch(authActions.setForgotPassword({ forgotPassword }));
  }

  public cleanForgotPasswordData() {
    this._store.dispatch(authActions.cleanForgotPasswordData());
  }

  private _setSelector() {
    this.isLoadingLogin$ = this._store.select(
      authSelectors.selectIsLoadingLogin
    );
    this.isAuthenticated$ = this._store.select(
      authSelectors.selectIsAuthenticated
    );
    this.isFirstLogin$ = this._store.select(authSelectors.selectIsFirstLogin);
    this.isLoadingChangePassword$ = this._store.select(
      authSelectors.selectIsLoadingChangePassword
    );
    this.isLoadingCreateOTP$ = this._store.select(
      authSelectors.selectIsLoadingCreateOTP
    );
    this.isLoadingCheckingOTP$ = this._store.select(
      authSelectors.selectIsLoadingCheckingOTP
    );
    this.isLoadingForgotPassword$ = this._store.select(
      authSelectors.selectIsLoadingForgotPassword
    );
    this.forgotPasswordEmail$ = this._store.select(
      authSelectors.selectForgotPasswordEmail
    );
    this.showOTPError$ = this._store
      .select(authSelectors.selectOTPErrorCounter)
      .pipe(map((counter) => counter >= APP_CONSTANTS.OTP_MAX_ATTEMPTS));
  }
}
