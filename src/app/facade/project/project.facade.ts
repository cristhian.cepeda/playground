import { Injectable } from '@angular/core';
import { Project } from '@app/core/models/project.model';
import * as projectSelectors from '@app/domain/store/project/project.selectors';
import { Store } from '@ngrx/store';
import { filter, Observable, take } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProjectFacade {
  public project$: Observable<Project>;
  public projectFamily$: Observable<string>;
  public isLoadingProject$: Observable<boolean>;

  constructor(private _store: Store) {
    this.project$ = this._store.select(projectSelectors.selectData);
    this.isLoadingProject$ = this._store.select(
      projectSelectors.selectIsLoadingProject
    );
    this.projectFamily$ = this._store
      .select(projectSelectors.selectProjectFamily)
      .pipe(
        filter((familyName) => !!familyName),
        take(1)
      );
  }
}
