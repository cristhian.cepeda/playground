import { Injectable } from '@angular/core';
import { User, UserFilters } from '@app/core/models/user.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import * as userActions from '@store/user/user.actions';
import * as userSelectors from '@store/user/user.selectors';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserFacade {
  public user$: Observable<User>;
  public isLoadingUser$: Observable<boolean>;
  public userList$: Observable<TableResponse<User>>;
  public isLoadingUsersList$: Observable<boolean>;

  constructor(private _store: Store) {
    this._setSelectors();
  }

  public getUserInfo() {
    this._store.dispatch(userActions.getUserInfo());
  }

  public initUsersList() {
    this._store.dispatch(userActions.initUsersList());
  }

  public updateFiltersUserList(filters: UserFilters) {
    this._store.dispatch(userActions.updateFiltersUserList({ filters }));
  }

  private _setSelectors() {
    this.user$ = this._store.select(userSelectors.selectUser);
    this.isLoadingUser$ = this._store.select(userSelectors.selectIsLoadingUser);
    this.userList$ = this._store.select(userSelectors.selectUsersList);
    this.isLoadingUsersList$ = this._store.select(
      userSelectors.selectIsLoadingUsersList
    );
  }
}
