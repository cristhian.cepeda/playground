import ENGLISH_BNPL_CUSTOMERS from '@app/plugins/modules/customers/core/i18n/lang-bnpl/en';
import ENGLISH_BNPL_LOAN_MANAGER from '@app/plugins/modules/loan-manager/core/i18n/lang-bnpl/en';
import ENGLISH_BNPL_MERCHANT_MANAGER from '@app/plugins/modules/merchant-manager/core/i18n/lang-bnpl/en';
import ENGLISH_BNPL_PRODUCT_CATALOGUE from '@app/plugins/modules/product-catalogue/core/i18n/lang-bnpl/en';

const ENGLISH_PLUGINS_BNPL_TRANSLATE = {
  CUSTOMERS: { ...ENGLISH_BNPL_CUSTOMERS },
  LOAN_MANAGER: { ...ENGLISH_BNPL_LOAN_MANAGER },
  PRODUCT_CATALOGUE: { ...ENGLISH_BNPL_PRODUCT_CATALOGUE },
  MERCHANT_MANAGER: { ...ENGLISH_BNPL_MERCHANT_MANAGER },
};
export default ENGLISH_PLUGINS_BNPL_TRANSLATE;
