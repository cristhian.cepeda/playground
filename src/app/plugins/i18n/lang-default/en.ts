import ENGLISH_DEFAULT_CUSTOMERS from '@app/plugins/modules/customers/core/i18n/lang-default/en';
import ENGLISH_DEFAULT_LOAN_MANAGER from '@app/plugins/modules/loan-manager/core/i18n/lang-default/en';
import ENGLISH_DEFAULT_MERCHANT_MANAGER from '@app/plugins/modules/merchant-manager/core/i18n/lang-default/en';
import ENGLISH_DEFAULT_PRODUCT_CATALOGUE from '@app/plugins/modules/product-catalogue/core/i18n/lang-default/en';

const ENGLISH_PLUGINS_DEFAULT_TRANSLATE = {
  CUSTOMERS: { ...ENGLISH_DEFAULT_CUSTOMERS },
  LOAN_MANAGER: { ...ENGLISH_DEFAULT_LOAN_MANAGER },
  PRODUCT_CATALOGUE: { ...ENGLISH_DEFAULT_PRODUCT_CATALOGUE },
  MERCHANT_MANAGER: { ...ENGLISH_DEFAULT_MERCHANT_MANAGER },
};
export default ENGLISH_PLUGINS_DEFAULT_TRANSLATE;
