export enum CUSTOMER_STATUS {
  ENABLED = 'enabled',
  DISABLED = 'disabled',
  DELETED = 'deleted',
  BLOCKED = 'blocked',
  UNREGISTERED = 'unregistered',
}

export enum LOANS_STATUS {
  ACTIVE = 'active',
  PAID = 'paid',
  FROZEN = 'frozen',
  CANCELED = 'canceled',
}
