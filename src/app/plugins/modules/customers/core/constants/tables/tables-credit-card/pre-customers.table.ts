import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PRE_CUSTOMERS_TABLE_DEFAULT } from '../tables-default/pre-customers.table';

export const TABLE_PRE_CUSTOMERS_TABLE_CREDIT_CARD: TableHeader[] =
  TABLE_PRE_CUSTOMERS_TABLE_DEFAULT;
