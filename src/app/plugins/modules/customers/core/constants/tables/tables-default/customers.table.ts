import { LOAN_MANAGER_URLS } from '@app/plugins/modules/customers/core/constants/urls.constants';
import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { LOANS_STATUS } from '../../status.constants';

export const TABLE_CUSTOMERS_DEFAULT: TableHeader[] = [
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'display_name',
    isSortable: true,
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
    template: null,
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.NUMBER_OF_ACTIVE_LOANS',
    size: 4,
    typeColumn: TYPE_COLUMN.COMPOUND_LINK,
    dataKey: 'active_loans',
    compoundLink: `${LOAN_MANAGER_URLS.LOANS}/`,
    compoundLinkParams: [
      {
        key: 'id',
        keyAlias: 'customer_id',
      },
      {
        key: 'status',
        value: LOANS_STATUS.ACTIVE,
      },
    ],
    isSortable: true,
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.EMAIL',
    size: 8,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'email',
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.PHONE_NUMBER',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'phone_number',
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.IDENTIFICATION_NUMBER',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'identification_number',
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.CREATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
    isSortable: true,
  },
  {
    label: 'CUSTOMERS.CUSTOMERS.TABLE.UPDATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'updated_at',
    isSortable: true,
  },
];

export const TABLE_CUSTOMER_PROFILE_ASSOCIATED_LOANS_DEFAULT: TableHeader[] = [
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.ASSOCIATED_LOANS.TABLE.ID',
    size: 4,
    typeColumn: TYPE_COLUMN.COMPOUND_LINK,
    dataKey: 'reference',
    compoundLink: `${LOAN_MANAGER_URLS.LOAN_PROFILE}/`,
    compoundLinkParams: [
      {
        key: 'id',
      },
    ],
  },
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.ASSOCIATED_LOANS.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.TITLE_CASE,
    dataKey: 'status',
  },
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.ASSOCIATED_LOANS.TABLE.LOAN_AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
  },
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.ASSOCIATED_LOANS.TABLE.CREATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
  },
];

export const TABLE_CUSTOMER_PROFILE_PAYMENTS_DEFAULT: TableHeader[] = [
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.RECENT_PAYMENTS.TABLE.REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'reference',
  },
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.RECENT_PAYMENTS.TABLE.CREDIT_REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.COMPOUND_LINK,
    dataKey: 'loan_reference',
    compoundLink: `${LOAN_MANAGER_URLS.LOAN_PROFILE}/`,
    compoundLinkParams: [
      {
        key: 'loan_id',
        keyAlias: 'id',
      },
    ],
  },
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.RECENT_PAYMENTS.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
  },
  {
    label: 'CUSTOMERS.CUSTOMER_PROFILE.RECENT_PAYMENTS.TABLE.PAID_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'paid_at',
  },
];
