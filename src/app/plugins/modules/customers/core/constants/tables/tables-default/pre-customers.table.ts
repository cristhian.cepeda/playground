import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_PRE_CUSTOMERS_TABLE_DEFAULT: TableHeader[] = [
  {
    label: 'CUSTOMERS.PRE_CUSTOMERS.TABLE.REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'CUSTOMERS.PRE_CUSTOMERS.TABLE.CREATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
    isSortable: true,
  },
  {
    label: 'CUSTOMERS.PRE_CUSTOMERS.TABLE.UPDATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'updated_at',
    isSortable: true,
  },
  {
    label: 'CUSTOMERS.PRE_CUSTOMERS.TABLE.ASSOCIATED_PRODUCT',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'products',
    template: null,
  },
];
