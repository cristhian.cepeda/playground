import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PRE_CUSTOMERS_TABLE_DEFAULT } from '../tables-default/pre-customers.table';

export const TABLE_PRE_CUSTOMERS_TABLE_MICRO_CREDIT: TableHeader[] =
  TABLE_PRE_CUSTOMERS_TABLE_DEFAULT;
