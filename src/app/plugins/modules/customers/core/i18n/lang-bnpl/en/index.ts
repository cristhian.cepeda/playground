import ENGLISH_DEFAULT_CUSTOMERS from '@app/plugins/modules/customers/core/i18n/lang-default/en';

const ENGLISH_BNPL_CUSTOMERS = {
  ...ENGLISH_DEFAULT_CUSTOMERS,
};
export default ENGLISH_BNPL_CUSTOMERS;
