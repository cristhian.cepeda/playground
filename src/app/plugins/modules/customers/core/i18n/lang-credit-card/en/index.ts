import ENGLISH_DEFAULT_CUSTOMERS from '@app/plugins/modules/customers/core/i18n/lang-default/en';

const ENGLISH_CREDIT_CARD_CUSTOMERS = {
  ...ENGLISH_DEFAULT_CUSTOMERS,
};
export default ENGLISH_CREDIT_CARD_CUSTOMERS;
