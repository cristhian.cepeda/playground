import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const CUSTOMERS_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Customers',
  _MENU: {
    OVERVIEW: { TITLE: 'Overview' },
    PRE_CUSTOMERS: {
      TITLE: 'Pre-customers',
      TOOLTIP_MESSAGE:
        'View all pre-customers in your credit operation, <br />access detailed information for each user.',
    },
    CUSTOMERS: {
      TITLE: 'Customers',
      TOOLTIP_MESSAGE:
        'View all customers in your credit operation, <br />access detailed information for each user.',
    },
  },
};
export default CUSTOMERS_FEATURE_DETAILS_TRANSLATE;
