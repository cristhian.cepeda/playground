import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const CUSTOMERS_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Clientes',
  _MENU: {
    OVERVIEW: { TITLE: 'Visión general' },
    PRE_CUSTOMERS: {
      TITLE: 'Pre-clientes',
      TOOLTIP_MESSAGE:
        'Visualiza todos los pre-clientes en tu operación de crédito, <br />accede a la información detallada de cada usuario.',
    },
    CUSTOMERS: {
      TITLE: 'Clientes',
      TOOLTIP_MESSAGE:
        'Visualiza todos los clientes de tu operación de crédito, <br />accede a la información detallada de cada usuario.',
    },
  },
};
export default CUSTOMERS_FEATURE_DETAILS_TRANSLATE;
