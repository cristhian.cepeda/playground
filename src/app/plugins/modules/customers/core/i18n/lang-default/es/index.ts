import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';
import CUSTOMERS_TRANSLATE from './customers.translate';
import CUSTOMERS_FEATURE_DETAILS_TRANSLATE from './feature-details.translate';
import OVERVIEW_TRANSLATE from './overview.translate';
import PRE_CUSTOMERS_TRANSLATE from './pre-customers.translate';

const SPANISH_DEFAULT_CUSTOMERS: FeatureDetailsTranslate = {
  ...CUSTOMERS_FEATURE_DETAILS_TRANSLATE,
  ...OVERVIEW_TRANSLATE,
  ...PRE_CUSTOMERS_TRANSLATE,
  ...CUSTOMERS_TRANSLATE,
};
export default SPANISH_DEFAULT_CUSTOMERS;
