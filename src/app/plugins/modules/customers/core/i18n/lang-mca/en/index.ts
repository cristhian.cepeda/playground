import ENGLISH_DEFAULT_CUSTOMERS from '@app/plugins/modules/customers/core/i18n/lang-default/en';

const ENGLISH_MCA_CUSTOMERS = {
  ...ENGLISH_DEFAULT_CUSTOMERS,
};
export default ENGLISH_MCA_CUSTOMERS;
