import SPANISH_DEFAULT_CUSTOMERS from '@app/plugins/modules/customers/core/i18n/lang-default/es';

const SPANISH_MICRO_CREDIT_CUSTOMERS = {
  ...SPANISH_DEFAULT_CUSTOMERS,
};
export default SPANISH_MICRO_CREDIT_CUSTOMERS;
