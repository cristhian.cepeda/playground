import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface CustomerPageTranslate {
  CUSTOMERS: CustomersTranslate;
  CUSTOMER_PROFILE: CustomerProfileTranslate;
}
export interface CustomersTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: CustomersFiltersTranslate;
  TABLE: CustomersTableTranslate;
}
export interface CustomersFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  NUMBER_OF_ACTIVE_LOANS: string;
  CUSTOMER_STATUS: string;
  // Advance filters
  CREATED_AT: string;
  UPDATED_AT: string;
  SELECT_PRODUCT: string;
}

export interface CustomersTableTranslate {
  REFERENCE: string;
  NAME: string;
  STATUS: string;
  NUMBER_OF_ACTIVE_LOANS: string;
  EMAIL: string;
  PHONE_NUMBER: string;
  IDENTIFICATION_NUMBER: string;
  CREATED_AT: string;
  UPDATED_AT: string;
}
export interface CustomerProfileTranslate {
  TITLE: string;
  DESCRIPTION: string;
  INFO: CustomerProfileInfoTranslate;
  DETAILS: CustomerProfileDetailsTranslate;
  PRODUCTS_AND_SCORES: CustomerProfileProductsAndScoreTranslate;
  ASSOCIATED_LOANS: CustomerProfileAssociatedLoansTranslate;
  RECENT_PAYMENTS: CustomerProfileRecentPaymentsTranslate;
}

export interface CustomerProfileInfoTranslate {
  CUSTOMER_ID: string;
  CREATED_AT: string;
  UPDATED_AT: string;
}

export interface CustomerProfileDetailsTranslate {
  TITLE: string;
  CONTACT: string;
  CUSTOMER_NAME: string;
  DOCUMENT_TYPE: string;
  NUMBER: string;
  EMAIL: string;
  PHONE_NUMBER: string;
  WEBSITE: string;
  OTHERS: string;
}

export interface CustomerProfileProductsAndScoreTranslate {
  TITLE: string;
  DESCRIPTION: string;
}

export interface CustomerProfileAssociatedLoansTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TABLE: CustomerProfileAssociatedLoansTableTranslate;
  BTN_VISIT_LOAN_MANAGER: string;
}

export interface CustomerProfileAssociatedLoansTableTranslate {
  ID: string;
  STATUS: string;
  LOAN_AMOUNT: string;
  CREATED_AT: string;
}

export interface CustomerProfileRecentPaymentsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TABLE: CustomerProfileRecentPaymentsTableTranslate;
  BTN_VISIT_PAYMENTS: string;
}

export interface CustomerProfileRecentPaymentsTableTranslate {
  REFERENCE: string;
  CREDIT_REFERENCE: string;
  AMOUNT: string;
  PAID_AT: string;
}
