export interface OverviewPageTranslate {
  OVERVIEW: OverviewTranslate;
}
export interface OverviewTranslate {
  TITLE: string;
  DESCRIPTION: string;
}
