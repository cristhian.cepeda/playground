import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface PreCustomerPageTransle {
  PRE_CUSTOMERS: PreCustomerTranslate;
  PRE_CUSTOMER_PROFILE: PreCustomerProfileTranslate;
}
export interface PreCustomerTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: PreCustomerFiltersTranslate;
  TABLE: PreCustomerTableTranslate;
}
export interface PreCustomerFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  ID: string;
  CREATED_AT: string;
  UPDATED_AT: string;
  SELECT_PRODUCT: string;
}

export interface PreCustomerTableTranslate {
  REFERENCE: string;
  DISPLAY_NAME: string;
  PHONE_NUMBER: string;
  CREATED_AT: string;
  UPDATED_AT: string;
  ASSOCIATED_PRODUCT: string;
}

export interface PreCustomerProfileTranslate {
  TITLE: string;
  DESCRIPTION: string;
  INFO: PreCustomerProfileInfoTranslate;
  DETAILS: PreCustomerProfileDetailsTranslate;
  PRODUCTS_AND_SCORES: PreCustomerProfileProductsAndScoreTranslate;
}

export interface PreCustomerProfileInfoTranslate {
  PRE_CUSTOMER_ID: string;
  CREATED_AT: string;
  UPDATED_AT: string;
}

export interface PreCustomerProfileDetailsTranslate {
  TITLE: string;
  CUSTOMER_NAME: string;
  DOCUMENT_TYPE: string;
  NUMBER: string;
  EMAIL: string;
  PHONE_NUMBER: string;
  OTHERS: string;
}

export interface PreCustomerProfileProductsAndScoreTranslate {
  TITLE: string;
}
