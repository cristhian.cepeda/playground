import { Filters } from '@app/core/models/filters.model';
import { CUSTOMER_STATUS } from '../constants/status.constants';
import { Loan } from './loan.model';

export interface Customer {
  id: string;
  external_id?: string;
  reference: string;
  status: CUSTOMER_STATUS;
  display_name: string;
  active_loans: number;
  email: string;
  phone_number: string;
  identification_number: string;
  created_at: Date;
  updated_at: Date;
  products?: string[];
}

export interface CustomerProfile {
  id: string;
  external_id?: string;
  created_at: Date;
  updated_at: Date;
  display_name: string;
  reference: string;
  status: string;
  email: string;
  phone_number: string;
  website?: string;
  identification_number: string;
  identification_type: string;
  others?: any;
  loans?: Loan[];
}

export interface CustomerFilters extends Filters {
  id?: string;
  created_at?: string;
  updated_at?: string;
  products?: string;
  search?: string;
  active_loans?: string;
  active_loans__gte?: string;
  status?: string;
}
