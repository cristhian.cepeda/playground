import { Filters } from '@app/core/models/filters.model';

export interface PreCustomer {
  id: string;
  external_id: string;
  reference: string;
  display_name?: string;
  phone_number?: string;
  created_at: Date;
  updated_at: Date;
  products: PreCustomerProduct[];
}

export interface PreCustomerProduct {
  id: string;
  name: string;
  status: string;
  reference: string;
}
export interface PreCustomerProfile {
  id: string;
  external_id: string;
  reference: string;
  created_at: Date;
  updated_at: Date;
  display_name: string;
  email: string;
  phone_number: string;
  website: string;
  identification_number: string;
  identification_type: string;
  others?: any;
}

export interface PreCustomerFilters extends Filters {
  reference?: string;
  created_at?: string;
  created_at__gte?: string;
  created_at__gt?: string;
  created_at__lte?: string;
  created_at__lt?: string;
  updated_at?: string;
  updated_at__gte?: string;
  updated_at__gt?: string;
  updated_at__lte?: string;
  updated_at__lt?: string;
  products?: string;
}
