import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  Customer,
  CustomerFilters,
  CustomerProfile,
} from '@app/plugins/modules/customers/core/models/customers.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class CustomerApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getCustomers(
    filters: CustomerFilters
  ): Observable<TableResponse<Customer>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Customer>>(
      API_URLS.CUSTOMERS.GET_CUSTOMERS,
      { params }
    );
  }

  public downloadCustomers(
    filters: CustomerFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(
      API_URLS.CUSTOMERS.GET_CUSTOMERS,
      filters
    );
  }

  public getCustomerProfile(customerId: string): Observable<CustomerProfile> {
    return this._http.get<CustomerProfile>(
      `${API_URLS.CUSTOMERS.GET_CUSTOMERS}/${customerId}`
    );
  }

  public getCustomerProductsAndScores(
    customerId: string
  ): Observable<ProductAndScore[]> {
    return this._http.get<ProductAndScore[]>(
      API_URLS.CUSTOMERS.GET_CUSTOMERS_PRODUCTS_AND_SCORES.replace(
        ':id',
        customerId
      )
    );
  }
}
