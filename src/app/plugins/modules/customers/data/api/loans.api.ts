import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {
  Loan,
  LoansFilters,
} from '@app/plugins/modules/customers/core/models/loan.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class LoansApi {
  constructor(private _http: HttpClient) {}

  public getLoans(filters: LoansFilters): Observable<TableResponse<Loan>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Loan>>(API_URLS.LOANS.GET_LOANS, {
      params,
    });
  }
}
