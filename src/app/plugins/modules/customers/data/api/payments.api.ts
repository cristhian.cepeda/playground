import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {
  Payment,
  PaymentFilters,
} from '@app/plugins/modules/customers/core/models/payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class PaymentsApi {
  constructor(private _http: HttpClient) {}

  public getPayments(
    filters: PaymentFilters
  ): Observable<TableResponse<Payment>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Payment>>(
      API_URLS.PAYMENTS.GET_PAYMENTS,
      {
        params,
      }
    );
  }
}
