import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  PreCustomer,
  PreCustomerFilters,
  PreCustomerProfile,
} from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class PreCustomerApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getPreCustomers(
    filters: PreCustomerFilters
  ): Observable<TableResponse<PreCustomer>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<PreCustomer>>(
      API_URLS.PRE_CUSTOMERS.GET_PRE_CUSTOMERS,
      { params }
    );
  }

  public downloadPreCustomers(
    filters: PreCustomerFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(
      API_URLS.PRE_CUSTOMERS.GET_PRE_CUSTOMERS,
      filters
    );
  }

  public getPreCustomerProfile(
    preCustomerId: string
  ): Observable<PreCustomerProfile> {
    return this._http.get<PreCustomerProfile>(
      `${API_URLS.PRE_CUSTOMERS.GET_PRE_CUSTOMERS}/${preCustomerId}`
    );
  }

  public getPreCustomerProductsAndScores(
    preCustomerId: string
  ): Observable<ProductAndScore[]> {
    return this._http.get<ProductAndScore[]>(
      API_URLS.PRE_CUSTOMERS.GET_PRE_CUSTOMERS_PRODUCTS_AND_SCORES.replace(
        ':id',
        preCustomerId
      )
    );
  }
}
