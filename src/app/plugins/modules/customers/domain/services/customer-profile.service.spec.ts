import { TestBed } from '@angular/core/testing';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { CustomerProfile } from '@app/plugins/modules/customers/core/models/customers.model';
import { CUSTOMER_PROFILE } from '@app/plugins/modules/customers/data/mocks/data/customers.mock';
import { TranslateModule } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { CustomerProfileService } from './customer-profile.service';

describe('CustomerProfileService', () => {
  let service: CustomerProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
    });
    service = TestBed.inject(CustomerProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be get Customer Items By Profile', () => {
    const customerProfile: CustomerProfile = CUSTOMER_PROFILE;
    const result = service.getCustomerItemsByProfile(customerProfile);
    expect(result).toBeInstanceOf(Observable<SummaryGroup>);
  });
});
