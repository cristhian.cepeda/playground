import { Injectable } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { CustomerProfile } from '@app/plugins/modules/customers/core/models/customers.model';
import { TranslateService } from '@ngx-translate/core';
import { map, Observable } from 'rxjs';
import { CustomerProfileDetailsTranslate } from '../../core/i18n/models/customer-translate';

@Injectable({
  providedIn: 'root',
})
export class CustomerProfileService {
  constructor(
    private _translateService: TranslateService,
    private _utilsService: UtilsService
  ) {}

  /**
   * Create the customer profile data to be used in the customer-profile details component
   * @param customerProfile
   * @returns
   */
  public getCustomerItemsByProfile(
    customerProfile: CustomerProfile
  ): Observable<SummaryGroup[]> {
    return this._translateService
      .get('CUSTOMERS.CUSTOMER_PROFILE.DETAILS')
      .pipe(
        map((DETAILS: CustomerProfileDetailsTranslate) => {
          return [
            {
              key: `${DETAILS.CUSTOMER_NAME}:`,
              value: customerProfile?.display_name,
            },
            {
              key: `${DETAILS.DOCUMENT_TYPE}:`,
              value: customerProfile?.identification_type,
            },
            {
              key: `${DETAILS.NUMBER}:`,
              value: customerProfile?.identification_number,
            },
            {
              key: `${DETAILS.EMAIL}:`,
              value: customerProfile?.email,
            },
            {
              key: `${DETAILS.PHONE_NUMBER}:`,
              value: customerProfile?.phone_number,
            },
            {
              key: `${DETAILS.WEBSITE}:`,
              value: customerProfile?.website,
            },
          ].filter((details) => details.value);
        })
      );
  }

  /**
   * Create the customer profile data to be used in the customer-profile details into "Others" section
   * @param others
   * @returns
   */
  public getCustomerOthersItemsByProfile(others: any): SummaryGroup[] {
    return this._utilsService.getSummayGroupByObject(others);
  }
}
