import { TestBed } from '@angular/core/testing';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { PreCustomerProfile } from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { TranslateModule } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { PRE_CUSTOMERS_PROFILE } from '../../data/mocks/data/pre-customers.mock';
import { PreCustomerProfileService } from './pre-customer-profile.service';

describe('PreCustomerProfileService', () => {
  let service: PreCustomerProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
    });
    service = TestBed.inject(PreCustomerProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be get pre customer Items By Profile', () => {
    const preCustomerProfile: PreCustomerProfile = PRE_CUSTOMERS_PROFILE;
    const result = service.getPreCustomerItemsByProfile(preCustomerProfile);
    expect(result).toBeInstanceOf(Observable<SummaryGroup>);
  });
});
