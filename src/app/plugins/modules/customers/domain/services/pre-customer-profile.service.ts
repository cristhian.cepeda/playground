import { Injectable } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { PreCustomerProfile } from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { TranslateService } from '@ngx-translate/core';
import { map, Observable } from 'rxjs';
import { PreCustomerProfileDetailsTranslate } from '../../core/i18n/models/pre-customer-translate';

@Injectable({
  providedIn: 'root',
})
export class PreCustomerProfileService {
  constructor(
    private _translateService: TranslateService,
    private _utilsService: UtilsService
  ) {}

  /**
   * Create the pre-customer profile data to be used in the pre-customer-profile details component
   * @param preCustomerProfile
   * @returns
   */

  public getPreCustomerItemsByProfile(
    preCustomerProfile: PreCustomerProfile
  ): Observable<SummaryGroup[]> {
    return this._translateService
      .get('CUSTOMERS.PRE_CUSTOMER_PROFILE.DETAILS')
      .pipe(
        map((DETAILS: PreCustomerProfileDetailsTranslate) => {
          return [
            {
              key: `${DETAILS.CUSTOMER_NAME}:`,
              value: preCustomerProfile?.display_name,
            },
            {
              key: `${DETAILS.DOCUMENT_TYPE}:`,
              value: preCustomerProfile?.identification_type,
            },
            {
              key: `${DETAILS.NUMBER}:`,
              value: preCustomerProfile?.identification_number,
            },
            {
              key: `${DETAILS.EMAIL}:`,
              value: preCustomerProfile?.email,
            },
            {
              key: `${DETAILS.PHONE_NUMBER}:`,
              value: preCustomerProfile?.phone_number,
            },
          ];
        })
      );
  }

  /**
   * Create the pre-customer profile data to be used in the pre-customer-profile details into "Others" section
   * @param others
   * @returns
   */
  public getPreCustomerOthersItemsByProfile(others: any): SummaryGroup[] {
    return this._utilsService.getSummayGroupByObject(others);
  }
}
