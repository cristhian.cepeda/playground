import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  Customer,
  CustomerFilters,
  CustomerProfile,
} from '@app/plugins/modules/customers/core/models/customers.model';
import { Loan } from '@app/plugins/modules/customers/core/models/loan.model';
import { Payment } from '@app/plugins/modules/customers/core/models/payment.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

// ********************
// CUSTOMERS
// ********************

export const initCustomersPage = createAction(
  '[Customers Page] Init Customers Page'
);

export const updateFiltersCustomer = createAction(
  '[Customers Filters Component] Update Filters Customers',
  props<{ filters: CustomerFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetCustomers = createAction(
  '[Customers Effects] Success Get Customers',
  props<{ customers?: TableResponse<Customer> }>()
);

export const failedGetCustomers = createAction(
  '[Customers Effects] Failed Get Customers'
);

export const downloadCustomers = createAction(
  '[Customers Page] Downloand Customers'
);

export const successDownloadCustomers = createAction(
  '[Customers Effects] success Downloand Customers',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadCustomers = createAction(
  '[Customers Effects] failed Downloand Customers'
);
// ********************
// CUSTOMER PROFILE
// ********************
export const updateCustomerId = createAction(
  '[Customer Profile Guard] Update Customer Id',
  props<{ customerId: string }>()
);

export const getCustomerProfile = createAction(
  '[Customer Profile Page] Get Customer Profile'
);

export const successGetCustomerProfile = createAction(
  '[Customers Effects] Success Get Customer  Profile',
  props<{ customerProfileData?: CustomerProfile }>()
);

export const failedGetCustomerProfile = createAction(
  '[Customers Effects] Failed Get Customer  Profile'
);

export const destroyCustomerProfilePage = createAction(
  '[Customer Profile Page] Destroy Customer Profile Page'
);

// Associated Loans
export const getCustomerAssociatedLoans = createAction(
  '[Customer Profile Page] Get Customer Associated Loans'
);

export const successGetCustomerAssociatedLoans = createAction(
  '[Customers Effects] Success Get Customer Associated Loans',
  props<{ associatedLoans: Loan[] }>()
);

export const failedGetCustomerAssociatedLoans = createAction(
  '[Customers Effects] Failed Get Customer Associated Loans'
);

// Products and scores
export const getCustomerProductsAndScores = createAction(
  '[Customer Profile Page] Get Customer Products And Scores'
);

export const successGetCustomerProductsAndScores = createAction(
  '[Customers Effects] Success Get Customer Products And Scores',
  props<{ productsAndScores: ProductAndScore[] }>()
);

export const failedGetCustomerProductsAndScores = createAction(
  '[Customers Effects] Failed Get Customer Products And Scores'
);

// Payments
export const getCustomerRecentPayments = createAction(
  '[Customer Profile Page] Get Customer Recent Payments'
);

export const successGetCustomerRecentPayments = createAction(
  '[Customers Effects] Success Get Customer Recent Payments',
  props<{ payments: Payment[] }>()
);

export const failedGetCustomerRecentPayments = createAction(
  '[Customers Effects] Failed Get Customer Recent Payments'
);
