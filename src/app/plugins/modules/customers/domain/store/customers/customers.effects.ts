import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { FILTERS_CUSTOMER_PROFILE_TABLES } from '@app/plugins/modules/customers/core/constants/filters.constant';
import { CustomerFilters } from '@app/plugins/modules/customers/core/models/customers.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { CustomerApi } from '@app/plugins/modules/customers/data/api/customer.api';
import { LoansApi } from '@app/plugins/modules/customers/data/api/loans.api';
import { PaymentsApi } from '@app/plugins/modules/customers/data/api/payments.api';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as customersActions from './customers.actions';
import * as customersSelectors from './customers.selectors';

@Injectable()
export class CustomerEffects {
  constructor(
    private _actions$: Actions,
    private _customerApi: CustomerApi,
    private _loansApi: LoansApi,
    private _paymentApi: PaymentsApi,
    private _store: Store,
    private _utilsService: UtilsService
  ) {}

  initCustomersPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(customersActions.initCustomersPage),
      map(() => {
        const filters: CustomerFilters = { ...FILTERS };
        return customersActions.updateFiltersCustomer({ filters });
      })
    )
  );

  getCustomers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(customersActions.updateFiltersCustomer),
      withLatestFrom(
        this._store.select(customersSelectors.selectCustomerFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._customerApi.getCustomers(filters).pipe(
          map((customers) =>
            customersActions.successGetCustomers({ customers })
          ),
          catchError((_) => of(customersActions.failedGetCustomers()))
        );
      })
    )
  );

  downloadCustomers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(customersActions.downloadCustomers),
      withLatestFrom(
        this._store.select(customersSelectors.selectCustomerFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._customerApi.downloadCustomers(fileFilters).pipe(
          map((fileResponse) =>
            customersActions.successDownloadCustomers({ fileResponse })
          ),
          catchError((_) => of(customersActions.failedDownloadCustomers()))
        );
      })
    )
  );

  successDownloadCustomers$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(customersActions.successDownloadCustomers),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            CUSTOMER_FEATURE.PAGES.CUSTOMERS.NAME
          )
        )
      ),
    { dispatch: false }
  );

  getCustomerProfile$ = createEffect(() =>
    this._actions$.pipe(
      ofType(customersActions.getCustomerProfile),
      withLatestFrom(this._store.select(customersSelectors.selectCustomerId)),
      switchMap(([_, customerId]) =>
        this._customerApi.getCustomerProfile(customerId).pipe(
          map((customerProfileData) =>
            customersActions.successGetCustomerProfile({ customerProfileData })
          ),
          catchError((_) => of(customersActions.failedGetCustomerProfile()))
        )
      )
    )
  );

  getCustomerAssociatedLoans$ = createEffect(() =>
    this._actions$.pipe(
      ofType(customersActions.getCustomerAssociatedLoans),
      withLatestFrom(this._store.select(customersSelectors.selectCustomerId)),
      switchMap(([_, customer_id]) =>
        this._loansApi
          .getLoans({
            ...FILTERS_CUSTOMER_PROFILE_TABLES,
            customer_id,
          })
          .pipe(
            map(({ results }) =>
              customersActions.successGetCustomerAssociatedLoans({
                associatedLoans: results,
              })
            ),
            catchError((_) =>
              of(customersActions.failedGetCustomerAssociatedLoans())
            )
          )
      )
    )
  );

  getCustomerProductsAndScores$ = createEffect(() =>
    this._actions$.pipe(
      ofType(customersActions.getCustomerProductsAndScores),
      withLatestFrom(this._store.select(customersSelectors.selectCustomerId)),
      switchMap(([_, customerId]) =>
        this._customerApi.getCustomerProductsAndScores(customerId).pipe(
          map((productsAndScores: ProductAndScore[]) =>
            customersActions.successGetCustomerProductsAndScores({
              productsAndScores,
            })
          ),
          catchError((_) =>
            of(customersActions.failedGetCustomerProductsAndScores())
          )
        )
      )
    )
  );

  getCustomerRecentPayments$ = createEffect(() =>
    this._actions$.pipe(
      ofType(customersActions.getCustomerRecentPayments),
      withLatestFrom(this._store.select(customersSelectors.selectCustomerId)),
      switchMap(([_, customer_id]) =>
        this._paymentApi
          .getPayments({
            ...FILTERS_CUSTOMER_PROFILE_TABLES,
            customer_id,
          })
          .pipe(
            map(({ results }) =>
              customersActions.successGetCustomerRecentPayments({
                payments: results,
              })
            ),
            catchError((_) =>
              of(customersActions.failedGetCustomerRecentPayments())
            )
          )
      )
    )
  );
}
