import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
  destroyCustomerProfilePage,
  failedGetCustomerAssociatedLoans,
  failedGetCustomerProductsAndScores,
  failedGetCustomerProfile,
  failedGetCustomerRecentPayments,
  failedGetCustomers,
  getCustomerAssociatedLoans,
  getCustomerProductsAndScores,
  getCustomerProfile,
  getCustomerRecentPayments,
  successGetCustomerAssociatedLoans,
  successGetCustomerProductsAndScores,
  successGetCustomerProfile,
  successGetCustomerRecentPayments,
  successGetCustomers,
  updateCustomerId,
  updateFiltersCustomer,
} from './customers.actions';
import { CustomerState } from './customers.state';

export const initialCustomerState: CustomerState = {
  filters: FILTERS,
};

const _customerReducer = createReducer(
  initialCustomerState,
  on(updateFiltersCustomer, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(successGetCustomers, (state, { customers }) => ({
    ...state,
    customers,
    isLoading: false,
  })),
  on(failedGetCustomers, (state) => ({
    ...state,
    customers: null,
    isLoading: false,
    isFiltred: false,
  })),
  on(updateCustomerId, (state, { customerId }) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      customerId,
    },
  })),
  on(getCustomerProfile, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingData: true,
    },
  })),
  on(successGetCustomerProfile, (state, { customerProfileData }) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      data: customerProfileData,
      isLoadingData: false,
    },
  })),
  on(failedGetCustomerProfile, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      data: null,
      isLoadingData: false,
    },
  })),
  on(destroyCustomerProfilePage, (state) => ({
    ...state,
    customerProfile: null,
  })),
  on(getCustomerAssociatedLoans, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingLoans: true,
    },
  })),
  on(successGetCustomerAssociatedLoans, (state, { associatedLoans }) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      loans: associatedLoans,
      isLoadingLoans: false,
    },
  })),
  on(failedGetCustomerAssociatedLoans, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingLoans: false,
      loans: null,
    },
  })),
  on(getCustomerRecentPayments, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingPayments: true,
    },
  })),
  on(successGetCustomerRecentPayments, (state, { payments }) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      payments,
      isLoadingPayments: false,
    },
  })),
  on(failedGetCustomerRecentPayments, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingPayments: false,
      payments: null,
    },
  })),
  on(getCustomerProductsAndScores, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingProductsAndScores: true,
    },
  })),
  on(successGetCustomerProductsAndScores, (state, { productsAndScores }) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingProductsAndScores: false,
      productsAndScores,
    },
  })),
  on(failedGetCustomerProductsAndScores, (state) => ({
    ...state,
    customerProfile: {
      ...state?.customerProfile,
      isLoadingProductsAndScores: false,
      productsAndScores: null,
    },
  }))
);

export function CustomerReducers(
  state: CustomerState | undefined,
  action: Action
) {
  return _customerReducer(state, action);
}
