import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CustomerState } from './customers.state';

export const getCustomerFeatureState = createFeatureSelector<CustomerState>(
  CUSTOMER_FEATURE.PAGES.CUSTOMERS.STORE_NAME
);

export const selectCustomerFilters = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.filters
);

export const selectCustomers = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customers
);

export const selectIsCustomersFiltred = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.isFiltred
);

export const selectIsLoadingCustomers = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.isLoading
);

export const selectCustomerId = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.customerId
);

export const selectIsLoadingCustomerProfile = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.isLoadingData
);

export const selectCustomerProfile = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.data
);

export const selectCustomerProfileOthers = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.data?.others
);

export const selectCustomerProfileAssociatedLoans = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.loans
);

export const selectIsLoadingCustomerProfileAssociatedLoans = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.isLoadingLoans
);

export const selectIsLoadingProductsAndScores = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.isLoadingProductsAndScores
);

export const selectProductsAndScores = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.productsAndScores
);

export const selectIsLoadingPayments = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.isLoadingPayments
);

export const selectPayments = createSelector(
  getCustomerFeatureState,
  (state: CustomerState) => state?.customerProfile?.payments
);
