import { TablePageActions } from '@app/core/models/page.model';
import {
  Customer,
  CustomerFilters,
  CustomerProfile,
} from '@app/plugins/modules/customers/core/models/customers.model';
import { Loan } from '@app/plugins/modules/customers/core/models/loan.model';
import { Payment } from '@app/plugins/modules/customers/core/models/payment.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface CustomerState extends TablePageActions {
  customers?: TableResponse<Customer>;
  filters?: CustomerFilters;
  customerProfile?: CustomerProfileState;
}

export interface CustomerProfileState {
  customerId?: string;
  data?: CustomerProfile;
  isLoadingData?: boolean;
  loans?: Loan[];
  isLoadingLoans?: boolean;
  productsAndScores?: ProductAndScore[];
  isLoadingProductsAndScores?: boolean;
  payments?: Payment[];
  isLoadingPayments?: boolean;
}
