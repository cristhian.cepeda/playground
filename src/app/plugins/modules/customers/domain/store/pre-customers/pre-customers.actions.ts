import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  PreCustomer,
  PreCustomerFilters,
  PreCustomerProfile,
} from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

// ********************
// PRE-CUSTOMERS
// ********************

export const initPreCustomersPage = createAction(
  '[Pre-Customers Page] Init Pre-Customers Page'
);

export const updateFiltersPreCustomer = createAction(
  '[Pre-Customers Filters Component] Update Filters Pre-Customers',
  props<{ filters: PreCustomerFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetPreCustomers = createAction(
  '[Pre-Customers Effects] Success Get Pre-Customers',
  props<{ preCustomers?: TableResponse<PreCustomer> }>()
);

export const failedGetPreCustomers = createAction(
  '[Pre-Customers Effects] Failed Get Pre-Customers'
);

export const downloadPreCustomers = createAction(
  '[Pre-Customers Page] Downloand Pre-Customers '
);

export const successDownloadPreCustomers = createAction(
  '[Pre-Customers Effects] success Downloand Pre-Customers ',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadPreCustomers = createAction(
  '[Pre-Customers Effects] failed Downloand Pre-Customers '
);

// ********************
// PRE-CUSTOMER PROFILE
// ********************
export const updatePreCustomerId = createAction(
  '[Pre-Customer Profile Guard] Update Pre-Customer Id',
  props<{ preCustomerId: string }>()
);

export const getPreCustomerProfile = createAction(
  '[Pre-Customer Profile Page] Get Pre-Customer Profile'
);

export const successGetPreCustomerProfile = createAction(
  '[Pre-Customers Effects] Success Get Pre-Customer Profile',
  props<{ preCustomerProfileData?: PreCustomerProfile }>()
);

export const failedGetPreCustomerProfile = createAction(
  '[Pre-Customers Effects] Failed Get Pre-Customer Profile'
);

export const destroyPreCustomerProfilePage = createAction(
  '[Pre-Customer Profile Page] Destroy Pre-Customer Profile Page'
);

// Products and scores
export const getPreCustomerProductsAndScores = createAction(
  '[Pre-Customer Profile Page] Get Products And Scores'
);

export const successGetPreCustomerProductsAndScores = createAction(
  '[Pre-Customers Effects] Success Get Products And Scores',
  props<{ productsAndScores: ProductAndScore[] }>()
);

export const failedGetPreCustomerProductsAndScores = createAction(
  '[Pre-Customers Effects] Failed Get Products And Scores'
);
