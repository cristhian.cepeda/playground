import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { PreCustomerFilters } from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { PreCustomerApi } from '@app/plugins/modules/customers/data/api/pre-customer.api';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as preCustomersActions from './pre-customers.actions';
import * as preCustomersSelectors from './pre-customers.selectors';

@Injectable()
export class PreCustomerEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _preCustomerApi: PreCustomerApi,
    private _utilsService: UtilsService
  ) {}

  initPreCustomersPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(preCustomersActions.initPreCustomersPage),
      map(() => {
        const filters: PreCustomerFilters = { ...FILTERS };
        return preCustomersActions.updateFiltersPreCustomer({ filters });
      })
    )
  );

  getPreCustomers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(preCustomersActions.updateFiltersPreCustomer),
      withLatestFrom(
        this._store.select(preCustomersSelectors.selectPreCustomerFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._preCustomerApi.getPreCustomers(filters).pipe(
          map((preCustomers) =>
            preCustomersActions.successGetPreCustomers({ preCustomers })
          ),
          catchError((_) => of(preCustomersActions.failedGetPreCustomers()))
        );
      })
    )
  );

  downloadPreCustomers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(preCustomersActions.downloadPreCustomers),
      withLatestFrom(
        this._store.select(preCustomersSelectors.selectPreCustomerFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._preCustomerApi.downloadPreCustomers(fileFilters).pipe(
          map((fileResponse) =>
            preCustomersActions.successDownloadPreCustomers({ fileResponse })
          ),
          catchError((_) =>
            of(preCustomersActions.failedDownloadPreCustomers())
          )
        );
      })
    )
  );

  successDownloadPreCustomers$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(preCustomersActions.successDownloadPreCustomers),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            CUSTOMER_FEATURE.PAGES.PRE_CUSTOMERS.NAME
          )
        )
      ),
    { dispatch: false }
  );

  getPreCustomerProfile$ = createEffect(() =>
    this._actions$.pipe(
      ofType(preCustomersActions.getPreCustomerProfile),
      withLatestFrom(
        this._store.select(preCustomersSelectors.selectPreCustomerId)
      ),
      switchMap(([_, preCustomerId]) =>
        this._preCustomerApi.getPreCustomerProfile(preCustomerId).pipe(
          map((preCustomerProfileData) =>
            preCustomersActions.successGetPreCustomerProfile({
              preCustomerProfileData,
            })
          ),
          catchError((_) =>
            of(preCustomersActions.failedGetPreCustomerProfile())
          )
        )
      )
    )
  );

  getPreCustomerProductsAndScores$ = createEffect(() =>
    this._actions$.pipe(
      ofType(
        preCustomersActions.getPreCustomerProductsAndScores,
        preCustomersActions.getPreCustomerProfile
      ),
      withLatestFrom(
        this._store.select(preCustomersSelectors.selectPreCustomerId)
      ),
      switchMap(([_, preCustomerId]) =>
        this._preCustomerApi
          .getPreCustomerProductsAndScores(preCustomerId)
          .pipe(
            map((productsAndScores: ProductAndScore[]) =>
              preCustomersActions.successGetPreCustomerProductsAndScores({
                productsAndScores,
              })
            ),
            catchError((_) =>
              of(preCustomersActions.failedGetPreCustomerProductsAndScores())
            )
          )
      )
    )
  );
}
