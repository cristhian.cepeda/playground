import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
  destroyPreCustomerProfilePage,
  failedGetPreCustomerProductsAndScores,
  failedGetPreCustomerProfile,
  failedGetPreCustomers,
  getPreCustomerProductsAndScores,
  getPreCustomerProfile,
  successGetPreCustomerProductsAndScores,
  successGetPreCustomerProfile,
  successGetPreCustomers,
  updateFiltersPreCustomer,
  updatePreCustomerId,
} from './pre-customers.actions';
import { PreCustomerState } from './pre-customers.state';

export const initialPreCustomerState: PreCustomerState = {
  filters: FILTERS,
};

const _preCustomerReducer = createReducer(
  initialPreCustomerState,
  on(updateFiltersPreCustomer, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(successGetPreCustomers, (state, { preCustomers }) => ({
    ...state,
    preCustomers,
    isLoading: false,
  })),
  on(failedGetPreCustomers, (state) => ({
    ...state,
    preCustomers: null,
    isLoading: false,
    isFiltred: false,
  })),
  on(updatePreCustomerId, (state, { preCustomerId }) => ({
    ...state,
    preCustomerProfile: {
      ...state?.preCustomerProfile,
      ...{ preCustomerId },
    },
  })),
  on(getPreCustomerProfile, (state) => ({
    ...state,
    preCustomerProfile: {
      ...state?.preCustomerProfile,
      ...{ isLoading: true },
    },
  })),
  on(successGetPreCustomerProfile, (state, { preCustomerProfileData }) => ({
    ...state,
    preCustomerProfile: {
      ...state?.preCustomerProfile,
      ...{ data: preCustomerProfileData, isLoading: false },
    },
  })),
  on(failedGetPreCustomerProfile, (state) => ({
    ...state,
    preCustomerProfile: {
      ...state?.preCustomerProfile,
      ...{ isLoading: false },
    },
  })),
  on(destroyPreCustomerProfilePage, (state) => ({
    ...state,
    preCustomerProfile: null,
  })),

  on(getPreCustomerProductsAndScores, (state) => ({
    ...state,
    preCustomerProfile: {
      ...state?.preCustomerProfile,
      ...{ isLoadingProductsAndScores: true },
    },
  })),
  on(
    successGetPreCustomerProductsAndScores,
    (state, { productsAndScores }) => ({
      ...state,
      preCustomerProfile: {
        ...state?.preCustomerProfile,
        ...{ isLoadingProductsAndScores: false, productsAndScores },
      },
    })
  ),
  on(failedGetPreCustomerProductsAndScores, (state) => ({
    ...state,
    preCustomerProfile: {
      ...state?.preCustomerProfile,
      ...{ isLoadingProductsAndScores: false },
    },
  }))
);

export function PreCustomerReducers(
  state: PreCustomerState | undefined,
  action: Action
) {
  return _preCustomerReducer(state, action);
}
