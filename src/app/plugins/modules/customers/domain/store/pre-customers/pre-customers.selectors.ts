import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PreCustomerState } from './pre-customers.state';

export const getPreCustomerFeatureState =
  createFeatureSelector<PreCustomerState>(
    CUSTOMER_FEATURE.PAGES.PRE_CUSTOMERS.STORE_NAME
  );

export const selectPreCustomerFilters = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.filters
);

export const selectIsPreCustomersFiltred = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.isFiltred
);

export const selectPreCustomers = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.preCustomers
);

export const selectIsLoadingPreCustomers = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.isLoading
);

export const selectPreCustomerId = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.preCustomerProfile?.preCustomerId
);

export const selectPreCustomerProfile = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.preCustomerProfile?.data
);

export const selectPreCustomerProfileOthers = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.preCustomerProfile?.data?.others
);

export const selectIsLoadingPreCustomerProfile = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.preCustomerProfile?.isLoading
);

export const selectIsLoadingProductsAndScores = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) =>
    state?.preCustomerProfile?.isLoadingProductsAndScores
);

export const selectProductsAndScores = createSelector(
  getPreCustomerFeatureState,
  (state: PreCustomerState) => state?.preCustomerProfile?.productsAndScores
);
