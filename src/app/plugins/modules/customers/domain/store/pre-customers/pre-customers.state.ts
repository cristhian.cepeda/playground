import { TablePageActions } from '@app/core/models/page.model';
import { CustomerFilters } from '@app/plugins/modules/customers/core/models/customers.model';
import {
  PreCustomer,
  PreCustomerProfile,
} from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface PreCustomerState extends TablePageActions {
  preCustomers?: TableResponse<PreCustomer>;
  filters?: CustomerFilters;
  preCustomerProfile?: PreCustomerProfileState;
}

export interface PreCustomerProfileState {
  preCustomerId?: string;
  data?: PreCustomerProfile;
  isLoading?: boolean;
  productsAndScores?: ProductAndScore[];
  isLoadingProductsAndScores?: boolean;
}
