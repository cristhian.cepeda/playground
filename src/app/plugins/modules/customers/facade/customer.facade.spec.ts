import { TestBed } from '@angular/core/testing';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import * as customersActions from '@app/plugins/modules/customers/domain/store/customers/customers.actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CustomerFilters } from '../core/models/customers.model';
import { CUSTOMER_PROFILE } from '../data/mocks/data/customers.mock';
import { CustomerProfileService } from '../domain/services/customer-profile.service';
import { CustomerFacade } from './customer.facade';

describe('CustomerFacade', () => {
  let facade: CustomerFacade;
  let store: MockStore;
  const initialState: any = {};
  let spy: jasmine.Spy<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [provideMockStore({ initialState })],
    });
    facade = TestBed.inject(CustomerFacade);
    store = TestBed.inject(MockStore);
    spy = spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be created and return mock in map function', (doneFn) => {
    store.setState({
      customers: { customerProfile: { data: CUSTOMER_PROFILE } },
    });
    const _customerProfileService: CustomerProfileService = TestBed.inject(
      CustomerProfileService
    );
    const mockGetCustomerItemsByProfile: SummaryGroup[] = [
      {
        key: 'Hola mundo',
      },
    ] as SummaryGroup[];
    spyOn(_customerProfileService, 'getCustomerItemsByProfile').and.returnValue(
      of(mockGetCustomerItemsByProfile)
    );
    facade.customerProfileItems$.subscribe((data) => {
      expect(data).toEqual(mockGetCustomerItemsByProfile);
      doneFn();
    });
  });

  it('should be call initCustomersPage action', () => {
    facade.initCustomersPage();
    expect(spy).toHaveBeenCalledWith(customersActions.initCustomersPage());
  });

  it('should be call updateFiltersCustomer action with isFiltred false', () => {
    const filters: CustomerFilters = {};
    const isFiltred: boolean = false;

    facade.updateFiltersCustomer(filters, isFiltred);
    expect(spy).toHaveBeenCalledWith(
      customersActions.updateFiltersCustomer({ filters, isFiltred })
    );
  });

  it('should be call updateFiltersCustomer action with isFiltred true', () => {
    const filters: CustomerFilters = {};
    const isFiltred: boolean = true;

    facade.updateFiltersCustomer(filters, isFiltred);
    expect(spy).toHaveBeenCalledWith(
      customersActions.updateFiltersCustomer({ filters, isFiltred })
    );
  });

  it('should be call getCustomers action', () => {
    facade.getCustomers();
    expect(spy).toHaveBeenCalledWith(customersActions.getCustomers());
  });

  it('should be call updateCustomerId action', () => {
    const customerId: string = 'customerId';
    facade.updateCustomerId(customerId);
    expect(spy).toHaveBeenCalledWith(
      customersActions.updateCustomerId({ customerId })
    );
  });

  it('should be call getCustomerProfile action', () => {
    facade.getCustomerProfile();
    expect(spy).toHaveBeenCalledWith(customersActions.getCustomerProfile());
  });

  it('should be call getCustomerAssociatedLoans action', () => {
    facade.getCustomerAssociatedLoans();
    expect(spy).toHaveBeenCalledWith(
      customersActions.getCustomerAssociatedLoans()
    );
  });

  it('should be call getCustomerProductsAndScores action', () => {
    facade.getCustomerProductsAndScores();
    expect(spy).toHaveBeenCalledWith(
      customersActions.getCustomerProductsAndScores()
    );
  });
});
