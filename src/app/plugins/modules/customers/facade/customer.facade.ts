import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  Customer,
  CustomerFilters,
  CustomerProfile,
} from '@app/plugins/modules/customers/core/models/customers.model';
import { Loan } from '@app/plugins/modules/customers/core/models/loan.model';
import { Payment } from '@app/plugins/modules/customers/core/models/payment.model';
import { CustomerProfileService } from '@app/plugins/modules/customers/domain/services/customer-profile.service';
import * as customersActions from '@app/plugins/modules/customers/domain/store/customers/customers.actions';
import * as customersSelectors from '@app/plugins/modules/customers/domain/store/customers/customers.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { filter, map, mergeMap, Observable } from 'rxjs';
import { ProductAndScore } from '../core/models/product-and-score.model';

@Injectable({ providedIn: 'root' })
export class CustomerFacade {
  public projectFamily$: Observable<string>;

  public customerTable$: Observable<TableResponse<Customer>>;
  public isCustomersFiltred$: Observable<boolean>;
  public isLoadingCustomers$: Observable<boolean>;

  public customerProfile$: Observable<CustomerProfile>;
  public customerId$: Observable<string>;
  public isLoadingCustomerProfile$: Observable<boolean>;
  public associatedLoans$: Observable<Loan[]>;
  public isLoadingAssociatedLoans$: Observable<boolean>;
  public customerProfileItems$: Observable<SummaryGroup[]>;
  public customerProfileOtherItems$: Observable<SummaryGroup[]>;
  public customerProductsAndScores$: Observable<ProductAndScore[]>;
  public isLoadingProductsAndScores$: Observable<boolean>;
  public customerRecentPayments$: Observable<Payment[]>;
  public isLoadingRecentPayments$: Observable<boolean>;

  constructor(
    private _store: Store,
    private _customerProfileService: CustomerProfileService,
    private _projectFacade: ProjectFacade
  ) {
    this._setSelectors();
  }

  public initCustomersPage(): void {
    this._store.dispatch(customersActions.initCustomersPage());
  }

  public updateFiltersCustomer(
    filters: CustomerFilters,
    filtersOptions?: FiltersOptions
  ): void {
    this._store.dispatch(
      customersActions.updateFiltersCustomer({ filters, filtersOptions })
    );
  }

  public downloadCustomers() {
    this._store.dispatch(customersActions.downloadCustomers());
  }

  public updateCustomerId(customerId: string): void {
    this._store.dispatch(customersActions.updateCustomerId({ customerId }));
  }

  public getCustomerProfile(): void {
    this._store.dispatch(customersActions.getCustomerProfile());
  }

  public getCustomerAssociatedLoans(): void {
    this._store.dispatch(customersActions.getCustomerAssociatedLoans());
  }

  public getCustomerProductsAndScores(): void {
    this._store.dispatch(customersActions.getCustomerProductsAndScores());
  }

  public getCustomerRecentPayments(): void {
    this._store.dispatch(customersActions.getCustomerRecentPayments());
  }

  public destroyCustomerProfilePage() {
    this._store.dispatch(customersActions.destroyCustomerProfilePage());
  }

  private _setSelectors() {
    this.customerTable$ = this._store.select(
      customersSelectors.selectCustomers
    );
    this.isCustomersFiltred$ = this._store.select(
      customersSelectors.selectIsCustomersFiltred
    );
    this.isLoadingCustomers$ = this._store.select(
      customersSelectors.selectIsLoadingCustomers
    );
    this.customerProfile$ = this._store.select(
      customersSelectors.selectCustomerProfile
    );
    this.isLoadingCustomerProfile$ = this._store.select(
      customersSelectors.selectIsLoadingCustomerProfile
    );
    this.associatedLoans$ = this._store.select(
      customersSelectors.selectCustomerProfileAssociatedLoans
    );
    this.isLoadingAssociatedLoans$ = this._store.select(
      customersSelectors.selectIsLoadingCustomerProfileAssociatedLoans
    );
    this.customerProfileItems$ = this.customerProfile$.pipe(
      filter((customerProfile: CustomerProfile) => !!customerProfile),
      mergeMap((customerProfile: CustomerProfile) =>
        this._customerProfileService.getCustomerItemsByProfile(customerProfile)
      )
    );
    this.customerProfileOtherItems$ = this._store
      .select(customersSelectors.selectCustomerProfileOthers)
      .pipe(
        filter((others: any) => !!others),
        map((others: any) =>
          this._customerProfileService.getCustomerOthersItemsByProfile(others)
        )
      );
    this.customerProductsAndScores$ = this._store.select(
      customersSelectors.selectProductsAndScores
    );
    this.isLoadingProductsAndScores$ = this._store.select(
      customersSelectors.selectIsLoadingProductsAndScores
    );
    this.customerRecentPayments$ = this._store.select(
      customersSelectors.selectPayments
    );
    this.isLoadingRecentPayments$ = this._store.select(
      customersSelectors.selectIsLoadingPayments
    );
    this.customerId$ = this.customerProfile$.pipe(
      filter((customer) => !!customer),
      map((customerProfile) => customerProfile?.id)
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
