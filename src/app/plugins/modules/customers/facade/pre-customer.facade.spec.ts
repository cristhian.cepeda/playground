import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { PreCustomerFilters } from '@app/plugins/modules/customers/core/models/pre-customer.model';
import * as preCustomersActions from '@app/plugins/modules/customers/domain/store/pre-customers/pre-customers.actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { PRE_CUSTOMERS_PROFILE } from '../data/mocks/data/pre-customers.mock';
import { PreCustomerProfileService } from '../domain/services/pre-customer-profile.service';
import { PreCustomerFacade } from './pre-customer.facade';

describe('PreCustomerFacade', () => {
  let facade: PreCustomerFacade;
  let store: MockStore;
  const initialState: any = {};
  let spy: jasmine.Spy<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    });
    facade = TestBed.inject(PreCustomerFacade);
    store = TestBed.inject(MockStore);
    spy = spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be created and return mock in map function', (doneFn) => {
    store.setState({
      preCustomers: { preCustomerProfile: { data: PRE_CUSTOMERS_PROFILE } },
    });
    const _preCustomerProfileService: PreCustomerProfileService =
      TestBed.inject(PreCustomerProfileService);
    const mockGetCustomerItemsByProfile: SummaryGroup[] = [
      {
        key: 'Hola mundo',
      },
    ] as SummaryGroup[];
    spyOn(
      _preCustomerProfileService,
      'getPreCustomerItemsByProfile'
    ).and.returnValue(of(mockGetCustomerItemsByProfile));
    facade.preCustomerProfileItems$.subscribe((data) => {
      expect(data).toEqual(mockGetCustomerItemsByProfile);
      doneFn();
    });
  });

  it('should be call initPreCustomersPage action', () => {
    facade.initPreCustomersPage();
    expect(spy).toHaveBeenCalledWith(
      preCustomersActions.initPreCustomersPage()
    );
  });

  it('should be call updateFiltersPreCustomer action with filters mock and isFiltred="false"', () => {
    const filters: PreCustomerFilters = {};
    const isFiltred = false;
    facade.updateFiltersPreCustomer(filters);
    expect(spy).toHaveBeenCalledWith(
      preCustomersActions.updateFiltersPreCustomer({
        filters,
        isFiltred,
      })
    );
  });

  it('should be call updateFiltersPreCustomer action with filters mock and isFiltred="true"', () => {
    const filters: PreCustomerFilters = {};
    const isFiltred = true;
    facade.updateFiltersPreCustomer(filters, isFiltred);
    expect(spy).toHaveBeenCalledWith(
      preCustomersActions.updateFiltersPreCustomer({
        filters,
        isFiltred,
      })
    );
  });

  it('should be call getPreCustomers action', () => {
    facade.getPreCustomers();
    expect(spy).toHaveBeenCalledWith(preCustomersActions.getPreCustomers());
  });

  it('should be call updatePreCustomerId action', () => {
    const preCustomerId: string = 'preCustomerId';
    facade.updatePreCustomerId(preCustomerId);
    expect(spy).toHaveBeenCalledWith(
      preCustomersActions.updatePreCustomerId({ preCustomerId })
    );
  });

  it('should be call getPreCustomerProfile action', () => {
    facade.getPreCustomerProfile();
    expect(spy).toHaveBeenCalledWith(
      preCustomersActions.getPreCustomerProfile()
    );
  });
});
