import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  PreCustomer,
  PreCustomerFilters,
  PreCustomerProfile,
} from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { PreCustomerProfileService } from '@app/plugins/modules/customers/domain/services/pre-customer-profile.service';
import * as preCustomersActions from '@app/plugins/modules/customers/domain/store/pre-customers/pre-customers.actions';
import * as preCustomersSelectors from '@app/plugins/modules/customers/domain/store/pre-customers/pre-customers.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { filter, map, mergeMap, Observable } from 'rxjs';
import { ProductAndScore } from '../core/models/product-and-score.model';

@Injectable({ providedIn: 'root' })
export class PreCustomerFacade {
  public projectFamily$: Observable<string>;

  public preCustomerTable$: Observable<TableResponse<PreCustomer>>;
  public isPreCustomersFiltred$: Observable<boolean>;
  public isLoadingPreCustomers$: Observable<boolean>;

  public preCustomerProfile$: Observable<PreCustomerProfile>;
  public isLoadingPreCustomerProfile$: Observable<boolean>;

  public preCustomerProfileItems$: Observable<SummaryGroup[]>;
  public preCustomerProfileOthersItems$: Observable<SummaryGroup[]>;

  public preCustomerProductsAndScores$: Observable<ProductAndScore[]>;
  public isLoadingProductsAndScores$: Observable<boolean>;

  constructor(
    private _store: Store,
    private _preCustomerProfileService: PreCustomerProfileService,
    private _projectFacade: ProjectFacade
  ) {
    this._setSelectors();
  }

  public initPreCustomersPage(): void {
    this._store.dispatch(preCustomersActions.initPreCustomersPage());
  }

  public updateFiltersPreCustomer(
    filters: PreCustomerFilters,
    filtersOptions?: FiltersOptions
  ): void {
    this._store.dispatch(
      preCustomersActions.updateFiltersPreCustomer({ filters, filtersOptions })
    );
  }

  public downloadPreCustomers() {
    this._store.dispatch(preCustomersActions.downloadPreCustomers());
  }

  public updatePreCustomerId(preCustomerId: string): void {
    this._store.dispatch(
      preCustomersActions.updatePreCustomerId({ preCustomerId })
    );
  }

  public getPreCustomerProfile(): void {
    this._store.dispatch(preCustomersActions.getPreCustomerProfile());
  }

  public destroyPreCustomerProfilePage() {
    this._store.dispatch(preCustomersActions.destroyPreCustomerProfilePage());
  }

  public getPreCustomerProductsAndScores(): void {
    this._store.dispatch(preCustomersActions.getPreCustomerProductsAndScores());
  }

  private _setSelectors() {
    this.preCustomerTable$ = this._store.select(
      preCustomersSelectors.selectPreCustomers
    );
    this.isPreCustomersFiltred$ = this._store.select(
      preCustomersSelectors.selectIsPreCustomersFiltred
    );
    this.isLoadingPreCustomers$ = this._store.select(
      preCustomersSelectors.selectIsLoadingPreCustomers
    );
    this.preCustomerProfile$ = this._store.select(
      preCustomersSelectors.selectPreCustomerProfile
    );
    this.isLoadingPreCustomerProfile$ = this._store.select(
      preCustomersSelectors.selectIsLoadingPreCustomerProfile
    );
    this.preCustomerProfileItems$ = this.preCustomerProfile$.pipe(
      filter((preCustomerProfile: PreCustomerProfile) => !!preCustomerProfile),
      mergeMap((preCustomerProfile: PreCustomerProfile) =>
        this._preCustomerProfileService.getPreCustomerItemsByProfile(
          preCustomerProfile
        )
      )
    );
    this.preCustomerProfileOthersItems$ = this._store
      .select(preCustomersSelectors.selectPreCustomerProfileOthers)
      .pipe(
        filter((others: any) => !!others),
        map((others: any) =>
          this._preCustomerProfileService.getPreCustomerOthersItemsByProfile(
            others
          )
        )
      );
    this.preCustomerProductsAndScores$ = this._store.select(
      preCustomersSelectors.selectProductsAndScores
    );
    this.isLoadingProductsAndScores$ = this._store.select(
      preCustomersSelectors.selectIsLoadingProductsAndScores
    );

    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
