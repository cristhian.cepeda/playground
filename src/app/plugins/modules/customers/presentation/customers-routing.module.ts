import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseContentComponent } from '@app/presentation/layout/components/base-content/base-content.component';
import { MENU_CONFIG_DISABLED } from '@instance-config/menu.config';
import { CUSTOMER_FEATURE } from '../core/constants/feature.constants';

const CHILDREN_ROUTES: Routes = [
  {
    path: CUSTOMER_FEATURE.PAGES.OVERVIEW.PATH,
    loadChildren: () =>
      import('./pages/overview/overview.module').then(
        (m) => m.OverviePagewModule
      ),
    data: {
      breadcrumb: 'CUSTOMERS.OVERVIEW.TITLE',
      disabled: MENU_CONFIG_DISABLED.CUSTOMER.OVERVIEW ?? false,
    },
  },
  {
    path: CUSTOMER_FEATURE.PAGES.PRE_CUSTOMERS.PATH,
    loadChildren: () =>
      import('./pages/pre-customers/pre-customers.module').then(
        (m) => m.PreCustomersModule
      ),
    data: {
      breadcrumb: 'CUSTOMERS.PRE_CUSTOMERS.TITLE',
      disabled: MENU_CONFIG_DISABLED.CUSTOMER.PRE_CUSTOMERS ?? false,
    },
  },
  {
    path: CUSTOMER_FEATURE.PAGES.CUSTOMERS.PATH,
    loadChildren: () =>
      import('./pages/customers/customers.module').then(
        (m) => m.CustomersModule
      ),
    data: {
      breadcrumb: 'CUSTOMERS.CUSTOMERS.TITLE',
      disabled: MENU_CONFIG_DISABLED.CUSTOMER.CUSTOMERS ?? false,
    },
  },
];

const routes: Routes = [
  {
    path: '',
    component: BaseContentComponent,
    children: [
      {
        path: '',
        redirectTo: CHILDREN_ROUTES.filter(
          (route) => !route?.data?.disabled
        ).shift()?.path,
        pathMatch: 'full',
      },
      ...CHILDREN_ROUTES,
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersRoutingModule {}
