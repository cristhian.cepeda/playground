import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { AllCustomersPageRoutingModule } from './all-customers-routing.module';
import { AllCustomersPage } from './all-customers.page';
import { FiltersComponent } from './components/filters/filters.component';

@NgModule({
  declarations: [AllCustomersPage, FiltersComponent],
  imports: [CommonModule, AllCustomersPageRoutingModule, LayoutModule],
})
export class AllCustomersPageModule {}
