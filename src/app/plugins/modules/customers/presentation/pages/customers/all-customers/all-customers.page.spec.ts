import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CustomerFilters } from '../../../../core/models/customers.model';
import { CustomerFacade } from '../../../../facade/customer.facade';
import { AllCustomersPage } from './all-customers.page';

describe('AllCustomersPage', () => {
  let component: AllCustomersPage;
  let fixture: ComponentFixture<AllCustomersPage>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), LayoutModule, RouterTestingModule],
      declarations: [AllCustomersPage],
      providers: [SnakeToCapitalPipe, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(AllCustomersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be on Change Page Filter', () => {
    const filters: CustomerFilters = {};
    const _customerFacade = TestBed.inject(CustomerFacade);
    const spy = spyOn(_customerFacade, 'updateFiltersCustomer');
    component.onChangePageFilter(filters);
    expect(spy).toHaveBeenCalledWith(filters);
  });
});
