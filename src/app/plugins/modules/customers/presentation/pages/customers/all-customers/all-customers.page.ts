import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  CUSTOMERS_TABLES,
  getCustomersTablePerFamily,
} from '@app/plugins/modules/customers/core/constants/tables';
import { CUSTOMERS_URLS } from '@app/plugins/modules/customers/core/constants/urls.constants';
import {
  Customer,
  CustomerFilters,
} from '@app/plugins/modules/customers/core/models/customers.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { CONTAINER_SIZE } from '@app/presentation/layout/mo-tables/constants/container.constants';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'all-customers-page',
  templateUrl: './all-customers.page.html',
  styleUrls: ['./all-customers.page.scss'],
})
export class AllCustomersPage
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  @ViewChild('statusTemplate', { static: true })
  statusTemplate: TemplateRef<ContextColumn>;

  public customerTable$: Observable<TableResponse<Customer>>;
  public isLoadingCustomers$: Observable<boolean>;

  public headers: TableHeader[];
  public containerSize: string;
  public customerProfileUrl: string;

  private _projectFamilySubscription: Subscription;

  constructor(private _customerFacade: CustomerFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangePageFilter(filters: CustomerFilters) {
    this._customerFacade.updateFiltersCustomer(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._customerFacade.downloadCustomers();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.containerSize = CONTAINER_SIZE.EXTRA_LARGE;
    this.customerProfileUrl = CUSTOMERS_URLS.CUSTOMERS_PROFILE;
    this._customerFacade.initCustomersPage();
    this.customerTable$ = this._customerFacade.customerTable$;
    this.isLoadingCustomers$ = this._customerFacade.isLoadingCustomers$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._customerFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getCustomersTablePerFamily(
          projectFamily,
          CUSTOMERS_TABLES.CUSTOMERS
        ).map((header) => {
          if (header.dataKey == 'status') header.template = this.statusTemplate;
          return header;
        });
      });
  }
}
