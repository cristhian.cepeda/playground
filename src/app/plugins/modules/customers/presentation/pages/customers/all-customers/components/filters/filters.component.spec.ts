import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FILTERS } from '@app/core/constants/filters.constant';
import { CustomerFilters } from '@app/plugins/modules/customers/core/models/customers.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FiltersComponent } from './filters.component';

describe('FiltersComponent', () => {
  let component: FiltersComponent;
  let fixture: ComponentFixture<FiltersComponent>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [FiltersComponent],
      providers: [UntypedFormBuilder, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(FiltersComponent);
    store = TestBed.inject(MockStore);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onFilter', () => {
    it('should be call update filters customer with empty', () => {
      const filters: CustomerFilters = {
        active_loans: '',
        created_at: '',
        offset: FILTERS.offset,
        product: '',
        search: '',
        status: '',
        updated_at: '',
      };
      const isFiltred: boolean = true;
      const _customerFacade = TestBed.inject(CustomerFacade);
      const spy = spyOn(_customerFacade, 'updateFiltersCustomer');
      const formDebugElement = fixture.debugElement.query(By.css('form'));
      formDebugElement.triggerEventHandler('ngSubmit', null);
      fixture.detectChanges();
      expect(spy).toHaveBeenCalledWith(filters, isFiltred);
    });

    it('should be call update filters customer with mock values', () => {
      const dateMock = '2021-01-01';
      component.form.patchValue({
        created_at: dateMock,
        updated_at: dateMock,
        offset: FILTERS.offset,
      });
      fixture.detectChanges();
      const filters: CustomerFilters = {
        active_loans: '',
        created_at: dateMock,
        offset: FILTERS.offset,
        product: '',
        search: '',
        status: '',
        updated_at: dateMock,
      };
      const isFiltred: boolean = true;
      const _customerFacade = TestBed.inject(CustomerFacade);
      const spy = spyOn(_customerFacade, 'updateFiltersCustomer');
      const formDebugElement = fixture.debugElement.query(By.css('form'));
      fixture.detectChanges();
      formDebugElement.triggerEventHandler('ngSubmit', null);
      expect(spy).toHaveBeenCalledWith(filters, isFiltred);
    });
  });

  it('should be call update filters customer with empty form', () => {
    const _customerFacade = TestBed.inject(CustomerFacade);
    const spy = spyOn(_customerFacade, 'updateFiltersCustomer');
    const formMock = {
      search: null,
      active_loans: null,
      status: null,
      created_at: null,
      updated_at: null,
      product: null,
    };
    component.onCleanFilters();
    expect(spy).toHaveBeenCalledWith(formMock);
  });
});
