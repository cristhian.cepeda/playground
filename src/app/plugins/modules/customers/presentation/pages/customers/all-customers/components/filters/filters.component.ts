import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import {
  Customer,
  CustomerFilters,
} from '@app/plugins/modules/customers/core/models/customers.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import {
  ACTIVE_LOANS_OPTIONS,
  CUSTOMER_STATUS_OPTIONS,
} from '../../constants/options.constants';

@Component({
  selector: 'customers-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public customerTable$: Observable<TableResponse<Customer>>;
  public isCustomersFiltred$: Observable<boolean>;
  public customerStatusOptions: SelectOption<string>[];
  public activeLoansOptions: SelectOption<string>[];
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _customerFacade: CustomerFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    const filters: CustomerFilters = this._getFilters();
    this._customerFacade.updateFiltersCustomer(
      setFiltersWithDateRanges(filters, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    const filters: CustomerFilters = this._resetFilters();
    this._customerFacade.updateFiltersCustomer(
      clearFiltersWithDateRanges(filters, this._controlsWithDateRanges)
    );
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at', 'updated_at'];
    this.customerTable$ = this._customerFacade.customerTable$;
    this.isCustomersFiltred$ = this._customerFacade.isCustomersFiltred$;
    this.activeLoansOptions = ACTIVE_LOANS_OPTIONS;
    this.customerStatusOptions = CUSTOMER_STATUS_OPTIONS;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      active_loans: [''],
      status: [''],
      created_at: [''],
      updated_at: [''],
      product: [''],
    });
  }

  private _getFilters(): CustomerFilters {
    let filters: CustomerFilters;
    const active_loans = this.form.get('active_loans').value;
    filters = this.form.value;
    if (active_loans != 0) {
      filters = {
        ...filters,
        active_loans__gte: active_loans,
        active_loans: '',
      };
    } else
      filters = {
        ...filters,
        active_loans__gte: '',
      };

    return filters;
  }

  private _resetFilters(): CustomerFilters {
    this.form.reset();
    let filters: CustomerFilters = this.form.value;
    filters = {
      ...filters,
      active_loans__gte: '',
    };

    return filters;
  }
}
