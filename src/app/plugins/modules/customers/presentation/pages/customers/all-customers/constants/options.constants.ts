import { CUSTOMER_STATUS } from '@app/plugins/modules/customers/core/constants/status.constants';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

export const ACTIVE_LOANS_OPTIONS: SelectOption<string>[] = [
  {
    key: '0',
    value: '0',
  },
  {
    key: '> 0',
    value: '1',
  },
];

export const CUSTOMER_STATUS_OPTIONS: SelectOption<string>[] = [
  {
    key: CUSTOMER_STATUS.ENABLED,
    value: CUSTOMER_STATUS.ENABLED,
  },
  {
    key: CUSTOMER_STATUS.DISABLED,
    value: CUSTOMER_STATUS.DISABLED,
  },
  {
    key: CUSTOMER_STATUS.DELETED,
    value: CUSTOMER_STATUS.DELETED,
  },
  {
    key: CUSTOMER_STATUS.BLOCKED,
    value: CUSTOMER_STATUS.BLOCKED,
  },
  {
    key: CUSTOMER_STATUS.UNREGISTERED,
    value: CUSTOMER_STATUS.UNREGISTERED,
  },
];
