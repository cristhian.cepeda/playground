import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOMER_PROFILE } from '@app/plugins/modules/customers/data/mocks/data/customers.mock';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { AssociatedLoansComponent } from './associated-loans.component';

describe('AssociatedLoansComponent', () => {
  let component: AssociatedLoansComponent;
  let fixture: ComponentFixture<AssociatedLoansComponent>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LayoutModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [AssociatedLoansComponent],
      providers: [provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be customer_id to equal mock id', (doneFn) => {
    const _customerFacade = TestBed.inject(CustomerFacade);
    store.setState({
      customers: { customerProfile: { data: CUSTOMER_PROFILE } },
    });
    _customerFacade.customerProfile$.subscribe((customrProfile) => {
      expect(customrProfile?.id).toBe(CUSTOMER_PROFILE?.id);
      doneFn();
    });
  });
});
