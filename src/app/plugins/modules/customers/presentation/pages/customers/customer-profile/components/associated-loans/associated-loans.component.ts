import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  CUSTOMERS_TABLES,
  getCustomersTablePerFamily,
} from '@app/plugins/modules/customers/core/constants/tables';
import { LOAN_MANAGER_URLS } from '@app/plugins/modules/customers/core/constants/urls.constants';
import { Loan } from '@app/plugins/modules/customers/core/models/loan.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TABLE_DESIGN_CLASS } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { filter, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'customer-profile-associated-loans',
  templateUrl: './associated-loans.component.html',
  styleUrls: ['./associated-loans.component.scss'],
})
export class AssociatedLoansComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public associatedLoans$: Observable<Loan[]>;
  public isLoading$: Observable<boolean>;

  public designClass: TABLE_DESIGN_CLASS;
  public headers: TableHeader[];
  public loanUrl: string;
  public customer_id: string;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  private _projectFamilySubscription: Subscription;
  private _customerSubscription: Subscription;

  constructor(private _customerFacade: CustomerFacade) {
    super();
    this.associatedLoans$ = this._customerFacade.associatedLoans$;
    this.isLoading$ = this._customerFacade.isLoadingAssociatedLoans$;
  }

  ngOnInit(): void {
    this._customerFacade.getCustomerAssociatedLoans();
    this._setInitialValues();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.designClass = TABLE_DESIGN_CLASS.ALTERNATIVE;
    this.loanUrl = LOAN_MANAGER_URLS.LOANS;
    this._customerSubscription = this._customerFacade.customerProfile$
      .pipe(filter((customer) => !!customer))
      .subscribe((customrProfile) => (this.customer_id = customrProfile?.id));
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._customerFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getCustomersTablePerFamily(
          projectFamily,
          CUSTOMERS_TABLES.CUSTOMER_PROFILE_ASSOCIATED_LOANS
        );
      });
  }
}
