import { Component, OnInit } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'customer-profile-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  public isLoading$: Observable<boolean>;
  public customerProfileItems$: Observable<SummaryGroup[]>;
  public customerProfileOtherItems$: Observable<SummaryGroup[]>;

  constructor(private _customerFacade: CustomerFacade) {}

  ngOnInit(): void {
    this.isLoading$ = this._customerFacade.isLoadingCustomerProfile$;
    this.customerProfileItems$ = this._customerFacade.customerProfileItems$;
    this.customerProfileOtherItems$ =
      this._customerFacade.customerProfileOtherItems$;
  }
}
