import { Component, OnInit } from '@angular/core';
import { CustomerProfile } from '@app/plugins/modules/customers/core/models/customers.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'customer-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  public customerProfile$: Observable<CustomerProfile>;

  constructor(private _customerFacade: CustomerFacade) {
    this.customerProfile$ = this._customerFacade.customerProfile$;
  }

  ngOnInit(): void {}
}
