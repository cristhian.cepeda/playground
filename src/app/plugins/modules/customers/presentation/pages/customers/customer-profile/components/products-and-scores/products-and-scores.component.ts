import { Component, OnInit } from '@angular/core';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'customer-products-and-scores',
  templateUrl: './products-and-scores.component.html',
  styleUrls: ['./products-and-scores.component.scss'],
})
export class ProductsAndScoresComponent implements OnInit {
  public customerProductsAndScores$: Observable<ProductAndScore[]>;
  public isLoadingProductsAndScores$: Observable<boolean>;

  constructor(private _customerFacade: CustomerFacade) {
    this.customerProductsAndScores$ =
      this._customerFacade.customerProductsAndScores$;
    this.isLoadingProductsAndScores$ =
      this._customerFacade.isLoadingProductsAndScores$;
  }

  ngOnInit(): void {
    this._customerFacade.getCustomerProductsAndScores();
  }
}
