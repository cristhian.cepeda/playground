import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  CUSTOMERS_TABLES,
  getCustomersTablePerFamily,
} from '@app/plugins/modules/customers/core/constants/tables';
import { LOAN_MANAGER_URLS } from '@app/plugins/modules/customers/core/constants/urls.constants';
import { Payment } from '@app/plugins/modules/customers/core/models/payment.model';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TABLE_DESIGN_CLASS } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'customer-profile-recent-payments',
  templateUrl: './recent-payments.component.html',
  styleUrls: ['./recent-payments.component.scss'],
})
export class RecentPaymentsComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public payments$: Observable<Payment[]>;
  public isLoading$: Observable<boolean>;
  public customerId$: Observable<string>;

  public designClass: TABLE_DESIGN_CLASS;
  public headers: TableHeader[];
  public paymentsUrl: string;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  private _projectFamilySubscription: Subscription;

  constructor(private _customerFacade: CustomerFacade) {
    super();
    this.payments$ = this._customerFacade.customerRecentPayments$;
    this.isLoading$ = this._customerFacade.isLoadingRecentPayments$;
  }

  ngOnInit(): void {
    this._customerFacade.getCustomerRecentPayments();
    this._setInitialValues();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.designClass = TABLE_DESIGN_CLASS.ALTERNATIVE;
    this.paymentsUrl = LOAN_MANAGER_URLS.PAYMENTS;
    this.customerId$ = this._customerFacade.customerId$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._customerFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getCustomersTablePerFamily(
          projectFamily,
          CUSTOMERS_TABLES.CUSTOMER_PROFILE_PAYMENTS
        );
      });
  }
}
