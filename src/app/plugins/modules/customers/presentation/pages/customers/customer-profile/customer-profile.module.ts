import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerProfilePageRoutingModule } from './customer-profile-routing.module';
import { CustomerProfilePage } from './customer-profile.page';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { InfoComponent } from './components/info/info.component';
import { DetailsComponent } from './components/details/details.component';
import { AssociatedLoansComponent } from './components/associated-loans/associated-loans.component';
import { ProductsAndScoresComponent } from './components/products-and-scores/products-and-scores.component';
import { RecentPaymentsComponent } from './components/recent-payments/recent-payments.component';

@NgModule({
  declarations: [CustomerProfilePage, InfoComponent, DetailsComponent, AssociatedLoansComponent, ProductsAndScoresComponent, RecentPaymentsComponent],
  imports: [CommonModule, CustomerProfilePageRoutingModule, LayoutModule],
})
export class CustomerProfilePageModule {}
