import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerState } from '@app/plugins/modules/customers/domain/store/customers/customers.state';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CustomerProfilePage } from './customer-profile.page';

describe('CustomerProfilePage', () => {
  let component: CustomerProfilePage;
  let fixture: ComponentFixture<CustomerProfilePage>;
  let facadeSpy: jasmine.SpyObj<CustomerFacade>;
  let store: MockStore;
  const initialState: CustomerState = {};

  beforeEach(async () => {
    facadeSpy = jasmine.createSpyObj('CustomerFacade', [
      'getCustomerProfile',
      'destroyCustomerProfilePage',
    ]);

    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [CustomerProfilePage],
      providers: [
        provideMockStore({ initialState }),
        {
          provide: CustomerFacade,
          useValue: facadeSpy,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    store = TestBed.inject(MockStore);
    facadeSpy = TestBed.inject(
      CustomerFacade
    ) as jasmine.SpyObj<CustomerFacade>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should getCustomerProfile has been called one time', () => {
    expect(facadeSpy.getCustomerProfile).toHaveBeenCalledTimes(1);
  });
});
