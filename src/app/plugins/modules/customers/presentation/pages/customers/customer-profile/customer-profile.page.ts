import { Component, OnDestroy } from '@angular/core';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';

@Component({
  selector: 'customer-profile',
  templateUrl: './customer-profile.page.html',
  styleUrls: ['./customer-profile.page.scss'],
})
export class CustomerProfilePage implements OnDestroy {
  constructor(private _customerFacade: CustomerFacade) {
    this._customerFacade.getCustomerProfile();
  }

  ngOnDestroy(): void {
    this._customerFacade.destroyCustomerProfilePage();
  }
}
