import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { CUSTOMERS_URLS } from '@app/plugins/modules/customers/core/constants/urls.constants';
import { CustomerFacade } from '@app/plugins/modules/customers/facade/customer.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomerProfileGuard implements CanActivate {
  constructor(
    private _customerFacade: CustomerFacade,
    private _router: Router
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._getCustomerIdParam(route);
  }

  private _getCustomerIdParam(
    route: ActivatedRouteSnapshot
  ): boolean | UrlTree {
    const customerId = route.queryParams?.id;
    if (!!customerId) {
      this._customerFacade.updateCustomerId(customerId);
      return true;
    }
    return this._router.createUrlTree([CUSTOMERS_URLS.CUSTOMERS]);
  }
}
