import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { CustomerProfileGuard } from './customer-profile/guards/customer-profile.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./all-customers/all-customers.module').then(
        (m) => m.AllCustomersPageModule
      ),
  },
  {
    canActivate: [CustomerProfileGuard],
    path: CUSTOMER_FEATURE.PAGES.CUSTOMERS.SUB_PAGES.CUSTOMERS_PROFILE.PATH,
    loadChildren: () =>
      import('./customer-profile/customer-profile.module').then(
        (m) => m.CustomerProfilePageModule
      ),
    data: {
      breadcrumb: 'CUSTOMERS.CUSTOMER_PROFILE.TITLE',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersRoutingModule {}
