import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { CustomerEffects } from '@app/plugins/modules/customers/domain/store/customers/customers.effects';
import { CustomerReducers } from '@app/plugins/modules/customers/domain/store/customers/customers.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CustomersRoutingModule } from './customers-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    StoreModule.forFeature(
      CUSTOMER_FEATURE.PAGES.CUSTOMERS.STORE_NAME,
      CustomerReducers
    ),
    EffectsModule.forFeature([CustomerEffects]),
  ],
})
export class CustomersModule {}
