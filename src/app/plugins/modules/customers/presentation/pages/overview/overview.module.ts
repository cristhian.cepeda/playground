import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OverviewPageRoutingModule } from './overview-routing.module';
import { OverviewPage } from './overview.page';
import { LayoutModule } from '@app/presentation/layout/layout.module';

@NgModule({
  declarations: [OverviewPage],
  imports: [CommonModule, OverviewPageRoutingModule, LayoutModule],
})
export class OverviePagewModule {}
