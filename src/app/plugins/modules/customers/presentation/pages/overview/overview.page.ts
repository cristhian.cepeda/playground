import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'customers-overview-page',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage implements OnInit {
  constructor() {}

  ngOnInit(): void {
    this._setInitialValues();
  }

  private _setInitialValues() {}
}
