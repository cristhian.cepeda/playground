import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreAllCustomersPage } from './all-pre-customers.page';

const routes: Routes = [
  {
    path: '',
    component: PreAllCustomersPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreAllCustomersPageRoutingModule {}
