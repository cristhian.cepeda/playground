import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { PreAllCustomersPageRoutingModule } from './all-pre-customers-routing.module';
import { PreAllCustomersPage } from './all-pre-customers.page';
import { FiltersComponent } from './components/filters/filters.component';

@NgModule({
  declarations: [PreAllCustomersPage, FiltersComponent],
  imports: [CommonModule, PreAllCustomersPageRoutingModule, LayoutModule],
})
export class PreAllCustomersPageModule {}
