import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { PreCustomerFilters } from '../../../../core/models/pre-customer.model';
import { PreCustomerFacade } from '../../../../facade/pre-customer.facade';
import { PreAllCustomersPage } from './all-pre-customers.page';

describe('PreAllCustomersPage', () => {
  let component: PreAllCustomersPage;
  let fixture: ComponentFixture<PreAllCustomersPage>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [PreAllCustomersPage],
      providers: [provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreAllCustomersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be on Change Page Filter', () => {
    const filters: PreCustomerFilters = {};
    const _preCustomerFacade = TestBed.inject(PreCustomerFacade);
    const spy = spyOn(_preCustomerFacade, 'updateFiltersPreCustomer');
    component.onChangePageFilter(filters);
    expect(spy).toHaveBeenCalledWith(filters);
  });
});
