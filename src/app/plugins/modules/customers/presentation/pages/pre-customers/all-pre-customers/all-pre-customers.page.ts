import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  CUSTOMERS_TABLES,
  getCustomersTablePerFamily,
} from '@app/plugins/modules/customers/core/constants/tables';
import { CUSTOMERS_URLS } from '@app/plugins/modules/customers/core/constants/urls.constants';
import {
  PreCustomer,
  PreCustomerFilters,
} from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'all-pre-customers-page',
  templateUrl: './all-pre-customers.page.html',
  styleUrls: ['./all-pre-customers.page.scss'],
})
export class PreAllCustomersPage
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  @ViewChild('associatedProductsTemplate', { static: true })
  associatedProductsTemplate: TemplateRef<ContextColumn>;

  public preCustomerTable$: Observable<TableResponse<PreCustomer>>;
  public isLoadingPreCustomers$: Observable<boolean>;

  public headers: TableHeader[];
  public preCustomerProfileUrl: string;
  private _projectFamilySubscription: Subscription;

  constructor(private _preCustomerFacade: PreCustomerFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangePageFilter(filters: PreCustomerFilters) {
    this._preCustomerFacade.updateFiltersPreCustomer(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._preCustomerFacade.downloadPreCustomers();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.preCustomerProfileUrl = CUSTOMERS_URLS.PRE_CUSTOMERS_PROFILE;
    this._preCustomerFacade.initPreCustomersPage();
    this.preCustomerTable$ = this._preCustomerFacade.preCustomerTable$;
    this.isLoadingPreCustomers$ =
      this._preCustomerFacade.isLoadingPreCustomers$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._preCustomerFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getCustomersTablePerFamily(
          projectFamily,
          CUSTOMERS_TABLES.PRE_CUSTOMER
        ).map((header) => {
          if (header.dataKey == 'products')
            header.template = this.associatedProductsTemplate;
          return header;
        });
      });
  }
}
