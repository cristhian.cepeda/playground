import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FILTERS } from '@app/core/constants/filters.constant';
import { PreCustomerFilters } from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';

import { FiltersComponent } from './filters.component';

describe('FiltersComponent', () => {
  let component: FiltersComponent;
  let fixture: ComponentFixture<FiltersComponent>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [FiltersComponent],
      providers: [UntypedFormBuilder, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(FiltersComponent);
    store = TestBed.inject(MockStore);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onFilter', () => {
    it('should be call update filters precustomer with empty', () => {
      const filters: PreCustomerFilters = {
        reference: '',
        created_at: '',
        updated_at: '',
        offset: FILTERS.offset,
        products: '',
      };
      const isFiltred: boolean = true;
      const _preCustomerFacade = TestBed.inject(PreCustomerFacade);
      const spy = spyOn(_preCustomerFacade, 'updateFiltersPreCustomer');
      const formDebugElement = fixture.debugElement.query(By.css('form'));
      formDebugElement.triggerEventHandler('ngSubmit', null);
      fixture.detectChanges();
      expect(spy).toHaveBeenCalledWith(filters, isFiltred);
    });

    it('should be call update filters precustomer with mock values', () => {
      const dateMock = '2021-01-01';
      component.form.patchValue({
        id: '',
        created_at: dateMock,
        updated_at: dateMock,
        offset: FILTERS.offset,
        products: '',
      });
      fixture.detectChanges();
      const filters: PreCustomerFilters = {
        reference: '',
        created_at: dateMock,
        updated_at: dateMock,
        offset: FILTERS.offset,
        products: '',
      };
      const isFiltred: boolean = true;
      const _preCustomerFacade = TestBed.inject(PreCustomerFacade);
      const spy = spyOn(_preCustomerFacade, 'updateFiltersPreCustomer');
      const formDebugElement = fixture.debugElement.query(By.css('form'));
      fixture.detectChanges();
      formDebugElement.triggerEventHandler('ngSubmit', null);
      expect(spy).toHaveBeenCalledWith(filters, isFiltred);
    });
  });

  it('should be call update filters customer with empty form', () => {
    const _preCustomerFacade = TestBed.inject(PreCustomerFacade);
    const spy = spyOn(_preCustomerFacade, 'updateFiltersPreCustomer');
    const formMock = {
      created_at: null,
      updated_at: null,
      id: null,
      products: null,
    };
    component.onCleanFilters();
    expect(spy).toHaveBeenCalledWith(formMock);
  });

  it('should disbled form when id has value', () => {
    component.form.patchValue({
      id: '1',
    });
    fixture.detectChanges();
    const created_at = component.form.get('created_at').value;
    const updated_at = component.form.get('updated_at').value;
    const products = component.form.get('products').value;
    expect(created_at).toBe('');
    expect(updated_at).toBe('');
    expect(products).toBe('');
  });
});
