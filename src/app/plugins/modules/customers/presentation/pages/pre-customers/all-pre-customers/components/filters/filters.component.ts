import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { PRODUCT_FAMILY } from '@app/core/constants/product.constants';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { PreCustomer } from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'pre-customers-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public projectFamily$: Observable<string>;

  public preCustomerTable$: Observable<TableResponse<PreCustomer>>;
  public isPreCustomersFiltred$: Observable<boolean>;
  public form: UntypedFormGroup;
  private _form$: Subscription;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;
  public PRODUCT_FAMILY = PRODUCT_FAMILY;

  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _preCustomerFacade: PreCustomerFacade
  ) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._preCustomerFacade.updateFiltersPreCustomer(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._preCustomerFacade.updateFiltersPreCustomer(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setInitialValues() {
    this.projectFamily$ = this._preCustomerFacade.projectFamily$;
    this.preCustomerTable$ = this._preCustomerFacade.preCustomerTable$;
    this.isPreCustomersFiltred$ =
      this._preCustomerFacade.isPreCustomersFiltred$;
    this._controlsWithDateRanges = ['created_at', 'updated_at'];
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      reference: [''],
      created_at: [''],
      updated_at: [''],
    });

    this._form$ = this.form
      .get('reference')
      .valueChanges.subscribe((reference) => {
        const controls: string[] = ['search', 'created_at', 'updated_at'];
        if (reference) {
          controls.forEach((control) => {
            this.form.get(control).setValue('');
            this.form.get(control).disable();
          });
          return;
        }
        controls.forEach((control) => {
          this.form.get(control).enable();
        });
      });
  }
}
