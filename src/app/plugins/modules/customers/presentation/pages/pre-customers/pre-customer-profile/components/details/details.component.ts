import { Component, OnInit } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'pre-customer-profile-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  public preCustomerItems$: Observable<SummaryGroup[]>;
  public preCustomerOthersItems$: Observable<SummaryGroup[]>;
  public isLoading$: Observable<boolean>;

  constructor(private _preCustomerFacade: PreCustomerFacade) {}

  ngOnInit(): void {
    this.isLoading$ = this._preCustomerFacade.isLoadingPreCustomerProfile$;
    this.preCustomerItems$ = this._preCustomerFacade.preCustomerProfileItems$;
    this.preCustomerOthersItems$ =
      this._preCustomerFacade.preCustomerProfileOthersItems$;
  }
}
