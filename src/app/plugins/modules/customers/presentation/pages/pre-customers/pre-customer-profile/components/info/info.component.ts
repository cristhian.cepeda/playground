import { Component } from '@angular/core';
import { PreCustomerProfile } from '@app/plugins/modules/customers/core/models/pre-customer.model';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'pre-customer-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent {
  public preCustomerProfile$: Observable<PreCustomerProfile>;
  public isLoading$: Observable<boolean>;

  constructor(private _preCustomerFacade: PreCustomerFacade) {
    this.preCustomerProfile$ = this._preCustomerFacade.preCustomerProfile$;
    this.isLoading$ = this._preCustomerFacade.isLoadingPreCustomerProfile$;
  }
}
