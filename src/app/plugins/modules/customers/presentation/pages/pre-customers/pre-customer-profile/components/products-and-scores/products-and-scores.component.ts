import { Component } from '@angular/core';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'pre-customer-products-and-scores',
  templateUrl: './products-and-scores.component.html',
  styleUrls: ['./products-and-scores.component.scss'],
})
export class ProductsAndScoresComponent {
  public preCustomerProductsAndScores$: Observable<ProductAndScore[]>;
  public isLoadingProductsAndScores$: Observable<boolean>;

  constructor(private _preCustomerFacade: PreCustomerFacade) {
    this.preCustomerProductsAndScores$ =
      this._preCustomerFacade.preCustomerProductsAndScores$;
    this.isLoadingProductsAndScores$ =
      this._preCustomerFacade.isLoadingProductsAndScores$;
  }
}
