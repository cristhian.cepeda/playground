import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { CUSTOMERS_URLS } from '@app/plugins/modules/customers/core/constants/urls.constants';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PreCustomerProfileGuard implements CanActivate {
  constructor(
    private _preCustomerFacade: PreCustomerFacade,
    private _router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._getPreCustomerIdParam(route);
  }

  private _getPreCustomerIdParam(
    route: ActivatedRouteSnapshot
  ): boolean | UrlTree {
    const preCustomerId = route.queryParams?.id;
    if (!!preCustomerId) {
      this._preCustomerFacade.updatePreCustomerId(preCustomerId);
      return true;
    }
    return this._router.createUrlTree([CUSTOMERS_URLS.PRE_CUSTOMERS]);
  }
}
