import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreCustomerProfilePage } from './pre-customer-profile.page';

const routes: Routes = [
  {
    path: '',
    component: PreCustomerProfilePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreCustomerProfilePageRoutingModule {}
