import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreCustomerProfilePageRoutingModule } from './pre-customer-profile-routing.module';
import { PreCustomerProfilePage } from './pre-customer-profile.page';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { DetailsComponent } from './components/details/details.component';
import { InfoComponent } from './components/info/info.component';
import { ProductsAndScoresComponent } from './components/products-and-scores/products-and-scores.component';

@NgModule({
  declarations: [PreCustomerProfilePage, DetailsComponent, InfoComponent, ProductsAndScoresComponent],
  imports: [CommonModule, PreCustomerProfilePageRoutingModule, LayoutModule],
})
export class PreCustomerProfilePageModule {}
