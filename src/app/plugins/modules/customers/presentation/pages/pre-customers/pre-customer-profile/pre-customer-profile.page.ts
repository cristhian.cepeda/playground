import { Component, OnDestroy } from '@angular/core';
import { ProductAndScore } from '@app/plugins/modules/customers/core/models/product-and-score.model';
import { PreCustomerFacade } from '@app/plugins/modules/customers/facade/pre-customer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'pre-customer-profile',
  templateUrl: './pre-customer-profile.page.html',
  styleUrls: ['./pre-customer-profile.page.scss'],
})
export class PreCustomerProfilePage implements OnDestroy {
  public preCustomerProductsAndScores$: Observable<ProductAndScore[]>;

  constructor(private _preCustomerFacade: PreCustomerFacade) {
    this.preCustomerProductsAndScores$ =
      this._preCustomerFacade.preCustomerProductsAndScores$;
    this._preCustomerFacade.getPreCustomerProfile();
  }

  ngOnDestroy(): void {
    this._preCustomerFacade.destroyPreCustomerProfilePage();
  }
}
