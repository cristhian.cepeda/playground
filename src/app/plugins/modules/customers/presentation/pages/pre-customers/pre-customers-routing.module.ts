import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { PreCustomerProfileGuard } from './pre-customer-profile/guards/pre-customer-profile.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./all-pre-customers/all-pre-customers.module').then(
        (m) => m.PreAllCustomersPageModule
      ),
  },
  {
    canActivate: [PreCustomerProfileGuard],
    path: CUSTOMER_FEATURE.PAGES.PRE_CUSTOMERS.SUB_PAGES.PRE_CUSTOMERS_PROFILE
      .PATH,
    loadChildren: () =>
      import('./pre-customer-profile/pre-customer-profile.module').then(
        (m) => m.PreCustomerProfilePageModule
      ),
    data: {
      breadcrumb: 'CUSTOMERS.PRE_CUSTOMER_PROFILE.TITLE',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreCustomersRoutingModule {}
