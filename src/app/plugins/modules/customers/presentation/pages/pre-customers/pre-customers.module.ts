import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CUSTOMER_FEATURE } from '@app/plugins/modules/customers/core/constants/feature.constants';
import { PreCustomerEffects } from '@app/plugins/modules/customers/domain/store/pre-customers/pre-customers.effects';
import { PreCustomerReducers } from '@app/plugins/modules/customers/domain/store/pre-customers/pre-customers.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PreCustomersRoutingModule } from './pre-customers-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PreCustomersRoutingModule,
    StoreModule.forFeature(
      CUSTOMER_FEATURE.PAGES.PRE_CUSTOMERS.STORE_NAME,
      PreCustomerReducers
    ),
    EffectsModule.forFeature([PreCustomerEffects]),
  ],
})
export class PreCustomersModule {}
