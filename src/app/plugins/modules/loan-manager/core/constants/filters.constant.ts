import { Filters } from '@app/core/models/filters.model';

export const FILTERS_LOAN_PROFILE_TABLES: Filters = {
  limit: 5,
  offset: 0,
};
