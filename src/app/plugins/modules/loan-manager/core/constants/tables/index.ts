import { PRODUCT_FAMILY } from '@app/core/constants/product.constants';
import { DEFAULT_FAMILY } from '@app/core/i18n/constants/translate.constants';
import {
  TableFamilyConfig,
  TableHeader,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_LOAN_REQUEST_BNPL } from './tables-bnpl/loans-request.table';
import {
  TABLE_LOANS_BNPL,
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_BNPL,
} from './tables-bnpl/loans.table';
import { TABLE_PAYMENTS_BNPL } from './tables-bnpl/payments.table';
import { TABLE_PRE_PAYMENTS_BNPL } from './tables-bnpl/pre-payments.table';
import { TABLE_TRANSACTIONS_BNPL } from './tables-bnpl/transactions.table';
import { TABLE_LOAN_REQUEST_CREDIT_CARD } from './tables-credit-card/loans-request.table';
import {
  TABLE_LOANS_CREDIT_CARD,
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_CREDIT_CARD,
} from './tables-credit-card/loans.table';
import { TABLE_PAYMENTS_CREDIT_CARD } from './tables-credit-card/payments.table';
import { TABLE_PRE_PAYMENTS_CREDIT_CARD } from './tables-credit-card/pre-payments.table';
import { TABLE_TRANSACTIONS_CREDIT_CARD } from './tables-credit-card/transactions.table';
import { TABLE_LOAN_REQUEST_DEFAULT } from './tables-default/loans-request.table';
import {
  TABLE_LOANS_DEFAULT,
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_DEFAULT,
} from './tables-default/loans.table';
import { TABLE_PAYMENTS_DEFAULT } from './tables-default/payments.table';
import { TABLE_PRE_PAYMENTS_DEFAULT } from './tables-default/pre-payments.table';
import { TABLE_TRANSACTIONS_DEFAULT } from './tables-default/transactions.table';
import { TABLE_LOAN_REQUEST_MCA } from './tables-mca/loans-request.table';
import {
  TABLE_LOANS_MCA,
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_MCA,
} from './tables-mca/loans.table';
import { TABLE_PAYMENTS_MCA } from './tables-mca/payments.table';
import { TABLE_PRE_PAYMENTS_MCA } from './tables-mca/pre-payments.table';
import { TABLE_TRANSACTIONS_MCA } from './tables-mca/transactions.table';
import { TABLE_LOAN_REQUEST_MICRO_CREDIT } from './tables-micro-credit/loans-request.table';
import {
  TABLE_LOANS_MICRO_CREDIT,
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_MICRO_CREDIT,
} from './tables-micro-credit/loans.table';
import { TABLE_PAYMENTS_MICRO_CREDIT } from './tables-micro-credit/payments.table';
import { TABLE_PRE_PAYMENTS_MICRO_CREDIT } from './tables-micro-credit/pre-payments.table';
import { TABLE_TRANSACTIONS_MICRO_CREDIT } from './tables-micro-credit/transactions.table';

export enum LOANS_TABLES {
  LOANS_REQUEST = 'loans-request',
  LOANS = 'loans',
  LOAN_PROFILE_PAYMENTS_SUMMARY = 'loan-profile-payments-summary',
  PAYMENTS = 'payments',
  PRE_PAYMENTS = 'pre-payments',
  TRANSACTIONS = 'transactions',
}

export const TABLES_BY_PRODUCT_FAMILY: TableFamilyConfig = {
  [DEFAULT_FAMILY]: {
    [LOANS_TABLES.LOANS_REQUEST]: TABLE_LOAN_REQUEST_DEFAULT,
    [LOANS_TABLES.LOANS]: TABLE_LOANS_DEFAULT,
    [LOANS_TABLES.LOAN_PROFILE_PAYMENTS_SUMMARY]:
      TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_DEFAULT,
    [LOANS_TABLES.PAYMENTS]: TABLE_PAYMENTS_DEFAULT,
    [LOANS_TABLES.PRE_PAYMENTS]: TABLE_PRE_PAYMENTS_DEFAULT,
    [LOANS_TABLES.TRANSACTIONS]: TABLE_TRANSACTIONS_DEFAULT,
  },
  [PRODUCT_FAMILY.BNPL]: {
    [LOANS_TABLES.LOANS_REQUEST]: TABLE_LOAN_REQUEST_BNPL,
    [LOANS_TABLES.LOANS]: TABLE_LOANS_BNPL,
    [LOANS_TABLES.LOAN_PROFILE_PAYMENTS_SUMMARY]:
      TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_BNPL,
    [LOANS_TABLES.PAYMENTS]: TABLE_PAYMENTS_BNPL,
    [LOANS_TABLES.PRE_PAYMENTS]: TABLE_PRE_PAYMENTS_BNPL,
    [LOANS_TABLES.TRANSACTIONS]: TABLE_TRANSACTIONS_BNPL,
  },
  [PRODUCT_FAMILY.CREDIT_CARD]: {
    [LOANS_TABLES.LOANS_REQUEST]: TABLE_LOAN_REQUEST_CREDIT_CARD,
    [LOANS_TABLES.LOANS]: TABLE_LOANS_CREDIT_CARD,
    [LOANS_TABLES.LOAN_PROFILE_PAYMENTS_SUMMARY]:
      TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_CREDIT_CARD,
    [LOANS_TABLES.PAYMENTS]: TABLE_PAYMENTS_CREDIT_CARD,
    [LOANS_TABLES.PRE_PAYMENTS]: TABLE_PRE_PAYMENTS_CREDIT_CARD,
    [LOANS_TABLES.TRANSACTIONS]: TABLE_TRANSACTIONS_CREDIT_CARD,
  },
  [PRODUCT_FAMILY.MCA]: {
    [LOANS_TABLES.LOANS_REQUEST]: TABLE_LOAN_REQUEST_MCA,
    [LOANS_TABLES.LOANS]: TABLE_LOANS_MCA,
    [LOANS_TABLES.LOAN_PROFILE_PAYMENTS_SUMMARY]:
      TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_MCA,
    [LOANS_TABLES.PAYMENTS]: TABLE_PAYMENTS_MCA,
    [LOANS_TABLES.PRE_PAYMENTS]: TABLE_PRE_PAYMENTS_MCA,
    [LOANS_TABLES.TRANSACTIONS]: TABLE_TRANSACTIONS_MCA,
  },
  [PRODUCT_FAMILY.MICRO_CREDIT]: {
    [LOANS_TABLES.LOANS_REQUEST]: TABLE_LOAN_REQUEST_MICRO_CREDIT,
    [LOANS_TABLES.LOANS]: TABLE_LOANS_MICRO_CREDIT,
    [LOANS_TABLES.LOAN_PROFILE_PAYMENTS_SUMMARY]:
      TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_MICRO_CREDIT,
    [LOANS_TABLES.PAYMENTS]: TABLE_PAYMENTS_MICRO_CREDIT,
    [LOANS_TABLES.PRE_PAYMENTS]: TABLE_PRE_PAYMENTS_MICRO_CREDIT,
    [LOANS_TABLES.TRANSACTIONS]: TABLE_TRANSACTIONS_MICRO_CREDIT,
  },
};

export const getLoansTablePerFamily = (
  family: string,
  table: LOANS_TABLES
): TableHeader[] => {
  return !!family
    ? TABLES_BY_PRODUCT_FAMILY[family][table]
    : TABLES_BY_PRODUCT_FAMILY[DEFAULT_FAMILY][table];
};
