import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_LOAN_REQUEST_BNPL: TableHeader[] = [
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.CUSTOMER_REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'customer_reference',
  },
  // TODO IMPLEMENT WHEN BACK ADD IT
  // {
  //   label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.MERCHANT_REFERENCE',
  //   size: 4,
  //   typeColumn: TYPE_COLUMN.LABEL,
  //   dataKey: 'merchant_reference',
  //   isSortable: true,
  // },
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
    template: null,
  },
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.DOWNPAYMENT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'downpayment',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.TAKEN_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.PRODUCT_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'product_name',
  },
  {
    label: 'LOAN_MANAGER.LOAN_REQUEST.TABLE.OFFER_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'offer_name',
  },
];
