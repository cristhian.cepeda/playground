import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_DEFAULT } from '../tables-default/loans.table';

export const TABLE_LOANS_BNPL: TableHeader[] = [
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.NATIONAL_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'identification_number',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.MERCHANT_REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_reference',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.TAKEN_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.PRODUCT_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'product_name',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.OFFER_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'offer_name',
  },
];
export const TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_BNPL: TableHeader[] =
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_DEFAULT;
