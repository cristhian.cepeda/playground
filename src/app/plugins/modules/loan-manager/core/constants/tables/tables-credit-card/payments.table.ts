import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PAYMENTS_DEFAULT } from '../tables-default/payments.table';

export const TABLE_PAYMENTS_CREDIT_CARD: TableHeader[] = TABLE_PAYMENTS_DEFAULT;
