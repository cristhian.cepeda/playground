import { LOAN_MANAGER_URLS } from '@app/plugins/modules/loan-manager/core/constants/urls.constants';
import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_LOANS_DEFAULT: TableHeader[] = [
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.CUSTOMER_REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'customer_reference',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.DISPLAY_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'display_name',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.TAKEN_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.PRODUCT_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'product_name',
  },
  {
    label: 'LOAN_MANAGER.LOANS.TABLE.OFFER_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'offer_name',
  },
];

export const TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_DEFAULT: TableHeader[] = [
  {
    label: 'LOAN_MANAGER.LOAN_PROFILE.PAYMENT_SUMMARY.TABLE.REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.COMPOUND_LINK,
    dataKey: 'reference',
    compoundLink: `${LOAN_MANAGER_URLS.PAYMENTS}/`,
    compoundLinkParams: [
      {
        key: 'reference',
        keyAlias: 'search',
      },
    ],
  },
  {
    label: 'LOAN_MANAGER.LOAN_PROFILE.PAYMENT_SUMMARY.TABLE.TYPE',
    size: 4,
    typeColumn: TYPE_COLUMN.TITLE_CASE,
    dataKey: 'type',
  },
  {
    label: 'LOAN_MANAGER.LOAN_PROFILE.PAYMENT_SUMMARY.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
  },
  {
    label: 'LOAN_MANAGER.LOAN_PROFILE.PAYMENT_SUMMARY.TABLE.PAID_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'paid_at',
  },
];
