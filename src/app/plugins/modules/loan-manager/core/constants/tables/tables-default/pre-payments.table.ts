import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_PRE_PAYMENTS_DEFAULT: TableHeader[] = [
  {
    label: 'LOAN_MANAGER.PRE_PAYMENTS.TABLE.ID',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'LOAN_MANAGER.PRE_PAYMENTS.TABLE.LOAN_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'loan_reference',
  },
  {
    label: 'LOAN_MANAGER.PRE_PAYMENTS.TABLE.CUSTOMER_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'customer_reference',
  },
  {
    label: 'LOAN_MANAGER.PRE_PAYMENTS.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
    template: null,
  },
  {
    label: 'LOAN_MANAGER.PRE_PAYMENTS.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.PRE_PAYMENTS.TABLE.TYPE',
    size: 4,
    typeColumn: TYPE_COLUMN.SNAKE,
    dataKey: 'type',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.PRE_PAYMENTS.TABLE.TAKEN_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
    isSortable: true,
  },
];
