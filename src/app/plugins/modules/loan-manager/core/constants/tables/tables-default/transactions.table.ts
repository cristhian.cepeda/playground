import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_TRANSACTIONS_DEFAULT: TableHeader[] = [
  {
    label: 'LOAN_MANAGER.TRANSACTIONS.TABLE.ID',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'LOAN_MANAGER.TRANSACTIONS.TABLE.CUSTOMER_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'customer_reference',
  },
  {
    label: 'LOAN_MANAGER.TRANSACTIONS.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.TRANSACTIONS.TABLE.TYPE',
    size: 4,
    typeColumn: TYPE_COLUMN.TITLE_CASE,
    dataKey: 'type',
    isSortable: true,
  },
  {
    label: 'LOAN_MANAGER.TRANSACTIONS.TABLE.DATE',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
    isSortable: true,
  },
];
