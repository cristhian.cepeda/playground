import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_LOAN_REQUEST_DEFAULT } from '../tables-default/loans-request.table';

export const TABLE_LOAN_REQUEST_MCA: TableHeader[] = TABLE_LOAN_REQUEST_DEFAULT;
