import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import {
  TABLE_LOANS_DEFAULT,
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_DEFAULT,
} from '../tables-default/loans.table';

export const TABLE_LOANS_MCA: TableHeader[] = TABLE_LOANS_DEFAULT;

export const TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_MCA: TableHeader[] =
  TABLE_LOAN_PROFILE_PAYMENTS_SUMMARY_DEFAULT;
