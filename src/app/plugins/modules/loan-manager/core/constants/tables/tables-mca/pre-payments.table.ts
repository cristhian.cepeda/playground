import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PRE_PAYMENTS_DEFAULT } from '../tables-default/pre-payments.table';

export const TABLE_PRE_PAYMENTS_MCA: TableHeader[] = TABLE_PRE_PAYMENTS_DEFAULT;
