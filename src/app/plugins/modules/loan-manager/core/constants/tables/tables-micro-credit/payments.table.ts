import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PAYMENTS_DEFAULT } from '../tables-default/payments.table';

export const TABLE_PAYMENTS_MICRO_CREDIT: TableHeader[] =
  TABLE_PAYMENTS_DEFAULT;
