import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_TRANSACTIONS_DEFAULT } from '../tables-default/transactions.table';

export const TABLE_TRANSACTIONS_MICRO_CREDIT: TableHeader[] =
  TABLE_TRANSACTIONS_DEFAULT;
