import ENGLISH_DEFAULT_LOAN_MANAGER from '@app/plugins/modules/loan-manager/core/i18n/lang-default/en';

const ENGLISH_CREDIT_CARD_LOAN_MANAGER = {
  ...ENGLISH_DEFAULT_LOAN_MANAGER,
};
export default ENGLISH_CREDIT_CARD_LOAN_MANAGER;
