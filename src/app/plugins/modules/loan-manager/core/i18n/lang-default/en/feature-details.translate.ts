import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const LOANS_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Credit Manager',
  _MENU: {
    OVERVIEW: { TITLE: 'Overview' },
    LOAN_REQUEST: {
      TITLE: 'Credit Requests',
      TOOLTIP_MESSAGE: 'View all the requested credits in your operation.',
    },
    LOANS: {
      TITLE: 'Credits',
      TOOLTIP_MESSAGE:
        'View all the serviced credits in your operation, <br />access detailed information for each credit. ',
    },
    PAYMENTS: {
      TITLE: 'Payments',
      TOOLTIP_MESSAGE:
        'View payments and transactions for all serviced credits.',
    },
    PRE_PAYMENTS: {
      TITLE: 'Pre-Payments',
      TOOLTIP_MESSAGE:
        'View all the pre-payments or attempted payments made in the credit operation',
    },
    TRANSACTIONS: {
      TITLE: 'Transactions',
      TOOLTIP_MESSAGE:
        'View all transactions made through your  credit operation.',
    },
  },
};
export default LOANS_FEATURE_DETAILS_TRANSLATE;
