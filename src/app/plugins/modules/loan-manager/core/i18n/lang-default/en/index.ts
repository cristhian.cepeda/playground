import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';
import LOANS_FEATURE_DETAILS_TRANSLATE from './feature-details.translate';
import LOAN_REQUEST_TRANSLATE from './loan-request.translate';
import LOANS_TRANSLATE from './loans.translate';
import OVERVIEW_TRANSLATE from './overview.translate';
import PAYMENTS_TRANSLATE from './paymants.translate';
import PRE_PAYMENTS_TRANSLATE from './pre-paymants.translate';
import TRANSACTIONS_TRANSLATE from './transactions.translate';

const ENGLISH_DEFAULT_LOAN_MANAGER: FeatureDetailsTranslate = {
  ...LOANS_FEATURE_DETAILS_TRANSLATE,
  ...OVERVIEW_TRANSLATE,
  ...LOAN_REQUEST_TRANSLATE,
  ...LOANS_TRANSLATE,
  ...PAYMENTS_TRANSLATE,
  ...PRE_PAYMENTS_TRANSLATE,
  ...TRANSACTIONS_TRANSLATE,
};
export default ENGLISH_DEFAULT_LOAN_MANAGER;
