import { OverviewPageTranslate } from '../../models/overview-translate';

const OVERVIEW_TRANSLATE: OverviewPageTranslate = {
  OVERVIEW: {
    TITLE: 'Overview',
    DESCRIPTION: '',
  },
};
export default OVERVIEW_TRANSLATE;
