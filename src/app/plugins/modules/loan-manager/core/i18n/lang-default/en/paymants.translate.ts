import { PaymentsPageTranslate } from '../../models/payments-translate';

const PAYMENTS_TRANSLATE: PaymentsPageTranslate = {
  PAYMENTS: {
    TITLE: 'Payments',
    DESCRIPTION:
      'Review all registered payments. Filter and download the requests database to <br />quickly navigate and analyze your payments.',
    TOTAL_TOOLTIP:
      'Total number of payments made within your credit operation.',
    FILTERS: {
      TITLE: 'Search tools',
      DESCRIPTION:
        'Filter your credit database by using default and advanced filters or use <br />free text search for specific fields.',
      SEARCH_BAR: 'Payment ID, Customer ID, Credit  ID',
      AMOUNT_TITLE: 'Payment amount',
      AMOUNT_LESS: 'From',
      AMOUNT_GRATHER: 'to',
      STATUS: 'Status',
      DATE_OF_PAYMENT_TITLE: 'Date of payment',
      DATE_OF_PAYMENT_PLACEHOLDER: 'Choose a range',
    },
    TABLE: {
      ID: 'Payment ID',
      LOAN_ID: 'Credit  ID',
      CUSTOMER_ID: 'Customer ID',
      STATUS: 'Status',
      AMOUNT: 'Amount',
      TYPE: 'Type',
      PAID_AT: 'Date',
    },
    TABLE_DETAILS: {
      TRANSACTION_DETAILS: 'Transaction details',
      TOTAL_PAID: 'Total paid',
      PAYMENT_SUMMARY: 'Payment summary:',
      TAX: 'Tax:',
      BTN_VISIT_CREDIT: 'Visit credit',
    },
  },
};
export default PAYMENTS_TRANSLATE;
