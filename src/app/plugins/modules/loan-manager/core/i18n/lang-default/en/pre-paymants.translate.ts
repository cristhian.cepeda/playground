import { PrePaymentsPageTranslate } from '../../models/pre-payments-translate';

const PRE_PAYMENTS_TRANSLATE: PrePaymentsPageTranslate = {
  PRE_PAYMENTS: {
    TITLE: 'Pre-Payments',
    DESCRIPTION:
      'Review all registered pre-payments. Filter and download the requests database to <br />quickly navigate and analyze your pre-payments.',
    TOTAL_TOOLTIP:
      'View all the pre-payments or attempted payments made in the credit operation',
    FILTERS: {
      TITLE: 'Search tools',
      DESCRIPTION:
        'Filter your pre-payment database by using default and advanced filters <br />or use free text search for specific fields.',
      SEARCH_BAR: 'Pre-Payment ID, Customer ID, Credit  ID',
      AMOUNT_TITLE: 'Payment amount',
      AMOUNT_LESS: 'From',
      AMOUNT_GRATHER: 'to',
      STATUS: 'Pre-Payment status',
      DATE_OF_PAYMENT_TITLE: 'Date of payment',
      DATE_OF_PAYMENT_PLACEHOLDER: 'Choose a range',
    },
    TABLE: {
      ID: 'Pre-Payment ID',
      LOAN_ID: 'Credit  ID',
      CUSTOMER_ID: 'Customer ID',
      STATUS: 'Status',
      AMOUNT: 'Amount',
      TYPE: 'Type',
      TAKEN_AT: 'Taken-at',
    },
  },
};
export default PRE_PAYMENTS_TRANSLATE;
