import { TransactionsPageTranslate } from '../../models/transactions-translate';

const TRANSACTIONS_TRANSLATE: TransactionsPageTranslate = {
  TRANSACTIONS: {
    TITLE: 'Transactions',
    DESCRIPTION:
      'Review all registered transactions. Filter and download the requests database to <br /> quickly navigate and analyze your payments.',
    TOTAL_TOOLTIP:
      'Total number of transactions made within your credit operation.',
    FILTERS: {
      TITLE: 'Search tools',
      DESCRIPTION:
        'Filter your transaction database by using default and advanced filters or <br />use free text search for specific fields.',
      SEARCH_BAR: 'Customer ID',
      AMOUNT_TITLE: 'Transaction amount',
      AMOUNT_LESS: 'From',
      AMOUNT_GRATHER: 'to',
      DATE_OF_TRANSACTION_TITLE: 'Date of transaction',
      DATE_OF_TRANSACTION_PLACEHOLDER: 'Choose a range',
    },
    TABLE: {
      ID: 'Transaction ID',
      CUSTOMER_ID: 'Customer ID',
      AMOUNT: 'Amount',
      TYPE: 'Type',
      DATE: 'Date',
    },
  },
};
export default TRANSACTIONS_TRANSLATE;
