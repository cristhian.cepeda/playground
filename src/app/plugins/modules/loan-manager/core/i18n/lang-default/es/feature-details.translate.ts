import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const LOANS_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Administrador de créditos',
  _MENU: {
    OVERVIEW: { TITLE: 'Visión general' },
    LOAN_REQUEST: {
      TITLE: 'Solicitudes de crédito',
      TOOLTIP_MESSAGE:
        'Consulta todos los créditos solicitados en tu operación.',
    },
    LOANS: {
      TITLE: 'Créditos',
      TOOLTIP_MESSAGE:
        'Visualiza todos los créditos atendidos en tu operación, <br />accede a la información detallada de cada crédito.',
    },
    PAYMENTS: {
      TITLE: 'Pagos',
      TOOLTIP_MESSAGE:
        'Ver pagos y transacciones para todos los créditos atendidos.',
    },
    PRE_PAYMENTS: {
      TITLE: 'Pre-pagos',
      TOOLTIP_MESSAGE:
        'Ver todos los pre-pagos o intentos de pago realizados en la operación de crédito',
    },
    TRANSACTIONS: {
      TITLE: 'Transacciones',
      TOOLTIP_MESSAGE:
        'Consulta todas las transacciones realizadas a través de tu operación de crédito.',
    },
  },
};
export default LOANS_FEATURE_DETAILS_TRANSLATE;
