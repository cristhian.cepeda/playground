import { OverviewPageTranslate } from '../../models/overview-translate';

const OVERVIEW_TRANSLATE: OverviewPageTranslate = {
  OVERVIEW: {
    TITLE: 'Visión general',
    DESCRIPTION: '',
  },
};
export default OVERVIEW_TRANSLATE;
