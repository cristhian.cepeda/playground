import { PrePaymentsPageTranslate } from '../../models/pre-payments-translate';

const PRE_PAYMENTS_TRANSLATE: PrePaymentsPageTranslate = {
  PRE_PAYMENTS: {
    TITLE: 'Pre-pagos',
    DESCRIPTION:
      'Revisa todos los pre-pagos registrados. Filtre y descargue la base de datos de solicitudes para <br />navegar y analizar rápidamente sus pre-pagos.',
    TOTAL_TOOLTIP:
      'Ver todos los pre-pagos o intentos de pago realizados en la operación de crédito',
    FILTERS: {
      TITLE: 'Herramientas de búsqueda',
      DESCRIPTION:
        'Filtre su base de datos de pre-pago utilizando filtros predeterminados y avanzados <br />o utilice la búsqueda de texto libre para campos específicos.',
      SEARCH_BAR: 'ID de pre-pago, ID de cliente, ID de crédito',
      AMOUNT_TITLE: 'Monto del pago',
      AMOUNT_LESS: 'Desde',
      AMOUNT_GRATHER: 'hasta',
      STATUS: 'Estado del pre-pago',
      DATE_OF_PAYMENT_TITLE: 'Fecha del pago',
      DATE_OF_PAYMENT_PLACEHOLDER: 'Elige un rango',
    },
    TABLE: {
      ID: 'ID de pre-pago',
      LOAN_ID: 'ID de crédito',
      CUSTOMER_ID: 'ID del cliente',
      STATUS: 'Estado',
      AMOUNT: 'Monto',
      TYPE: 'Tipo',
      TAKEN_AT: 'Tomado en',
    },
  },
};
export default PRE_PAYMENTS_TRANSLATE;
