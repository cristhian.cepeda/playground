import { TransactionsPageTranslate } from '../../models/transactions-translate';

const TRANSACTIONS_TRANSLATE: TransactionsPageTranslate = {
  TRANSACTIONS: {
    TITLE: 'Transacciones',
    DESCRIPTION:
      'Revisa todas las transacciones registradas. Filtre y descargue la base de datos de solicitudes para <br /> navegar y analizar rápidamente sus pagos.',
    TOTAL_TOOLTIP:
      'Número total de transacciones realizadas dentro de su operación de crédito.',
    FILTERS: {
      TITLE: 'Herramientas de búsqueda',
      DESCRIPTION:
        'Filtre su base de datos de transacciones utilizando filtros predeterminados y avanzados o <br />utilice la búsqueda de texto libre para campos específicos.',
      SEARCH_BAR: 'ID de cliente',
      AMOUNT_TITLE: 'Monto de la transacción',
      AMOUNT_LESS: 'Desde',
      AMOUNT_GRATHER: 'hasta',
      DATE_OF_TRANSACTION_TITLE: 'Fecha de la transacción',
      DATE_OF_TRANSACTION_PLACEHOLDER: 'Elige un rango',
    },
    TABLE: {
      ID: 'ID de transacción',
      CUSTOMER_ID: 'ID del cliente',
      AMOUNT: 'Monto',
      TYPE: 'Tipo',
      DATE: 'Fecha',
    },
  },
};
export default TRANSACTIONS_TRANSLATE;
