import ENGLISH_DEFAULT_LOAN_MANAGER from '@app/plugins/modules/loan-manager/core/i18n/lang-default/en';
import LOAN_REQUEST_TRANSLATE from './loan-request.translate';
import LOANS_TRANSLATE from './loans.translate';
import PAYMENTS_TRANSLATE from './paymants.translate';

const ENGLISH_MCA_LOAN_MANAGER = {
  ...ENGLISH_DEFAULT_LOAN_MANAGER,
  ...LOANS_TRANSLATE,
  ...LOAN_REQUEST_TRANSLATE,
  ...PAYMENTS_TRANSLATE,
};
export default ENGLISH_MCA_LOAN_MANAGER;
