import SPANISH_DEFAULT_LOAN_MANAGER from '@app/plugins/modules/loan-manager/core/i18n/lang-default/es';

const SPANISH_MICRO_CREDIT_LOAN_MANAGER = {
  ...SPANISH_DEFAULT_LOAN_MANAGER,
};
export default SPANISH_MICRO_CREDIT_LOAN_MANAGER;
