import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface LoanRequestPageTranslate {
  LOAN_REQUEST: LoanRequestTranslate;
}
export interface LoanRequestTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: LoanRequestFiltersTranslate;
  TABLE: LoanRequestTableTranslate;
}
export interface LoanRequestFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  AMOUNT_TITLE: string;
  AMOUNT_LESS: string;
  AMOUNT_GRATHER: string;
  STATUS: string;
  TAKEN_AT: string;
}
export interface LoanRequestTableTranslate {
  REFERENCE: string;
  CUSTOMER_REFERENCE: string;
  MERCHANT_REFERENCE: string;
  DISPLAY_NAME: string;
  STATUS: string;
  AMOUNT: string;
  DOWNPAYMENT: string;
  TAKEN_AT: string;
  PRODUCT_NAME: string;
  OFFER_NAME: string;
}
