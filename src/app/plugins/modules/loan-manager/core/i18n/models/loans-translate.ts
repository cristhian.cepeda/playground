import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';
export interface LoansPageTranslate {
  LOANS: LoansTranslate;
  LOAN_PROFILE: LoanProfileTranslate;
}
export interface LoansTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: LoansFiltersTranslate;
  TABLE: LoansTableTranslate;
}
export interface LoansFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  AMOUNT_TITLE: string;
  AMOUNT_LESS: string;
  AMOUNT_GRATHER: string;
  STATUS: string;
  TAKEN_AT: string;
}

export interface LoansTableTranslate {
  REFERENCE: string;
  NATIONAL_ID: string;
  CUSTOMER_REFERENCE: string;
  MERCHANT_REFERENCE: string;
  DISPLAY_NAME: string;
  STATUS: string;
  AMOUNT: string;
  TAKEN_AT: string;
  PRODUCT_NAME: string;
  OFFER_NAME: string;
}

export interface LoanProfileTranslate {
  TITLE: string;
  DESCRIPTION: string;
  INFO: LoanProfileInfoTranslate;
  LOAN_SUMMARY: LoanProfileSummaryTranslate;
  PAYMENT_SUMMARY: LoanProfilePaymentSummaryTranslate;
}

export interface LoanProfileInfoTranslate {
  LOAN_ID: string;
  MERCHANT: string;
  CUSTOMER_ID: string;
  TAKEN_AT: string;
  MAX_DUE_DATE: string;
  FROZEN_AT: string;
  DAYS_FRONZEN: string;
  UNFROZEN_AT: string;
}

export interface LoanProfileSummaryTranslate {
  TITLE: string;
  TOTALS: LoanProfileSummaryTotalsTranslate;
  LOAN_DETAILS: LoanProfileSummaryDetailsTranslate;
  CHARGES: string;
  LOAN_TYC: string;
}
export interface LoanProfileSummaryTotalsTranslate {
  DEBT: LoanProfileSummaryTotalsCardTranslate;
  PAID: LoanProfileSummaryTotalsCardTranslate;
  CHARGES_DETAILS: string;
}

export interface LoanProfileSummaryTotalsCardTranslate {
  TITLE: string;
  TOTAL: string;
  TOTAL_CREDIT?: string;
  TOTAL_TOOLTIP: string;
  CAPITAL: string;
  CHARGES: string;
}
export interface LoanProfileSummaryDetailsTranslate {
  TITLE: string;
  PURCHASE_AMOUNT: string;
  REQUESTED_AMOUNT: string;
  LOAN_TERM: string;
  REPAYMENT_FRECUENCY: string;
  INSTALLMENT: string;
  NUMBER_OF_INSTALLMENTS: string;
}

export interface LoanProfilePaymentSummaryTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TABLE: LoanProfilePaymentSummaryTableTranslate;
  BTN_VISIT_PAYMENTS: string;
}

export interface LoanProfilePaymentSummaryTableTranslate {
  REFERENCE: string;
  TYPE: string;
  AMOUNT: string;
  PAID_AT: string;
}
