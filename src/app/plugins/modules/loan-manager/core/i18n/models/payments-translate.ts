import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface PaymentsPageTranslate {
  PAYMENTS: PaymentsTranslate;
}
export interface PaymentsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: PaymentsFiltersTranslate;
  TABLE: PaymentsTableTranslate;
  TABLE_DETAILS: PaymentsTableDetailsTranslate;
}

export interface PaymentsFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  AMOUNT_TITLE: string;
  AMOUNT_LESS: string;
  AMOUNT_GRATHER: string;
  STATUS: string;
  DATE_OF_PAYMENT_TITLE: string;
  DATE_OF_PAYMENT_PLACEHOLDER: string;
}

export interface PaymentsTableTranslate {
  ID: string;
  LOAN_ID: string;
  CUSTOMER_ID: string;
  STATUS: string;
  AMOUNT: string;
  TYPE: string;
  PAID_AT: string;
}

export interface PaymentsTableDetailsTranslate {
  TRANSACTION_DETAILS: string;
  TOTAL_PAID: string;
  PAYMENT_SUMMARY: string;
  TAX: string;
  BTN_VISIT_CREDIT: string;
}
