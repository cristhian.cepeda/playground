import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface PrePaymentsPageTranslate {
  PRE_PAYMENTS: PrePaymentsTranslate;
}
export interface PrePaymentsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: PrePaymentsFiltersTranslate;
  TABLE: PrePaymentsTableTranslate;
}

export interface PrePaymentsFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  AMOUNT_TITLE: string;
  AMOUNT_LESS: string;
  AMOUNT_GRATHER: string;
  DATE_OF_PAYMENT_TITLE: string;
  DATE_OF_PAYMENT_PLACEHOLDER: string;
  STATUS: string;
}

export interface PrePaymentsTableTranslate {
  ID: string;
  LOAN_ID: string;
  CUSTOMER_ID: string;
  STATUS: string;
  AMOUNT: string;
  TYPE: string;
  TAKEN_AT: string;
}
