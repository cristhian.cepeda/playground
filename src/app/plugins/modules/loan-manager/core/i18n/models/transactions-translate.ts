import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface TransactionsPageTranslate {
  TRANSACTIONS: TransactionsTranslate;
}
export interface TransactionsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: TransactionsFiltersTranslate;
  TABLE: TransactionsTableTranslate;
}

export interface TransactionsFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  AMOUNT_TITLE: string;
  AMOUNT_LESS: string;
  AMOUNT_GRATHER: string;
  DATE_OF_TRANSACTION_TITLE: string;
  DATE_OF_TRANSACTION_PLACEHOLDER: string;
}

export interface TransactionsTableTranslate {
  ID: string;
  CUSTOMER_ID: string;
  AMOUNT: string;
  TYPE: string;
  DATE: string;
}
