export enum CHARGE_FREQUENCY {
  DAILY = 'daily',
  WEEKLY = 'weekly',
  MONTHLY = 'monthly',
  UNIQUE = 'unique',
}
export enum CHARGE_VALUE_TYPE {
  FIXED = 'fixed',
  PERCENTAGE = 'percentage',
}
export enum CHARGE_TYPE {
  CAPITAL = 'capital',
  INTEREST = 'interest',
  COST = 'cost',
  COLLECTION_COST = 'collection_cost',
  DEFAULT_INTEREST = 'default_interest',
}
