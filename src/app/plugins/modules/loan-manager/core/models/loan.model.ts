import { Filters } from '@app/core/models/filters.model';
import { Score } from '@app/core/models/score.model';
import { LOANS_STATUS } from '../constants/loans.constants';
import {
  CHARGE_FREQUENCY,
  CHARGE_TYPE,
  CHARGE_VALUE_TYPE,
} from './charge.model';

export interface Loan {
  amount: number;
  created_at: Date;
  currency?: string;
  customer_id?: string;
  customer_reference?: string;
  display_name?: string;
  id: string;
  identification_number?: string;
  installment_outstanding?: number;
  installment_value?: number;
  installments_number?: number;
  loan_term_periodicity?: string;
  loan_term_value?: number;
  maximum_payment_date?: Date;
  merchant_id?: string;
  merchant_reference?: string;
  merchant_display_name?: string;
  offer_name?: string;
  product_name?: string;
  reference: string;
  repayment_frecuency?: string;
  request_amount: number;
  requested_amount?: number;
  status: LOANS_STATUS;
  loan_freeze_info?: LoanFreezeInfo;
}

export interface LoanFreezeInfo {
  freeze_at?: Date;
  unfreeze_at?: Date;
  days_frozen?: number;
}

export interface LoanTotals {
  debt: LoanTotalsDebt;
  paid: LoanTotalsPaid;
}

export interface LoanTotalsDebt {
  total_debt: number;
  total_credit: number;
  capital_debt: number;
  charges_debt: number;
  charges: LoanTotalsCharge[];
}

export interface LoanTotalsPaid {
  total_paid: number;
  paid_capital: number;
  paid_charges: number;
  charges: LoanTotalsCharge[];
}

export interface LoanTotalsCharge {
  name: string;
  value: number;
  type: CHARGE_TYPE;
}

export interface LoanTotalsCard {
  debt: LoanTotalsCardItem;
  paid: LoanTotalsCardItem;
}

export interface LoanTotalsCardItem {
  total: number;
  capital: number;
  charge: number;
  totalCredit?: number;
  charges: LoanTotalsCharge[];
}

export interface LoanRequest {
  id: string;
  external_id: string;
  reference: string;
  customer_id: string;
  customer_reference: string;
  merchant_id?: string;
  merchant_reference?: string;
  display_name: string;
  status: number | string;
  amount: number | string;
  downpayment: number | string;
  created_at: Date;
  product_name: string;
  offer_name: string;
}

export interface LoansFilters extends Filters {
  merchant_id?: string;
  customer_id?: string;
  search?: string;
  status?: string;
  amount__gte?: string;
  amount__lte?: string;
  created_at?: string;
}

export interface LoansRequestFilters extends Filters {
  search?: string;
  amount__lte?: string;
  amount__gte?: string;
  status?: string;
  created_at?: string;
}

export interface LoanCharge {
  name: string;
  value: number | string;
  value_type: CHARGE_VALUE_TYPE;
  type: CHARGE_TYPE;
  tax?: {
    name: string;
    value: number | string;
  };
  detail?: {
    periodicity: CHARGE_FREQUENCY;
    value: number | string;
  };
}

export interface LoansScore extends Score {}
