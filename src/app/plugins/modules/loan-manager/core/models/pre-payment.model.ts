import { Filters } from '@app/core/models/filters.model';

export interface PrePayment {
  amount: number;
  created_at: Date;
  customer_reference: string;
  id: string;
  loan_reference: string;
  reference: string;
  status: PRE_PAYMENT_STATUS;
  type: string;
}

export interface PrePaymentFilters extends Filters {
  reference?: string;
  amount?: string;
  amount__gte?: string;
  amount__gt?: string;
  amount__lte?: string;
  amount__lt?: string;
  status?: string;
  created_at?: string;
  created_at__gte?: string;
  created_at__gt?: string;
  created_at__lte?: string;
  created_at__lt?: string;
  search?: string;
}

export enum PRE_PAYMENT_STATUS {
  PENDING = 'pending',
  REJECTED = 'rejected',
  COMPLETED = 'completed',
}
