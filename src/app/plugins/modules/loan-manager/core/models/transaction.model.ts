import { Filters } from '@app/core/models/filters.model';

export interface Transaction {
  id: string;
  reference: string;
  customer_id: string;
  customer_reference: string;
  amount: string;
  type: string;
  created_at: Date;
}

export interface TransactionFilters extends Filters {
  search?: string;
  amount__lte?: string;
  amount__gte?: string;
  date?: string;
}
