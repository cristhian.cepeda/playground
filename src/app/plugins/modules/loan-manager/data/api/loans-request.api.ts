import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  LoanRequest,
  LoansRequestFilters,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class LoansRequestApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getLoansRequest(
    filters: LoansRequestFilters
  ): Observable<TableResponse<LoanRequest>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<LoanRequest>>(
      API_URLS.GET_LOANS_REQUEST,
      { params }
    );
  }

  public downloadLoansRequest(
    filters: LoansRequestFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(API_URLS.GET_LOANS_REQUEST, filters);
  }
}
