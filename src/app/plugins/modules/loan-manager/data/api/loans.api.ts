import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  Loan,
  LoanCharge,
  LoansFilters,
  LoansScore,
  LoanTotals,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class LoansApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getLoans(filters: LoansFilters): Observable<TableResponse<Loan>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Loan>>(API_URLS.GET_LOANS, { params });
  }

  public downloadLoans(
    filters: LoansFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(API_URLS.GET_LOANS, filters);
  }

  public getLoan(loanId: string): Observable<Loan> {
    return this._http.get<Loan>(`${API_URLS.GET_LOANS}/${loanId}`);
  }

  public getLoanTotals(loanId: string): Observable<LoanTotals> {
    return this._http.get<LoanTotals>(
      API_URLS.GET_LOAN_TOTALS.replace(':id', loanId)
    );
  }

  public getLoanCharges(loanId: string): Observable<LoanCharge[]> {
    return this._http.get<LoanCharge[]>(
      API_URLS.GET_LOAN_CHARGES.replace(':id', loanId)
    );
  }

  public getLoansScores(loanId: string): Observable<LoansScore[]> {
    return this._http.get<LoansScore[]>(
      API_URLS.GET_LOAN_SCORES.replace(':id', loanId)
    );
  }
}
