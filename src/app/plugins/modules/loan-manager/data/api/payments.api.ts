import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  Payment,
  PaymentFilters,
  PaymentSummary,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class PaymentsApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getPayments(
    filters: PaymentFilters
  ): Observable<TableResponse<Payment>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Payment>>(API_URLS.GET_PAYMENTS, {
      params,
    });
  }

  public getPaymentSummary(id: string): Observable<PaymentSummary[]> {
    return this._http.get<PaymentSummary[]>(
      API_URLS.GET_PAYMENT_SUMMARY.replace(':id', id)
    );
  }

  public downloadPayments(
    filters: PaymentFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(API_URLS.GET_PAYMENTS, filters);
  }
}
