import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  PrePayment,
  PrePaymentFilters,
} from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class PrePaymentsApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getPrePayments(
    filters: PrePaymentFilters
  ): Observable<TableResponse<PrePayment>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<PrePayment>>(
      API_URLS.GET_PRE_PAYMENTS,
      {
        params,
      }
    );
  }

  public downloadPrePayments(
    filters: PrePaymentFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(API_URLS.GET_PRE_PAYMENTS, filters);
  }
}
