import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  Transaction,
  TransactionFilters,
} from '@app/plugins/modules/loan-manager/core/models/transaction.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class TransactionsApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getTransactions(
    filters: TransactionFilters
  ): Observable<TableResponse<Transaction>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Transaction>>(
      API_URLS.GET_TRANSACTIONS,
      {
        params,
      }
    );
  }

  public downloadTransactions(
    filters: TransactionFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(API_URLS.GET_TRANSACTIONS, filters);
  }
}
