import { CHARGE_TYPE } from '@app/plugins/modules/loan-manager/core/models/charge.model';
import {
  Payment,
  PaymentSummary,
  PAYMENT_STATUS,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const PAYMENTS: TableResponse<Payment> = {
  results: [
    {
      id: '1234567',
      reference: '812348-123123',
      loan_id: '123456',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PAYMENT_STATUS.ACCEPTED,
      amount: 110010,
      type: 'discount',
      paid_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '0987654',
      reference: '812348-123123',
      loan_id: '123456',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PAYMENT_STATUS.REVERSED,
      amount: 110010,
      type: 'discount',
      paid_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '34523457',
      reference: '812348-123123',
      loan_id: '123456',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PAYMENT_STATUS.REJECTED,
      amount: 110010,
      type: 'discount',
      paid_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '876345364',
      reference: '812348-123123',
      loan_id: '123456',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PAYMENT_STATUS.ACCEPTED,
      amount: 110010,
      type: 'discount',
      paid_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '0904545633',
      reference: '812348-123123',
      loan_id: '123456',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PAYMENT_STATUS.REVERSED,
      amount: 110010,
      type: 'discount',
      paid_at: new Date('2019-10-01T15:59:15-05:00'),
    },
  ],
  filtered: 5,
  count: 5,
};

export const PAYMENT_SUMMARY: PaymentSummary[] = [
  {
    amount: '50.0000000000',
    condition: CHARGE_TYPE.COST,
    tax_amount: '9.5000000000',
  },
  {
    amount: '50.0000000000',
    condition: CHARGE_TYPE.DEFAULT_INTEREST,
    tax_amount: '0.0000000000',
  },
  {
    amount: '20.0000000000',
    condition: CHARGE_TYPE.COLLECTION_COST,
    tax_amount: '3.8000000000',
  },
  {
    amount: '150.0000000000',
    condition: CHARGE_TYPE.INTEREST,
    tax_amount: '0.0000000000',
  },
  {
    amount: '250.0000000000',
    condition: CHARGE_TYPE.CAPITAL,
    tax_amount: '0.0000000000',
  },
];
