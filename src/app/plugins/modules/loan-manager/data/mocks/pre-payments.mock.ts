import {
  PrePayment,
  PRE_PAYMENT_STATUS,
} from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const PRE_PAYMENTS: TableResponse<PrePayment> = {
  results: [
    {
      id: '1234567',
      reference: '812348-123123',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PRE_PAYMENT_STATUS.COMPLETED,
      amount: 110010,
      type: 'manual',
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '0987654',
      reference: '812348-123123',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PRE_PAYMENT_STATUS.PENDING,
      amount: 110010,
      type: 'automatic',
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '34523457',
      reference: '812348-123123',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PRE_PAYMENT_STATUS.REJECTED,
      amount: 110010,
      type: 'forced',
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '876345364',
      reference: '812348-123123',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PRE_PAYMENT_STATUS.COMPLETED,
      amount: 110010,
      type: 'voluntary',
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      id: '0904545633',
      reference: '812348-123123',
      loan_reference: '12345ASD',
      customer_reference: '12318112312',
      status: PRE_PAYMENT_STATUS.PENDING,
      amount: 110010,
      type: 'credit_note',
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
  ],
  filtered: 5,
  count: 5,
};
