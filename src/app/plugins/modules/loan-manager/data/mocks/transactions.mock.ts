import { Transaction } from '@app/plugins/modules/loan-manager/core/models/transaction.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TRANSACTIONS: TableResponse<Transaction> = {
  results: [
    {
      id: '9da97574-0f77-4e5a-84c6-99ac5da2b235',
      reference: 'WqBUWhkrjmQu',
      customer_id: '42d45093-22ed-4d18-9542-15f9b43a9613',
      customer_reference: 'IxEvmTXuConV',
      amount: '8649.2674',
      type: 'manual',
      created_at: new Date('2022-12-12T14:14:45.137751Z'),
    },
    {
      id: '2349da9-0f77-4e5a-84c6-99ac5da2b235',
      reference: 'WqBUWhkrjmQu',
      customer_id: '42d45093-22ed-4d18-9542-15f9b43a9613',
      customer_reference: 'IxEvmTXuConV',
      amount: '85439.2674',
      type: 'discount',
      created_at: new Date('2022-12-12T14:14:45.137751Z'),
    },
    {
      id: '2349da9-0f77-4e5a-234-99ac5da2b235',
      reference: 'WqBUWhkrjmQu',
      customer_id: '42d45093-22ed-4d18-9542-15f9b43a9613',
      customer_reference: 'IxEvmTXuConV',
      amount: '5649.2674',
      type: 'manual',
      created_at: new Date('2022-12-12T14:14:45.137751Z'),
    },
    {
      id: '2349da9-0f77-4e5a-234-l2k3jh4l23l4j',
      reference: 'WqBUWhkrjmQu',
      customer_id: '42d45093-22ed-4d18-9542-15f9b43a9613',
      customer_reference: 'IxEvmTXuConV',
      amount: '32649.2674',
      type: 'discount',
      created_at: new Date('2022-12-12T14:14:45.137751Z'),
    },
  ],
  filtered: 5,
  count: 5,
};
