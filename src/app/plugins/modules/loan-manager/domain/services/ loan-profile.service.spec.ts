import { TestBed } from '@angular/core/testing';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { Loan, LoanCharge } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LOAN, LOAN_CHARGES } from '@app/plugins/modules/loan-manager/data/mocks/loans.mock';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { LoanProfileService } from './loan-profile.service';

describe('LoanProfileService', () => {
  let service: LoanProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [CustomCurrencyPipe, SnakeToCapitalPipe],
    });
    service = TestBed.inject(LoanProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be get Loan Items', () => {
    const loan: Loan = LOAN;
    const result = service.getLoanItems(loan);
    expect(result).toBeInstanceOf(Observable<SummaryGroup>);
  });

  it('should be get Charges Items By Loan Charges return instance of summary group array', () => {
    const loanCharges: LoanCharge[] = LOAN_CHARGES;
    const result = service.getChargesItemsByLoanCharges(loanCharges);
    expect(result).toBeInstanceOf(Array<SummaryGroup>);
  });

  it('should be get Charges Items By Loan Charges return instance of summary group array with value empty', () => {
    let loanCharges: LoanCharge[] = LOAN_CHARGES;
    loanCharges[3].value = null;
    const result = service.getChargesItemsByLoanCharges(loanCharges);
    expect(result).toBeInstanceOf(Array<SummaryGroup>);
  });

  it('should be get Charges Items By Loan Charges return empty', () => {
    const loanCharges: LoanCharge[] = [];
    const result = service.getChargesItemsByLoanCharges(loanCharges);
    expect(result).toEqual([]);
  });
});
