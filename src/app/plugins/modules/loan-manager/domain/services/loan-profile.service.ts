import { Injectable } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { IKeyValue } from '@app/core/models/utils.model';
import {
  CHARGE_TYPE,
  CHARGE_VALUE_TYPE,
} from '@app/plugins/modules/loan-manager/core/models/charge.model';
import {
  Loan,
  LoanCharge,
  LoanTotals,
  LoanTotalsCard,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { TranslateService } from '@ngx-translate/core';
import { map, Observable } from 'rxjs';
import { LoanProfileSummaryDetailsTranslate } from '../../core/i18n/models/loans-translate';

@Injectable({
  providedIn: 'root',
})
export class LoanProfileService {
  constructor(
    private _currency: CustomCurrencyPipe,
    private _skaneToCapital: SnakeToCapitalPipe,
    private _translateService: TranslateService
  ) {}

  /**
   * Make the loan detail data to be used in the loan details component
   * @param loan Loan, to get detail
   * @returns SummaryGroup[]
   */
  public getLoanItems(loan: Loan): Observable<SummaryGroup[]> {
    return this._translateService
      .get('LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.LOAN_DETAILS')
      .pipe(
        map((DETAILS: LoanProfileSummaryDetailsTranslate) => {
          return [
            {
              key: `${DETAILS.PURCHASE_AMOUNT}:`,
              value: this._currency.transform(loan?.amount),
            },
            {
              key: `${DETAILS.REQUESTED_AMOUNT}:`,
              value: this._currency.transform(loan?.request_amount),
            },
            {
              key: `${DETAILS.LOAN_TERM}:`,
              value: `${loan?.loan_term_value} ${this._skaneToCapital.transform(
                loan?.loan_term_periodicity
              )}`,
            },
            {
              key: `${DETAILS.REPAYMENT_FRECUENCY}:`,
              value: this._skaneToCapital.transform(loan?.repayment_frecuency),
            },
            {
              key: `${DETAILS.INSTALLMENT}:`,
              value: this._currency.transform(loan?.installment_outstanding),
            },
            {
              key: `${DETAILS.NUMBER_OF_INSTALLMENTS}:`,
              value: loan?.installments_number,
            },
          ];
        })
      );
  }

  /**
   * Make the loan profile charges to be used in the loan details component
   * @param loanCharges LoanCharges[], to get detail items
   * @returns SummaryGroup[]
   */
  public getChargesItemsByLoanCharges(
    loanCharges: LoanCharge[]
  ): SummaryGroup[] {
    let chargesItems: SummaryGroup[] = [];

    if (!loanCharges || !(loanCharges?.length > 0)) return [];

    loanCharges.forEach((charge: LoanCharge) => {
      const chargeItemObject: SummaryGroup = {
        key: this._skaneToCapital.transform(charge?.name),
        value: this._getChargeValue(charge?.value, charge),
      };
      // If the charge has detail, add it to the extraData property
      chargeItemObject.extraData = this._getChargeExtraData(charge);

      chargesItems.push(chargeItemObject);
    });
    return chargesItems;
  }

  /**
   * Make the loan profile totals to be used in the loan total component
   * @param loanTotals LoanTotals, to get total items
   * @returns LoanTotalsCard
   */
  public getLoanTotalsCards(loanTotals: LoanTotals): LoanTotalsCard {
    return {
      debt: {
        total: loanTotals?.debt?.total_debt,
        capital: loanTotals?.debt?.capital_debt,
        charge: loanTotals?.debt?.charges_debt,
        totalCredit: loanTotals?.debt?.total_credit,
        charges: loanTotals?.debt?.charges,
      },
      paid: {
        total: loanTotals?.paid?.total_paid,
        capital: loanTotals?.paid?.paid_capital,
        charge: loanTotals?.paid?.paid_charges,
        charges: loanTotals?.paid?.charges,
      },
    };
  }

  /**
   * Make the extraData to be used in the loan details extraData item
   * @param charge LoanCharge
   * @returns IKeyValue[]
   */
  private _getChargeExtraData(charge: LoanCharge): IKeyValue[] {
    return charge?.type == CHARGE_TYPE.INTEREST
      ? [
          {
            key: `${this._skaneToCapital.transform(charge?.name)} 
            (${charge?.detail?.periodicity})`,
            value: this._getChargeValue(charge?.detail?.value, charge),
          },
        ]
      : null;
  }

  /**
   * Make value to charge
   * @param value number, value unformated
   * @param charge LoanCharge, charge to get value format
   * @returns string, value formatted
   */
  private _getChargeValue(value: number | string, charge: LoanCharge): string {
    if (!value) return '0';
    const valueFormat =
      charge?.value_type === CHARGE_VALUE_TYPE.PERCENTAGE
        ? `${parseFloat(String(value))}%`
        : this._currency.transform(value);
    const valueWithTax = charge?.tax?.name
      ? `${valueFormat} + ${this._skaneToCapital.transform(charge?.tax?.name)}`
      : valueFormat;
    return valueWithTax;
  }
}
