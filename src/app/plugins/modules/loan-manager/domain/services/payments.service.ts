import { Injectable } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import {
  Payment,
  PaymentProfile,
  PaymentSummary,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';

@Injectable({
  providedIn: 'root',
})
export class PaymentsService {
  constructor(
    private _currency: CustomCurrencyPipe,
    private _skaneToCapital: SnakeToCapitalPipe
  ) {}

  public setPaymentProfile(
    payment: Payment,
    paymentSummary: PaymentSummary[]
  ): PaymentProfile {
    const summary: SummaryGroup[] = paymentSummary.map((paymentSummary) =>
      Number(paymentSummary?.amount) > 0
        ? {
            key: this._skaneToCapital.transform(paymentSummary?.condition),
            value: this._currency.transform(paymentSummary?.amount),
            extraData:
              Number(paymentSummary?.tax_amount) > 0
                ? [
                    {
                      key: 'LOAN_MANAGER.PAYMENTS.TABLE_DETAILS.TAX',
                      value: this._currency.transform(
                        paymentSummary?.tax_amount
                      ),
                    },
                  ]
                : [],
          }
        : {}
    );
    const paymentProfile: PaymentProfile = {
      id: payment?.id,
      loanId: payment?.loan_id,
      amount: payment?.amount,
      summary,
    };
    return paymentProfile;
  }
}
