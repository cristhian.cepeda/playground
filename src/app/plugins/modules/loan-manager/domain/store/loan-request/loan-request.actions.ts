import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  LoanRequest,
  LoansRequestFilters,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initLoansRequestPage = createAction(
  '[Loans Request Page] init loans request page'
);

export const successInitLoansRequestPage = createAction(
  '[Loans Request Effects] success init loans request page',
  props<{ loansRequest: TableResponse<LoanRequest> }>()
);

export const failedInitLoansRequestPage = createAction(
  '[Loans Request Effects] failed init loans request page'
);

export const updateLoansRequestFilters = createAction(
  '[Loans Request Effects] update filters loans request page',
  props<{ filters: LoansRequestFilters; filtersOptions?: FiltersOptions }>()
);

export const downloadLoansRequest = createAction(
  '[Loans Request Page] Downloand Loans Request '
);

export const successDownloadLoansRequest = createAction(
  '[Loans Request Effects] success Downloand Loans Request ',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadLoansRequest = createAction(
  '[Loans Request Effects] failed Downloand Loans Request '
);
