import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { LoanRequest } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoansRequestApi } from '@app/plugins/modules/loan-manager/data/api/loans-request.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as loansRequestActions from './loan-request.actions';
import * as loanRequestSelectors from './loan-request.selectors';

@Injectable()
export class LoanRequestEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _loansRequestApi: LoansRequestApi,
    private _utilsService: UtilsService
  ) {}

  public initLoansRequestPageEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansRequestActions.initLoansRequestPage),
      map(() =>
        loansRequestActions.updateLoansRequestFilters({
          filters: FILTERS,
        })
      )
    )
  );

  public getLoansRequestEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansRequestActions.updateLoansRequestFilters),
      withLatestFrom(
        this._store.select(loanRequestSelectors.selectLoansRequestFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._loansRequestApi.getLoansRequest(filters).pipe(
          map((loansRequest: TableResponse<LoanRequest>) =>
            loansRequestActions.successInitLoansRequestPage({ loansRequest })
          ),
          catchError((_) =>
            of(loansRequestActions.failedInitLoansRequestPage())
          )
        );
      })
    )
  );

  public downloadLoansRequestEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansRequestActions.downloadLoansRequest),
      withLatestFrom(
        this._store.select(loanRequestSelectors.selectLoansRequestFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._loansRequestApi.downloadLoansRequest(fileFilters).pipe(
          map((fileResponse) =>
            loansRequestActions.successDownloadLoansRequest({ fileResponse })
          ),
          catchError((_) =>
            of(loansRequestActions.failedDownloadLoansRequest())
          )
        );
      })
    )
  );

  public successDownloadLoansRequestEffect$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(loansRequestActions.successDownloadLoansRequest),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            LOAN_MANAGER_FEATURE.PAGES.LOAN_REQUEST.NAME
          )
        )
      ),
    { dispatch: false }
  );
}
