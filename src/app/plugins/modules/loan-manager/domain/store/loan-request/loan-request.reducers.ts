import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as loansRequestActions from './loan-request.actions';
import { LoanRequestState } from './loan-request.state';

export const initialLoanRequestState: LoanRequestState = {
  filters: FILTERS,
};

const _loanRequestReducer = createReducer(
  initialLoanRequestState,
  on(loansRequestActions.initLoansRequestPage, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(
    loansRequestActions.successInitLoansRequestPage,
    (state, { loansRequest }) => ({
      ...state,
      loansRequest,
      isLoading: false,
    })
  ),
  on(loansRequestActions.failedInitLoansRequestPage, (state) => ({
    ...state,
    loansRequest: null,
    isFiltred: false,
    isLoading: false,
  })),
  on(
    loansRequestActions.updateLoansRequestFilters,
    (state, { filters, filtersOptions }) => {
      const FiltersValidationsOptions: FiltersValidationsOptions =
        setFilterValidation(state, filters, filtersOptions);
      return {
        ...state,
        isLoading: true,
        isFiltred: FiltersValidationsOptions?.isFiltred,
        filters: FiltersValidationsOptions?.filters,
      };
    }
  )
);

export function LoanRequestReducers(
  state: LoanRequestState | undefined,
  action: Action
) {
  return _loanRequestReducer(state, action);
}
