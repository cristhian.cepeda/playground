import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { LoanRequestState } from './loan-request.state';

export const getLoanRequestFeatureState =
  createFeatureSelector<LoanRequestState>(
    LOAN_MANAGER_FEATURE.PAGES.LOAN_REQUEST.STORE_NAME
  );

export const selectLoansRequest = createSelector(
  getLoanRequestFeatureState,
  (state: LoanRequestState) => state?.loansRequest
);

export const selectIsLoadingLoansRequest = createSelector(
  getLoanRequestFeatureState,
  (state: LoanRequestState) => state?.isLoading
);
export const selectIsLoansRequestFiltred = createSelector(
  getLoanRequestFeatureState,
  (state: LoanRequestState) => state?.isFiltred
);

export const selectLoansRequestFilters = createSelector(
  getLoanRequestFeatureState,
  (state: LoanRequestState) => state?.filters
);
