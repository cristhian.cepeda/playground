import { TablePageActions } from '@app/core/models/page.model';
import {
  LoanRequest,
  LoansRequestFilters,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface LoanRequestState extends TablePageActions {
  loansRequest?: TableResponse<LoanRequest>;
  filters: LoansRequestFilters;
}
