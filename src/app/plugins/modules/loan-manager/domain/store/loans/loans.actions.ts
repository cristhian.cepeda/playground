import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  Loan,
  LoanCharge,
  LoansFilters,
  LoansScore,
  LoanTotals,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { Payment } from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initLoansPage = createAction(
  '[Loans Page] Init Loans Page',
  props<{ filters: LoansFilters }>()
);

export const updateFiltersLoans = createAction(
  '[Loans Filters Component] Update Filters Loans',
  props<{ filters: LoansFilters; filtersOptions?: FiltersOptions }>()
);

export const updateLoanId = createAction(
  '[Loan Guard] Update Loan Id',
  props<{ loanId: string }>()
);

export const successGetLoans = createAction(
  '[Loans Effects] Success Get Loans',
  props<{ loans: TableResponse<Loan> }>()
);
export const failedGetLoans = createAction('[Loans Effects] Failed Get Loans');

export const downloadLoans = createAction('[Loans Page] Downloand Loans');

export const successDownloadLoans = createAction(
  '[Loans Effects] success Downloand Loans',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadLoans = createAction(
  '[Loans Effects] failed Downloand Loans'
);

export const getLoan = createAction('[Loan Profile Page] Get Loan Detail');
export const successGetLoan = createAction(
  '[Loans Effects] Success Get Loan Detail',
  props<{ loan: Loan }>()
);
export const failedGetLoan = createAction(
  '[Loans Effects] Failed Get Loan Detail'
);
export const destroyLoanProfilePage = createAction(
  '[Loan Profile Page] Destroy Loan Profile Page'
);

export const getLoanTotals = createAction(
  '[Loan Profile Page] Get Loan Detail Totals'
);
export const successGetLoanTotals = createAction(
  '[Loans Effects] Success Get Loan Detail Totals',
  props<{ loanTotals: LoanTotals }>()
);
export const failedGetLoanTotals = createAction(
  '[Loans Effects] Failed Get Loan Detail Totals'
);

export const getLoanCharges = createAction(
  '[Loan Profile Page] Get Loan Detail Charges'
);
export const successGetLoanCharges = createAction(
  '[Loans Effects] Success Get Loan Detail Charges',
  props<{ loanCharges: LoanCharge[] }>()
);
export const failedGetLoanCharges = createAction(
  '[Loans Effects] Failed Get Loan Detail Charges'
);

export const getLoansScores = createAction(
  '[Loan Profile Page] Get loans Scores'
);
export const successLoansScores = createAction(
  '[Loans Effects] Success loans Scores',
  props<{ loansScores: LoansScore[] }>()
);
export const failedLoansScores = createAction(
  '[Loans Effects] Failed loans Scores'
);

export const getPaymentsSummary = createAction(
  '[Loan Profile Page] Get Payments Summary'
);
export const successGetPaymentsSummary = createAction(
  '[Loans Effects] Success Get Payments Summary',
  props<{ payments: Payment[] }>()
);
export const failedGetPaymentsSummary = createAction(
  '[Loans Effects] Failed Get Payments Summary'
);
