import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';

import { UtilsService } from '@app/core/services/utils/utils.service';
import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { FILTERS_LOAN_PROFILE_TABLES } from '@app/plugins/modules/loan-manager/core/constants/filters.constant';
import {
  Loan,
  LoanCharge,
  LoansFilters,
  LoansScore,
  LoanTotals,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoansApi } from '@app/plugins/modules/loan-manager/data/api/loans.api';
import { PaymentsApi } from '@app/plugins/modules/loan-manager/data/api/payments.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as loansActions from './loans.actions';
import * as loansSelectors from './loans.selectors';

@Injectable()
export class LoanEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _loansApi: LoansApi,
    private _paymentsApi: PaymentsApi,
    private _utilsService: UtilsService
  ) {}

  public initLoansPageEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.initLoansPage),
      map(({ filters }) => {
        const loanFilters: LoansFilters = {
          ...FILTERS,
          ...filters,
        };
        return loansActions.updateFiltersLoans({
          filters: loanFilters,
          filtersOptions: { isFiltred: !!filters },
        });
      })
    )
  );

  public getLoansEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.updateFiltersLoans),
      withLatestFrom(this._store.select(loansSelectors.selectLoansFilters)),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._loansApi.getLoans(filters).pipe(
          map((loans: TableResponse<Loan>) =>
            loansActions.successGetLoans({ loans })
          ),
          catchError((_) => of(loansActions.failedGetLoans()))
        );
      })
    )
  );

  public downloadLoansEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.downloadLoans),
      withLatestFrom(this._store.select(loansSelectors.selectLoansFilters)),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._loansApi.downloadLoans(fileFilters).pipe(
          map((fileResponse) =>
            loansActions.successDownloadLoans({ fileResponse })
          ),
          catchError((_) => of(loansActions.failedDownloadLoans()))
        );
      })
    )
  );

  public successDownloadLoansEffect$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(loansActions.successDownloadLoans),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            LOAN_MANAGER_FEATURE.PAGES.LOANS.NAME
          )
        )
      ),
    { dispatch: false }
  );

  public getLoanEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.getLoan),
      withLatestFrom(this._store.select(loansSelectors.selectLoanId)),
      switchMap(([_, loanId]) => {
        return this._loansApi.getLoan(loanId).pipe(
          map((loan: Loan) => loansActions.successGetLoan({ loan })),
          catchError((_) => of(loansActions.failedGetLoan()))
        );
      })
    )
  );

  public getLoanTotalsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.getLoanTotals),
      withLatestFrom(this._store.select(loansSelectors.selectLoanId)),
      switchMap(([_, loanId]) => {
        return this._loansApi.getLoanTotals(loanId).pipe(
          map((loanTotals: LoanTotals) =>
            loansActions.successGetLoanTotals({ loanTotals })
          ),
          catchError((_) => of(loansActions.failedGetLoanTotals()))
        );
      })
    )
  );

  public getLoanChargesEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.getLoanCharges),
      withLatestFrom(this._store.select(loansSelectors.selectLoanId)),
      switchMap(([_, loanId]) => {
        return this._loansApi.getLoanCharges(loanId).pipe(
          map((loanCharges: LoanCharge[]) =>
            loansActions.successGetLoanCharges({ loanCharges })
          ),
          catchError((_) => of(loansActions.failedGetLoanCharges()))
        );
      })
    )
  );

  public getLoansScoresEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.getLoansScores),
      withLatestFrom(this._store.select(loansSelectors.selectLoanId)),
      switchMap(([_, loanId]) => {
        return this._loansApi.getLoansScores(loanId).pipe(
          map((loansScores: LoansScore[]) =>
            loansActions.successLoansScores({ loansScores })
          ),
          catchError((_) => of(loansActions.failedLoansScores()))
        );
      })
    )
  );

  public getPaymentsSummaryEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(loansActions.getPaymentsSummary),
      withLatestFrom(this._store.select(loansSelectors.selectLoanId)),
      switchMap(([_, loan_id]) =>
        this._paymentsApi
          .getPayments({
            ...FILTERS_LOAN_PROFILE_TABLES,
            loan_id,
          })
          .pipe(
            map(({ results }) =>
              loansActions.successGetPaymentsSummary({
                payments: results,
              })
            ),
            catchError((_) => of(loansActions.failedGetPaymentsSummary()))
          )
      )
    )
  );
}
