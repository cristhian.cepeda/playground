import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as loansActions from './loans.actions';
import { LoanState } from './loans.state';

export const initialLoanState: LoanState = {
  filters: FILTERS,
};

const _loanReducer = createReducer(
  initialLoanState,
  on(loansActions.initLoansPage, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(loansActions.updateFiltersLoans, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(loansActions.successGetLoans, (state, { loans }) => ({
    ...state,
    loans,
    isLoading: false,
  })),
  on(loansActions.failedGetLoans, (state) => ({
    ...state,
    loans: null,
    isFiltred: false,
    isLoading: false,
  })),
  on(loansActions.updateLoanId, (state, { loanId }) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      loanId,
    },
  })),
  on(loansActions.getLoan, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingLoan: true,
    },
  })),
  on(loansActions.successGetLoan, (state, { loan }) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      loan,
      isLoadingLoan: false,
    },
  })),
  on(loansActions.failedGetLoan, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingLoan: false,
      loan: null,
    },
  })),
  on(loansActions.destroyLoanProfilePage, (state) => ({
    ...state,
    loanProfile: null,
  })),
  on(loansActions.getLoanTotals, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingLoanTotals: true,
    },
  })),
  on(loansActions.successGetLoanTotals, (state, { loanTotals }) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      loanTotals,
      isLoadingLoanTotals: false,
    },
  })),
  on(loansActions.failedGetLoanTotals, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      loanTotals: null,
      isLoadingLoanTotals: false,
    },
  })),
  on(loansActions.getLoanCharges, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingLoanCharges: true,
    },
  })),
  on(loansActions.successGetLoanCharges, (state, { loanCharges }) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      loanCharges,
      isLoadingLoanCharges: false,
    },
  })),
  on(loansActions.failedGetLoanCharges, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingLoanCharges: false,
      loanCharges: null,
    },
  })),
  on(loansActions.getLoansScores, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingLoansScores: true,
    },
  })),
  on(loansActions.successLoansScores, (state, { loansScores }) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      loansScores,
      isLoadingLoansScores: false,
    },
  })),
  on(loansActions.failedLoansScores, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingLoansScores: false,
      loansScores: null,
    },
  })),
  on(loansActions.getPaymentsSummary, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingPayments: true,
    },
  })),
  on(loansActions.successGetPaymentsSummary, (state, { payments }) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      payments,
      isLoadingPayments: false,
    },
  })),
  on(loansActions.failedGetPaymentsSummary, (state) => ({
    ...state,
    loanProfile: {
      ...state?.loanProfile,
      isLoadingPayments: false,
      payments: null,
    },
  }))
);

export function LoanReducers(state: LoanState | undefined, action: Action) {
  return _loanReducer(state, action);
}
