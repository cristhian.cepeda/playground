import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { LoanState } from './loans.state';

export const getLoanFeatureState = createFeatureSelector<LoanState>(
  LOAN_MANAGER_FEATURE.PAGES.LOANS.STORE_NAME
);

export const selectLoans = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loans
);

export const selectIsLoadingLoans = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.isLoading
);

export const selectIsLoansFiltred = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.isFiltred
);

export const selectLoansFilters = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.filters
);

export const selectLoanId = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.loanId
);

export const selectLoan = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.loan
);

export const selectIsLoadingLoan = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.isLoadingLoan
);

export const selectLoanTotals = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.loanTotals
);

export const selectIsLoadingLoanTotals = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.isLoadingLoanTotals
);

export const selectLoanCharges = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.loanCharges
);

export const selectIsLoadingLoanCharges = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.isLoadingLoanCharges
);

export const selectLoansScores = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.loansScores
);

export const selectIsLoadingLoansScores = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.isLoadingLoansScores
);

export const selectIsLoadingPayments = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.isLoadingPayments
);

export const selectPayments = createSelector(
  getLoanFeatureState,
  (state: LoanState) => state?.loanProfile?.payments
);
