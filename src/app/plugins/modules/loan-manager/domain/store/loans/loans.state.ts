import { TablePageActions } from '@app/core/models/page.model';
import {
  Loan,
  LoanCharge,
  LoansFilters,
  LoansScore,
  LoanTotals,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { Payment } from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface LoanState extends TablePageActions {
  loans?: TableResponse<Loan>;
  filters: LoansFilters;
  loanProfile?: LoanProfileState;
}

export interface LoanProfileState {
  loanId: string;
  loan: Loan;
  isLoadingLoan: boolean;
  loanTotals: LoanTotals;
  isLoadingLoanTotals: boolean;
  loanCharges: LoanCharge[];
  isLoadingLoanCharges: boolean;
  loansScores: LoansScore[];
  isLoadingLoansScores: boolean;
  payments?: Payment[];
  isLoadingPayments?: boolean;
}
