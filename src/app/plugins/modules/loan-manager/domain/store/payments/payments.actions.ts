import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  Payment,
  PaymentFilters,
  PaymentProfile,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initPaymentsPage = createAction(
  '[Payments Page] init payments page',
  props<{ filters: PaymentFilters }>()
);

export const successInitPaymentsPage = createAction(
  '[Payments Effects] success init payments page',
  props<{ payments: TableResponse<Payment> }>()
);

export const failedInitPaymentsPage = createAction(
  '[Payments Effects] failed init payments page'
);

export const updatePaymentsFilters = createAction(
  '[Payments Effects] update filters payments page',
  props<{ filters: PaymentFilters; filtersOptions?: FiltersOptions }>()
);

export const getPaymentProfile = createAction(
  '[Payments Effects] Get payment profile ',
  props<{ payment: Payment }>()
);

export const successPaymentProfile = createAction(
  '[Payments Effects] Success payment profile ',
  props<{ paymentProfile: PaymentProfile }>()
);

export const failedGetPaymentProfile = createAction(
  '[Payments Effects] failed payment profile',
  props<{ id: string }>()
);

export const downloadPayments = createAction(
  '[Payments Page] Downloand Payments'
);

export const successDownloadPayments = createAction(
  '[Payments Effects] success Downloand Payments',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadPayments = createAction(
  '[Payments Effects] failed Downloand Payments'
);
