import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { PaymentFilters } from '@app/plugins/modules/customers/core/models/payment.model';
import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import {
  Payment,
  PaymentProfile,
  PaymentSummary,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { PaymentsApi } from '@app/plugins/modules/loan-manager/data/api/payments.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import { PaymentsService } from '../../services/payments.service';
import * as paymentsActions from './payments.actions';
import * as paymentsSelectors from './payments.selectors';

@Injectable()
export class PaymentsEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _paymentsApi: PaymentsApi,
    private _paymentsService: PaymentsService,
    private _utilsService: UtilsService
  ) {}

  public initPaymentsPageEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(paymentsActions.initPaymentsPage),
      map(({ filters }) => {
        const paymentFilters: PaymentFilters = {
          ...FILTERS,
          ...filters,
        };
        return paymentsActions.updatePaymentsFilters({
          filters: paymentFilters,
          filtersOptions: { isFiltred: !!filters },
        });
      })
    )
  );

  public getPaymentsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(paymentsActions.updatePaymentsFilters),
      withLatestFrom(
        this._store.select(paymentsSelectors.selectPaymentsFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._paymentsApi.getPayments(filters).pipe(
          map((payments: TableResponse<Payment>) =>
            paymentsActions.successInitPaymentsPage({ payments })
          ),
          catchError((_) => of(paymentsActions.failedInitPaymentsPage()))
        );
      })
    )
  );

  public getPaymentProfileEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(paymentsActions.getPaymentProfile),
      switchMap(({ payment }) => {
        return this._paymentsApi.getPaymentSummary(payment?.id).pipe(
          map((paymentSummary: PaymentSummary[]) => {
            const paymentProfile: PaymentProfile =
              this._paymentsService.setPaymentProfile(payment, paymentSummary);
            return paymentsActions.successPaymentProfile({
              paymentProfile,
            });
          }),
          catchError((_) =>
            of(paymentsActions.failedGetPaymentProfile({ id: payment?.id }))
          )
        );
      })
    )
  );

  public downloadPaymentsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(paymentsActions.downloadPayments),
      withLatestFrom(
        this._store.select(paymentsSelectors.selectPaymentsFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._paymentsApi.downloadPayments(fileFilters).pipe(
          map((fileResponse) =>
            paymentsActions.successDownloadPayments({ fileResponse })
          ),
          catchError((_) => of(paymentsActions.failedDownloadPayments()))
        );
      })
    )
  );

  public successDownloadPaymentsEffect$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(paymentsActions.successDownloadPayments),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            LOAN_MANAGER_FEATURE.PAGES.PAYMENTS.NAME
          )
        )
      ),
    { dispatch: false }
  );
}
