import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as paymentsActions from './payments.actions';
import { PaymentState } from './payments.state';
export const initialPaymentState: PaymentState = {
  filters: FILTERS,
};

const _paymentsReducer = createReducer(
  initialPaymentState,
  on(paymentsActions.initPaymentsPage, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(paymentsActions.successInitPaymentsPage, (state, { payments }) => ({
    ...state,
    payments,
    isLoading: false,
  })),
  on(paymentsActions.failedInitPaymentsPage, (state) => ({
    ...state,
    isLoading: false,
    isFiltred: false,
    payments: null,
  })),
  on(
    paymentsActions.updatePaymentsFilters,
    (state, { filters, filtersOptions }) => {
      const FiltersValidationsOptions: FiltersValidationsOptions =
        setFilterValidation(state, filters, filtersOptions);
      return {
        ...state,
        isLoading: true,
        isFiltred: FiltersValidationsOptions?.isFiltred,
        filters: FiltersValidationsOptions?.filters,
      };
    }
  ),
  on(paymentsActions.getPaymentProfile, (state, { payment }) => ({
    ...state,
    isLoadingPaymentProfiles: {
      ...state?.isLoadingPaymentProfiles,
      [payment?.id]: true,
    },
  })),
  on(paymentsActions.successPaymentProfile, (state, { paymentProfile }) => ({
    ...state,
    paymentProfiles: {
      ...state?.paymentProfiles,
      [paymentProfile?.id]: { ...paymentProfile },
    },
    isLoadingPaymentProfiles: {
      ...state?.isLoadingPaymentProfiles,
      [paymentProfile?.id]: false,
    },
  })),
  on(paymentsActions.failedGetPaymentProfile, (state, { id }) => ({
    ...state,
    isLoadingPaymentProfiles: {
      ...state?.isLoadingPaymentProfiles,
      [id]: false,
    },
  }))
);

export function PaymentsReducers(
  state: PaymentState | undefined,
  action: Action
) {
  return _paymentsReducer(state, action);
}
