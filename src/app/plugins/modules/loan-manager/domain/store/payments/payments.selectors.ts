import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PaymentState } from './payments.state';

export const getPaymentFeatureState = createFeatureSelector<PaymentState>(
  LOAN_MANAGER_FEATURE.PAGES.PAYMENTS.STORE_NAME
);

export const selectPayments = createSelector(
  getPaymentFeatureState,
  (state: PaymentState) => state?.payments
);

export const selectIsLoadingPayments = createSelector(
  getPaymentFeatureState,
  (state: PaymentState) => state?.isLoading
);

export const selectPaymentsFilters = createSelector(
  getPaymentFeatureState,
  (state: PaymentState) => state?.filters
);

export const selectIsPaymentsFiltred = createSelector(
  getPaymentFeatureState,
  (state: PaymentState) => state?.isFiltred
);

export const selectPaymentProfiles = createSelector(
  getPaymentFeatureState,
  (state: PaymentState) => state?.paymentProfiles
);

export const selectIsLoadingPaymentProfiles = createSelector(
  getPaymentFeatureState,
  (state: PaymentState) => state?.isLoadingPaymentProfiles
);
