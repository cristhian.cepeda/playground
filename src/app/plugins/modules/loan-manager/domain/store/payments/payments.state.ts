import { TablePageActions } from '@app/core/models/page.model';
import {
  Payment,
  PaymentFilters,
  PaymentProfile,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface PaymentState extends TablePageActions {
  payments?: TableResponse<Payment>;
  filters: PaymentFilters;
  paymentProfiles?: PaymentProfile[];
  isLoadingPaymentProfiles?: { [key: string]: boolean };
}
