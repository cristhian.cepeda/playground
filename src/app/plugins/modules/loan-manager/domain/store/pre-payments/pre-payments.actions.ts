import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  PrePayment,
  PrePaymentFilters,
} from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initPrePaymentsPage = createAction(
  '[Pre-Payments Page] init Pre-payments page',
  props<{ filters: PrePaymentFilters }>()
);

export const successInitPrePaymentsPage = createAction(
  '[Pre-Payments Effects] success init Pre-payments page',
  props<{ prePayments: TableResponse<PrePayment> }>()
);

export const failedInitPrePaymentsPage = createAction(
  '[Pre-Payments Effects] failed init Pre-payments page'
);

export const updatePrePaymentsFilters = createAction(
  '[Pre-Payments Effects] update filters Pre-payments page',
  props<{ filters: PrePaymentFilters; filtersOptions?: FiltersOptions }>()
);

export const downloadPrePayments = createAction(
  '[Pre-Payments Page] Downloand Pre-payments'
);

export const successDownloadPrePayments = createAction(
  '[Pre-Payments Effects] success Downloand Pre-payments',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadPrePayments = createAction(
  '[Pre-Payments Effects] failed Downloand Pre-payments'
);
