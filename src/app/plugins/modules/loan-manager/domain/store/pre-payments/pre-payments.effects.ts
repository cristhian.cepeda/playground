import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import {
  PrePayment,
  PrePaymentFilters,
} from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { PrePaymentsApi } from '@app/plugins/modules/loan-manager/data/api/pre-payments.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';

import * as prePaymentsActions from './pre-payments.actions';
import * as prePaymentsSelectors from './pre-payments.selectors';

@Injectable()
export class PrePaymentsEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _prePaymentsApi: PrePaymentsApi,
    private _utilsService: UtilsService
  ) {}

  public initPrePaymentsPageEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(prePaymentsActions.initPrePaymentsPage),
      map(({ filters }) => {
        const prePaymentFilters: PrePaymentFilters = {
          ...FILTERS,
          ...filters,
        };
        return prePaymentsActions.updatePrePaymentsFilters({
          filters: prePaymentFilters,
          filtersOptions: { isFiltred: !!filters },
        });
      })
    )
  );

  public getPrePaymentsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(prePaymentsActions.updatePrePaymentsFilters),
      withLatestFrom(
        this._store.select(prePaymentsSelectors.selectPrePaymentsFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._prePaymentsApi.getPrePayments(filters).pipe(
          map((prePayments: TableResponse<PrePayment>) =>
            prePaymentsActions.successInitPrePaymentsPage({ prePayments })
          ),
          catchError((_) => of(prePaymentsActions.failedInitPrePaymentsPage()))
        );
      })
    )
  );

  public downloadPrePaymentsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(prePaymentsActions.downloadPrePayments),
      withLatestFrom(
        this._store.select(prePaymentsSelectors.selectPrePaymentsFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._prePaymentsApi.downloadPrePayments(fileFilters).pipe(
          map((fileResponse) =>
            prePaymentsActions.successDownloadPrePayments({ fileResponse })
          ),
          catchError((_) => of(prePaymentsActions.failedDownloadPrePayments()))
        );
      })
    )
  );

  public successDownloadPrePaymentsEffect$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(prePaymentsActions.successDownloadPrePayments),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            LOAN_MANAGER_FEATURE.PAGES.PRE_PAYMENTS.NAME
          )
        )
      ),
    { dispatch: false }
  );
}
