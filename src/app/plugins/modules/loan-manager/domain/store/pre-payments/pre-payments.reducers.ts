import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as prePaymentsActions from './pre-payments.actions';
import { PrePaymentState } from './pre-payments.state';
export const initialPrePaymentState: PrePaymentState = {
  filters: FILTERS,
};

const _prePaymentsReducer = createReducer(
  initialPrePaymentState,
  on(prePaymentsActions.initPrePaymentsPage, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(
    prePaymentsActions.successInitPrePaymentsPage,
    (state, { prePayments }) => ({
      ...state,
      prePayments,
      isLoading: false,
    })
  ),
  on(prePaymentsActions.failedInitPrePaymentsPage, (state) => ({
    ...state,
    isLoading: false,
    isFiltred: false,
    prePayments: null,
  })),
  on(
    prePaymentsActions.updatePrePaymentsFilters,
    (state, { filters, filtersOptions }) => {
      const FiltersValidationsOptions: FiltersValidationsOptions =
        setFilterValidation(state, filters, filtersOptions);
      return {
        ...state,
        isLoading: true,
        isFiltred: FiltersValidationsOptions?.isFiltred,
        filters: FiltersValidationsOptions?.filters,
      };
    }
  )
);

export function PrePaymentsReducers(
  state: PrePaymentState | undefined,
  action: Action
) {
  return _prePaymentsReducer(state, action);
}
