import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PrePaymentState } from './pre-payments.state';

export const getPrePaymentFeatureState = createFeatureSelector<PrePaymentState>(
  LOAN_MANAGER_FEATURE.PAGES.PRE_PAYMENTS.STORE_NAME
);

export const selectPrePayments = createSelector(
  getPrePaymentFeatureState,
  (state: PrePaymentState) => state?.prePayments
);

export const selectIsLoadingPrePayments = createSelector(
  getPrePaymentFeatureState,
  (state: PrePaymentState) => state?.isLoading
);

export const selectPrePaymentsFilters = createSelector(
  getPrePaymentFeatureState,
  (state: PrePaymentState) => state?.filters
);

export const selectIsPrePaymentsFiltred = createSelector(
  getPrePaymentFeatureState,
  (state: PrePaymentState) => state?.isFiltred
);
