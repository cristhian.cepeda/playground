import { TablePageActions } from '@app/core/models/page.model';
import {
  PrePayment,
  PrePaymentFilters,
} from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface PrePaymentState extends TablePageActions {
  prePayments?: TableResponse<PrePayment>;
  filters: PrePaymentFilters;
}
