import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';

import {
  Transaction,
  TransactionFilters,
} from '@app/plugins/modules/loan-manager/core/models/transaction.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initTransactionsPage = createAction(
  '[Transactions Page] Init Transactions Page',
  props<{ filters: TransactionFilters }>()
);

export const successInitTransactionsPage = createAction(
  '[Transactions Effects] Success Init Transactions Page',
  props<{ transactions: TableResponse<Transaction> }>()
);

export const failedInitTransactionsPage = createAction(
  '[Transactions Effects] Failed Init Transactions Page'
);

export const updateTransactionsFilters = createAction(
  '[Transactions Effects] Update filters Transactions Page',
  props<{ filters: TransactionFilters; filtersOptions?: FiltersOptions }>()
);

export const downloadTransactions = createAction(
  '[Transactions Page] Downloand Transactions'
);

export const successDownloadTransactions = createAction(
  '[Transactions Effects] Success Downloand Transactions',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadTransactions = createAction(
  '[Transactions Effects] Failed Downloand Transactions'
);
