import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';

import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import {
  Transaction,
  TransactionFilters,
} from '@app/plugins/modules/loan-manager/core/models/transaction.model';
import { TransactionsApi } from '@app/plugins/modules/loan-manager/data/api/transactions.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as transactionsActions from './transactions.actions';
import * as transactionsSelectors from './transactions.selectors';

@Injectable()
export class TransactionsEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _transactionsApi: TransactionsApi,
    private _utilsService: UtilsService
  ) {}

  public initTransactionsPageEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(transactionsActions.initTransactionsPage),
      map(({ filters }) => {
        const transactionFilters: TransactionFilters = {
          ...FILTERS,
          ...filters,
        };
        return transactionsActions.updateTransactionsFilters({
          filters: transactionFilters,
          filtersOptions: { isFiltred: !!filters },
        });
      })
    )
  );

  public getTransactionsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(transactionsActions.updateTransactionsFilters),
      withLatestFrom(
        this._store.select(transactionsSelectors.selectTransactionsFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._transactionsApi.getTransactions(filters).pipe(
          map((transactions: TableResponse<Transaction>) =>
            transactionsActions.successInitTransactionsPage({ transactions })
          ),
          catchError((_) =>
            of(transactionsActions.failedInitTransactionsPage())
          )
        );
      })
    )
  );

  public downloadTransactionsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(transactionsActions.downloadTransactions),
      withLatestFrom(
        this._store.select(transactionsSelectors.selectTransactionsFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._transactionsApi.downloadTransactions(fileFilters).pipe(
          map((fileResponse) =>
            transactionsActions.successDownloadTransactions({ fileResponse })
          ),
          catchError((_) =>
            of(transactionsActions.failedDownloadTransactions())
          )
        );
      })
    )
  );

  public successDownloadTransactionsEffect$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(transactionsActions.successDownloadTransactions),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            LOAN_MANAGER_FEATURE.PAGES.TRANSACTIONS.NAME
          )
        )
      ),
    { dispatch: false }
  );
}
