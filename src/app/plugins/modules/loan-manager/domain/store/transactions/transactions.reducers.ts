import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as transactionsActions from './transactions.actions';
import { TransactionsState } from './transactions.state';
export const initialTransactionsState: TransactionsState = {
  filters: FILTERS,
};

const _transactionsReducer = createReducer(
  initialTransactionsState,
  on(transactionsActions.initTransactionsPage, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(
    transactionsActions.successInitTransactionsPage,
    (state, { transactions }) => ({
      ...state,
      transactions,
      isLoading: false,
    })
  ),
  on(transactionsActions.failedInitTransactionsPage, (state) => ({
    ...state,
    isLoading: false,
    isFiltred: false,
    transactions: null,
  })),
  on(
    transactionsActions.updateTransactionsFilters,
    (state, { filters, filtersOptions }) => {
      const FiltersValidationsOptions: FiltersValidationsOptions =
        setFilterValidation(state, filters, filtersOptions);
      return {
        ...state,
        isLoading: true,
        isFiltred: FiltersValidationsOptions?.isFiltred,
        filters: FiltersValidationsOptions?.filters,
      };
    }
  )
);

export function TransactionsReducers(
  state: TransactionsState | undefined,
  action: Action
) {
  return _transactionsReducer(state, action);
}
