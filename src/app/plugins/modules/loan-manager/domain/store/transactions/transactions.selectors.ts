import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TransactionsState } from './transactions.state';

export const getTransactionFeatureState =
  createFeatureSelector<TransactionsState>(
    LOAN_MANAGER_FEATURE.PAGES.TRANSACTIONS.STORE_NAME
  );

export const selectTransactions = createSelector(
  getTransactionFeatureState,
  (state: TransactionsState) => state?.transactions
);

export const selectIsLoadingTransactions = createSelector(
  getTransactionFeatureState,
  (state: TransactionsState) => state?.isLoading
);

export const selectTransactionsFilters = createSelector(
  getTransactionFeatureState,
  (state: TransactionsState) => state?.filters
);

export const selectIsTransactionsFiltred = createSelector(
  getTransactionFeatureState,
  (state: TransactionsState) => state?.isFiltred
);
