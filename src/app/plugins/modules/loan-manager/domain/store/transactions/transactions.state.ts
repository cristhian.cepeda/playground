import { TablePageActions } from '@app/core/models/page.model';
import {
  Transaction,
  TransactionFilters,
} from '@app/plugins/modules/loan-manager/core/models/transaction.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface TransactionsState extends TablePageActions {
  transactions?: TableResponse<Transaction>;
  filters: TransactionFilters;
}
