import { TestBed } from '@angular/core/testing';
import { FiltersOptions } from '@app/core/models/filters.model';
import * as loansRequestActions from '@app/plugins/modules/loan-manager/domain/store/loan-request/loan-request.actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { LoansRequestFilters } from '../core/models/loan.model';
import { LoanRequestFacade } from './loan-request.facade';

describe('LoanRequestFacade', () => {
  let facade: LoanRequestFacade;
  let store: MockStore;
  const initialState: any = {};
  let spy: jasmine.Spy<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })],
    });
    facade = TestBed.inject(LoanRequestFacade);
    store = TestBed.inject(MockStore);
    spy = spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be call initLoansRequestPage action', () => {
    facade.initLoansRequestPage();
    expect(spy).toHaveBeenCalledWith(
      loansRequestActions.initLoansRequestPage()
    );
  });

  it('should be call updateLoansRequestFilters action', () => {
    const filters: LoansRequestFilters = {};
    const filtersOptions: FiltersOptions = { isFiltred: false };
    facade.updateLoansRequestFilters(filters, filtersOptions);
    expect(spy).toHaveBeenCalledWith(
      loansRequestActions.updateLoansRequestFilters({ filters, filtersOptions })
    );
  });
});
