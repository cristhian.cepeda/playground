import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  LoanRequest,
  LoansRequestFilters,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import * as loansRequestActions from '@app/plugins/modules/loan-manager/domain/store/loan-request/loan-request.actions';
import * as loansRequestSelectors from '@app/plugins/modules/loan-manager/domain/store/loan-request/loan-request.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoanRequestFacade {
  public projectFamily$: Observable<string>;

  public loansRequest$: Observable<TableResponse<LoanRequest>>;
  public isLoadingLoansRequest$: Observable<boolean>;
  public isLoansRequestFiltred$: Observable<boolean>;

  constructor(private _store: Store, private _projectFacade: ProjectFacade) {
    this._setSelectors();
  }

  public initLoansRequestPage() {
    this._store.dispatch(loansRequestActions.initLoansRequestPage());
  }

  public updateLoansRequestFilters(
    filters: LoansRequestFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      loansRequestActions.updateLoansRequestFilters({ filters, filtersOptions })
    );
  }

  public downloadLoansRequest() {
    this._store.dispatch(loansRequestActions.downloadLoansRequest());
  }

  private _setSelectors() {
    this.loansRequest$ = this._store.select(
      loansRequestSelectors.selectLoansRequest
    );
    this.isLoadingLoansRequest$ = this._store.select(
      loansRequestSelectors.selectIsLoadingLoansRequest
    );
    this.isLoansRequestFiltred$ = this._store.select(
      loansRequestSelectors.selectIsLoansRequestFiltred
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
