import { TestBed } from '@angular/core/testing';
import { FiltersOptions } from '@app/core/models/filters.model';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import * as loansActions from '@app/plugins/modules/loan-manager/domain/store/loans/loans.actions';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { LoansFilters } from '../core/models/loan.model';
import { LOAN, LOAN_CHARGES } from '../data/mocks/loans.mock';
import { LoanProfileService } from '../domain/services/loan-profile.service';
import { LoanFacade } from './loan.facade';

describe('LoanFacade', () => {
  let facade: LoanFacade;
  let store: MockStore;
  const initialState: any = {};
  let spy: jasmine.Spy<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        SnakeToCapitalPipe,
        CustomCurrencyPipe,
        provideMockStore({ initialState }),
      ],
    });
    facade = TestBed.inject(LoanFacade);
    store = TestBed.inject(MockStore);
    spy = spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be call initLoansPage action', () => {
    const loansFilters: LoansFilters = { customer_id: 'customer_id' };
    facade.initLoansPage(loansFilters);
    expect(spy).toHaveBeenCalledWith(
      loansActions.initLoansPage({ filters: loansFilters })
    );
  });

  it('should be call updateFiltersLoans action', () => {
    const filters: LoansFilters = {};
    const filtersOptions: FiltersOptions = { isFiltred: false };
    facade.updateFiltersLoans(filters, filtersOptions);
    expect(spy).toHaveBeenCalledWith(
      loansActions.updateFiltersLoans({ filters, filtersOptions })
    );
  });

  it('should be call updateLoanId action', () => {
    const loanId: string = 'loanId';
    facade.updateLoanId(loanId);
    expect(spy).toHaveBeenCalledWith(loansActions.updateLoanId({ loanId }));
  });

  it('should be call getLoan action', () => {
    facade.getLoan();
    expect(spy).toHaveBeenCalledWith(loansActions.getLoan());
  });

  it('should be call getLoanTotals action', () => {
    facade.getLoanTotals();
    expect(spy).toHaveBeenCalledWith(loansActions.getLoanTotals());
  });

  it('should be call getLoanCharges action', () => {
    facade.getLoanCharges();
    expect(spy).toHaveBeenCalledWith(loansActions.getLoanCharges());
  });

  it('should be call getLoansScores action', () => {
    facade.getLoansScores();
    expect(spy).toHaveBeenCalledWith(loansActions.getLoansScores());
  });

  it('should be charges items has equal to mock charges items', (doneFn) => {
    const _loanProfileService = TestBed.inject(LoanProfileService);
    store.setState({ loans: { loanProfile: { loanCharges: LOAN_CHARGES } } });
    const mockSummaryGroup: SummaryGroup[] = [] as SummaryGroup[];
    const spy = spyOn(
      _loanProfileService,
      'getChargesItemsByLoanCharges'
    ).and.returnValue(mockSummaryGroup);
    facade.chargesItems$.subscribe((charges: SummaryGroup[]) => {
      expect(charges).toEqual(mockSummaryGroup);
      doneFn();
    });
  });

  it('should be loan items has equal to mock loan items', (doneFn) => {
    const _loanProfileService = TestBed.inject(LoanProfileService);
    store.setState({ loans: { loanProfile: { loan: LOAN } } });
    const mockSummaryGroup: SummaryGroup[] = [] as SummaryGroup[];
    const spy = spyOn(_loanProfileService, 'getLoanItems').and.returnValue(
      of(mockSummaryGroup)
    );
    facade.loanItems$.subscribe((loan: SummaryGroup[]) => {
      expect(loan).toEqual(mockSummaryGroup);
      doneFn();
    });
  });
});
