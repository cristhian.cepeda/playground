import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  Loan,
  LoanCharge,
  LoansFilters,
  LoansScore,
  LoanTotals,
  LoanTotalsCard,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoanProfileService } from '@app/plugins/modules/loan-manager/domain/services/loan-profile.service';
import * as loansActions from '@app/plugins/modules/loan-manager/domain/store/loans/loans.actions';
import * as loansSelectors from '@app/plugins/modules/loan-manager/domain/store/loans/loans.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { filter, map, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Payment } from '../core/models/payment.model';

@Injectable({ providedIn: 'root' })
export class LoanFacade {
  public projectFamily$: Observable<string>;

  public loans$: Observable<TableResponse<Loan>>;
  public isLoansFiltred$: Observable<boolean>;
  public isLoadingLoans$: Observable<boolean>;

  public loanProfile$: Observable<Loan>;
  public loanId$: Observable<string>;
  public loanItems$: Observable<SummaryGroup[]>;
  public isLoadingLoan$: Observable<boolean>;

  public loanTotals$: Observable<LoanTotals>;
  public loanTotalsCard$: Observable<LoanTotalsCard>;
  public isLoadingLoanTotals$: Observable<boolean>;

  public chargesItems$: Observable<SummaryGroup[]>;
  public isLoadingLoanCharges$: Observable<boolean>;

  public loansScores$: Observable<LoansScore[]>;
  public isLoadingLoansScores$: Observable<boolean>;

  public payments$: Observable<Payment[]>;
  public isLoadingPayments$: Observable<boolean>;

  constructor(
    private _store: Store,
    private _loanProfileService: LoanProfileService,
    private _projectFacade: ProjectFacade
  ) {
    this._setSelectors();
  }

  public initLoansPage(filters?: LoansFilters) {
    this._store.dispatch(loansActions.initLoansPage({ filters }));
  }

  public updateFiltersLoans(
    filters: LoansFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      loansActions.updateFiltersLoans({ filters, filtersOptions })
    );
  }

  public downloadLoans() {
    this._store.dispatch(loansActions.downloadLoans());
  }

  public updateLoanId(loanId: string) {
    this._store.dispatch(loansActions.updateLoanId({ loanId }));
  }

  public getLoan() {
    this._store.dispatch(loansActions.getLoan());
  }

  public getLoanTotals() {
    this._store.dispatch(loansActions.getLoanTotals());
  }

  public getLoanCharges() {
    this._store.dispatch(loansActions.getLoanCharges());
  }

  public getLoansScores() {
    this._store.dispatch(loansActions.getLoansScores());
  }

  public getPaymentsSummary() {
    this._store.dispatch(loansActions.getPaymentsSummary());
  }

  public destroyLoanProfilePage() {
    this._store.dispatch(loansActions.destroyLoanProfilePage());
  }

  private _setSelectors() {
    this.loans$ = this._store.select(loansSelectors.selectLoans);
    this.isLoadingLoans$ = this._store.select(
      loansSelectors.selectIsLoadingLoans
    );
    this.isLoansFiltred$ = this._store.select(
      loansSelectors.selectIsLoansFiltred
    );

    this.loanProfile$ = this._store.select(loansSelectors.selectLoan);
    this.loanItems$ = this._store.select(loansSelectors.selectLoan).pipe(
      filter((loan: Loan) => !!loan),
      mergeMap((loan: Loan) => this._loanProfileService.getLoanItems(loan))
    );
    this.isLoadingLoan$ = this._store.select(
      loansSelectors.selectIsLoadingLoan
    );

    this.loanTotals$ = this._store.select(loansSelectors.selectLoanTotals);
    this.isLoadingLoanTotals$ = this._store.select(
      loansSelectors.selectIsLoadingLoanTotals
    );
    this.loanTotalsCard$ = this.loanTotals$.pipe(
      filter((loanTotals: LoanTotals) => !!loanTotals),
      map((loanTotals: LoanTotals) =>
        this._loanProfileService.getLoanTotalsCards(loanTotals)
      )
    );

    this.chargesItems$ = this._store
      .select(loansSelectors.selectLoanCharges)
      .pipe(
        filter((charges: LoanCharge[]) => !!charges && charges?.length > 0),
        map((charges: LoanCharge[]) =>
          this._loanProfileService.getChargesItemsByLoanCharges(charges)
        )
      );

    this.isLoadingLoanCharges$ = this._store.select(
      loansSelectors.selectIsLoadingLoanCharges
    );

    this.loansScores$ = this._store.select(loansSelectors.selectLoansScores);
    this.isLoadingLoansScores$ = this._store.select(
      loansSelectors.selectIsLoadingLoansScores
    );
    this.loanId$ = this.loanProfile$.pipe(
      filter((loan) => !!loan),
      map((loanProfile) => loanProfile?.id)
    );
    this.payments$ = this._store.select(loansSelectors.selectPayments);
    this.isLoadingPayments$ = this._store.select(
      loansSelectors.selectIsLoadingPayments
    );

    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
