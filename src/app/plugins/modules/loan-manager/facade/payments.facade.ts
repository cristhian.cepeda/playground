import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  Payment,
  PaymentProfile,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import * as paymentsActions from '@app/plugins/modules/loan-manager/domain/store/payments/payments.actions';
import * as paymentsSelectors from '@app/plugins/modules/loan-manager/domain/store/payments/payments.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PaymentFilters } from '../core/models/payment.model';
@Injectable({
  providedIn: 'root',
})
export class PaymentsFacade {
  public projectFamily$: Observable<string>;

  public paymentsTable$: Observable<TableResponse<Payment>>;
  public isLoadingPayments$: Observable<boolean>;
  public isPaymentsFiltred$: Observable<boolean>;

  public paymentProfiles$: Observable<PaymentProfile[]>;
  public isLoadingPaymentProfiles$: Observable<{ [key: string]: boolean }>;

  constructor(private _store: Store, private _projectFacade: ProjectFacade) {
    this._setSelectors();
  }

  public initPaymentsPage(filters?: PaymentFilters): void {
    this._store.dispatch(paymentsActions.initPaymentsPage({ filters }));
  }

  public updatePaymentsFilters(
    filters: PaymentFilters,
    filtersOptions?: FiltersOptions
  ): void {
    this._store.dispatch(
      paymentsActions.updatePaymentsFilters({ filters, filtersOptions })
    );
  }

  public getPaymentProfile(payment: Payment) {
    this._store.dispatch(paymentsActions.getPaymentProfile({ payment }));
  }

  public downloadPayments() {
    this._store.dispatch(paymentsActions.downloadPayments());
  }

  private _setSelectors() {
    this.paymentsTable$ = this._store.select(paymentsSelectors.selectPayments);
    this.isLoadingPayments$ = this._store.select(
      paymentsSelectors.selectIsLoadingPayments
    );
    this.isPaymentsFiltred$ = this._store.select(
      paymentsSelectors.selectIsPaymentsFiltred
    );
    this.paymentProfiles$ = this._store.select(
      paymentsSelectors.selectPaymentProfiles
    );
    this.isLoadingPaymentProfiles$ = this._store.select(
      paymentsSelectors.selectIsLoadingPaymentProfiles
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
