import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { PrePaymentsFacade } from './pre-payments.facade';

describe('PrePaymentsFacade', () => {
  let service: PrePaymentsFacade;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })],
    });
    service = TestBed.inject(PrePaymentsFacade);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
