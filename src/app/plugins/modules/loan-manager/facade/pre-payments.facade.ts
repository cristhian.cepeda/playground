import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import * as prePaymentsActions from '@app/plugins/modules/loan-manager/domain/store/pre-payments/pre-payments.actions';
import * as prePaymentsSelectors from '@app/plugins/modules/loan-manager/domain/store/pre-payments/pre-payments.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  PrePayment,
  PrePaymentFilters,
} from '../core/models/pre-payment.model';

@Injectable({
  providedIn: 'root',
})
export class PrePaymentsFacade {
  public projectFamily$: Observable<string>;

  public prePaymentsTable$: Observable<TableResponse<PrePayment>>;
  public isLoadingPrePayments$: Observable<boolean>;
  public isPrePaymentsFiltred$: Observable<boolean>;

  constructor(private _store: Store, private _projectFacade: ProjectFacade) {
    this._setSelectors();
  }

  public initPrePaymentsPage(filters?: PrePaymentFilters): void {
    this._store.dispatch(prePaymentsActions.initPrePaymentsPage({ filters }));
  }

  public updatePrePaymentsFilters(
    filters: PrePaymentFilters,
    filtersOptions?: FiltersOptions
  ): void {
    this._store.dispatch(
      prePaymentsActions.updatePrePaymentsFilters({ filters, filtersOptions })
    );
  }

  public downloadPrePayments() {
    this._store.dispatch(prePaymentsActions.downloadPrePayments());
  }

  private _setSelectors() {
    this.prePaymentsTable$ = this._store.select(
      prePaymentsSelectors.selectPrePayments
    );
    this.isLoadingPrePayments$ = this._store.select(
      prePaymentsSelectors.selectIsLoadingPrePayments
    );
    this.isPrePaymentsFiltred$ = this._store.select(
      prePaymentsSelectors.selectIsPrePaymentsFiltred
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
