import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { TransactionsFacade } from './transactions.facade';

describe('TransactionsFacade', () => {
  let service: TransactionsFacade;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })],
    });
    service = TestBed.inject(TransactionsFacade);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
