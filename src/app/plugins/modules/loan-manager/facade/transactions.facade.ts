import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import * as transactionsActions from '@app/plugins/modules/loan-manager/domain/store/transactions/transactions.actions';
import * as transactionsSelectors from '@app/plugins/modules/loan-manager/domain/store/transactions/transactions.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import {
  Transaction,
  TransactionFilters,
} from '../core/models/transaction.model';
@Injectable({
  providedIn: 'root',
})
export class TransactionsFacade {
  public projectFamily$: Observable<string>;

  public transactionsTable$: Observable<TableResponse<Transaction>>;
  public isLoadingTransactions$: Observable<boolean>;
  public isTransactionsFiltred$: Observable<boolean>;

  constructor(private _store: Store, private _projectFacade: ProjectFacade) {
    this._setSelectors();
  }

  public initTransactionsPage(filters?: TransactionFilters): void {
    this._store.dispatch(transactionsActions.initTransactionsPage({ filters }));
  }

  public updateTransactionsFilters(
    filters: TransactionFilters,
    filtersOptions?: FiltersOptions
  ): void {
    this._store.dispatch(
      transactionsActions.updateTransactionsFilters({ filters, filtersOptions })
    );
  }

  public downloadTransactions() {
    this._store.dispatch(transactionsActions.downloadTransactions());
  }

  private _setSelectors() {
    this.transactionsTable$ = this._store.select(
      transactionsSelectors.selectTransactions
    );
    this.isLoadingTransactions$ = this._store.select(
      transactionsSelectors.selectIsLoadingTransactions
    );
    this.isTransactionsFiltred$ = this._store.select(
      transactionsSelectors.selectIsTransactionsFiltred
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
