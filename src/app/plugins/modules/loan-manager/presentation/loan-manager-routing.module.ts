import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { BaseContentComponent } from '@app/presentation/layout/components/base-content/base-content.component';
import { MENU_CONFIG_DISABLED } from '@instance-config/menu.config';

const CHILDREN_ROUTES: Routes = [
  {
    path: LOAN_MANAGER_FEATURE.PAGES.OVERVIEW.PATH,
    loadChildren: () =>
      import('./pages/overview/overview.module').then(
        (m) => m.OverviewPageModule
      ),
    data: {
      breadcrumb: 'LOAN_MANAGER.OVERVIEW.TITLE',
      disabled: MENU_CONFIG_DISABLED.LOAN_MANAGER.OVERVIEW ?? false,
    },
  },
  {
    path: LOAN_MANAGER_FEATURE.PAGES.LOAN_REQUEST.PATH,
    loadChildren: () =>
      import('./pages/loan-request/loan-request.module').then(
        (m) => m.LoanRequestPageModule
      ),
    data: {
      breadcrumb: 'LOAN_MANAGER.LOAN_REQUEST.TITLE',
      disabled: MENU_CONFIG_DISABLED.LOAN_MANAGER.LOANS_REQUEST ?? false,
    },
  },
  {
    path: LOAN_MANAGER_FEATURE.PAGES.LOANS.PATH,
    loadChildren: () =>
      import('./pages/loans/loans.module').then((m) => m.LoansModule),
    data: {
      breadcrumb: 'LOAN_MANAGER.LOANS.TITLE',
      disabled: MENU_CONFIG_DISABLED.LOAN_MANAGER.LOANS ?? false,
    },
  },
  {
    path: LOAN_MANAGER_FEATURE.PAGES.PAYMENTS.PATH,
    loadChildren: () =>
      import('./pages/payments/payments.module').then(
        (m) => m.PaymentsPageModule
      ),
    data: {
      breadcrumb: 'LOAN_MANAGER.PAYMENTS.TITLE',
      disabled: MENU_CONFIG_DISABLED.LOAN_MANAGER.PAYMENTS ?? false,
    },
  },
  {
    path: LOAN_MANAGER_FEATURE.PAGES.PRE_PAYMENTS.PATH,
    loadChildren: () =>
      import('./pages/pre-payments/pre-payments.module').then(
        (m) => m.PrePaymentsPageModule
      ),
    data: {
      breadcrumb: 'LOAN_MANAGER.PRE_PAYMENTS.TITLE',
      disabled: MENU_CONFIG_DISABLED.LOAN_MANAGER.PRE_PAYMENTS ?? false,
    },
  },
  {
    path: LOAN_MANAGER_FEATURE.PAGES.TRANSACTIONS.PATH,
    loadChildren: () =>
      import('./pages/transactions/transactions.module').then(
        (m) => m.TransactionsPageModule
      ),
    data: {
      breadcrumb: 'LOAN_MANAGER.TRANSACTIONS.TITLE',
      disabled: MENU_CONFIG_DISABLED.LOAN_MANAGER.TRANSACTIONS ?? false,
    },
  },
];

const routes: Routes = [
  {
    path: '',
    component: BaseContentComponent,
    children: [
      {
        path: '',
        redirectTo: CHILDREN_ROUTES.filter(
          (route) => !route?.data?.disabled
        ).shift()?.path,
        pathMatch: 'full',
      },
      ...CHILDREN_ROUTES,
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoanManagerRoutingModule {}
