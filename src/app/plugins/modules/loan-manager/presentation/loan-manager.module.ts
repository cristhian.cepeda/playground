import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoanManagerRoutingModule } from './loan-manager-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, LoanManagerRoutingModule],
})
export class LoanManagerModule {}
