import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { LoanRequest } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoanRequestFacade } from '@app/plugins/modules/loan-manager/facade/loan-request.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { LOANS_REQUEST_STATUS_OPTIONS } from '../../constants/status.constants';

@Component({
  selector: 'loan-request-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public loansRequest$: Observable<TableResponse<LoanRequest>>;
  public isLoansRequestFiltred$: Observable<boolean>;

  public form: UntypedFormGroup;
  public statusOptions: SelectOption<string>[];

  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _loansRequestFacade: LoanRequestFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._loansRequestFacade.updateLoansRequestFilters(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._loansRequestFacade.updateLoansRequestFilters(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at'];
    this.statusOptions = LOANS_REQUEST_STATUS_OPTIONS;
    this.loansRequest$ = this._loansRequestFacade.loansRequest$;
    this.isLoansRequestFiltred$ =
      this._loansRequestFacade.isLoansRequestFiltred$;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      amount__lte: [''],
      amount__gte: [''],
      status: [''],
      created_at: [''],
    });
  }
}
