import { LOANS_REQUEST_STATUS } from '@app/plugins/modules/loan-manager/core/constants/loans.constants';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

export const LOANS_REQUEST_STATUS_OPTIONS: SelectOption<string>[] = [
  {
    key: LOANS_REQUEST_STATUS.REJECTED,
    value: LOANS_REQUEST_STATUS.REJECTED,
  },
  {
    key: LOANS_REQUEST_STATUS.COMPLETED,
    value: LOANS_REQUEST_STATUS.COMPLETED,
  },
  {
    key: LOANS_REQUEST_STATUS.PENDING,
    value: LOANS_REQUEST_STATUS.PENDING,
  },
];
