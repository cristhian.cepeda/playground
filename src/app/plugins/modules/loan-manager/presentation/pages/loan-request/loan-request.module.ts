import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { LoanRequestEffects } from '@app/plugins/modules/loan-manager/domain/store/loan-request/loan-request.effects';
import { LoanRequestReducers } from '@app/plugins/modules/loan-manager/domain/store/loan-request/loan-request.reducers';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LoanRequestPageRoutingModule } from './loan-request-routing.module';
import { LoanRequestPage } from './loan-request.page';
import { FiltersComponent } from './components/filters/filters.component';

@NgModule({
  declarations: [LoanRequestPage, FiltersComponent],
  imports: [
    CommonModule,
    LoanRequestPageRoutingModule,
    LayoutModule,
    StoreModule.forFeature(
      LOAN_MANAGER_FEATURE.PAGES.LOAN_REQUEST.STORE_NAME,
      LoanRequestReducers
    ),
    EffectsModule.forFeature([LoanRequestEffects]),
  ],
})
export class LoanRequestPageModule {}
