import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LoansRequestFilters } from '../../../core/models/loan.model';
import { LoanRequestFacade } from '../../../facade/loan-request.facade';
import { LoanRequestPage } from './loan-request.page';

describe('LoanRequestPage', () => {
  let component: LoanRequestPage;
  let fixture: ComponentFixture<LoanRequestPage>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [LoanRequestPage],
      providers: [UntypedFormBuilder, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should on Change Table Filters call with filters empty', () => {
    const filters: LoansRequestFilters = {};
    const _loansRequestFacade = TestBed.inject(LoanRequestFacade);
    const spy = spyOn(_loansRequestFacade, 'updateLoansRequestFilters');
    component.onChangeTableFilters(filters);
    expect(spy).toHaveBeenCalledWith(filters);
  });
});
