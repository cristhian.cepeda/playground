import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getLoansTablePerFamily,
  LOANS_TABLES,
} from '@app/plugins/modules/loan-manager/core/constants/tables';
import {
  LoanRequest,
  LoansRequestFilters,
} from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoanRequestFacade } from '@app/plugins/modules/loan-manager/facade/loan-request.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'loan-manager-loan-request',
  templateUrl: './loan-request.page.html',
  styleUrls: ['./loan-request.page.scss'],
})
export class LoanRequestPage extends AutoUnsubscribeOnDetroy implements OnInit {
  @ViewChild('loanRequestStatusTemplate', { static: true })
  loanRequestStatusTemplate: TemplateRef<ContextColumn>;

  public headers: TableHeader[];
  public loansRequest$: Observable<TableResponse<LoanRequest>>;
  public isLoadingLoansRequest$: Observable<boolean>;

  private _projectFamilySubscription: Subscription;

  constructor(private _loansRequestFacade: LoanRequestFacade) {
    super();
  }

  ngOnInit(): void {
    this._setinitialValues();
  }

  public onChangeTableFilters(filters: LoansRequestFilters) {
    this._loansRequestFacade.updateLoansRequestFilters(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._loansRequestFacade.downloadLoansRequest();
  }

  private _setinitialValues() {
    this._mapHeaders();
    this._loansRequestFacade.initLoansRequestPage();
    this.loansRequest$ = this._loansRequestFacade.loansRequest$;
    this.isLoadingLoansRequest$ =
      this._loansRequestFacade.isLoadingLoansRequest$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._loansRequestFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getLoansTablePerFamily(
          projectFamily,
          LOANS_TABLES.LOANS_REQUEST
        ).map((header) => {
          if (header.dataKey == 'status')
            header.template = this.loanRequestStatusTemplate;
          return header;
        });
      });
  }
}
