import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { AmountCardComponent } from './amount-card.component';

describe('AmountsCardComponent', () => {
  let component: AmountCardComponent;
  let fixture: ComponentFixture<AmountCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LayoutModule],
      declarations: [AmountCardComponent],
      providers: [CustomCurrencyPipe],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
