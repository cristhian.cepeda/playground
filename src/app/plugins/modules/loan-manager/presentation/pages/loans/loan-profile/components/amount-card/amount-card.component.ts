import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { Skeleton } from '@app/core/models/skeleton.model';
import { LoanTotalsCardItem } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import {
  AMOUNT_CARD_TYPE,
  AMOUNT_CARD_TYPE_DEBT,
  AMOUNT_CARD_TYPE_PAID,
  SKELETON_ITEMS_LEFT,
  SKELETON_ITEMS_RIGHT,
} from '../../../loans/constants/amount-card.constants';

@Component({
  selector: 'loan-profile-amounts-card',
  templateUrl: './amount-card.component.html',
  styleUrls: ['./amount-card.component.scss'],
  animations: [
    trigger('expandCollapseAnimation', [
      state('ex', style({ maxHeight: '120px', overflowY: 'auto' })),
      state('coll', style({ height: '0px', overflow: 'hidden' })),
      transition('ex <=> coll', animate('400ms ease-in')),
    ]),
  ],
})
export class AmountCardComponent implements OnInit {
  @Input() cardType: AMOUNT_CARD_TYPE;
  @Input() loanTotal: LoanTotalsCardItem;
  @Input() isLoading: boolean;
  public active: boolean;

  public AMOUNT_CARD_TYPE = AMOUNT_CARD_TYPE;
  public content: any;
  public skeletonItemsRight: Skeleton[];
  public skeletonItemsLeft: Skeleton[];

  constructor() {
    this.skeletonItemsRight = SKELETON_ITEMS_RIGHT;
    this.skeletonItemsLeft = SKELETON_ITEMS_LEFT;
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onShowDetails(): void {
    this.active = !this.active;
  }

  private _setInitialValues() {
    const contents = {
      [AMOUNT_CARD_TYPE.DEBT]: AMOUNT_CARD_TYPE_DEBT,
      [AMOUNT_CARD_TYPE.PAID]: AMOUNT_CARD_TYPE_PAID,
    };
    this.content = contents[this.cardType];
  }
}
