import { Component, OnInit } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';

import { LoanFacade } from '@app/plugins/modules/loan-manager/facade/loan.facade';
import { SUMMARY_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import { Observable } from 'rxjs';

@Component({
  selector: 'loan-profile-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  public loanItems$: Observable<SummaryGroup[]>;
  public isLoadingLoan$: Observable<boolean>;
  public chargesItems$: Observable<SummaryGroup[]>;
  public SUMMARY_DESIGN_CLASS = SUMMARY_DESIGN_CLASS;

  constructor(private _loanFacade: LoanFacade) {}

  ngOnInit(): void {
    this.isLoadingLoan$ = this._loanFacade.isLoadingLoan$;
    this.loanItems$ = this._loanFacade.loanItems$;
    this.chargesItems$ = this._loanFacade.chargesItems$;
    this._loanFacade.getLoanCharges();
  }
}
