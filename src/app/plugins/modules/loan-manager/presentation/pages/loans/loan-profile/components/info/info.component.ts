import { Component, OnInit } from '@angular/core';
import { PRODUCT_FAMILY } from '@app/core/constants/product.constants';
import { Skeleton } from '@app/core/models/skeleton.model';
import { LOANS_STATUS } from '@app/plugins/modules/loan-manager/core/constants/loans.constants';
import { Loan } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoanFacade } from '@app/plugins/modules/loan-manager/facade/loan.facade';
import { Observable } from 'rxjs';
import {
  SKELETON_ITEMS_LEFT,
  SKELETON_ITEMS_RIGHT,
} from '../../../loans/constants/info.constants';

@Component({
  selector: 'loan-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  public projectFamily$: Observable<string>;
  public loan$: Observable<Loan>;
  public isLoadingLoan$: Observable<boolean>;

  public skeletonItemsLeft: Skeleton[];
  public skeletonItemsRight: Skeleton[];
  public PRODUCT_FAMILY = PRODUCT_FAMILY;
  public LOANS_STATUS = LOANS_STATUS;

  constructor(private _loanFacade: LoanFacade) {}

  ngOnInit(): void {
    this.loan$ = this._loanFacade.loanProfile$;
    this.projectFamily$ = this._loanFacade.projectFamily$;
    this.isLoadingLoan$ = this._loanFacade.isLoadingLoan$;
    this.skeletonItemsLeft = SKELETON_ITEMS_LEFT;
    this.skeletonItemsRight = SKELETON_ITEMS_RIGHT;
  }
}
