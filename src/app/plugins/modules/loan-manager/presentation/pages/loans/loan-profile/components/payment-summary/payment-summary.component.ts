import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getLoansTablePerFamily,
  LOANS_TABLES,
} from '@app/plugins/modules/loan-manager/core/constants/tables';
import { LOAN_MANAGER_URLS } from '@app/plugins/modules/loan-manager/core/constants/urls.constants';
import { Payment } from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { LoanFacade } from '@app/plugins/modules/loan-manager/facade/loan.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TABLE_DESIGN_CLASS } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'loan-profile-payment-summary',
  templateUrl: './payment-summary.component.html',
  styleUrls: ['./payment-summary.component.scss'],
})
export class PaymentSummaryComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public payments$: Observable<Payment[]>;
  public isLoading$: Observable<boolean>;
  public loanId$: Observable<string>;

  public designClass: TABLE_DESIGN_CLASS;
  public headers: TableHeader[];
  public paymentsUrl: string;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  private _projectFamilySubscription: Subscription;

  constructor(private _loanFacade: LoanFacade) {
    super();
    this.payments$ = this._loanFacade.payments$;
    this.isLoading$ = this._loanFacade.isLoadingPayments$;
  }

  ngOnInit(): void {
    this._loanFacade.getPaymentsSummary();
    this._setInitialValues();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.designClass = TABLE_DESIGN_CLASS.ALTERNATIVE;
    this.paymentsUrl = LOAN_MANAGER_URLS.PAYMENTS;
    this.loanId$ = this._loanFacade.loanId$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription = this._loanFacade.projectFamily$.subscribe(
      (projectFamily) => {
        this.headers = getLoansTablePerFamily(
          projectFamily,
          LOANS_TABLES.LOAN_PROFILE_PAYMENTS_SUMMARY
        );
      }
    );
  }
}
