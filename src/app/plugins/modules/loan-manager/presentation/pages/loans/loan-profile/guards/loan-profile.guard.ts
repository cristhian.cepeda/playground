import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { LOAN_MANAGER_URLS } from '@app/plugins/modules/loan-manager/core/constants/urls.constants';
import { LoanFacade } from '@app/plugins/modules/loan-manager/facade/loan.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoanProfileGuard implements CanActivate {
  constructor(private _loanFacade: LoanFacade, private _router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._getLoanIdParam(route);
  }

  private _getLoanIdParam(route: ActivatedRouteSnapshot): boolean | UrlTree {
    const loanId = route.queryParams?.id;
    if (!!loanId) {
      this._loanFacade.updateLoanId(loanId);
      return true;
    }
    return this._router.createUrlTree([LOAN_MANAGER_URLS.LOANS]);
  }
}
