import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoanProfilePage } from './loan-profile.page';

const routes: Routes = [
  {
    path: '',
    component: LoanProfilePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoanProfilePageRoutingModule {}
