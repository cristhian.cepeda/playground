import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { AmountCardComponent } from './components/amount-card/amount-card.component';
import { DetailsComponent } from './components/details/details.component';
import { InfoComponent } from './components/info/info.component';
import { PaymentSummaryComponent } from './components/payment-summary/payment-summary.component';
import { LoanProfilePageRoutingModule } from './loan-profile-routing.module';
import { LoanProfilePage } from './loan-profile.page';

@NgModule({
  declarations: [
    LoanProfilePage,
    DetailsComponent,
    AmountCardComponent,
    InfoComponent,
    PaymentSummaryComponent,
  ],
  imports: [CommonModule, LoanProfilePageRoutingModule, LayoutModule],
})
export class LoanProfilePageModule {}
