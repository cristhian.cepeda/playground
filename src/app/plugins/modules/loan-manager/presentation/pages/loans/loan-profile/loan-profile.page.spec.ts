import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { initialLoanState } from '@app/plugins/modules/loan-manager/domain/store/loans/loans.reducers';
import { LoanState } from '@app/plugins/modules/loan-manager/domain/store/loans/loans.state';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LoanProfilePage } from './loan-profile.page';

describe('LoanProfilePage', () => {
  let component: LoanProfilePage;
  let fixture: ComponentFixture<LoanProfilePage>;
  let store: MockStore;
  const initialState: LoanState = initialLoanState;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [LoanProfilePage],
      providers: [
        SnakeToCapitalPipe,
        CustomCurrencyPipe,
        provideMockStore({ initialState }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(LoanProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
