import { Component, OnInit } from '@angular/core';
import { LoanTotalsCard } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoanFacade } from '@app/plugins/modules/loan-manager/facade/loan.facade';
import { Observable } from 'rxjs';
import { AMOUNT_CARD_TYPE } from '../loans/constants/amount-card.constants';

@Component({
  selector: 'loans-loan-profile',
  templateUrl: './loan-profile.page.html',
  styleUrls: ['./loan-profile.page.scss'],
})
export class LoanProfilePage implements OnInit {
  public AMOUNT_CARD_TYPE = AMOUNT_CARD_TYPE;
  public loanTotalsCard$: Observable<LoanTotalsCard>;
  public isLoadingLoanTotals$: Observable<boolean>;

  constructor(private _loanFacade: LoanFacade) {}

  ngOnInit(): void {
    this._loanFacade.getLoan();
    this._loanFacade.getLoanTotals();
    this.loanTotalsCard$ = this._loanFacade.loanTotalsCard$;
    this.isLoadingLoanTotals$ = this._loanFacade.isLoadingLoanTotals$;
  }
}
