import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { LoanProfileGuard } from './loan-profile/guards/loan-profile.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./loans/loans.module').then((m) => m.LoansPageModule),
  },
  {
    canActivate: [LoanProfileGuard],
    path: LOAN_MANAGER_FEATURE.PAGES.LOANS.SUB_PAGES.LOAN_PROFILE.PATH,
    loadChildren: () =>
      import('./loan-profile/loan-profile.module').then(
        (m) => m.LoanProfilePageModule
      ),
    data: {
      breadcrumb: LOAN_MANAGER_FEATURE.PAGES.LOANS.SUB_PAGES.LOAN_PROFILE.NAME,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoansRoutingModule {}
