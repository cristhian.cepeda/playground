import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { LoanEffects } from '@app/plugins/modules/loan-manager/domain/store/loans/loans.effects';
import { LoanReducers } from '@app/plugins/modules/loan-manager/domain/store/loans/loans.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LoansRoutingModule } from './loans-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LoansRoutingModule,
    StoreModule.forFeature(
      LOAN_MANAGER_FEATURE.PAGES.LOANS.STORE_NAME,
      LoanReducers
    ),
    EffectsModule.forFeature([LoanEffects]),
  ],
})
export class LoansModule {}
