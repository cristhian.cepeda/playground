import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { Loan } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoanFacade } from '@app/plugins/modules/loan-manager/facade/loan.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { LOANS_STATUS_OPTIONS } from '../../constants/status.constants';

@Component({
  selector: 'loans-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public loans$: Observable<TableResponse<Loan>>;
  public isLoansFiltred$: Observable<boolean>;

  public statusOptions: SelectOption<string>[];
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;
  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _loanFacade: LoanFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._loanFacade.updateFiltersLoans(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._loanFacade.updateFiltersLoans(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at'];
    this.loans$ = this._loanFacade.loans$;
    this.isLoansFiltred$ = this._loanFacade.isLoansFiltred$;
    this.statusOptions = LOANS_STATUS_OPTIONS;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      status: [''],
      amount__gte: [''],
      amount__lte: [''],
      created_at: [''],
    });
  }
}
