import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

export enum AMOUNT_CARD_TYPE {
  PAID = 'PAID',
  DEBT = 'DEBT',
}

const margin: string = '0 5px 5px 0';
const minWidth: number = 60;
const maxWidth: number = 100;

const skeletonTheme: SkeletonTheme = { minWidth, maxWidth, margin };

export const SKELETON_ITEMS_RIGHT: Skeleton[] = [
  ...getSkeleton(1, 3, skeletonTheme),
  ...getSkeleton(2, 1, skeletonTheme),
];

export const SKELETON_ITEMS_LEFT: Skeleton[] = getSkeleton(2, 4, skeletonTheme);

export const AMOUNT_CARD_TYPE_DEBT = {
  status: {
    title: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.DEBT.TITLE',
    class: 'debt',
  },
  amountTitle: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.DEBT.TOTAL',
  totalCredit:
    'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.DEBT.TOTAL_CREDIT',
  totalTooltip:
    'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.DEBT.TOTAL_TOOLTIP',
  charges: {
    capitalTitle: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.DEBT.CAPITAL',
    chargeTitle: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.DEBT.CHARGES',
  },
};

export const AMOUNT_CARD_TYPE_PAID = {
  status: {
    title: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.PAID.TITLE',
    class: 'paid',
  },
  amountTitle: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.PAID.TOTAL',
  totalTooltip:
    'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.PAID.TOTAL_TOOLTIP',
  charges: {
    capitalTitle: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.PAID.CAPITAL',
    chargeTitle: 'LOAN_MANAGER.LOAN_PROFILE.LOAN_SUMMARY.TOTALS.PAID.CHARGES',
  },
};
