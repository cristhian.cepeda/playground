import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const margin: string = '0 5px 5px 0';
const minWidth: number = 60;
const maxWidth: number = 200;

const skeletonTheme: SkeletonTheme = { minWidth, maxWidth, margin };

export const SKELETON_ITEMS_LEFT: Skeleton[] = [
  ...getSkeleton(1, 1, skeletonTheme),
  ...getSkeleton(2, 3, skeletonTheme),
];

export const SKELETON_ITEMS_RIGHT: Skeleton[] = [
  ...getSkeleton(2, 1, { margin, width: '100px' }),
  ...getSkeleton(2, 1, { margin, width: '140px' }),
];
