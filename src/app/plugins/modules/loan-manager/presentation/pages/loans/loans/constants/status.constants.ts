import { LOANS_STATUS } from '@app/plugins/modules/loan-manager/core/constants/loans.constants';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

export const LOANS_STATUS_OPTIONS: SelectOption<string>[] = [
  {
    key: LOANS_STATUS.ACTIVE,
    value: LOANS_STATUS.ACTIVE,
  },
  {
    key: LOANS_STATUS.PAID,
    value: LOANS_STATUS.PAID,
  },
  {
    key: LOANS_STATUS.FROZEN,
    value: LOANS_STATUS.FROZEN,
  },
  {
    key: LOANS_STATUS.CANCELED,
    value: LOANS_STATUS.CANCELED,
  },
];
