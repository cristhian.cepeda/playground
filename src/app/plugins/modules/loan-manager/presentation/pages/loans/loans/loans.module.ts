import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { FiltersComponent } from './components/filters/filters.component';
import { LoansPageRoutingModule } from './loans-routing.module';
import { LoansPage } from './loans.page';

@NgModule({
  declarations: [LoansPage, FiltersComponent],
  imports: [CommonModule, LoansPageRoutingModule, LayoutModule],
})
export class LoansPageModule {}
