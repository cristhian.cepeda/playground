import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Filters } from '@app/core/models/filters.model';

import { initialLoanState } from '@app/plugins/modules/loan-manager/domain/store/loans/loans.reducers';
import { LoanState } from '@app/plugins/modules/loan-manager/domain/store/loans/loans.state';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LoanFacade } from '../../../../facade/loan.facade';
import { LoansPage } from './loans.page';

describe('LoansPage', () => {
  let component: LoansPage;
  let fixture: ComponentFixture<LoansPage>;
  let store: MockStore;
  const initialState: LoanState = initialLoanState;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoansPage],
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        SnakeToCapitalPipe,
        CustomCurrencyPipe,
        provideMockStore({ initialState }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(LoansPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be updateFiltersloans called with filters mock', () => {
    const filters: Filters = {};
    const _loanFacade = TestBed.inject(LoanFacade);
    const spy = spyOn(_loanFacade, 'updateFiltersLoans');
    component.onChangeTableFilters(filters);
    expect(spy).toHaveBeenCalledWith(filters);
  });
});
