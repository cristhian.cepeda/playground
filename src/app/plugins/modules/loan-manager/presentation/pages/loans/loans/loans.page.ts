import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { Filters } from '@app/core/models/filters.model';
import {
  getLoansTablePerFamily,
  LOANS_TABLES,
} from '@app/plugins/modules/loan-manager/core/constants/tables';
import { LOAN_MANAGER_URLS } from '@app/plugins/modules/loan-manager/core/constants/urls.constants';
import { Loan } from '@app/plugins/modules/loan-manager/core/models/loan.model';
import { LoanFacade } from '@app/plugins/modules/loan-manager/facade/loan.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription, take } from 'rxjs';

@Component({
  selector: 'loan-manager-loans',
  templateUrl: './loans.page.html',
  styleUrls: ['./loans.page.scss'],
})
export class LoansPage extends AutoUnsubscribeOnDetroy implements OnInit {
  @ViewChild('loanStatusTemplate', { static: true })
  loanStatusTemplate: TemplateRef<ContextColumn>;

  public headers: TableHeader[];
  public loanProfileUrl: string;

  public loans$: Observable<TableResponse<Loan>>;
  public isLoadingLoans$: Observable<boolean>;

  private _projectFamilySubscription: Subscription;

  constructor(
    private _loanFacade: LoanFacade,
    private _activeRouted: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangeTableFilters(filters: Filters) {
    this._loanFacade.updateFiltersLoans(filters, { isNativationFilter: true });
  }

  public onDownload(): void {
    this._loanFacade.downloadLoans();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.loanProfileUrl = LOAN_MANAGER_URLS.LOAN_PROFILE;
    this.loans$ = this._loanFacade.loans$;
    this.isLoadingLoans$ = this._loanFacade.isLoadingLoans$;

    this._activeRouted.queryParamMap.pipe(take(1)).subscribe((params) => {
      const customer_id = params.get('customer_id');
      const status = params.get('status');
      const merchant_id = params.get('merchant_id');
      if (customer_id || status || merchant_id) {
        this._loanFacade.initLoansPage({ customer_id, status, merchant_id });
        return;
      }
      this._loanFacade.initLoansPage();
    });
  }

  private _mapHeaders() {
    this._projectFamilySubscription = this._loanFacade.projectFamily$.subscribe(
      (projectFamily) => {
        this.headers = getLoansTablePerFamily(
          projectFamily,
          LOANS_TABLES.LOANS
        ).map((header) => {
          if (header.dataKey == 'status')
            header.template = this.loanStatusTemplate;
          return header;
        });
      }
    );
  }
}
