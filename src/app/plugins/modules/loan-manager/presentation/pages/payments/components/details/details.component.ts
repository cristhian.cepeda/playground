import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Skeleton } from '@app/core/models/skeleton.model';
import { LOAN_MANAGER_URLS } from '@app/plugins/modules/loan-manager/core/constants/urls.constants';
import { PaymentProfile } from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { SUMMARY_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import {
  SKELETON_ITEMS_SUMMARY,
  SKELETON_ITEMS_TOTALS,
} from '../../constants/payments.constants';

@Component({
  selector: 'payments-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  @Input() paymentProfile: PaymentProfile;
  @Input() isLoading: boolean;

  public SUMMARY_DESIGN_CLASS = SUMMARY_DESIGN_CLASS;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public loanProfileUrl: string;
  public loanId: string;

  public skeletonItemsTotals: Skeleton[];
  public skeletonItemsSummary: Skeleton[];

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['paymentProfile'] && this.paymentProfile)
      this.loanId = this.paymentProfile?.loanId;
  }

  ngOnInit(): void {
    this.loanProfileUrl = LOAN_MANAGER_URLS.LOAN_PROFILE;
    this.loanId = this.paymentProfile?.loanId;
    this.skeletonItemsTotals = SKELETON_ITEMS_TOTALS;
    this.skeletonItemsSummary = SKELETON_ITEMS_SUMMARY;
  }
}
