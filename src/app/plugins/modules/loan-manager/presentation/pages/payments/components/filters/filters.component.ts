import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { Payment } from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { PaymentsFacade } from '@app/plugins/modules/loan-manager/facade/payments.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { PAYMENTS_STATUS } from '../../constants/status.constants';

@Component({
  selector: 'payments-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public paymentsTable$: Observable<TableResponse<Payment>>;
  public isPaymentsFiltred$: Observable<boolean>;
  public statusOptions: SelectOption<string>[];
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  private _controlsWithDateRanges: string[];
  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _paymentsFacade: PaymentsFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._paymentsFacade.updatePaymentsFilters(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._paymentsFacade.updatePaymentsFilters(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      amount__lte: [''],
      amount__gte: [''],
      status: [''],
      paid_at: [''],
    });
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['paid_at'];
    this.paymentsTable$ = this._paymentsFacade.paymentsTable$;
    this.isPaymentsFiltred$ = this._paymentsFacade.isPaymentsFiltred$;
    this.statusOptions = PAYMENTS_STATUS;
  }
}
