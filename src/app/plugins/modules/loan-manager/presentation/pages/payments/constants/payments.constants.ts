import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const margin: string = '0 5px 5px 0';
const minWidth: number = 100;
const maxWidth: number = 150;

const skeletonTheme: SkeletonTheme = { minWidth, maxWidth, margin };

export const SKELETON_ITEMS_TOTALS: Skeleton[] = [
  ...getSkeleton(2, 3, skeletonTheme),
  ...getSkeleton(2, 1, skeletonTheme),
];
export const SKELETON_ITEMS_SUMMARY: Skeleton[] = [
  ...getSkeleton(2, 3, skeletonTheme),
  ...getSkeleton(2, 1, skeletonTheme),
];
