import { PAYMENT_STATUS } from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

export const PAYMENTS_STATUS: SelectOption<string>[] = [
  {
    key: PAYMENT_STATUS.ACCEPTED,
    value: PAYMENT_STATUS.ACCEPTED,
  },
  {
    key: PAYMENT_STATUS.REVERSED,
    value: PAYMENT_STATUS.REVERSED,
  },
  {
    key: PAYMENT_STATUS.REJECTED,
    value: PAYMENT_STATUS.REJECTED,
  },
];
