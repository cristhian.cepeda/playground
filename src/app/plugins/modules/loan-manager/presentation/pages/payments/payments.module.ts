import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { PaymentsEffects } from '@app/plugins/modules/loan-manager/domain/store/payments/payments.effects';
import { PaymentsReducers } from '@app/plugins/modules/loan-manager/domain/store/payments/payments.reducers';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FiltersComponent } from './components/filters/filters.component';
import { PaymentsPageRoutingModule } from './payments-routing.module';
import { PaymentsPage } from './payments.page';
import { DetailsComponent } from './components/details/details.component';

@NgModule({
  declarations: [PaymentsPage, FiltersComponent, DetailsComponent],
  imports: [
    CommonModule,
    PaymentsPageRoutingModule,
    LayoutModule,
    StoreModule.forFeature(
      LOAN_MANAGER_FEATURE.PAGES.PAYMENTS.STORE_NAME,
      PaymentsReducers
    ),
    EffectsModule.forFeature([PaymentsEffects]),
  ],
})
export class PaymentsPageModule {}
