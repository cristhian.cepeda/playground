import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getLoansTablePerFamily,
  LOANS_TABLES,
} from '@app/plugins/modules/loan-manager/core/constants/tables';
import {
  Payment,
  PaymentFilters,
  PaymentProfile,
} from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { PaymentsFacade } from '@app/plugins/modules/loan-manager/facade/payments.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription, take } from 'rxjs';

@Component({
  selector: 'loan-manager-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
})
export class PaymentsPage extends AutoUnsubscribeOnDetroy implements OnInit {
  @ViewChild('statusTemplate', { static: true })
  statusTemplate: TemplateRef<ContextColumn>;

  public paymentsTable$: Observable<TableResponse<Payment>>;
  public isLoadingPayments$: Observable<boolean>;

  public paymentProfiles$: Observable<PaymentProfile[]>;
  public isLoadingPaymentProfiles$: Observable<{ [key: string]: boolean }>;

  public headers: TableHeader[];
  public showDetails: boolean;

  private _projectFamilySubscription: Subscription;

  constructor(
    private _paymentsFacade: PaymentsFacade,
    private _activeRouted: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangePageFilter(filters: PaymentFilters) {
    this._paymentsFacade.updatePaymentsFilters(filters, {
      isNativationFilter: true,
    });
  }

  public onToggleDetails(payment: Payment): void {
    this._paymentsFacade.getPaymentProfile(payment);
  }

  public onDownload(): void {
    this._paymentsFacade.downloadPayments();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this._getQueryParams();
    this.paymentsTable$ = this._paymentsFacade.paymentsTable$;
    this.isLoadingPayments$ = this._paymentsFacade.isLoadingPayments$;
    this.paymentProfiles$ = this._paymentsFacade.paymentProfiles$;
    this.isLoadingPaymentProfiles$ =
      this._paymentsFacade.isLoadingPaymentProfiles$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._paymentsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getLoansTablePerFamily(
          projectFamily,
          LOANS_TABLES.PAYMENTS
        ).map((header) => {
          if (header.dataKey == 'status') header.template = this.statusTemplate;
          return header;
        });
      });
  }

  private _getQueryParams() {
    this._activeRouted.queryParamMap.pipe(take(1)).subscribe((params) => {
      const customer_id = params.get('customer_id');
      const loan_id = params.get('loan_id');
      const search = params.get('search');
      this.showDetails = !!search;
      if (customer_id || loan_id || search) {
        this._paymentsFacade.initPaymentsPage({ customer_id, loan_id, search });
        return;
      }
      this._paymentsFacade.initPaymentsPage();
    });
  }
}
