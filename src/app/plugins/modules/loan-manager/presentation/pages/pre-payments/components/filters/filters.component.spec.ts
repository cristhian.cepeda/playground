import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FILTERS } from '@app/core/constants/filters.constant';
import { PaymentFilters } from '@app/plugins/modules/loan-manager/core/models/payment.model';
import { PaymentsFacade } from '@app/plugins/modules/loan-manager/facade/payments.facade';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';

import { FiltersComponent } from './filters.component';

describe('FiltersComponent', () => {
  let component: FiltersComponent;
  let fixture: ComponentFixture<FiltersComponent>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [FiltersComponent],
      providers: [UntypedFormBuilder, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(FiltersComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onFilter', () => {
    it('should be call update filters customer with empty', () => {
      const filters: PaymentFilters = {
        search: '',
        amount__lte: '',
        amount__gte: '',
        status: '',
        paid_at: '',
        offset: FILTERS.offset,
      };
      const isFiltred: boolean = true;
      const _paymentsFacade = TestBed.inject(PaymentsFacade);
      const spy = spyOn(_paymentsFacade, 'updatePaymentsFilters');
      const formDebugElement = fixture.debugElement.query(By.css('form'));
      formDebugElement.triggerEventHandler('ngSubmit', null);
      fixture.detectChanges();
      expect(spy).toHaveBeenCalledWith(filters, isFiltred);
    });

    it('should be call update filters customer with mock values', () => {
      const dateMock = '2021-01-01';
      component.form.patchValue({
        search: 'asd',
        amount__lte: '123',
        amount__gte: '123',
        status: '',
        paid_at: dateMock,
        offset: FILTERS.offset,
      });
      fixture.detectChanges();
      const filters: PaymentFilters = {
        search: 'asd',
        amount__lte: '123',
        amount__gte: '123',
        status: '',
        paid_at: dateMock,
        offset: FILTERS.offset,
      };
      const isFiltred: boolean = true;
      const _paymentsFacade = TestBed.inject(PaymentsFacade);
      const spy = spyOn(_paymentsFacade, 'updatePaymentsFilters');
      const formDebugElement = fixture.debugElement.query(By.css('form'));
      fixture.detectChanges();
      formDebugElement.triggerEventHandler('ngSubmit', null);
      expect(spy).toHaveBeenCalledWith(filters, isFiltred);
    });

    it('should be call update filters customer with mock values except paid_at', () => {
      component.form.patchValue({
        search: 'asd',
        amount__lte: '123',
        amount__gte: '123',
        status: '',
        paid_at: '',
        offset: FILTERS.offset,
      });
      fixture.detectChanges();
      const filters: PaymentFilters = {
        search: 'asd',
        amount__lte: '123',
        amount__gte: '123',
        status: '',
        paid_at: '',
        offset: FILTERS.offset,
      };
      const isFiltred: boolean = true;
      const _paymentsFacade = TestBed.inject(PaymentsFacade);
      const spy = spyOn(_paymentsFacade, 'updatePaymentsFilters');
      const formDebugElement = fixture.debugElement.query(By.css('form'));
      fixture.detectChanges();
      formDebugElement.triggerEventHandler('ngSubmit', null);
      expect(spy).toHaveBeenCalledWith(filters, isFiltred);
    });
  });

  it('should be call update filters customer with empty form', () => {
    const _paymentsFacade = TestBed.inject(PaymentsFacade);
    const spy = spyOn(_paymentsFacade, 'updatePaymentsFilters');
    const formMock = {
      amount__lte: null,
      amount__gte: null,
      paid_at: null,
      search: null,
      status: null,
    };
    component.onCleanFilters();
    expect(spy).toHaveBeenCalledWith(formMock);
  });
});
