import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { PrePayment } from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { PrePaymentsFacade } from '@app/plugins/modules/loan-manager/facade/pre-payments.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { PRE_PAYMENTS_STATUS } from '../../constants/status.constants';

@Component({
  selector: 'pre-payments-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public prePaymentsTable$: Observable<TableResponse<PrePayment>>;
  public isPrePaymentsFiltred$: Observable<boolean>;

  public statusOptions: SelectOption<string>[];
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _prePaymentsFacade: PrePaymentsFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._prePaymentsFacade.updatePrePaymentsFilters(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._prePaymentsFacade.updatePrePaymentsFilters(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      amount__lte: [''],
      amount__gte: [''],
      status: [''],
      created_at: [''],
    });
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at'];
    this.prePaymentsTable$ = this._prePaymentsFacade.prePaymentsTable$;
    this.isPrePaymentsFiltred$ = this._prePaymentsFacade.isPrePaymentsFiltred$;
    this.statusOptions = PRE_PAYMENTS_STATUS;
  }
}
