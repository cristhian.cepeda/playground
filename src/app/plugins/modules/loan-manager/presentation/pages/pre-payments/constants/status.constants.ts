import { PRE_PAYMENT_STATUS } from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

export const PRE_PAYMENTS_STATUS: SelectOption<string>[] = [
  {
    key: PRE_PAYMENT_STATUS.COMPLETED,
    value: PRE_PAYMENT_STATUS.COMPLETED,
  },
  {
    key: PRE_PAYMENT_STATUS.PENDING,
    value: PRE_PAYMENT_STATUS.PENDING,
  },
  {
    key: PRE_PAYMENT_STATUS.REJECTED,
    value: PRE_PAYMENT_STATUS.REJECTED,
  },
];
