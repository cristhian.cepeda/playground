import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrePaymentsPage } from './pre-payments.page';

const routes: Routes = [
  {
    path: '',
    component: PrePaymentsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrePaymentsPageRoutingModule {}
