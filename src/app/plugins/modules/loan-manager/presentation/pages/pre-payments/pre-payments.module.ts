import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { PrePaymentsEffects } from '@app/plugins/modules/loan-manager/domain/store/pre-payments/pre-payments.effects';
import { PrePaymentsReducers } from '@app/plugins/modules/loan-manager/domain/store/pre-payments/pre-payments.reducers';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { FiltersComponent } from './components/filters/filters.component';
import { PrePaymentsPageRoutingModule } from './pre-payments-routing.module';
import { PrePaymentsPage } from './pre-payments.page';

@NgModule({
  declarations: [PrePaymentsPage, FiltersComponent],
  imports: [
    CommonModule,
    PrePaymentsPageRoutingModule,
    LayoutModule,
    StoreModule.forFeature(
      LOAN_MANAGER_FEATURE.PAGES.PRE_PAYMENTS.STORE_NAME,
      PrePaymentsReducers
    ),
    EffectsModule.forFeature([PrePaymentsEffects]),
  ],
})
export class PrePaymentsPageModule {}
