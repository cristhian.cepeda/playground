import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PaymentState } from '@app/plugins/modules/loan-manager/domain/store/payments/payments.state';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';

import { initialPaymentState } from '@app/plugins/modules/loan-manager/domain/store/payments/payments.reducers';
import { PrePaymentsPage } from './pre-payments.page';

describe('PrePaymentsPage', () => {
  let component: PrePaymentsPage;
  let fixture: ComponentFixture<PrePaymentsPage>;
  let store: MockStore;
  const initialState: PaymentState = initialPaymentState;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [PrePaymentsPage],
      providers: [provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrePaymentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
