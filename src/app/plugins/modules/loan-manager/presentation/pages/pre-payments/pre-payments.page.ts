import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getLoansTablePerFamily,
  LOANS_TABLES,
} from '@app/plugins/modules/loan-manager/core/constants/tables';
import {
  PrePayment,
  PrePaymentFilters,
} from '@app/plugins/modules/loan-manager/core/models/pre-payment.model';
import { PrePaymentsFacade } from '@app/plugins/modules/loan-manager/facade/pre-payments.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'loan-manager-pre-payments',
  templateUrl: './pre-payments.page.html',
  styleUrls: ['./pre-payments.page.scss'],
})
export class PrePaymentsPage extends AutoUnsubscribeOnDetroy implements OnInit {
  @ViewChild('statusTemplate', { static: true })
  statusTemplate: TemplateRef<ContextColumn>;

  public prePaymentsTable$: Observable<TableResponse<PrePayment>>;
  public isLoadingPrePayments$: Observable<boolean>;
  public headers: TableHeader[];

  private _projectFamilySubscription: Subscription;

  constructor(private _prePaymentsFacade: PrePaymentsFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangePageFilter(filters: PrePaymentFilters) {
    this._prePaymentsFacade.updatePrePaymentsFilters(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._prePaymentsFacade.downloadPrePayments();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this._prePaymentsFacade.initPrePaymentsPage();

    this.prePaymentsTable$ = this._prePaymentsFacade.prePaymentsTable$;
    this.isLoadingPrePayments$ = this._prePaymentsFacade.isLoadingPrePayments$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._prePaymentsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getLoansTablePerFamily(
          projectFamily,
          LOANS_TABLES.PRE_PAYMENTS
        ).map((header) => {
          if (header.dataKey == 'status') header.template = this.statusTemplate;
          return header;
        });
      });
  }
}
