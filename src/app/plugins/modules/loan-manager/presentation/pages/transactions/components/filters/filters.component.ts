import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { Transaction } from '@app/plugins/modules/loan-manager/core/models/transaction.model';
import { TransactionsFacade } from '@app/plugins/modules/loan-manager/facade/transactions.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
@Component({
  selector: 'transactions-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public transactionsTable$: Observable<TableResponse<Transaction>>;
  public isTransactionsFiltred$: Observable<boolean>;

  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _transactionsFacade: TransactionsFacade
  ) {}
  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._transactionsFacade.updateTransactionsFilters(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._transactionsFacade.updateTransactionsFilters(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      amount__lte: [''],
      amount__gte: [''],
      created_at: [''],
    });
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at'];
    this.transactionsTable$ = this._transactionsFacade.transactionsTable$;
    this.isTransactionsFiltred$ =
      this._transactionsFacade.isTransactionsFiltred$;
  }
}
