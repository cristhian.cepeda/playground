import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LOAN_MANAGER_FEATURE } from '@app/plugins/modules/loan-manager/core/constants/feature.constants';
import { TransactionsEffects } from '@app/plugins/modules/loan-manager/domain/store/transactions/transactions.effects';
import { TransactionsReducers } from '@app/plugins/modules/loan-manager/domain/store/transactions/transactions.reducers';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FiltersComponent } from './components/filters/filters.component';
import { TransactionsPageRoutingModule } from './transactions-routing.module';
import { TransactionsPage } from './transactions.page';

@NgModule({
  declarations: [TransactionsPage, FiltersComponent],
  imports: [
    CommonModule,
    TransactionsPageRoutingModule,
    LayoutModule,
    StoreModule.forFeature(
      LOAN_MANAGER_FEATURE.PAGES.TRANSACTIONS.STORE_NAME,
      TransactionsReducers
    ),
    EffectsModule.forFeature([TransactionsEffects]),
  ],
})
export class TransactionsPageModule {}
