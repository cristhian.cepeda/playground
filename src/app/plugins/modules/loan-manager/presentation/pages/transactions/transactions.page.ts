import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { Filters } from '@app/core/models/filters.model';
import {
  getLoansTablePerFamily,
  LOANS_TABLES,
} from '@app/plugins/modules/loan-manager/core/constants/tables';
import { Transaction } from '@app/plugins/modules/loan-manager/core/models/transaction.model';
import { TransactionsFacade } from '@app/plugins/modules/loan-manager/facade/transactions.facade';
import {
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public headers: TableHeader[];

  public transactionsTable$: Observable<TableResponse<Transaction>>;
  public isLoadingTransactions$: Observable<boolean>;

  private _projectFamilySubscription: Subscription;

  constructor(private _transactionsFacade: TransactionsFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangePageFilter(filters: Filters) {
    this._transactionsFacade.updateTransactionsFilters(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._transactionsFacade.downloadTransactions();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this._transactionsFacade.initTransactionsPage();
    this.transactionsTable$ = this._transactionsFacade.transactionsTable$;
    this.isLoadingTransactions$ =
      this._transactionsFacade.isLoadingTransactions$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._transactionsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getLoansTablePerFamily(
          projectFamily,
          LOANS_TABLES.TRANSACTIONS
        );
      });
  }
}
