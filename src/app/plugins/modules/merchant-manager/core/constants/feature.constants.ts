export const MERCHANT_MANAGER_FEATURE = {
  NAME: 'Merchant Manager',
  PATH: 'merchant-manager',
  PAGES: {
    OVERVIEW: {
      NAME: 'Overview',
      PATH: 'overview',
    },
    MERCHANTS: {
      NAME: 'Merchants',
      PATH: 'merchants',
      STORE_NAME: 'merchants',
      SUB_PAGES: {
        MERCHANT_PROFILE: {
          NAME: 'Merchant profile',
          PATH: 'merchant-profile',
        },
      },
    },
    PURCHASES: {
      NAME: 'Purchases',
      PATH: 'purchases',
      STORE_NAME: 'purchases',
    },
    PAYOUTS: {
      NAME: 'Payouts',
      PATH: 'payouts',
      STORE_NAME: 'payouts',
    },
    MOVEMENTS: {
      NAME: 'Movements',
      PATH: 'movements',
      STORE_NAME: 'movements',
    },
    MERCHANT_GROUPS: {
      NAME: 'Merchant Groups',
      PATH: 'merchant-groups',
      STORE_NAME: 'merchant-groups',
      SUB_PAGES: {
        MERCHANT_GROUP_PROFILE: {
          NAME: 'Merchant Group',
          PATH: 'merchant-group',
          SUB_PAGES: {
            ADD_OR_REMOVE_MERCHANTS: {
              NAME: 'Add Or Remove Merchants',
              PATH: 'add-or-remove-merchants',
            },
            ADD_OR_REMOVE_OFFERS: {
              NAME: 'Add Or Remove Offers',
              PATH: 'add-or-remove-offers',
            },
          },
        },
        NEW_GROUP: {
          NAME: 'New Group',
          PATH: 'new-group',
          SUB_PAGES: {
            NEW_GROUP_DETAILS: {
              NAME: 'New Group Details',
              PATH: 'details',
            },
            NEW_GROUP_MERCHANTS: {
              NAME: 'New Group Merchants',
              PATH: 'merchants',
            },
            NEW_GROUP_OFFERS: {
              NAME: 'New Group Offers',
              PATH: 'offers',
            },
            NEW_GROUP_SUCCESS: {
              NAME: 'New Group Success',
              PATH: 'success',
            },
          },
        },
      },
    },
  },
};

export const LOAN_MANAGER_FEATURE = {
  NAME: 'Credit Manager',
  PATH: 'credit-manager',
  PAGES: {
    LOANS: {
      NAME: 'Credits',
      PATH: 'credits',
      STORE_NAME: 'loans',
      SUB_PAGES: {
        LOAN_PROFILE: {
          NAME: 'Credits profile',
          PATH: 'credit-profile',
        },
      },
    },
  },
};
