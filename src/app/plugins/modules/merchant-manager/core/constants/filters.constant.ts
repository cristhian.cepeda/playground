import { Filters } from '@app/core/models/filters.model';

export const FILTERS_MERCHANTS: Filters = {
  limit: 6,
  offset: 0,
};

export const FILTERS_MERCHANT_PROFILE_TABLES: Filters = {
  limit: 5,
  offset: 0,
};
