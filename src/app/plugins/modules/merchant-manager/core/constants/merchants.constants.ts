export enum MERCHANT_STATUS {
  ACTIVE = 'active',
  ARCHIVED = 'archived',
  INACTIVE = 'inactive',
  UNSIGNED = 'unsigned',
  UNREGISTERED = 'unregistered',
}

export const MERCHANT_GROUP_BY_DEFAULT = 'Default';
export const MERCHANT_GROUP_BY_DEFAULT_BACK = '_';
