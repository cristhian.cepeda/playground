import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import {
  TABLE_MERCHANTS_DEFAULT,
  TABLE_MERCHANT_PROFILE_MOVEMENTS_DEFAULT,
  TABLE_MERCHANT_PROFILE_PURCHASES_DEFAULT,
} from '../tables-default/merchants.table';

export const TABLE_MERCHANTS_BNPL: TableHeader[] = TABLE_MERCHANTS_DEFAULT;

export const TABLE_MERCHANT_PROFILE_PURCHASES_BNPL: TableHeader[] =
  TABLE_MERCHANT_PROFILE_PURCHASES_DEFAULT;

export const TABLE_MERCHANT_PROFILE_MOVEMENTS_BNPL: TableHeader[] =
  TABLE_MERCHANT_PROFILE_MOVEMENTS_DEFAULT;
