import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_MERCHANT_GROUP_MERCHANTS_DEFAULT } from '../tables-default/merchant-groups.table';

export const TABLE_MERCHANT_GROUP_MERCHANTS_CREDIT_CARD: TableHeader[] =
  TABLE_MERCHANT_GROUP_MERCHANTS_DEFAULT;
