import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PAYOUTS_DEFAULT } from '../tables-default/payouts.table';

export const TABLE_PAYOUTS_CREDIT_CARD: TableHeader[] = TABLE_PAYOUTS_DEFAULT;
