import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_MERCHANT_GROUP_MERCHANTS_DEFAULT: TableHeader[] = [
  {
    label: '',
    size: 2,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'id',
    template: null,
  },
  {
    label:
      'MERCHANT_MANAGER.NEW_GROUP.MERCHANTS.MERCHANT_LIST_TABLE.MERCHANT_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'display_name',
  },
  {
    label: 'MERCHANT_MANAGER.NEW_GROUP.MERCHANTS.MERCHANT_LIST_TABLE.MCC',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'mcc',
    isSortable: true,
  },
  {
    label:
      'MERCHANT_MANAGER.NEW_GROUP.MERCHANTS.MERCHANT_LIST_TABLE.MERCHANT_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'MERCHANT_MANAGER.NEW_GROUP.MERCHANTS.MERCHANT_LIST_TABLE.GROUP',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_group',
    isSortable: true,
  },
];
