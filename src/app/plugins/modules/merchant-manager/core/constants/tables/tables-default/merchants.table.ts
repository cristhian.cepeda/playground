import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_MERCHANTS_DEFAULT: TableHeader[] = [
  {
    label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.ID',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.MERCHANT_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'display_name',
    isSortable: true,
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.NATIONAL_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'national_id',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.MERCHANT_GROUP',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_group',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
    template: null,
  },
  // TODO IMPLEMENT WHEN BACK ADD IT
  // {
  //   label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.SALES',
  //   size: 4,
  //   typeColumn: TYPE_COLUMN.CURRENCY,
  //   dataKey: 'sales',
  //   isSortable: true,
  // },
  // {
  //   label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.TOTAL_PAYOUT',
  //   size: 4,
  //   typeColumn: TYPE_COLUMN.CURRENCY,
  //   dataKey: 'payout',
  // },
  {
    label: 'MERCHANT_MANAGER.MERCHANTS.TABLE.CREATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
  },
];

export const TABLE_MERCHANT_PROFILE_PURCHASES_DEFAULT: TableHeader[] = [
  {
    label: 'MERCHANT_MANAGER.MERCHANT_PROFILE.PURCHASES.TABLE.PURCHASE_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'reference',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANT_PROFILE.PURCHASES.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANT_PROFILE.PURCHASES.TABLE.DATE_OF_PURCHASE',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
  },
];

export const TABLE_MERCHANT_PROFILE_MOVEMENTS_DEFAULT: TableHeader[] = [
  {
    label: 'MERCHANT_MANAGER.MERCHANT_PROFILE.MOVEMENTS.TABLE.MOVEMENT_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'reference',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANT_PROFILE.MOVEMENTS.TABLE.CONCEPT',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'concept',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANT_PROFILE.MOVEMENTS.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
  },
  {
    label: 'MERCHANT_MANAGER.MERCHANT_PROFILE.MOVEMENTS.TABLE.DATE_OF_MOVEMENT',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'date',
  },
];
