import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_MOVEMENTS_DEFAULT: TableHeader[] = [
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.CREATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.PURCHASE_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'purchase_reference',
    isSortable: true,
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.MERCHANT_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_reference',
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.MERCHANT_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_display_name',
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.PAYOUT_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'payout_reference',
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.COMMISSION',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'fee_amount',
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.TAX',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'fee_tax_amount',
  },
  {
    label: 'MERCHANT_MANAGER.MOVEMENTS.TABLE.TYPE',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'type',
  },
];
