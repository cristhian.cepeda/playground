import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_PAYOUTS_DEFAULT: TableHeader[] = [
  {
    label: 'MERCHANT_MANAGER.PAYOUTS.TABLE.DATE',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'date',
  },
  {
    label: 'MERCHANT_MANAGER.PAYOUTS.TABLE.ID',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'reference',
    isSortable: true,
  },
  {
    label: 'MERCHANT_MANAGER.PAYOUTS.TABLE.MERCHANT_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_reference',
  },
  {
    label: 'MERCHANT_MANAGER.PAYOUTS.TABLE.MERCHANT_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_display_name',
  },
  {
    label: 'MERCHANT_MANAGER.PAYOUTS.TABLE.MERCHANT_GROUP',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_group_name',
  },
  {
    label: 'MERCHANT_MANAGER.PAYOUTS.TABLE.TOTAL_AMOUNT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'amount',
  },
];
