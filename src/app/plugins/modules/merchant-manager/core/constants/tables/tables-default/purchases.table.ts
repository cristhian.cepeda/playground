import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_PURCHASES_DEFAULT: TableHeader[] = [
  {
    label: 'MERCHANT_MANAGER.PURCHASES.TABLE.ID',
    size: 4,
    typeColumn: TYPE_COLUMN.ID,
    dataKey: 'purchase_reference',
  },
  {
    label: 'MERCHANT_MANAGER.PURCHASES.TABLE.MERCHANT_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'merchant_reference',
    isSortable: true,
  },
  {
    label: 'MERCHANT_MANAGER.PURCHASES.TABLE.TICKET',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'ticket',
  },
  {
    label: 'MERCHANT_MANAGER.PURCHASES.TABLE.DOWN_PAYMENT',
    size: 4,
    typeColumn: TYPE_COLUMN.CURRENCY,
    dataKey: 'downpayment',
  },
  {
    label: 'MERCHANT_MANAGER.PURCHASES.TABLE.CREDIT_ID',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'loan_reference',
    isSortable: true,
  },
  {
    label: 'MERCHANT_MANAGER.PURCHASES.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
  },
  {
    label: 'MERCHANT_MANAGER.PURCHASES.TABLE.CREATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
  },
];
