import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import {
  TABLE_MERCHANTS_DEFAULT,
  TABLE_MERCHANT_PROFILE_MOVEMENTS_DEFAULT,
  TABLE_MERCHANT_PROFILE_PURCHASES_DEFAULT,
} from '../tables-default/merchants.table';

export const TABLE_MERCHANTS_MCA: TableHeader[] = TABLE_MERCHANTS_DEFAULT;

export const TABLE_MERCHANT_PROFILE_PURCHASES_MCA: TableHeader[] =
  TABLE_MERCHANT_PROFILE_PURCHASES_DEFAULT;

export const TABLE_MERCHANT_PROFILE_MOVEMENTS_MCA: TableHeader[] =
  TABLE_MERCHANT_PROFILE_MOVEMENTS_DEFAULT;
