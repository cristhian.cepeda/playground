import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_MOVEMENTS_DEFAULT } from '../tables-default/movements.table';

export const TABLE_MOVEMENTS_MCA: TableHeader[] = TABLE_MOVEMENTS_DEFAULT;
