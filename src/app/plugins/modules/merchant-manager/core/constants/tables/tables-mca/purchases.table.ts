import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PURCHASES_DEFAULT } from '../tables-default/purchases.table';

export const TABLE_PURCHASES_MCA: TableHeader[] = TABLE_PURCHASES_DEFAULT;
