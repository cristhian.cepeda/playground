import {
  LOAN_MANAGER_FEATURE,
  MERCHANT_MANAGER_FEATURE,
} from './feature.constants';

export const MERCHANT_MANAGER_URLS = {
  OVERVIEW: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.OVERVIEW.PATH}`,
  MERCHANTS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.PATH}`,
  MERCHANT_PROFILE: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.SUB_PAGES.MERCHANT_PROFILE.PATH}`,
  PURCHASES: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.PURCHASES.PATH}`,
  PAYOUTS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.PAYOUTS.PATH}`,
  MOVEMENTS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MOVEMENTS.PATH}`,
  MERCHANT_GROUPS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}`,
  MERCHANT_GROUP_PROFILE: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.MERCHANT_GROUP_PROFILE.PATH}`,
  MERCHANT_GROUP_PROFILE_ADD_OR_REMOVE_MERCHANTS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.MERCHANT_GROUP_PROFILE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.MERCHANT_GROUP_PROFILE.SUB_PAGES.ADD_OR_REMOVE_MERCHANTS.PATH}`,
  MERCHANT_GROUP_PROFILE_ADD_OR_REMOVE_OFFERS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.MERCHANT_GROUP_PROFILE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.MERCHANT_GROUP_PROFILE.SUB_PAGES.ADD_OR_REMOVE_OFFERS.PATH}`,
  NEW_MERCHANT_GROUP_DETAILS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.SUB_PAGES.NEW_GROUP_DETAILS.PATH}`,
  NEW_MERCHANT_GROUP_MERCHANTS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.SUB_PAGES.NEW_GROUP_MERCHANTS.PATH}`,
  NEW_MERCHANT_GROUP_OFFERS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.SUB_PAGES.NEW_GROUP_OFFERS.PATH}`,
  NEW_MERCHANT_GROUP_SUCCESS: `/${MERCHANT_MANAGER_FEATURE.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.PATH}/${MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP.SUB_PAGES.NEW_GROUP_SUCCESS.PATH}`,
};

export const LOAN_MANAGER_URLS = {
  LOANS: `/${LOAN_MANAGER_FEATURE.PATH}/${LOAN_MANAGER_FEATURE.PAGES.LOANS.PATH}`,
  LOAN_PROFILE: `/${LOAN_MANAGER_FEATURE.PATH}/${LOAN_MANAGER_FEATURE.PAGES.LOANS.PATH}/${LOAN_MANAGER_FEATURE.PAGES.LOANS.SUB_PAGES.LOAN_PROFILE.PATH}`,
};
