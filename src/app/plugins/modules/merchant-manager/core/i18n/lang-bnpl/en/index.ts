import ENGLISH_DEFAULT_MERCHANT_MANAGER from '@app/plugins/modules/merchant-manager/core/i18n/lang-default/en';
const ENGLISH_BNPL_MERCHANT_MANAGER = {
  ...ENGLISH_DEFAULT_MERCHANT_MANAGER,
};
export default ENGLISH_BNPL_MERCHANT_MANAGER;
