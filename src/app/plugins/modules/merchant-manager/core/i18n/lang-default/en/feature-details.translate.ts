import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const MERCHANT_MANAGER_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Merchant Manager',
  _MENU: {
    OVERVIEW: { TITLE: 'Overview' },
    MERCHANTS: {
      TITLE: 'Merchants',
      TOOLTIP_MESSAGE:
        'View all current merchants associated in your BNPL credit operation.',
    },
    PURCHASES: {
      TITLE: 'Purchases',
      TOOLTIP_MESSAGE:
        'View all purchases made through your BNPL credit operation.',
    },
    PAYOUTS: {
      TITLE: 'Payouts',
      TOOLTIP_MESSAGE:
        'View all payouts, the remuneration owed to merchants for each <br />purchase made through your BNPL credit operation.',
    },
    MOVEMENTS: {
      TITLE: 'Movements',
      TOOLTIP_MESSAGE:
        'View all movements, the amount paid to the merchants for all <br />purchases made during the set repayment frequency.',
    },
    MERCHANT_GROUPS: {
      TITLE: 'Merchant Groups',
      TOOLTIP_MESSAGE:
        'Review and manage how merchants are grouped within your credit operation.',
    },
  },
};
export default MERCHANT_MANAGER_FEATURE_DETAILS_TRANSLATE;
