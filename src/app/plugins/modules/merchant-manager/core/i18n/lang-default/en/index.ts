import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';
import MERCHANT_MANAGER_FEATURE_DETAILS_TRANSLATE from './feature-details.translate';
import MERCHANT_GROUPS_TRANSLATE from './merchant-groups.translate';
import MERCHANT_TRANSLATE from './merchants.translate';
import MOVEMENTS_TRANSLATE from './movements.translate';
import OVERVIEW_TRANSLATE from './overview.translate';
import PAYOUTS_TRANSLATE from './payouts.translate';
import PURCHASES_TRANSLATE from './purchases.translate';

const ENGLISH_DEFAULT_MERCHANT_MANAGER: FeatureDetailsTranslate = {
  ...MERCHANT_MANAGER_FEATURE_DETAILS_TRANSLATE,
  ...OVERVIEW_TRANSLATE,
  ...MERCHANT_TRANSLATE,
  ...PURCHASES_TRANSLATE,
  ...PAYOUTS_TRANSLATE,
  ...MOVEMENTS_TRANSLATE,
  ...MERCHANT_GROUPS_TRANSLATE,
};
export default ENGLISH_DEFAULT_MERCHANT_MANAGER;
