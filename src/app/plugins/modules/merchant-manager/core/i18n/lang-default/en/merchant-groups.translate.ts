import { MerchantGroupsPageTranslate } from '../../models/merchant-groups-translate';

const MERCHANT_GROUPS_TRANSLATE: MerchantGroupsPageTranslate = {
  MERCHANT_GROUPS: {
    TITLE: 'Merchant Groups',
    DESCRIPTION:
      'Create new and review existing merchant groups. Filter by merchant name and group name.',
    TOTAL_TOOLTIP:
      'Total number of merchant groups currently in your credit operation.',
    FILTERS: {
      SEARCH_BAR: 'Group name',
    },
    BTN_CREATE_NEW_GROUPS: 'Create new group',
  },
  MERCHANT_GROUP_PROFILE: {
    GROUP_PROFILE: {
      BREADCRUMB: 'Group Profile',
      TITLE: 'Group Profile',
      DESCRIPTION:
        'Manage and review your merchant group. Take a closer look into existing merchants <br />and their associated offers.',
      INFO: {
        CREATED_AT: 'Date created:',
        UPDATED_AT: 'Last modified:',
        DELETE_GROUP: 'Delete group',
      },
      MERCHANTS_CARD: {
        TITLE: 'Merchants',
        SEARCH_BAR: 'Merchant Name',
        ADD_OR_REMOVE: 'Add or remove merchant',
      },
      OFFERS_CARD: {
        TITLE: 'Offers',
        OFFERS: 'Offers',
        COMMISTION: 'Commission',
        TAX: 'Tax',
        ADD_OR_REMOVE: 'Add or remove offer',
        BTN_EDIT: 'Edit values',
        BTN_CONFIRM: 'Confirm',
      },
      DELETE_MODAL: {
        TITLE: 'Are you sure you want to delete <br />this merchant group?',
        DESCRIPTION:
          'By deleting this group, merchants will not be able to issue anymore <br />purchases with the associated offers. Already existing credits with <br />these merchants will be affected by this action.',
        BTN_CANCEL: 'Cancel',
        BTN_CONFIRM: 'Confirm',
      },
    },
    ADD_OR_REMOVE_MERCHANTS: {
      BREADCRUMB: 'Add or remove merchant',
      TITLE: 'Add or remove merchant',
      DESCRIPTION:
        'Add or remove merchants, remember that all group settings will apply to those <br />who are active in the group',
      BTN_CANCEL: 'Cancel',
      BTN_CONFIRM: 'Confirm',
      MERCHANT_BOX_NOTE: 'Merchants included will be appear in this box',
      MERCHANT_LIST_FILTERS: {
        SEARCH: 'Merchant Name, Merchant ID',
        MCC_ID: 'MCC ID',
      },
      MERCHANT_LIST_TABLE: {
        MERCHANT_NAME: 'Merchant Name',
        MCC: 'MCC',
        MERCHANT_ID: 'Merchant ID',
        GROUP: 'Group',
        ADD: 'Add',
      },
      REMOVE_MERCHANT_MODAL: {
        TITLE: 'Are you sure you want to <br />remove this merchant?',
        DESCRIPTION:
          'By removing this merchants, merchants will not be able to issue <br />anymore purchases with the associated offers. Already existing <br />credits with these merchants will not be affected by this action.',
        BTN_CANCEL: 'Cancel',
        BTN_CONFIRM: 'Confirm',
      },
    },
    ADD_OR_REMOVE_OFFERS: {
      BREADCRUMB: 'Add or remove offers',
      TITLE: 'Add or remove offers',
      DESCRIPTION:
        'Add or remove offers, remember that all group active offers will apply to those <br />merchants who are active in the group',
      FIRTERS: {
        AMOUNT: 'Amount',
        NUMBER_OF_INSTALLMENTS: 'Number of Installments',
      },
      CURRENT_OFFERS_TITLE: 'Current Offers',
      NEW_OFFERS_TITLE: 'New Offers',
      OFFER_CARD: {
        OFFER: 'Offers',
        COMMISSIONS: 'Commissions',
        TAXES: 'Taxes',
        ITEM: {
          AMOUNT: 'Amount',
          NUMBER_OF_INSTALLMENTS: 'Number of Installments',
        },
      },
      BTN_CONFIRM: 'Confirm',
      REMOVE_OFFER_MODAL: {
        TITLE: 'Are you sure you want to <br />remove this offer?',
        DESCRIPTION:
          'By removing this offer, merchants will not be able to issue anymore <br />purchases with this configuration. Already existing credits with <br />these offer will not be affected by this action.',
        BTN_CANCEL: 'Cancel',
        BTN_CONFIRM: 'Confirm',
      },
    },
  },
  NEW_GROUP: {
    BREADCRUMB: 'Create group',
    TITLE: 'New Group',
    DETAILS: {
      DESCRIPTION:
        'Describe your Merchant Group according to the grouping criteria you decide. <br />This will allow others to understand and identify these merchants within MO Manage.',
      GROUP_NAME: 'Group name:',
      SHORT_DESCRIPTION: 'Short description:',
      GROUP_DESCRIPTION: 'Description:',
      BTN_CANCEL: 'Cancel',
      BTN_CONFIRM: 'Confirm ',
      WORDS: 'words',
    },
    MERCHANTS: {
      DESCRIPTION:
        'Set up and configure a new group for your merchants. Our configurations settings <br />will give you the flexibility to assigned differents commissions to different offers.',
      BTN_CANCEL: 'Cancel',
      BTN_CONTINUE: 'Continue',
      MERCHANT_BOX_NOTE: 'Merchants included will be appear in this box',
      MERCHANT_LIST_TITLE: 'Create the group by selecting merchants',
      MERCHANT_LIST_DESCRIPTION: `Choose the merchants you wish to include in this group. Keep in mind that a <br />merchant can't belong to more than one group at any given time.`,
      MERCHANT_LIST_FILTERS: {
        SEARCH: 'Merchant Name, Merchant ID',
        MCC_ID: 'MCC ID',
      },
      MERCHANT_LIST_TABLE: {
        MERCHANT_NAME: 'Merchant Name',
        MCC: 'MCC',
        MERCHANT_ID: 'Merchant ID',
        GROUP: 'Group',
        ADD: 'Add',
      },
    },
    OFFERS: {
      DESCRIPTION:
        'Set up and configure a new group for your merchants. Our configurations settings <br />will give you the flexibility to assigned differents commissions to different offers.',
      NOTE: 'Active offers for this group merchants and assign commissions accordingly.',
      FIRTERS: {
        AMOUNT_TOOLTIP:
          'This represents the average purchase amount expected for each offer.',
        AMOUNT: 'Amount',
        NUMBER_OF_INSTALLMENTS: 'Number of Installments',
      },
      OFFER_CARD: {
        OFFER: 'Offers',
        COMMISSIONS: 'Commissions',
        TAXES: 'Taxes',
        ITEM: {
          AMOUNT: 'Amount',
          NUMBER_OF_INSTALLMENTS: 'Number of Installments',
        },
      },
      BTN_CANCEL: 'Cancel',
      BTN_CONTINUE: 'Continue',
    },
    CONGRATULATIONS: {
      TITLE: 'Congratulations',
      DESCRIPTION:
        'Your new <b>Merchant Group</b> <span class="primary-text">{{ merchant_name }}</span> is ready. <br />With <span class="primary-text">{{ merchants_number }}</span> merchants included and <span class="primary-text">{{ offers_number }}</span> related offers.',
      BTN_BACK_MERCHANT_GROUPS: 'Back to Merchant Groups',
    },
    ERROR_MODAL: {
      TITLE: '¡Ooops!',
      DESCRIPTION:
        'There was a problem creating the group, <br />please try again later.',
    },
  },
};
export default MERCHANT_GROUPS_TRANSLATE;
