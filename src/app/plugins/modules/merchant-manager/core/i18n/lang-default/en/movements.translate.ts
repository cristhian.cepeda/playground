import { MovementsPageTranslate } from '../../models/movements-translate';

const MOVEMENTS_TRANSLATE: MovementsPageTranslate = {
  MOVEMENTS: {
    TITLE: 'Movements',
    DESCRIPTION:
      'Review all serviced merchants movements associated to a payout. Filter and <br />download the movements database to quickly navigate and analyze their <br />purchases.',
    TOTAL_TOOLTIP:
      'Total number of movements paid to the merchants for all purchases made during the set repayment frequency.',
    FILTERS: {
      TITLE: 'Search tools',
      DESCRIPTION:
        'Filter your credit database by using default and advanced filters or use <br />free text search for specific fields.',
      SEARCH_BAR: 'Merchant Name, Payout ID',
      DATE_CREATED_TITLE: 'Date created',
      DATE_CREATED_PLACEHOLDER: 'Choose a range',
      TYPE: 'Type',
    },
    TABLE: {
      CREATED_AT: 'Date created',
      PURCHASE_ID: 'Purchase ID',
      MERCHANT_ID: 'Merchant  ID',
      MERCHANT_NAME: 'Merchant  name',
      PAYOUT_ID: 'Payout ID',
      AMOUNT: 'Amount',
      COMMISSION: 'Commission',
      TAX: 'Tax',
      TYPE: 'Type',
    },
  },
};
export default MOVEMENTS_TRANSLATE;
