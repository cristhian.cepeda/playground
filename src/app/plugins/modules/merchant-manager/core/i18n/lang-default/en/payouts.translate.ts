import { PayoutsPageTranslate } from '../../models/payouts-translate';

const PAYOUTS_TRANSLATE: PayoutsPageTranslate = {
  PAYOUTS: {
    TITLE: 'Payouts',
    DESCRIPTION:
      'Review all serviced merchants payouts. Filter and download the payout database to <br />quickly navigate and analyze their purchases.',
    TOTAL_TOOLTIP:
      'Total number of payouts owed to merchant for each purchase made through the BNPL operation.',
    FILTERS: {
      TITLE: 'Search tools',
      DESCRIPTION:
        'Filter your credit database by using default and advanced filters or use <br />free text search for specific fields.',
      SEARCH_BAR: 'Merchant Name, Merchant ID ',
      DATE_TITLE: 'Date',
      DATE_PLACEHOLDER: 'Choose a range',
    },
    TABLE: {
      DATE: 'Date',
      ID: 'Payout ID',
      MERCHANT_ID: 'Merchant ID',
      MERCHANT_NAME: 'Merchant name',
      MERCHANT_GROUP: 'Merchant group',
      TOTAL_AMOUNT: 'Total amount',
    },
  },
};
export default PAYOUTS_TRANSLATE;
