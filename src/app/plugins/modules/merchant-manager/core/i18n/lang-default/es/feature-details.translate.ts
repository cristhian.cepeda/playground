import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const MERCHANT_MANAGER_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Administrador de comercios',
  _MENU: {
    OVERVIEW: { TITLE: 'Visión general' },
    MERCHANTS: {
      TITLE: 'Comercios',
      TOOLTIP_MESSAGE:
        'Visualiza todos los comercios actuales asociados en tu operación de crédito BNPL.',
    },
    PURCHASES: {
      TITLE: 'Compras',
      TOOLTIP_MESSAGE:
        'Consulta todas las compras realizadas a través de tu operación de crédito BNPL.',
    },
    PAYOUTS: {
      TITLE: 'Liquidación',
      TOOLTIP_MESSAGE:
        'Ver todos los pagos, la remuneración adeudada a los comerciantes por cada <br />compra realizada a través de su operación de crédito BNPL.',
    },
    MOVEMENTS: {
      TITLE: 'Movimientos',
      TOOLTIP_MESSAGE:
        'Ver todos los movimientos, el monto pagado a los comerciantes por <br />todas las compras realizadas durante la frecuencia de pago establecida.',
    },
    MERCHANT_GROUPS: {
      TITLE: 'Grupos de Comercios',
      TOOLTIP_MESSAGE:
        'Revise y administre cómo se agrupan los comerciantes dentro de su operación de crédito.',
    },
  },
};
export default MERCHANT_MANAGER_FEATURE_DETAILS_TRANSLATE;
