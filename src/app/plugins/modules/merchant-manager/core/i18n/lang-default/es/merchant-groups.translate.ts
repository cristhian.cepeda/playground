import { MerchantGroupsPageTranslate } from '../../models/merchant-groups-translate';

const MERCHANT_GROUPS_TRANSLATE: MerchantGroupsPageTranslate = {
  MERCHANT_GROUPS: {
    TITLE: 'Grupos de Comercios',
    DESCRIPTION:
      'Cree nuevos grupos de comercios y revise los existentes. Filtre por nombre de comercio y nombre de grupo.',
    TOTAL_TOOLTIP:
      'Número total de grupos de comercios actualmente en su operación de crédito.',
    FILTERS: {
      SEARCH_BAR: 'Nombre del grupo',
    },
    BTN_CREATE_NEW_GROUPS: 'Crear nuevo grupo',
  },
  MERCHANT_GROUP_PROFILE: {
    GROUP_PROFILE: {
      BREADCRUMB: 'Perfil de grupo',
      TITLE: 'Perfil de grupo',
      DESCRIPTION:
        'Administre y revise su grupo de comercios. Eche un vistazo más de cerca a los comercios existentes <br />y sus ofertas asociadas.',
      INFO: {
        CREATED_AT: 'Fecha de creacion:',
        UPDATED_AT: 'Última modificación:',
        DELETE_GROUP: 'Eliminar grupo',
      },
      MERCHANTS_CARD: {
        TITLE: 'Comercios',
        SEARCH_BAR: 'Nombre del comercio',
        ADD_OR_REMOVE: 'Agregar o eliminar comercio',
      },
      OFFERS_CARD: {
        TITLE: 'Ofertas',
        OFFERS: 'Ofertas',
        COMMISTION: 'Comisión',
        TAX: 'Impuesto',
        ADD_OR_REMOVE: 'Agregar o quitar oferta',
        BTN_EDIT: 'Editar valores',
        BTN_CONFIRM: 'Confirmar',
      },
      DELETE_MODAL: {
        TITLE:
          '¿Está seguro de que desea eliminar <br />este grupo de comercios?',
        DESCRIPTION:
          'Al eliminar este grupo, los comercios no podrán realizar más <br />compras con las ofertas asociadas. Los créditos ya existentes con <br />estos comercios se verán afectados por esta acción.',
        BTN_CANCEL: 'Cancelar',
        BTN_CONFIRM: 'Confirmar',
      },
    },
    ADD_OR_REMOVE_MERCHANTS: {
      BREADCRUMB: 'Agregar o eliminar comercio',
      TITLE: 'Agregar o eliminar comercio',
      DESCRIPTION:
        'Agregue o elimine comercios, recuerde que todas las configuraciones del grupo se aplicarán a aquellos <br />que están activos en el grupo',
      BTN_CANCEL: 'Cancelar',
      BTN_CONFIRM: 'Confirmar',
      MERCHANT_BOX_NOTE: 'Los comercios incluidos aparecerán en este cuadro',
      MERCHANT_LIST_FILTERS: {
        SEARCH: 'Nombre del comercio, ID del comercio',
        MCC_ID: 'MCC ID',
      },
      MERCHANT_LIST_TABLE: {
        MERCHANT_NAME: 'Nombre del comercio',
        MCC: 'MCC',
        MERCHANT_ID: 'ID del comercio',
        GROUP: 'Grupo',
        ADD: 'Agregar',
      },
      REMOVE_MERCHANT_MODAL: {
        TITLE: '¿Estás seguro de que quieres <br />eliminar a este comercio?',
        DESCRIPTION:
          'Al eliminar a estos comercios, los comercios no podrán realizar <br />más compras con las ofertas asociadas. Los créditos ya existentes <br />con estos comercios no se verán afectados por esta acción.',
        BTN_CANCEL: 'Cancelar',
        BTN_CONFIRM: 'Confirmar',
      },
    },
    ADD_OR_REMOVE_OFFERS: {
      BREADCRUMB: 'Agregar o eliminar ofertas',
      TITLE: 'Agregar o eliminar ofertas',
      DESCRIPTION:
        'Agregue o elimine ofertas, recuerde que todas las ofertas activas del grupo se aplicarán a aquellos <br />comercios que estén activos en el grupo',
      FIRTERS: { AMOUNT: 'Monto', NUMBER_OF_INSTALLMENTS: 'Número de plazos' },
      CURRENT_OFFERS_TITLE: 'Ofertas Actuales',
      NEW_OFFERS_TITLE: 'Nuevas ofertas',
      OFFER_CARD: {
        OFFER: 'Ofertas',
        COMMISSIONS: 'Comisiones',
        TAXES: 'Impuestos',
        ITEM: {
          AMOUNT: 'Monto',
          NUMBER_OF_INSTALLMENTS: 'Número de cuotas',
        },
      },
      BTN_CONFIRM: 'Confirmar',
      REMOVE_OFFER_MODAL: {
        TITLE: '¿Está seguro de que desea <br />eliminar esta oferta?',
        DESCRIPTION:
          'Al eliminar esta oferta, los comercios no podrán realizar más compras <br />con esta configuración. Los créditos ya existentes con esta <br />oferta no se verán afectados por esta acción.',
        BTN_CANCEL: 'Cancelar',
        BTN_CONFIRM: 'Confirmar',
      },
    },
  },
  NEW_GROUP: {
    BREADCRUMB: 'Crea un grupo',
    TITLE: 'Nuevo grupo',
    DETAILS: {
      DESCRIPTION:
        'Describa su Grupo de Comercios según los criterios de agrupación que decida. <br />Esto permitirá que otros comprendan e identifiquen a estos comercios dentro de MO Manage.',
      GROUP_NAME: 'Nombre del grupo:',
      SHORT_DESCRIPTION: 'Descripción corta:',
      GROUP_DESCRIPTION: 'Descripción:',
      BTN_CANCEL: 'Cancelar',
      BTN_CONFIRM: 'Confirmar',
      WORDS: 'palabras',
    },
    MERCHANTS: {
      DESCRIPTION:
        'Instale y configure un nuevo grupo para sus comercios. Nuestros ajustes de configuración <br />le darán la flexibilidad de asignar diferentes comisiones a diferentes ofertas.',
      BTN_CANCEL: 'Cancelar',
      BTN_CONTINUE: 'Continuar',
      MERCHANT_BOX_NOTE: 'Los comercios incluidos aparecerán en este cuadro',
      MERCHANT_LIST_TITLE: 'Crear el grupo seleccionando comercios',
      MERCHANT_LIST_DESCRIPTION:
        'Elija los comercios que desea incluir en este grupo. Tenga en cuenta que un <br />comercio no puede pertenecer a más de un grupo en un momento dado.',
      MERCHANT_LIST_FILTERS: {
        SEARCH: 'Nombre del comercio, ID del comercio',
        MCC_ID: 'MCC ID',
      },
      MERCHANT_LIST_TABLE: {
        MERCHANT_NAME: 'Nombre del comercio',
        MCC: 'MCC',
        MERCHANT_ID: 'ID del comercio',
        GROUP: 'Grupo',
        ADD: 'Agregar',
      },
    },
    OFFERS: {
      DESCRIPTION:
        'Instale y configure un nuevo grupo para sus comercios. Nuestros ajustes de configuración <br />le darán la flexibilidad de asignar diferentes comisiones a diferentes ofertas.',
      NOTE: 'Active ofertas para este grupo de comercios y asigne comisiones en consecuencia.',
      FIRTERS: {
        AMOUNT_TOOLTIP:
          'Esto representa el monto de compra promedio esperado para cada oferta.',
        AMOUNT: 'Monto',
        NUMBER_OF_INSTALLMENTS: 'Número de cuotas',
      },
      OFFER_CARD: {
        OFFER: 'Ofertas',
        COMMISSIONS: 'Comisiones',
        TAXES: 'Impuestos',
        ITEM: {
          AMOUNT: 'Monto',
          NUMBER_OF_INSTALLMENTS: 'Número de cuotas',
        },
      },
      BTN_CANCEL: 'Cancelar',
      BTN_CONTINUE: 'Continuar',
    },
    CONGRATULATIONS: {
      TITLE: 'Felicidades',
      DESCRIPTION:
        'Su nuevo <b>grupo de comercios</b> <span class="primary-text">{{ merchant_name }}</span> está listo. <br />Con <span class="primary-text">{{ merchants_number }}</span> comercios incluidos y <span class="primary-text">{{ offers_number }}</span> ofertas relacionadas.',
      BTN_BACK_MERCHANT_GROUPS: 'Volver a Grupos de comercios',
    },
    ERROR_MODAL: {
      TITLE: '¡Ooops!',
      DESCRIPTION:
        'Hubo un problema al crear el grupo, <br />inténtalo de nuevo más tarde.',
    },
  },
};
export default MERCHANT_GROUPS_TRANSLATE;
