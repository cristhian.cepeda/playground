import { MovementsPageTranslate } from '../../models/movements-translate';

const MOVEMENTS_TRANSLATE: MovementsPageTranslate = {
  MOVEMENTS: {
    TITLE: 'Movimientos',
    DESCRIPTION:
      'Revise todos los movimientos de comercios atendidos asociados a un pago. Filtre y <br />descargue la base de datos de movimientos para navegar y analizar rápidamente sus <br />compras.',
    TOTAL_TOOLTIP:
      'Número total de movimientos pagados a los comercios por todas las compras realizadas durante la frecuencia de devolución establecida.',
    FILTERS: {
      TITLE: 'Herramientas de búsqueda',
      DESCRIPTION:
        'Filtre su base de datos de crédito utilizando filtros predeterminados y avanzados o utilice la búsqueda de <br />texto libre para campos específicos.',
      SEARCH_BAR: 'Nombre del comercio, ID de liquidación',
      DATE_CREATED_TITLE: 'Fecha de creación',
      DATE_CREATED_PLACEHOLDER: 'Elige un rango',
      TYPE: 'Tipo',
    },
    TABLE: {
      CREATED_AT: 'Fecha de creación',
      PURCHASE_ID: 'ID de compra',
      MERCHANT_ID: 'ID del comercio',
      MERCHANT_NAME: 'Nombre del comercio',
      PAYOUT_ID: 'ID de liquidación',
      AMOUNT: 'Monto',
      COMMISSION: 'Comisión',
      TAX: 'Impuesto',
      TYPE: 'Tipo',
    },
  },
};
export default MOVEMENTS_TRANSLATE;
