import { PayoutsPageTranslate } from '../../models/payouts-translate';

const PAYOUTS_TRANSLATE: PayoutsPageTranslate = {
  PAYOUTS: {
    TITLE: 'Liquidación',
    DESCRIPTION:
      'Revise todos los pagos de comercios atendidos. Filtre y descargue la base de datos de pagos para <br />navegar y analizar rápidamente sus compras.',
    TOTAL_TOOLTIP:
      'Número total de pagos adeudados al comerciante por cada compra realizada a través de la operación BNPL.',
    FILTERS: {
      TITLE: 'Herramientas de búsqueda',
      DESCRIPTION:
        'Filtre su base de datos de crédito utilizando filtros predeterminados y avanzados o utilice <br />la búsqueda de texto libre para campos específicos.',
      SEARCH_BAR: 'Nombre del comercio, ID del comercio',
      DATE_TITLE: 'Fecha',
      DATE_PLACEHOLDER: 'Elige un rango',
    },
    TABLE: {
      DATE: 'Fecha',
      ID: 'ID de liquidación',
      MERCHANT_ID: 'ID del comercio',
      MERCHANT_NAME: 'Nombre del comercio',
      MERCHANT_GROUP: 'Grupo de comercios',
      TOTAL_AMOUNT: 'Monto total',
    },
  },
};
export default PAYOUTS_TRANSLATE;
