import SPANISH_DEFAULT_MERCHANT_MANAGER from '@app/plugins/modules/merchant-manager/core/i18n/lang-default/es';

const SPANISH_MICRO_CREDIT_MERCHANT_MANAGER = {
  ...SPANISH_DEFAULT_MERCHANT_MANAGER,
};
export default SPANISH_MICRO_CREDIT_MERCHANT_MANAGER;
