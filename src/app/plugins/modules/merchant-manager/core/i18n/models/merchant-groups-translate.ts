export interface MerchantGroupsPageTranslate {
  MERCHANT_GROUPS: MerchantGroupsTranslate;
  MERCHANT_GROUP_PROFILE: MerchantGroupProfilePagesTranslate;
  NEW_GROUP: MerchantGroupsNewGroupsTranslate;
}
/**
 * MERCHANT GROUPS
 */
export interface MerchantGroupsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: MerchantGroupsFiltersTranslate;
  BTN_CREATE_NEW_GROUPS: string;
}
export interface MerchantGroupsFiltersTranslate {
  SEARCH_BAR: string;
}

/**
 * MERCHANT GROUP PROFILE
 */

export interface MerchantGroupProfilePagesTranslate {
  GROUP_PROFILE: MerchantGroupProfileTranslate;
  ADD_OR_REMOVE_MERCHANTS: MerchantGroupProfileAddOrRemoveMerchantTranslate;
  ADD_OR_REMOVE_OFFERS: MerchantGroupProfileAddOrRemoveOfferTranslate;
}

// PROFILE

export interface MerchantGroupProfileTranslate {
  BREADCRUMB: string;
  TITLE: string;
  DESCRIPTION: string;
  INFO: MerchantGroupProfileInfoTranslate;
  MERCHANTS_CARD: MerchantGroupProfileMerchantsCardTranslate;
  OFFERS_CARD: MerchantGroupProfileOffersCardTranslate;
  DELETE_MODAL: MerchantGroupProfileDeleteModalTranslate;
}

export interface MerchantGroupProfileInfoTranslate {
  CREATED_AT: string;
  UPDATED_AT: string;
  DELETE_GROUP: string;
}

export interface MerchantGroupProfileMerchantsCardTranslate {
  TITLE: string;
  SEARCH_BAR: string;
  ADD_OR_REMOVE: string;
}

export interface MerchantGroupProfileOffersCardTranslate {
  TITLE: string;
  OFFERS: string;
  COMMISTION: string;
  TAX: string;
  ADD_OR_REMOVE: string;
  BTN_EDIT: string;
  BTN_CONFIRM: string;
}

export interface MerchantGroupProfileDeleteModalTranslate {
  TITLE: string;
  DESCRIPTION: string;
  BTN_CANCEL: string;
  BTN_CONFIRM: string;
}

// ADD OR REMOVE MERCHANT

export interface MerchantGroupProfileAddOrRemoveMerchantTranslate {
  BREADCRUMB: string;
  TITLE: string;
  DESCRIPTION: string;
  BTN_CANCEL: string;
  BTN_CONFIRM: string;
  MERCHANT_BOX_NOTE: string;
  MERCHANT_LIST_FILTERS: MerchantGroupProfileAddOrRemoveMerchantFiltersTranslate;
  MERCHANT_LIST_TABLE: MerchantGroupProfileAddOrRemoveMerchantsTableTranslate;
  REMOVE_MERCHANT_MODAL: MerchantGroupProfileAddOrRemoveMerchantsModalTranslate;
}

export interface MerchantGroupProfileAddOrRemoveMerchantFiltersTranslate {
  SEARCH: string;
  MCC_ID: string;
}

export interface MerchantGroupProfileAddOrRemoveMerchantsTableTranslate {
  MERCHANT_NAME: string;
  MCC: string;
  MERCHANT_ID: string;
  GROUP: string;
  ADD: string;
}

export interface MerchantGroupProfileAddOrRemoveMerchantsModalTranslate {
  TITLE: string;
  DESCRIPTION: string;
  BTN_CANCEL: string;
  BTN_CONFIRM: string;
}

// ADD OR REMOVE OFFER
export interface MerchantGroupProfileAddOrRemoveOfferTranslate {
  BREADCRUMB: string;
  TITLE: string;
  DESCRIPTION: string;
  FIRTERS: MerchantGroupProfileAddOrRemoveOfferFiltersTranslate;
  CURRENT_OFFERS_TITLE: string;
  NEW_OFFERS_TITLE: string;
  OFFER_CARD: MerchantGroupProfileAddOrRemoveOfferOfferCardTranslate;
  BTN_CONFIRM: string;
  REMOVE_OFFER_MODAL: MerchantGroupProfileAddOrRemoveOfferModalTranslate;
}

export interface MerchantGroupProfileAddOrRemoveOfferFiltersTranslate {
  AMOUNT: string;
  NUMBER_OF_INSTALLMENTS: string;
}
export interface MerchantGroupProfileAddOrRemoveOfferOfferCardTranslate {
  OFFER: string;
  COMMISSIONS: string;
  TAXES: string;
  ITEM: MerchantGroupProfileAddOrRemoveOfferOfferCardItemTranslate;
}

export interface MerchantGroupProfileAddOrRemoveOfferOfferCardItemTranslate {
  AMOUNT: string;
  NUMBER_OF_INSTALLMENTS: string;
}

export interface MerchantGroupProfileAddOrRemoveOfferModalTranslate {
  TITLE: string;
  DESCRIPTION: string;
  BTN_CANCEL: string;
  BTN_CONFIRM: string;
}

/**
 * NEW MERCHANT GROUP
 */
export interface MerchantGroupsNewGroupsTranslate {
  BREADCRUMB: string;
  TITLE: string;
  DETAILS: MerchantGroupsNewGroupsDetailsTranslate;
  MERCHANTS: MerchantGroupsNewGroupsMerchantsTranslate;
  OFFERS: MerchantGroupsNewGroupsOffersTranslate;
  CONGRATULATIONS: MerchantGroupsNewGroupsCongratulationsTranslate;
  ERROR_MODAL: MerchantGroupsNewGroupsErrorModalTranslate;
}

export interface MerchantGroupsNewGroupsDetailsTranslate {
  DESCRIPTION: string;
  GROUP_NAME: string;
  SHORT_DESCRIPTION: string;
  GROUP_DESCRIPTION: string;
  BTN_CANCEL: string;
  BTN_CONFIRM: string;
  WORDS: string;
}

export interface MerchantGroupsNewGroupsMerchantsTranslate {
  DESCRIPTION: string;
  BTN_CANCEL: string;
  BTN_CONTINUE: string;
  MERCHANT_BOX_NOTE: string;
  MERCHANT_LIST_TITLE: string;
  MERCHANT_LIST_DESCRIPTION: string;
  MERCHANT_LIST_FILTERS: MerchantGroupsNewGroupsMerchantsFiltersTranslate;
  MERCHANT_LIST_TABLE: MerchantGroupsNewGroupsMerchantsTableTranslate;
}

export interface MerchantGroupsNewGroupsMerchantsFiltersTranslate {
  SEARCH: string;
  MCC_ID: string;
}

export interface MerchantGroupsNewGroupsMerchantsTableTranslate {
  MERCHANT_NAME: string;
  MCC: string;
  MERCHANT_ID: string;
  GROUP: string;
  ADD: string;
}

export interface MerchantGroupsNewGroupsOffersTranslate {
  DESCRIPTION: string;
  NOTE: string;
  FIRTERS: MerchantGroupsNewGroupsOffersFiltersTranslate;
  OFFER_CARD: MerchantGroupsNewGroupsOffersOfferCardTranslate;
  BTN_CANCEL: string;
  BTN_CONTINUE: string;
}

export interface MerchantGroupsNewGroupsOffersFiltersTranslate {
  AMOUNT_TOOLTIP: string;
  AMOUNT: string;
  NUMBER_OF_INSTALLMENTS: string;
}

export interface MerchantGroupsNewGroupsOffersOfferCardTranslate {
  OFFER: string;
  COMMISSIONS: string;
  TAXES: string;
  ITEM: MerchantGroupsNewGroupsOffersOfferCardItemTranslate;
}

export interface MerchantGroupsNewGroupsOffersOfferCardItemTranslate {
  AMOUNT: string;
  NUMBER_OF_INSTALLMENTS: string;
}

export interface MerchantGroupsNewGroupsCongratulationsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  BTN_BACK_MERCHANT_GROUPS: string;
}

export interface MerchantGroupsNewGroupsErrorModalTranslate {
  TITLE: string;
  DESCRIPTION: string;
}
