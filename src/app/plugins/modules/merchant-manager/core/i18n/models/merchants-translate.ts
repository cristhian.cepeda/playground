import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface MerchantsPageTranslate {
  MERCHANTS: MerchantsTranslate;
  MERCHANT_PROFILE: MerchantProfileTranslate;
}
export interface MerchantsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: MerchantsFiltersTranslate;
  TABLE: MerchantsTableTranslate;
}
export interface MerchantsFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  STATUS: string;
  DATE_CREATED_TITLE: string;
  DATE_CREATED_PLACEHOLDER: string;
  SALES_TITLE: string;
  SALES_LESS: string;
  SALES_GRATHER: string;
  PAYOUT_TITLE: string;
  PAYOUT_LESS: string;
  PAYOUT_GRATHER: string;
}

export interface MerchantsTableTranslate {
  ID: string;
  MERCHANT_NAME: string;
  NATIONAL_ID: string;
  MERCHANT_GROUP: string;
  STATUS: string;
  SALES: string;
  TOTAL_PAYOUT: string;
  CREATED_AT: string;
}
export interface MerchantProfileTranslate {
  TITLE: string;
  DESCRIPTION: string;
  INFO: MerchantProfileInfoTranslate;
  SUMMARY: MerchantProfileSummaryTranslate;
  DETAILS: MerchantProfileDetailsTranslate;
  PURCHASES: MerchantProfilePurchasesTranslate;
  MOVEMENTS: MerchantProfileMovementTranslate;
  GROUP_AND_OFFERS: MerchantProfileGroupOfferTranslate;
}
export interface MerchantProfileInfoTranslate {
  MERCHANT_ID: string;
  CREATED_AT: string;
}

export interface MerchantProfileSummaryTranslate {
  TITLE: string;
  TOTAL_SALES: string;
  TOTAL_NUMBER_OF_SALES: string;
  AVERAGE_TICKET: string;
  AVERAGE_NUMBER_PURCHASES_CUSTOMERS: string;
  MONTH: string;
  LAST_MONTH: string;
}
export interface MerchantProfileDetailsTranslate {
  TITLE: string;
  MERCHANT_NAME: string;
  TYPE_OF_ID: string;
  ID_NUMBER: string;
  ADDRESS: string;
  ONLINE_STORE: string;
  CONTACT_NUMBER: string;
  CONTACT_EMAIL: string;
  TYPE_OF_SERVICE: string;
}

export interface MerchantProfilePurchasesTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TABLE: MerchantProfilePurchasesTableTranslate;
  BTN_VISIT_CREDITS: string;
  BTN_VISIT_PURCHASES: string;
}

export interface MerchantProfilePurchasesTableTranslate {
  PURCHASE_ID: string;
  CREDIT_ID: string;
  AMOUNT: string;
  DATE_OF_PURCHASE: string;
}

export interface MerchantProfileMovementTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TABLE: MerchantProfileMovementTableTranslate;
  BTN_VISIT_TRANSACTION: string;
}

export interface MerchantProfileMovementTableTranslate {
  MOVEMENT_ID: string;
  CONCEPT: string;
  AMOUNT: string;
  DATE_OF_MOVEMENT: string;
}

export interface MerchantProfileGroupOfferTranslate {
  TITLE: string;
  DESCRIPTION: string;
  BTN_VISIT_GROUPS: string;
}
