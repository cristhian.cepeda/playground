import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface MovementsPageTranslate {
  MOVEMENTS: MovementsTranslate;
}
export interface MovementsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: MovementsFiltersTranslate;
  TABLE: MovementsTableTranslate;
}
export interface MovementsFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  DATE_CREATED_TITLE: string;
  DATE_CREATED_PLACEHOLDER: string;
  TYPE: string;
}

export interface MovementsTableTranslate {
  CREATED_AT: string;
  PURCHASE_ID: string;
  MERCHANT_ID: string;
  MERCHANT_NAME: string;
  PAYOUT_ID: string;
  AMOUNT: string;
  COMMISSION: string;
  TAX: string;
  TYPE: string;
}
