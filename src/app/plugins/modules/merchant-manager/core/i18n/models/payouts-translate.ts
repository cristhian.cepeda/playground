import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface PayoutsPageTranslate {
  PAYOUTS: PayoutsTranslate;
}
export interface PayoutsTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: PayoutsFiltersTranslate;
  TABLE: PayoutsTableTranslate;
}
export interface PayoutsFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  DATE_TITLE: string;
  DATE_PLACEHOLDER: string;
}

export interface PayoutsTableTranslate {
  DATE: string;
  ID: string;
  MERCHANT_ID: string;
  MERCHANT_NAME: string;
  MERCHANT_GROUP: string;
  TOTAL_AMOUNT: string;
}
