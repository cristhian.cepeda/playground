import { FiltersTranslate } from '@app/core/i18n/models/app/filters-translate';

export interface PurchasesPageTranslate {
  PURCHASES: PurchasesTranslate;
}
export interface PurchasesTranslate {
  TITLE: string;
  DESCRIPTION: string;
  TOTAL_TOOLTIP: string;
  FILTERS: PurchasesFiltersTranslate;
  TABLE: PurchasesTableTranslate;
}
export interface PurchasesFiltersTranslate extends FiltersTranslate {
  SEARCH_BAR: string;
  DATE_CREATED_TITLE: string;
  DATE_CREATED_PLACEHOLDER: string;
  TICKETS_TITLE: string;
  TICKETS_LESS: string;
  TICKETS_GRATHER: string;
  DOWN_PAYMENT_TITLE: string;
  DOWN_PAYMENT_LESS: string;
  DOWN_PAYMENT_GRATHER: string;
}

export interface PurchasesTableTranslate {
  ID: string;
  MERCHANT_ID: string;
  TICKET: string;
  DOWN_PAYMENT: string;
  CREDIT_ID: string;
  STATUS: string;
  CREATED_AT: string;
}
