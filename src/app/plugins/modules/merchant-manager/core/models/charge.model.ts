export enum CHARGE_VALUE_TYPE {
  FIXED = 'fixed',
  PERCENTAGE = 'percentage',
}
