import { Filters } from '@app/core/models/filters.model';

export interface Loan {
  amount: number;
  created_at: Date;
  currency?: string;
  customer_id?: string;
  customer_reference?: string;
  display_name?: string;
  id: string;
  identification_number?: string;
  installment_outstanding?: number;
  installment_value?: number;
  installments_number?: number;
  loan_term_periodicity?: string;
  loan_term_value?: number;
  maximum_payment_date?: Date;
  merchant_id?: string;
  merchant_reference?: string;
  merchant_display_name?: string;
  offer_name?: string;
  product_name?: string;
  reference: string;
  repayment_frecuency?: string;
  request_amount: number;
  requested_amount?: number;
  status: LOANS_STATUS;
  loan_freeze_info?: LoanFreezeInfo;
}

export interface LoanFreezeInfo {
  freeze_at?: Date;
  unfreeze_at?: Date;
  days_frozen?: number;
}

export interface LoansFilters extends Filters {
  merchant_id?: string;
  customer_id?: string;
  search?: string;
  status?: string;
  amount__gte?: string;
  amount__lte?: string;
  disbursed_at?: string;
}

export enum LOANS_STATUS {
  ACTIVE = 'active',
  PAID = 'paid',
  FROZEN = 'frozen',
  CANCELED = 'canceled',
}
