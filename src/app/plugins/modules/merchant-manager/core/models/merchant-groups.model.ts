import { Filters } from '@app/core/models/filters.model';
import { CHARGE_VALUE_TYPE } from '@app/plugins/modules/merchant-manager/core/models/charge.model';
import { Merchant } from './merchant.model';

export interface MerchantGroup {
  id: string;
  name: string;
  description: string;
  short_description: string;
  reference: string;
  number_of_merchants: number;
  merchants: Merchant[];
}

export interface MerchantGroupFilters extends Filters {
  search?: string;
  merchant_id?: string;
}

export interface MerchantGroupFees {
  offer_id: string;
  tax_id: string;
  type: CHARGE_VALUE_TYPE;
  value: string;
}

// Merchant Group Profile
export interface MerchantGroupProfile {
  name: string;
  description: string;
  created_at: Date;
  updated_at: Date;
}

// Merchant Group Profile - Add Or Remove Merchants
export interface UpdateMerchantsOfMerchantGroup {
  merchants_ids: string[];
}

// Merchant Group Profile - Add Or Remove Offers
export interface UpdateOffersOfMerchantGroup {
  merchant_group_fees: MerchantGroupFees[];
}
export interface RemoveOffersOfMerchantGroup {
  offer_ids: string[];
}

// New Merchant Group
export interface NewMerchantGroup {
  details?: NewMerchantGroupDetails;
  merchantsSelected?: Merchant[];
  fees?: MerchantGroupFees[];
}

export interface NewMerchantGroupDetailsValidations {
  isValidMerchantName?: boolean;
  isLoadingMerchantName?: boolean;
}

export interface NewMerchantGroupDetails {
  name: string;
  short_description: string;
  description: string;
}

export interface CreateNewMerchantGroup {
  name?: string;
  short_description?: string;
  description?: string;
  merchants?: string[];
  fees?: MerchantGroupFees[];
}

export interface MerchantGroupOffer {
  offer_id: string;
  offer_reference: string;
  is_new: boolean;
  offer_name: string;
  offer_minimum_loan_amount: string;
  offer_maximum_loan_amount: string;
  product_name: string;
  number_of_installments: number;
  offer_value: string;
  merchant_group_id: string;
}
