import { Filters } from '@app/core/models/filters.model';
import { ICON_COLORS } from '@app/presentation/layout/components/icon/icon.constants';
import { MERCHANT_STATUS } from '../constants/merchants.constants';

export interface Merchant {
  id: string;
  reference: string;
  display_name: string;
  national_id: string;
  merchant_group: string;
  status: MERCHANT_STATUS;
  mcc: string;
  sales?: number;
  payout?: number;
  created_at: Date;
}

export interface MerchantProfile {
  id: string;
  reference: string;
  display_name: string;
  status: string;
  created_at: Date;
  national_id_type: string;
  national_id: string;
  billing_address: string;
  online_store: string;
  contact_numbers: string[];
  contact_email: string;
  service_type: string;
}

export interface MerchantFilters extends Filters {
  search?: string;
  status?: string;
  created_at?: string;
  created_at__lte?: string;
  created_at__gte?: string;
  sales?: string;
  sales__lte?: string;
  sales__gte?: string;
  payout?: string;
  payout__lte?: string;
  payout__gte?: string;
  mcc?: string;
  merchant_group_id?: string;
}

export interface MerchantSummary {
  total_sales_amount: SummaryItem;
  total_number_of_sales: SummaryItem;
  avg_ticket: SummaryItem;
  avg_number_of_purchases: SummaryItem;
}
export interface SummaryItem {
  value: string | number;
  percentage: string | number;
  isPositive?: boolean;
  iconColor?: ICON_COLORS;
  icon?: string;
}

export interface MerchantGroupAndOffersProfile {
  merchant_group_name: string;
  merchant_product_and_offers: MerchantProductAndOffer[];
}

export interface MerchantProductAndOffer {
  product_id: string;
  product_name: string;
  offer_name: string;
  offer_value: number;
}
