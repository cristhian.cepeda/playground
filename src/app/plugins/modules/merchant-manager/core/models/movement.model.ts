import { Filters } from '@app/core/models/filters.model';

export enum MOVEMENT_TYPE {
  IN = 'in',
  OUT = 'out',
}

export interface Movement {
  amount: string;
  created_at: Date;
  fee_amount: string;
  fee_tax_amount: string;
  id: string;
  merchant_display_name: string;
  merchant_id: string;
  merchant_reference: string;
  payout_id: string;
  payout_reference: string;
  purchase_id: string;
  purchase_reference: string;
  type: MOVEMENT_TYPE;
}

export interface MovementFilters extends Filters {
  search?: string;
  type?: string;
  created_at?: string;
  created_at__gte?: string;
  created_at__lt?: string;
  created_at__lte?: string;
}
