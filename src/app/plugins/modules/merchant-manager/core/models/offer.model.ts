import { Filters } from '@app/core/models/filters.model';

export interface Offer {
  name?: string;
  status?: OFFER_STATUS;
  reference?: string;
  product_id?: string;
  product_reference?: string;
  product_name?: string;
  downpayment?: string;
  number_of_installments?: string;
  offer_value?: string;
  id?: string;
  is_new?: boolean;
  minimum_loan_amount?: string;
  maximum_loan_amount?: string;
  maximum_number_active_loans?: number;
  created_at?: Date;
  updated_at?: Date;
  // TODO VALIDATE
  action?: OFFER_ACTION;
}

export interface OffersFilters extends Filters {
  product_reference?: string;
  amount?: string;
  installments?: string;
  merchant_group_id?: string;
}

export enum OFFER_STATUS {
  DRAFT = 'draft',
  LIVE = 'live',
  HIDDEN = 'hidden',
}

export enum OFFER_STATUS_ICONS {
  LIVE = 'uil-wifi',
  DRAFT = 'uil-edit',
  HIDDEN = 'uil-eye-slash',
}

export enum OFFER_ACTION {
  IN_USE = 'in-use',
  INACTIVE = 'inactive',
  IDLE = 'idle',
}

export enum OFFER_OPTION {
  FIXED = 'Fixed',
  RELATIVE = 'Relative',
  PERCENT = 'Percent',
}

export enum OFFER_FREQUENCY_TYPE {
  DAILY = 'daily',
  WEEKLY = 'weekly',
  MONTHLY = 'monthly',
  UNIQUE = 'unique',
}
