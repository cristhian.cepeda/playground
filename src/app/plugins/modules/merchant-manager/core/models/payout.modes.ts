import { Filters } from '@app/core/models/filters.model';

export interface Payout {
  id: string;
  reference: string;
  date: Date;
  merchant_reference: string;
  merchant_display_name: string;
  merchant_group_name: string;
  amount: number;
}

export interface PayoutFilters extends Filters {
  id?: string;
  date?: string;
  date__gte?: string;
  date__gt?: string;
  date__lte?: string;
  date__lt?: string;
  search?: string;
}
