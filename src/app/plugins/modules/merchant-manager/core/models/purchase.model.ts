import { Filters } from '@app/core/models/filters.model';

export enum PURCHASE_STATUS {
  CAPTURED = 'captured',
  REJECTED = 'rejected',
  RETURNED = 'returned',
}

export interface Purchase {
  purchase_id: string;
  purchase_reference: string;
  merchant_reference: string;
  ticket: string;
  loan_reference: string;
  downpayment: number;
  status: PURCHASE_STATUS;
  created_at: Date;
}

export interface PurchaseFilters extends Filters {
  search?: string;
  created_at?: string;
  created_at__lte?: string;
  created_at__gte?: string;
  ticket?: string;
  ticket__lte?: string;
  ticket__gte?: string;
  downpayment?: string;
  downpayment__lte?: string;
  downpayment__gte?: string;
}
