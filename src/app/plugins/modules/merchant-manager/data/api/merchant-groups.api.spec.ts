import { TestBed } from '@angular/core/testing';

import { MerchantGroupsApi } from './merchant-groups.api';

describe('MerchantGroupsApi', () => {
  let service: MerchantGroupsApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MerchantGroupsApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
