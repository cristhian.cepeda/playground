import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RESPONSE_OBSERVE } from '@app/core/constants/http.constants';
import {
  CreateNewMerchantGroup,
  MerchantGroup,
  MerchantGroupFilters,
  MerchantGroupProfile,
  RemoveOffersOfMerchantGroup,
  UpdateMerchantsOfMerchantGroup,
  UpdateOffersOfMerchantGroup,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class MerchantGroupsApi {
  constructor(private _http: HttpClient) {}

  public getMerchantGroups(
    filters: MerchantGroupFilters
  ): Observable<TableResponse<MerchantGroup>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<MerchantGroup>>(
      API_URLS.MERCHANT_GROUPS.GET_MERCHANT_GROUPS,
      { params }
    );
  }

  public checkMerchantGroupName(name: string): Observable<HttpResponse<any>> {
    return this._http.post(
      API_URLS.MERCHANT_GROUPS.POST_CHECK_MERCHANT_GROUP_NAME,
      { name },
      {
        observe: RESPONSE_OBSERVE.RESPONSE,
      }
    );
  }

  public createNewMerchantGroup(
    newMerchantGroup: CreateNewMerchantGroup
  ): Observable<any> {
    return this._http.post(
      API_URLS.MERCHANT_GROUPS.POST_CREATE_NEW_MERCHANT_GROUP,
      newMerchantGroup
    );
  }

  public deleteMerchantGroup(merchantGroupId: string) {
    return this._http.delete<any>(
      `${API_URLS.MERCHANT_GROUP_PROFILE.DELETE_MERCHANT_GROUP}/${merchantGroupId}`
    );
  }

  public getMerchantGroupProfileInfo(
    merchantGroupId: string
  ): Observable<MerchantGroupProfile> {
    return this._http.get<MerchantGroupProfile>(
      `${API_URLS.MERCHANT_GROUP_PROFILE.GET_MERCHANT_GROUP_INFO}/${merchantGroupId}`
    );
  }

  public updateCommissionMerchantGroupProfile(
    merchantGroupId: string,
    updateOffersCommission: UpdateOffersOfMerchantGroup
  ): Observable<any> {
    return this._http.post<any>(
      API_URLS.MERCHANT_GROUP_PROFILE.POST_UPDATE_OFFER_COMMISSIONS_OF_MERCHANT_GROUP.replace(
        ':id',
        merchantGroupId
      ),
      updateOffersCommission
    );
  }

  public updateMerchantGroupMerchants(
    merchantGroupId: string,
    updateMerchants: UpdateMerchantsOfMerchantGroup
  ): Observable<any> {
    return this._http.post<any>(
      API_URLS.MERCHANT_GROUP_PROFILE.POST_UPDATE_MERCHANTS_OF_MERCHANT_GROUP.replace(
        ':id',
        merchantGroupId
      ),
      updateMerchants
    );
  }

  public removeMerchantGroupOffers(
    merchantGroupId: string,
    removeOffers: RemoveOffersOfMerchantGroup
  ): Observable<any> {
    return this._http.delete<any>(
      API_URLS.MERCHANT_GROUP_PROFILE.DELETE_OFFER_OF_MERCHANT_GROUP.replace(
        ':id',
        merchantGroupId
      ),
      { body: removeOffers }
    );
  }

  public addMerchantGroupOffer(
    merchantGroupId: string,
    addOffers: UpdateOffersOfMerchantGroup
  ): Observable<any> {
    return this._http.post<any>(
      API_URLS.MERCHANT_GROUP_PROFILE.POST_ADD_OFFER_OF_MERCHANT_GROUP.replace(
        ':id',
        merchantGroupId
      ),
      addOffers
    );
  }
}
