import { TestBed } from '@angular/core/testing';

import { MerchantsApi } from './merchants.api';

describe('MerchantsApi', () => {
  let service: MerchantsApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MerchantsApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
