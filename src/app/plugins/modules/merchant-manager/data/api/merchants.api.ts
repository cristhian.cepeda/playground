import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  Loan,
  LoansFilters,
} from '@app/plugins/modules/merchant-manager/core/models/loan.model';
import {
  Merchant,
  MerchantFilters,
  MerchantGroupAndOffersProfile,
  MerchantProfile,
  MerchantSummary,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class MerchantsApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getMerchants(
    filters: MerchantFilters
  ): Observable<TableResponse<Merchant> | Merchant[]> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Merchant> | Merchant[]>(
      API_URLS.MERCHANTS.GET_MERCHANTS,
      { params }
    );
  }

  public downloadMerchants(
    filters: MerchantFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(
      API_URLS.MERCHANTS.GET_MERCHANTS,
      filters
    );
  }

  public getMerchantProfile(merchantId: string): Observable<MerchantProfile> {
    return this._http.get<MerchantProfile>(
      `${API_URLS.MERCHANTS.GET_MERCHANTS}/${merchantId}`
    );
  }

  public getMerchantSummary(merchantId: string): Observable<MerchantSummary> {
    return this._http.get<MerchantSummary>(
      API_URLS.MERCHANTS.GET_MERCHANTS_SUMMARY.replace(':id', merchantId)
    );
  }

  public getMerchantPurchases(
    filters: LoansFilters
  ): Observable<TableResponse<Loan>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Loan>>(
      API_URLS.MERCHANTS.GET_MERCHANTS_PURCHASES,
      {
        params,
      }
    );
  }

  public getMerchantGroupAndOffers(
    merchantId: string
  ): Observable<MerchantGroupAndOffersProfile> {
    return this._http.get<MerchantGroupAndOffersProfile>(
      API_URLS.MERCHANTS.GET_MERCHANTS_GROUP_AND_OFFERS.replace(
        ':id',
        merchantId
      )
    );
  }
}
