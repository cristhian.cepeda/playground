import { TestBed } from '@angular/core/testing';
import { MovementsApi } from './movements.api';

describe('MovementsApi', () => {
  let service: MovementsApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MovementsApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
