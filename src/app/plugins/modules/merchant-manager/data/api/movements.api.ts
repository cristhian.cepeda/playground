import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { Movement, MovementFilters } from '../../core/models/movement.model';

import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class MovementsApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getMovements(
    filters?: MovementFilters
  ): Observable<TableResponse<Movement>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Movement>>(
      API_URLS.MOVEMENTS.GET_MOVEMENTS,
      {
        params,
      }
    );
  }

  public downloadMovements(
    filters: MovementFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(
      API_URLS.MOVEMENTS.GET_MOVEMENTS,
      filters
    );
  }
}
