import { TestBed } from '@angular/core/testing';

import { OfferApi } from './offer.api';

describe('OfferApi', () => {
  let service: OfferApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfferApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
