import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MerchantGroupOffer } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { OffersFilters } from '@app/plugins/modules/merchant-manager/core/models/offer.model';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class OfferApi {
  constructor(private _http: HttpClient) {}

  public getOffers(filters?: OffersFilters): Observable<MerchantGroupOffer[]> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<MerchantGroupOffer[]>(
      API_URLS.MERCHANT_GROUPS.GET_MERCHANT_GROUP_OFFERS,
      { params }
    );
  }
}
