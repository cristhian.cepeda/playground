import { TestBed } from '@angular/core/testing';

import { PayoutsApi } from './payouts.api';

describe('PayoutsApi', () => {
  let service: PayoutsApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PayoutsApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
