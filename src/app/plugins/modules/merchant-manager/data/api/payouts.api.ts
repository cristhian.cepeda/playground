import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { Payout, PayoutFilters } from '../../core/models/payout.modes';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class PayoutsApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getPayouts(
    filters?: PayoutFilters
  ): Observable<TableResponse<Payout>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Payout>>(API_URLS.PAYOUTS.GET_PAYOUTS, {
      params,
    });
  }

  public downloadPayouts(
    filters: PayoutFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(API_URLS.PAYOUTS.GET_PAYOUTS, filters);
  }
}
