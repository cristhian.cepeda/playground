import { TestBed } from '@angular/core/testing';

import { PurchasesApi } from './purchases.api';

describe('PurchasesApi', () => {
  let service: PurchasesApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PurchasesApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
