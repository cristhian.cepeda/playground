import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesApi } from '@app/data/api/files/files.api';
import {
  Purchase,
  PurchaseFilters,
} from '@app/plugins/modules/merchant-manager/core/models/purchase.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class PurchasesApi {
  constructor(private _http: HttpClient, private _filesApi: FilesApi) {}

  public getPurchases(
    filters: PurchaseFilters
  ): Observable<TableResponse<Purchase>> {
    let params = new HttpParams();
    params = params.appendAll({ ...filters });
    return this._http.get<TableResponse<Purchase>>(
      API_URLS.PURCHASES.GET_PURCHASES,
      { params }
    );
  }

  public downloadPurchases(
    filters: PurchaseFilters
  ): Observable<HttpResponse<string>> {
    return this._filesApi.downloadFile(
      API_URLS.PURCHASES.GET_PURCHASES,
      filters
    );
  }
}
