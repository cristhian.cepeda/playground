export const API_URLS = {
  PURCHASES: {
    GET_PURCHASES: '/merchants/purchases',
  },
  PAYOUTS: {
    GET_PAYOUTS: '/payouts',
  },
  MOVEMENTS: {
    GET_MOVEMENTS: '/payouts/movements',
  },
  MERCHANTS: {
    GET_MERCHANTS: '/merchants',
    GET_MERCHANTS_PURCHASES: '/loans',
    GET_MERCHANTS_SUMMARY: '/merchants/:id/summary',
    GET_MERCHANTS_GROUP_AND_OFFERS: '/merchants/:id/group-and-offers',
  },
  MERCHANT_GROUPS: {
    GET_MERCHANT_GROUPS: '/merchants/groups',
    GET_MERCHANT_GROUP_OFFERS: '/merchants/groups/offers',
    POST_CHECK_MERCHANT_GROUP_NAME: '/merchants/groups/exists',
    POST_CREATE_NEW_MERCHANT_GROUP: '/merchants/groups',
  },
  MERCHANT_GROUP_PROFILE: {
    GET_MERCHANT_GROUP_INFO: '/merchants/groups',
    DELETE_MERCHANT_GROUP: '/merchants/groups',
    POST_UPDATE_OFFER_COMMISSIONS_OF_MERCHANT_GROUP:
      '/merchants/groups/:id/fees/value',
    POST_UPDATE_MERCHANTS_OF_MERCHANT_GROUP: '/merchants/groups/:id/merchants',
    POST_ADD_OFFER_OF_MERCHANT_GROUP: '/merchants/groups/:id/fees',
    DELETE_OFFER_OF_MERCHANT_GROUP: '/merchants/groups/:id/fees',
  },
};
