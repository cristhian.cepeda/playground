import { MOCK_TABLE_FILTER } from '@app/core/constants/filters.constant';
import { RoutesMock } from '@app/core/models/routes-mock.model';
import { generateQueryParamsFromObject } from '@app/core/models/utils.model';
import { APP_CONSTANTS } from '../../../../../core/constants/app.constants';
import {
  MERCHANT_GROUPS,
  MERCHANT_GROUP_INFO,
  MERCHANT_GROUP_OFFERS,
} from '../mocks/merchant-groups.mock';
import {
  MERCHANTS,
  MERCHANTS_TABLE,
  MERCHANT_GROUP_AND_OFFERS,
  MERCHANT_PROFILE,
  MERCHANT_SUMMARY,
} from '../mocks/merchants.mock';
import { MOVEMENTS } from '../mocks/movements.mock';
import { PAYOUTS } from '../mocks/payouts.mock';
import { PURCHASES } from '../mocks/purchases.mock';
import { API_URLS } from './api.constants';

const ID_REGEX = '[0-9a-z-]+';

export const MERCHANT_MANAGER_ROUTES_MOCK: RoutesMock[] = [
  // ********************
  // PURCHASES
  // ********************
  {
    url: API_URLS.PURCHASES.GET_PURCHASES,
    data: PURCHASES,
    method: 'GET',
  },
  // ********************
  // PAYOUTS
  // ********************
  {
    url: API_URLS.PAYOUTS.GET_PAYOUTS,
    data: PAYOUTS,
    method: 'GET',
  },
  // ********************
  // MOVEMENTS
  // ********************
  {
    url: API_URLS.MOVEMENTS.GET_MOVEMENTS,
    data: MOVEMENTS,
    method: 'GET',
  },
  // ********************
  // MERCHANT GROUPS
  // ********************
  {
    url: API_URLS.MERCHANT_GROUPS.GET_MERCHANT_GROUPS,
    data: MERCHANT_GROUPS,
    method: 'GET',
  },
  // MERCHANT GROUP PROFILE
  {
    url: API_URLS.MERCHANT_GROUPS.GET_MERCHANT_GROUP_OFFERS,
    data: MERCHANT_GROUP_OFFERS,
    method: 'GET',
  },
  {
    url: `${API_URLS.MERCHANT_GROUP_PROFILE.GET_MERCHANT_GROUP_INFO}/${ID_REGEX}`,
    data: MERCHANT_GROUP_INFO,
    method: 'GET',
  },
  {
    url: `${API_URLS.MERCHANT_GROUP_PROFILE.DELETE_MERCHANT_GROUP}/${ID_REGEX}`,
    data: {},
    method: 'DELETE',
  },
  {
    url: API_URLS.MERCHANT_GROUP_PROFILE.POST_UPDATE_OFFER_COMMISSIONS_OF_MERCHANT_GROUP.replace(
      ':id',
      ID_REGEX
    ),
    data: {},
    method: 'POST',
  },
  // MERCHANT GROUP PROFILE - ADD OR REMOVE MERCHANTS
  {
    url: API_URLS.MERCHANT_GROUP_PROFILE.POST_UPDATE_MERCHANTS_OF_MERCHANT_GROUP.replace(
      ':id',
      ID_REGEX
    ),
    data: {},
    method: 'POST',
  },
  // MERCHANT GROUP PROFILE - ADD OR REMOVE OFFERS
  // Remove Offers
  {
    url: API_URLS.MERCHANT_GROUP_PROFILE.DELETE_OFFER_OF_MERCHANT_GROUP.replace(
      ':id',
      ID_REGEX
    ),
    data: {},
    method: 'DELETE',
  },
  // Add Offers
  {
    url: API_URLS.MERCHANT_GROUP_PROFILE.POST_ADD_OFFER_OF_MERCHANT_GROUP.replace(
      ':id',
      ID_REGEX
    ),
    data: {},
    method: 'POST',
  },
  // NEW MERCHANT GROUP
  {
    url: API_URLS.MERCHANT_GROUPS.POST_CHECK_MERCHANT_GROUP_NAME,
    data: {},
    method: 'POST',
    statusCodeResponse: APP_CONSTANTS.HTTP_CODES.NOT_CONTENT,
  },
  {
    url: API_URLS.MERCHANT_GROUPS.POST_CREATE_NEW_MERCHANT_GROUP,
    data: {},
    method: 'POST',
  },

  // ********************
  // MERCHANTS
  // ********************
  {
    url: `${API_URLS.MERCHANTS.GET_MERCHANTS}${generateQueryParamsFromObject(
      MOCK_TABLE_FILTER
    )}`,
    data: MERCHANTS_TABLE,
    method: 'GET',
    validateParam: true,
  },
  // ********************
  // MERCHANT PROFILE
  // ********************
  {
    url: `${API_URLS.MERCHANTS.GET_MERCHANTS}/${ID_REGEX}`,
    data: MERCHANT_PROFILE,
    method: 'GET',
  },
  {
    url: `${API_URLS.MERCHANTS.GET_MERCHANTS}${generateQueryParamsFromObject({
      merchant_group_id: ID_REGEX,
    })}`,
    data: MERCHANTS,
    method: 'GET',
    validateParam: true,
  },
  {
    url: API_URLS.MERCHANTS.GET_MERCHANTS_SUMMARY.replace(':id', ID_REGEX),
    data: MERCHANT_SUMMARY,
    method: 'GET',
  },
  {
    url: API_URLS.MERCHANTS.GET_MERCHANTS_GROUP_AND_OFFERS.replace(
      ':id',
      ID_REGEX
    ),
    data: MERCHANT_GROUP_AND_OFFERS,
    method: 'GET',
  },
];
