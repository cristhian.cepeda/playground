import { MERCHANT_STATUS } from '@app/plugins/modules/merchant-manager/core/constants/merchants.constants';
import {
  MerchantGroup,
  MerchantGroupOffer,
  MerchantGroupProfile,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const MERCHANT_GROUPS: TableResponse<MerchantGroup> = {
  filtered: 300,
  count: 3,
  results: [
    {
      id: '123123-asdasd-123123',
      name: 'Fashion',
      short_description: 'Lorem ipsum dolor sit amet, consectetur',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam.',
      reference: '234234234234',
      merchants: [
        {
          id: '123123-asdasd-123123',
          reference: '0001',
          display_name: 'Merchant name',
          national_id: '1234567890',
          merchant_group: 'Fashion',
          status: MERCHANT_STATUS.ACTIVE,
          mcc: '1234',
          sales: 1000,
          payout: 20000,
          created_at: new Date('2019-10-01T15:59:15-05:00'),
        },
      ],
      number_of_merchants: 25,
    },
    {
      id: '123123-asdasd-123123',
      name: 'Houte Couture',
      short_description: 'Lorem ipsum dolor sit amet, consectetur',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam.',
      reference: '234234234234',
      merchants: [
        {
          id: '123123-asdasd-123123',
          reference: '0001',
          display_name: 'Merchant name',
          national_id: '1234567890',
          merchant_group: 'Houte Couture',
          status: MERCHANT_STATUS.ACTIVE,
          mcc: '1234',
          sales: 1000,
          payout: 20000,
          created_at: new Date('2019-10-01T15:59:15-05:00'),
        },
      ],
      number_of_merchants: 35,
    },
    {
      id: '123123-asdasd-123123',
      name: 'Toys & Kids',
      short_description: 'Lorem ipsum dolor sit amet, consectetur',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam.',
      reference: '234234234234',
      merchants: [
        {
          id: '123123-asdasd-123123',
          reference: '0001',
          display_name: 'Merchant name',
          national_id: '1234567890',
          merchant_group: 'Toys & Kids',
          status: MERCHANT_STATUS.ACTIVE,
          mcc: '1234',
          sales: 1000,
          payout: 20000,
          created_at: new Date('2019-10-01T15:59:15-05:00'),
        },
      ],
      number_of_merchants: 35,
    },
    {
      id: '123123-asdasd-123123',
      name: 'Electronics',
      short_description: 'Lorem ipsum dolor sit amet, consectetur',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam.',
      reference: '234234234234',
      merchants: [
        {
          id: '123123-asdasd-123123',
          reference: '0001',
          display_name: 'Merchant name',
          national_id: '1234567890',
          merchant_group: 'Toys & Kids',
          status: MERCHANT_STATUS.ACTIVE,
          mcc: '1234',
          sales: 1000,
          payout: 20000,
          created_at: new Date('2019-10-01T15:59:15-05:00'),
        },
      ],
      number_of_merchants: 55,
    },
    {
      id: '123123-asdasd-123123',
      name: 'Sports',
      short_description: 'Lorem ipsum dolor sit amet, consectetur',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam.',
      reference: '234234234234',
      merchants: [
        {
          id: '123123-asdasd-123123',
          reference: '0001',
          display_name: 'Merchant name',
          merchant_group: 'Toys & Kids',
          national_id: '1234567890',
          status: MERCHANT_STATUS.ACTIVE,
          mcc: '1234',
          sales: 1000,
          payout: 20000,
          created_at: new Date('2019-10-01T15:59:15-05:00'),
        },
      ],
      number_of_merchants: 45,
    },
    {
      id: '123123-asdasd-123123',
      name: 'Homewear',
      short_description: 'Lorem ipsum dolor sit amet, consectetur',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam.',
      reference: '234234234234',
      merchants: [
        {
          id: '123123-asdasd-123123',
          reference: '0001',
          display_name: 'Merchant name',
          merchant_group: 'Toys & Kids',
          national_id: '1234567890',
          status: MERCHANT_STATUS.ACTIVE,
          mcc: '1234',
          sales: 1000,
          payout: 20000,
          created_at: new Date('2019-10-01T15:59:15-05:00'),
        },
      ],
      number_of_merchants: 2,
    },
  ],
};

export const MERCHANT_GROUP_INFO: MerchantGroupProfile = {
  name: 'Melinda Hardin',
  description:
    'Way someone onto law myself able. I reach thus put generation kitchen.',
  created_at: new Date('2022-11-28T16:15:18.993261Z'),
  updated_at: new Date('2022-11-28T16:15:18.993267Z'),
};

export const MERCHANT_GROUP_OFFERS: MerchantGroupOffer[] = [
  {
    offer_id: '0acf9acc-3300-4ced-bb56-205b98df8c7d',
    offer_reference: 'c6cd9b06-6d1c-473c-913a-0f32ad964cc9',
    is_new: false,
    offer_name: 'Offer-task-force',
    offer_minimum_loan_amount: '100000.0000000000',
    offer_maximum_loan_amount: '300000.0000000000',
    product_name: 'Chips',
    number_of_installments: 1,
    offer_value: '1.0000000000',
    merchant_group_id: '21075216-2045-412b-9486-8158d478290c',
  },
  {
    offer_id: '0b4a2aaa-efa5-43c5-8d83-fa02c6653e74',
    offer_reference: 'b3d20e39-224b-4361-88de-40c4110f4e0e',
    is_new: true,
    offer_name: 'Offer-leverage',
    offer_minimum_loan_amount: '100000.0000000000',
    offer_maximum_loan_amount: '300000.0000000000',
    product_name: 'Product Home',
    number_of_installments: 1,
    offer_value: '12.0000000000',
    merchant_group_id: '0e277793-f52f-49ae-9816-257bc7073fcd',
  },
  {
    offer_id: '10f8aac8-f954-416d-93c7-45020f4f77c6',
    offer_reference: '6c19c62e-9fe6-4d2a-a813-748d722cfb22',
    is_new: false,
    offer_name: 'Offer-synergy',
    offer_minimum_loan_amount: '100000.0000000000',
    offer_maximum_loan_amount: '300000.0000000000',
    product_name: 'Chips',
    number_of_installments: 30,
    offer_value: '1.0000000000',
    merchant_group_id: '21fd2a11-47b2-4036-9bc2-067fb1a94106',
  },
  {
    offer_id: '18d8bcaa-c554-448e-a202-95aa0e39b856',
    offer_reference: 'gcFDUzoiOwhBlPffxPWm',
    is_new: false,
    offer_name: 'Casey Wu',
    offer_minimum_loan_amount: '100000.0000000000',
    offer_maximum_loan_amount: '300000.0000000000',
    product_name: 'Mr. Richard Harrell DVM',
    number_of_installments: 1,
    offer_value: '4.0000000000',
    merchant_group_id: 'e9ed4b2a-f6ec-4ca3-9c33-6784c8f62335',
  },
  {
    offer_id: '1ba5409d-9a59-4461-af7e-49926298a633',
    offer_reference: '905cf338-a8a3-4d50-90ae-5964f7d6d67d',
    is_new: false,
    offer_name: 'Offer-access',
    offer_minimum_loan_amount: '100000.0000000000',
    offer_maximum_loan_amount: '300000.0000000000',
    product_name: 'Product Home',
    number_of_installments: 1,
    offer_value: '1.0000000000',
    merchant_group_id: 'fde9023f-cdb3-46a4-9933-bccee9b51eb6',
  },
];
