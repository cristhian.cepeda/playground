import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Payout } from '../../core/models/payout.modes';

export const PAYOUTS: TableResponse<Payout> = {
  filtered: 500,
  count: 3,
  results: [
    {
      id: '123123-asdasd-123123',
      reference: '0002',
      date: new Date('2019-10-01T15:59:15-05:00'),
      merchant_reference: 'AWIOAN',
      merchant_display_name: 'Adidas',
      merchant_group_name: 'Sports',
      amount: 5000,
    },
    {
      id: '123123-ASDASD-123123',
      reference: '3',
      date: new Date('2019-10-01T15:59:15-05:00'),
      merchant_reference: 'QIWOEQI',
      merchant_display_name: 'Nike',
      merchant_group_name: 'Sports',
      amount: 1000,
    },
    {
      id: '123123-asdasd-123123',
      reference: '123',
      date: new Date('2019-10-01T15:59:15-05:00'),
      merchant_reference: 'QWERTY',
      merchant_display_name: 'Adidas',
      merchant_group_name: 'Sports',
      amount: 15000,
    },
  ],
};
