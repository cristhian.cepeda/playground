import {
  Purchase,
  PURCHASE_STATUS,
} from '@app/plugins/modules/merchant-manager/core/models/purchase.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const PURCHASES: TableResponse<Purchase> = {
  filtered: 500,
  count: 3,
  results: [
    {
      purchase_id: '123123-asdasd-123123',
      purchase_reference: '0001',
      merchant_reference: '1234567890',
      ticket: '1000',
      loan_reference: '12312PASD',
      downpayment: 2000,
      status: PURCHASE_STATUS.CAPTURED,
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      purchase_id: '123123-asdasd-123123',
      purchase_reference: '0002',
      merchant_reference: '1234567890',
      ticket: '21000',
      loan_reference: '12312PASD',
      downpayment: 2000,
      status: PURCHASE_STATUS.REJECTED,
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
    {
      purchase_id: '123123-asdasd-123123',
      purchase_reference: '0003',
      merchant_reference: '1234567890',
      ticket: '41000',
      downpayment: 2000,
      loan_reference: '12312PASD',
      status: PURCHASE_STATUS.RETURNED,
      created_at: new Date('2019-10-01T15:59:15-05:00'),
    },
  ],
};
