import { TestBed } from '@angular/core/testing';

import { MerchantGroupsService } from './merchant-groups.service';

describe('MerchantGroupsService', () => {
  let service: MerchantGroupsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MerchantGroupsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
