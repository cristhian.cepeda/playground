import { Injectable } from '@angular/core';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { Project } from '@app/core/models/project.model';
import { deepCopy } from '@app/core/models/utils.model';
import { CHARGE_VALUE_TYPE } from '@app/plugins/modules/merchant-manager/core/models/charge.model';
import {
  CreateNewMerchantGroup,
  MerchantGroupFees,
  MerchantGroupOffer,
  NewMerchantGroup,
  UpdateMerchantsOfMerchantGroup,
  UpdateOffersOfMerchantGroup,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { Offer } from '@app/plugins/modules/merchant-manager/core/models/offer.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import {
  MERCHANT_GROUP_BY_DEFAULT,
  MERCHANT_GROUP_BY_DEFAULT_BACK,
} from '../../core/constants/merchants.constants';

@Injectable({
  providedIn: 'root',
})
export class MerchantGroupsService {
  public setCreateNewMerchantGroup(
    newMerchantGroup: NewMerchantGroup
  ): CreateNewMerchantGroup {
    const createNewMerchantGroup: CreateNewMerchantGroup = {
      ...newMerchantGroup?.details,
      merchants: newMerchantGroup?.merchantsSelected.map(
        (merchant) => merchant?.id
      ),
      fees: newMerchantGroup?.fees,
    };
    return createNewMerchantGroup;
  }

  public setOffersToFees(
    offers: Offer[],
    project: Project
  ): MerchantGroupFees[] {
    const fees: MerchantGroupFees[] = offers.map((offer) => ({
      offer_id: offer?.id,
      value: offer?.offer_value,
      tax_id: project?.default_tax_id,
      type: CHARGE_VALUE_TYPE.PERCENTAGE,
    }));
    return fees;
  }

  public setProductOfferListItemFromOffers(
    offers: MerchantGroupOffer[]
  ): MerchantGroupProductOfferListItem[] {
    return offers.map((offer) => ({
      offerId: offer?.offer_id,
      productName: offer?.product_name,
      offerName: offer?.offer_name,
      amountMin: offer?.offer_minimum_loan_amount,
      amountMax: offer?.offer_maximum_loan_amount,
      numberOfInstallments: offer?.number_of_installments,
      commission: offer?.offer_value,
      isNew: offer?.is_new,
      offer,
    }));
  }

  public setUpdateOfferCommissions(
    fees: MerchantGroupFees[],
    offers: MerchantGroupOffer[]
  ): UpdateOffersOfMerchantGroup {
    const merchant_group_fees: MerchantGroupFees[] = fees.filter((item) =>
      offers.some(
        (offer) =>
          offer?.offer_id === item?.offer_id &&
          offer?.offer_value != item?.value
      )
    );
    const updateOffersOfMerchantGroup: UpdateOffersOfMerchantGroup = {
      merchant_group_fees,
    };

    return updateOffersOfMerchantGroup;
  }

  public setUpdateMerchantsOfMerchantGroup(
    merchants: Merchant[]
  ): UpdateMerchantsOfMerchantGroup {
    const merchants_ids: string[] = merchants?.map((merchant) => merchant?.id);
    const updateMerchants: UpdateMerchantsOfMerchantGroup = {
      merchants_ids,
    };
    return updateMerchants;
  }

  public setOfferIdsToRemove(
    offers: MerchantGroupOffer[] = [],
    offersSelected: MerchantGroupOffer[] = []
  ): string[] {
    const offers_ids: string[] = offers
      .filter(
        (offer) =>
          !offersSelected.some(
            (offerSelected) => offer?.offer_id === offerSelected?.offer_id
          )
      )
      ?.map((offer) => offer?.offer_id);

    return offers_ids;
  }

  public validateMerchantRemoved(
    merchant: Merchant[] = [],
    merchantSelected: Merchant[] = []
  ): boolean {
    const removeSomeMerchant: boolean =
      merchant.filter(
        (merchant) =>
          !merchantSelected.some(
            (merchantSelected) => merchant?.id === merchantSelected?.id
          )
      )?.length > 0;

    return removeSomeMerchant;
  }

  public mapMerchantsWithMerchantsSeleted(
    merchantResponse: TableResponse<Merchant>,
    merchantsSelected: Merchant[] = []
  ): TableResponse<Merchant> {
    let response: TableResponse<Merchant> = deepCopy(merchantResponse);
    response.results = response?.results
      .filter((merchant) =>
        merchantsSelected.every(
          (merchantSelected) => merchant?.id !== merchantSelected?.id
        )
      )
      .map((merchant) => {
        if (merchant?.merchant_group === MERCHANT_GROUP_BY_DEFAULT_BACK)
          merchant.merchant_group = MERCHANT_GROUP_BY_DEFAULT;
        return merchant;
      });
    return response;
  }
}
