import { Injectable } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { deepCopy } from '@app/core/models/utils.model';
import { MerchantProfileDetailsTranslate } from '@app/plugins/modules/merchant-manager/core/i18n/models/merchants-translate';
import {
  MerchantProfile,
  MerchantSummary,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { ICON_COLORS } from '@app/presentation/layout/components/icon/icon.constants';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { TranslateService } from '@ngx-translate/core';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MerchantProfileService {
  constructor(
    private _translateService: TranslateService,
    private _snakeToCapitalPipe: SnakeToCapitalPipe
  ) {}

  public getMerchantItemsByProfile(
    merchantProfile: MerchantProfile
  ): Observable<SummaryGroup[]> {
    return this._translateService
      .get('MERCHANT_MANAGER.MERCHANT_PROFILE.DETAILS')
      .pipe(
        map((DETAILS: MerchantProfileDetailsTranslate) => {
          return [
            {
              key: `${DETAILS.MERCHANT_NAME}:`,
              value: merchantProfile?.display_name,
            },
            {
              key: `${DETAILS.TYPE_OF_ID}:`,
              value: merchantProfile?.national_id_type,
            },
            {
              key: `${DETAILS.ID_NUMBER}:`,
              value: merchantProfile?.national_id,
            },
            {
              key: `${DETAILS.ADDRESS}:`,
              value: merchantProfile?.billing_address,
            },
            {
              key: `${DETAILS.ONLINE_STORE}:`,
              value: merchantProfile?.online_store,
            },
            {
              key: `${DETAILS.CONTACT_NUMBER}:`,
              value: String(merchantProfile?.contact_numbers),
            },
            {
              key: `${DETAILS.CONTACT_EMAIL}:`,
              value: merchantProfile?.contact_email,
            },
            {
              key: `${DETAILS.TYPE_OF_SERVICE}:`,
              value: this._snakeToCapitalPipe.transform(
                merchantProfile?.service_type
              ),
            },
          ].filter((details) => details.value);
        })
      );
  }

  public setMerchantSummary(merchantSummary: MerchantSummary): MerchantSummary {
    let summary: MerchantSummary = deepCopy(merchantSummary);
    Object.keys(summary).forEach((key) => {
      let isPositive: boolean = summary[key]?.percentage >= 0;
      if (!isPositive)
        summary[key].percentage = Math.abs(summary[key]?.percentage);

      summary[key].isPositive = isPositive;
      summary[key].iconColor = isPositive
        ? ICON_COLORS.SUCCESS
        : ICON_COLORS.ERROR;
      summary[key].icon = isPositive ? 'uil-arrow-up' : 'uil-arrow-down';
    });
    return summary;
  }
}
