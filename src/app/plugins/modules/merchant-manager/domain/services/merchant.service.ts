import { Injectable } from '@angular/core';
import { deepCopy } from '@app/core/models/utils.model';
import {
  Merchant,
  MerchantGroupAndOffersProfile,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import {
  MERCHANT_GROUP_BY_DEFAULT,
  MERCHANT_GROUP_BY_DEFAULT_BACK,
} from '../../core/constants/merchants.constants';

@Injectable({
  providedIn: 'root',
})
export class MerchantService {
  public mapMerchants(
    merchantResponse: TableResponse<Merchant>
  ): TableResponse<Merchant> {
    let response: TableResponse<Merchant> = deepCopy(merchantResponse);
    response?.results.map((merchant) => {
      if (merchant?.merchant_group === MERCHANT_GROUP_BY_DEFAULT_BACK)
        merchant.merchant_group = MERCHANT_GROUP_BY_DEFAULT;
    });
    return response;
  }

  public mapMerchantGroupAndOffers(
    merchantGroupAndOffers: MerchantGroupAndOffersProfile
  ): MerchantGroupAndOffersProfile {
    let response: MerchantGroupAndOffersProfile = deepCopy(
      merchantGroupAndOffers
    );
    if (response.merchant_group_name === MERCHANT_GROUP_BY_DEFAULT_BACK)
      response.merchant_group_name = MERCHANT_GROUP_BY_DEFAULT;

    return response;
  }
}
