import { FiltersOptions } from '@app/core/models/filters.model';
import {
  MerchantGroup,
  MerchantGroupFees,
  MerchantGroupFilters,
  MerchantGroupOffer,
  MerchantGroupProfile,
  NewMerchantGroup,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import {
  Merchant,
  MerchantFilters,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { OffersFilters } from '@app/plugins/modules/merchant-manager/core/models/offer.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

// ********************
// MERCHANT GROUPS
// ********************

export const initMerchantGroupsPage = createAction(
  '[Merchant Groups Page] Init Merchant Groups Page',
  props<{ filters: MerchantGroupFilters }>()
);

export const updateFiltersMerchantGroups = createAction(
  '[Merchant Groups Filters Component] Update Filters Merchant Groups',
  props<{ filters: MerchantGroupFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetMerchantGroups = createAction(
  '[Merchant Groups Effects] Success Get Merchant Groups',
  props<{ merchantGroups?: TableResponse<MerchantGroup> }>()
);

export const failedGetMerchantGroups = createAction(
  '[Merchant Groups Effects] Failed Get Merchant Groups'
);

// MERCHANTS

export const updateFiltersMerchantGroupMerchants = createAction(
  '[Merchant Group Filters Component] Update Filters Merchant Group Merchants',
  props<{ filters: MerchantFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetMerchantGroupMerchants = createAction(
  '[Merchant Groups Effects] Success Get Merchant Group Merchants',
  props<{ merchants?: TableResponse<Merchant> }>()
);

export const failedGetMerchantGroupMerchants = createAction(
  '[Merchant Groups Effects] Failed Get Merchant Group Merchants'
);

// OFFERS
export const getMerchantGroupOffers = createAction(
  '[Merchant Group Pages] Get Merchant Group Offers',
  props<{ filters?: OffersFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetMerchantGroupOffers = createAction(
  '[Merchant Groups Effects] Success Get Merchant Group Offers',
  props<{ offers: MerchantGroupOffer[] }>()
);

export const failedGetMerchantGroupOffers = createAction(
  '[Merchant Groups Effects] Failed Get Merchant Group Offers'
);

// ***********************
// MERCHANT GROUP PROFILE
// ***********************

export const destroyMerchantGroupProfilePage = createAction(
  '[Merchant Group Profile Guard] Destroy Merchant Group Profile'
);

export const updateMerchantGroupProfileId = createAction(
  '[Merchant Group Profile Guard] Update Merchant Group Id',
  props<{ id: string }>()
);

// INFO

export const getMerchantGroupProfileInfo = createAction(
  '[Merchant Group Profile Page] Get Merchant Group Info'
);

export const successGetMerchantGroupProfileInfo = createAction(
  '[Merchant Group Profile Page] Success Get Merchant Group Info',
  props<{ info: MerchantGroupProfile }>()
);

export const failedGetMerchantGroupProfileInfo = createAction(
  '[Merchant Group Profile Page] Failed Get Merchant Group Info'
);

// DELETE MERCHANT GROUP

export const deleteMerchantGroup = createAction(
  '[Merchant Group Profile Page] Delete Merchant Group'
);

export const successDeleteMerchantGroup = createAction(
  '[Merchant Group Profile Page] Success Delete Merchant Group'
);

export const failedDeleteMerchantGroup = createAction(
  '[Merchant Group Profile Page] Failed Delete Merchant Group'
);
// MERCHANTS

export const getMerchantGroupProfileMerchants = createAction(
  '[Merchant Group Profile Page] Get Merchant Group Profile Merchants'
);

export const filterMerchantGroupProfileMerchants = createAction(
  '[Merchant Group Profile Page] Filter Merchant Group Profile Merchants',
  props<{ merchantNameFilter: string }>()
);

export const successGetMerchantGroupProfileMerchants = createAction(
  '[Merchant Groups Effects] Success Get Merchant Group Profile Merchants',
  props<{ merchants?: Merchant[] }>()
);

export const failedGetMerchantGroupProfileMerchants = createAction(
  '[Merchant Groups Effects] Failed Get Merchant Group Profile Merchants'
);

// OFFERS

export const getMerchantGroupProfileOffers = createAction(
  '[Merchant Group Profile Page] Get Merchant Group Profile Offers'
);

export const successGetMerchantGroupProfileOffers = createAction(
  '[Merchant Groups Effects] Success Get Merchant Group Profile Offers',
  props<{ offers?: MerchantGroupOffer[] }>()
);

export const failedGetMerchantGroupProfileOffers = createAction(
  '[Merchant Groups Effects] Failed Get Merchant Group Profile Offers'
);

export const updateCommissionMerchantGroupProfile = createAction(
  '[Merchant Group Profile Page] Update Commissions Merchant Group Profile',
  props<{ fees: MerchantGroupFees[] }>()
);

export const successUpdateCommissionMerchantGroupProfile = createAction(
  '[Merchant Groups Effects] Success Update Commissions Merchant Group Profile'
);

export const failedUpdateCommissionMerchantGroupProfile = createAction(
  '[Merchant Groups Effects] Failed Update Commissions Merchant Group Profile'
);

export const cancelUpdateCommissionMerchantGroupProfile = createAction(
  '[Merchant Groups Effects] Cancel Update Commissions Merchant Group Profile'
);

// ADD OR REMOVE MERCHANTS
export const initAddOrRemoveMerchantsPage = createAction(
  '[Add Or Remove Merchant Page] Init Add Or Remove Merchants Page'
);

export const updateAddOrRemoveMerchantsSelected = createAction(
  '[Add Or Remove Merchant Group Filters Component] Update Add Or Remove Merchants Selected',
  props<{ merchantSelected: Merchant; remove: boolean }>()
);

export const updateMerchantGroupMerchants = createAction(
  '[Add Or Remove Merchant Group Page] Update Merchant Group Merchants'
);

export const showModalConfirmRemoveMerchant = createAction(
  '[Add Or Remove Merchant - Merchant Groups Effects] Show Modal Confirm Remove Merchants of Merchant Group'
);

export const confirmRemoveMerchantGroupMerchants = createAction(
  '[Add Or Remove Merchant  - Merchant Groups Effects] Confirm Update Merchant Group Merchants'
);

export const cancelRemoveMerchantGroupMerchants = createAction(
  '[Modal Confirm Remove Merchant] Cancel Update Merchant Group Merchants'
);

export const successUpdateMerchantGroupMerchants = createAction(
  '[Add Or Remove Merchant  - Merchant Groups Effects] Success Update Merchant Group Merchants',
  props<{ merchant_group_id: string }>()
);

export const failedUpdateMerchantGroupMerchants = createAction(
  '[Add Or Remove Merchant  - Merchant Groups Effects] Failed Update Merchant Group Merchants'
);

// ADD OR REMOVE OFFERS
export const initAddOrRemoveOffersPage = createAction(
  '[Add Or Remove Offer Page] Init Add Or Remove Offers Page'
);

export const updateAddOrRemoveOffersSelected = createAction(
  '[Add Or Remove Offer Group Filters Component] Update Add Or Remove Offers Selected',
  props<{ offerSelected: MerchantGroupOffer; remove: boolean }>()
);

export const updateMerchantGroupOffers = createAction(
  '[Add Or Remove Offer Page] Update Offers of Merchant Group',
  props<{ feesToAdd: MerchantGroupFees[] }>()
);

export const showModalConfirmRemoveOffer = createAction(
  '[Add Or Remove Offer - Merchant Groups Effects] Show Modal Confirm Remove Offers of Merchant Group',
  props<{ offerIdsToRemove?: string[] }>()
);

export const cancelRemoveMerchantGroupOffers = createAction(
  '[Modal Confirm Remove Offers] Cancel Remove Offers of Merchant Group'
);

export const removeMerchantGroupOffers = createAction(
  '[Add Or Remove Offer - Merchant Groups Effects] Remove Offers of Merchant Group'
);

export const failedRemoveMerchantGroupOffers = createAction(
  '[Add Or Remove Offer - Merchant Groups Effects] Failed Remove Offers of Merchant Group'
);

export const addMerchantGroupOffers = createAction(
  '[Add Or Remove Offer - Merchant Groups Effects] Add Offers of Merchant Group'
);

export const successAddMerchantGroupOffers = createAction(
  '[Add Or Remove Offer - Merchant Groups Effects] Success Add Offers of Merchant Group',
  props<{ merchant_group_id: string }>()
);

export const failedAddMerchantGroupOffers = createAction(
  '[Add Or Remove Offer - Merchant Groups Effects] Failed Add Offers of Merchant Group'
);

// ********************
// NEW MERCHANT GROUPS
// ********************

export const destroyNewMerchantGroupPages = createAction(
  '[New Merchant Group Guard] Destroy New Merchant Group Pages'
);

export const updateNewMerchantGroupData = createAction(
  '[New Merchant Group Pages] Update Merchant Group Data',
  props<{ newMerchantGroupData: NewMerchantGroup }>()
);

// DETAILS
export const checkMerchantGroupName = createAction(
  '[New Merchant Group Page] Check Merchant Group Name',
  props<{ name: string }>()
);

export const successCheckMerchantGroupName = createAction(
  '[New Merchant Group Page] Success Check Merchant Group Name'
);

export const failedCheckMerchantGroupName = createAction(
  '[New Merchant Group Page] Failed Check Merchant Group Name'
);

// MERCHANTS
export const initNewGroupMerchantsPage = createAction(
  '[New Merchant Group Page] Init New Group Merchants Page'
);

export const updateNewGroupSelectedMerchants = createAction(
  '[New Merchant Group Filters Component] Update New Group Selected Merchants',
  props<{ merchantSelected: Merchant; remove: boolean }>()
);

// CREATE NEW MERCHANT GROUP

export const createNewMerchantGroup = createAction(
  '[New Merchant Group Page] Create New Merchant Group'
);

export const successCreateNewMerchantGroup = createAction(
  '[Merchant Groups Effects] Success Create New Merchant Group'
);

export const failedCreateNewMerchantGroup = createAction(
  '[Merchant Groups Effects] Failed Create New Merchant Group'
);

export const resetNewMerchantGroup = createAction(
  '[New Merchant Group Page] Reset New Merchant Group'
);
