import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { APP_CONSTANTS } from '@app/core/constants/app.constants';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { AppFacade } from '@app/facade/app/app.facade';
import { MerchantGroupsApi } from '@app/plugins/modules/merchant-manager//data/api/merchant-groups.api';
import { FILTERS_MERCHANTS } from '@app/plugins/modules/merchant-manager/core/constants/filters.constant';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import {
  CreateNewMerchantGroup,
  MerchantGroupFilters,
  MerchantGroupOffer,
  RemoveOffersOfMerchantGroup,
  UpdateMerchantsOfMerchantGroup,
  UpdateOffersOfMerchantGroup,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantsApi } from '@app/plugins/modules/merchant-manager/data/api/merchants.api';
import { OfferApi } from '@app/plugins/modules/merchant-manager/data/api/offer.api';
import { MerchantGroupsService } from '@app/plugins/modules/merchant-manager/domain/services/merchant-groups.service';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, tap, withLatestFrom } from 'rxjs';
import * as merchantGroupsActions from './merchant-groups.actions';
import * as merchantsGroupsSelectors from './merchant-groups.selectors';

@Injectable()
export class MerchantGroupsEffects {
  constructor(
    private _actions$: Actions,
    private _merchantGroupApi: MerchantGroupsApi,
    private _merchantApi: MerchantsApi,
    private _offerApi: OfferApi,
    private _merchantGroupsService: MerchantGroupsService,
    private _router: Router,
    private _store: Store,
    private _utilsService: UtilsService,
    private _appFacade: AppFacade
  ) {}

  // ********************
  // MERCHANT GROUPS
  // ********************
  initMerchantGroupsPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.initMerchantGroupsPage),
      map(({ filters }) => {
        const merchantGroupFilters: MerchantGroupFilters = {
          ...FILTERS_MERCHANTS,
          ...filters,
        };
        return merchantGroupsActions.updateFiltersMerchantGroups({
          filters: merchantGroupFilters,
          filtersOptions: { isFiltred: !!filters },
        });
      })
    )
  );

  getMerchantGroups$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.updateFiltersMerchantGroups),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupsFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._merchantGroupApi.getMerchantGroups(filters).pipe(
          map((merchantGroups) =>
            merchantGroupsActions.successGetMerchantGroups({
              merchantGroups,
            })
          ),
          catchError((_) => of(merchantGroupsActions.failedGetMerchantGroups()))
        );
      })
    )
  );

  getMerchantGroupOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.getMerchantGroupOffers),
      switchMap(({ filters }) =>
        this._offerApi.getOffers(filters).pipe(
          map((offers: MerchantGroupOffer[]) =>
            merchantGroupsActions.successGetMerchantGroupOffers({
              offers,
            })
          ),
          catchError((_) =>
            of(merchantGroupsActions.failedGetMerchantGroupOffers())
          )
        )
      )
    )
  );

  updateFiltersMerchantGroupMerchants$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.updateFiltersMerchantGroupMerchants),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectGroupMerchantsFilters)
      ),
      switchMap(([_, filters]) =>
        this._merchantApi.getMerchants(filters).pipe(
          map((merchants: TableResponse<Merchant>) =>
            merchantGroupsActions.successGetMerchantGroupMerchants({
              merchants,
            })
          ),
          catchError((_) =>
            of(merchantGroupsActions.failedGetMerchantGroupMerchants())
          )
        )
      )
    )
  );

  // ***********************
  // MERCHANT GROUP PROFILE
  // ***********************

  getMerchantGroupProfileInfo$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.getMerchantGroupProfileInfo),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([_, merchantGroupId]) => {
        return this._merchantGroupApi
          .getMerchantGroupProfileInfo(merchantGroupId)
          .pipe(
            map((info) =>
              merchantGroupsActions.successGetMerchantGroupProfileInfo({
                info,
              })
            ),
            catchError((_) =>
              of(merchantGroupsActions.failedGetMerchantGroupProfileInfo())
            )
          );
      })
    )
  );

  deleteMerchantGroup$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.deleteMerchantGroup),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([_, merchantGroupId]) => {
        return this._merchantGroupApi.deleteMerchantGroup(merchantGroupId).pipe(
          map(() => merchantGroupsActions.successDeleteMerchantGroup()),
          catchError((_) =>
            of(merchantGroupsActions.failedDeleteMerchantGroup())
          )
        );
      })
    )
  );

  successDeleteMerchantGroup$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantGroupsActions.successDeleteMerchantGroup),
        map(() => {
          this._appFacade.toggleModal();
          this._router.navigateByUrl(MERCHANT_MANAGER_URLS.MERCHANT_GROUPS);
        })
      ),
    {
      dispatch: false,
    }
  );

  getMerchantGroupProfileMerchants$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.getMerchantGroupProfileMerchants),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([_, merchant_group_id]) =>
        this._merchantApi.getMerchants({ merchant_group_id }).pipe(
          map((merchants: Merchant[]) =>
            merchantGroupsActions.successGetMerchantGroupProfileMerchants({
              merchants,
            })
          ),
          catchError((_) =>
            of(merchantGroupsActions.failedGetMerchantGroupProfileMerchants())
          )
        )
      )
    )
  );

  getMerchantGroupProfileOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(
        merchantGroupsActions.getMerchantGroupProfileOffers,
        merchantGroupsActions.successUpdateCommissionMerchantGroupProfile
      ),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([_, merchant_group_id]) =>
        this._offerApi.getOffers({ merchant_group_id }).pipe(
          map((offers: MerchantGroupOffer[]) =>
            merchantGroupsActions.successGetMerchantGroupProfileOffers({
              offers,
            })
          ),
          catchError((_) =>
            of(merchantGroupsActions.failedGetMerchantGroupProfileOffers())
          )
        )
      )
    )
  );

  updateCommissionMerchantGroupProfile$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.updateCommissionMerchantGroupProfile),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupOffers),
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([{ fees }, merchantGroupOffers, merchant_group_id]) => {
        const updateOfferCommissions: UpdateOffersOfMerchantGroup =
          this._merchantGroupsService.setUpdateOfferCommissions(
            fees,
            merchantGroupOffers
          );
        return updateOfferCommissions?.merchant_group_fees?.length > 0
          ? this._merchantGroupApi
              .updateCommissionMerchantGroupProfile(
                merchant_group_id,
                updateOfferCommissions
              )
              .pipe(
                map(() =>
                  merchantGroupsActions.successUpdateCommissionMerchantGroupProfile()
                ),
                catchError((_) =>
                  of(
                    merchantGroupsActions.failedUpdateCommissionMerchantGroupProfile()
                  )
                )
              )
          : of(
              merchantGroupsActions.cancelUpdateCommissionMerchantGroupProfile()
            );
      })
    )
  );

  // ADD OR REMOVE MERCHANTS
  initAddOrRemoveMerchantsPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.initAddOrRemoveMerchantsPage),
      map(() =>
        merchantGroupsActions.updateFiltersMerchantGroupMerchants({
          filters: FILTERS,
        })
      )
    )
  );

  updateMerchantGroupMerchants$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.updateMerchantGroupMerchants),
      withLatestFrom(
        this._store.select(
          merchantsGroupsSelectors.selectMerchantGroupMerchants
        ),
        this._store.select(
          merchantsGroupsSelectors.selectAddOrRemoveMerchantsSelected
        )
      ),
      switchMap(([_, merchants, merchantsSelected]) => {
        const removeSomeMerchant: boolean =
          this._merchantGroupsService.validateMerchantRemoved(
            merchants,
            merchantsSelected
          );
        return removeSomeMerchant
          ? of(merchantGroupsActions.showModalConfirmRemoveMerchant())
          : of(merchantGroupsActions.confirmRemoveMerchantGroupMerchants());
      })
    )
  );

  showModalConfirmRemoveMerchant$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantGroupsActions.showModalConfirmRemoveMerchant),
        map(() => this._appFacade.toggleModal())
      ),
    {
      dispatch: false,
    }
  );

  confirmRemoveMerchantGroupMerchants$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.confirmRemoveMerchantGroupMerchants),
      withLatestFrom(
        this._store.select(
          merchantsGroupsSelectors.selectAddOrRemoveMerchantsSelected
        ),
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([_, merchants, merchant_group_id]) => {
        const updateMerchants: UpdateMerchantsOfMerchantGroup =
          this._merchantGroupsService.setUpdateMerchantsOfMerchantGroup(
            merchants
          );
        return this._merchantGroupApi
          .updateMerchantGroupMerchants(merchant_group_id, updateMerchants)
          .pipe(
            map(() =>
              merchantGroupsActions.successUpdateMerchantGroupMerchants({
                merchant_group_id,
              })
            ),
            catchError((_) =>
              of(merchantGroupsActions.failedUpdateMerchantGroupMerchants())
            )
          );
      })
    )
  );

  successUpdateMerchantGroupMerchants$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantGroupsActions.successUpdateMerchantGroupMerchants),
        map(({ merchant_group_id }) =>
          this._router.navigate(
            [MERCHANT_MANAGER_URLS.MERCHANT_GROUP_PROFILE],
            { queryParams: { id: merchant_group_id } }
          )
        )
      ),
    {
      dispatch: false,
    }
  );

  // ADD OR REMOVE OFFERS
  initAddOrRemoveOffersPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.initAddOrRemoveOffersPage),
      map(() => merchantGroupsActions.getMerchantGroupOffers({}))
    )
  );

  updateMerchantGroupOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.updateMerchantGroupOffers),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupOffers),
        this._store.select(
          merchantsGroupsSelectors.selectAddOrRemoveOffersSelected
        )
      ),
      switchMap(([_, offers, offersSeleted]) => {
        const offerIdsToRemove: string[] =
          this._merchantGroupsService.setOfferIdsToRemove(
            offers,
            offersSeleted
          );
        return offerIdsToRemove?.length > 0
          ? of(
              merchantGroupsActions.showModalConfirmRemoveOffer({
                offerIdsToRemove,
              })
            )
          : of(merchantGroupsActions.addMerchantGroupOffers());
      })
    )
  );

  showModalConfirmRemoveOffer$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantGroupsActions.showModalConfirmRemoveOffer),
        map(() => this._appFacade.toggleModal())
      ),
    {
      dispatch: false,
    }
  );

  removeMerchantGroupOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.removeMerchantGroupOffers),
      withLatestFrom(
        this._store.select(
          merchantsGroupsSelectors.selectAddOrRemoveOffersOfferIdsToRemove
        ),
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([_, offer_ids, merchant_group_id]) => {
        const removeOffers: RemoveOffersOfMerchantGroup = {
          offer_ids,
        };
        return this._merchantGroupApi
          .removeMerchantGroupOffers(merchant_group_id, removeOffers)
          .pipe(
            map(() => merchantGroupsActions.addMerchantGroupOffers()),
            catchError((_) =>
              of(merchantGroupsActions.failedRemoveMerchantGroupOffers())
            )
          );
      })
    )
  );

  addMerchantGroupOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.addMerchantGroupOffers),
      withLatestFrom(
        this._store.select(
          merchantsGroupsSelectors.selectAddOrRemoveOffersFeesToAdd
        ),
        this._store.select(merchantsGroupsSelectors.selectMerchantGroupId)
      ),
      switchMap(([_, merchant_group_fees, merchant_group_id]) => {
        const updateOffers: UpdateOffersOfMerchantGroup = {
          merchant_group_fees,
        };
        return updateOffers?.merchant_group_fees?.length > 0
          ? this._merchantGroupApi
              .addMerchantGroupOffer(merchant_group_id, updateOffers)
              .pipe(
                map(() =>
                  merchantGroupsActions.successAddMerchantGroupOffers({
                    merchant_group_id,
                  })
                ),
                catchError((_) =>
                  of(merchantGroupsActions.failedAddMerchantGroupOffers())
                )
              )
          : of(
              merchantGroupsActions.successAddMerchantGroupOffers({
                merchant_group_id,
              })
            );
      })
    )
  );

  successAddMerchantGroupOffers$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantGroupsActions.successAddMerchantGroupOffers),
        map(({ merchant_group_id }) =>
          this._router.navigate(
            [MERCHANT_MANAGER_URLS.MERCHANT_GROUP_PROFILE],
            { queryParams: { id: merchant_group_id } }
          )
        )
      ),
    {
      dispatch: false,
    }
  );
  // ********************
  // NEW MERCHANT GROUPS
  // ********************

  checkMerchantGroupName$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.checkMerchantGroupName),
      switchMap(({ name }) =>
        this._merchantGroupApi.checkMerchantGroupName(name).pipe(
          map((response) =>
            response?.status === APP_CONSTANTS.HTTP_CODES.NOT_CONTENT
              ? merchantGroupsActions.successCheckMerchantGroupName()
              : merchantGroupsActions.failedCheckMerchantGroupName()
          ),
          catchError((_) =>
            of(merchantGroupsActions.failedCheckMerchantGroupName())
          )
        )
      )
    )
  );

  initNewGroupMerchantsPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.initNewGroupMerchantsPage),
      map(() =>
        merchantGroupsActions.updateFiltersMerchantGroupMerchants({
          filters: FILTERS,
        })
      )
    )
  );

  createNewMerchantGroup$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantGroupsActions.createNewMerchantGroup),
      withLatestFrom(
        this._store.select(merchantsGroupsSelectors.selectNewMerchantGroupData)
      ),
      switchMap(([_, newMerchantGroup]) => {
        const createNewMerchantGroup: CreateNewMerchantGroup =
          this._merchantGroupsService.setCreateNewMerchantGroup(
            newMerchantGroup
          );
        return this._merchantGroupApi
          .createNewMerchantGroup(createNewMerchantGroup)
          .pipe(
            map(() => merchantGroupsActions.successCreateNewMerchantGroup()),
            catchError((_) =>
              of(merchantGroupsActions.failedCreateNewMerchantGroup())
            )
          );
      })
    )
  );

  successCreateNewMerchantGroup$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantGroupsActions.successCreateNewMerchantGroup),
        tap(() => {
          this._router.navigateByUrl(
            MERCHANT_MANAGER_URLS.NEW_MERCHANT_GROUP_SUCCESS
          );
        })
      ),
    { dispatch: false }
  );

  resetNewMerchantGroup$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantGroupsActions.resetNewMerchantGroup),
        tap(() => {
          this._router.navigateByUrl(MERCHANT_MANAGER_URLS.MERCHANT_GROUPS);
        })
      ),
    { dispatch: false }
  );
}
