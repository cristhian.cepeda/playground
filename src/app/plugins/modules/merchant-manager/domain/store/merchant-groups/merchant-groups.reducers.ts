import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { deepCopy, setFilterValidation } from '@app/core/models/utils.model';
import { FILTERS_MERCHANTS } from '@app/plugins/modules/merchant-manager/core/constants/filters.constant';
import { MerchantGroupOffer } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
  cancelRemoveMerchantGroupMerchants,
  cancelRemoveMerchantGroupOffers,
  cancelUpdateCommissionMerchantGroupProfile,
  checkMerchantGroupName,
  destroyMerchantGroupProfilePage,
  destroyNewMerchantGroupPages,
  failedAddMerchantGroupOffers,
  failedCheckMerchantGroupName,
  failedGetMerchantGroupMerchants,
  failedGetMerchantGroupOffers,
  failedGetMerchantGroupProfileInfo,
  failedGetMerchantGroupProfileMerchants,
  failedGetMerchantGroupProfileOffers,
  failedGetMerchantGroups,
  failedUpdateCommissionMerchantGroupProfile,
  failedUpdateMerchantGroupMerchants,
  filterMerchantGroupProfileMerchants,
  getMerchantGroupOffers,
  getMerchantGroupProfileInfo,
  getMerchantGroupProfileMerchants,
  getMerchantGroupProfileOffers,
  initAddOrRemoveMerchantsPage,
  initAddOrRemoveOffersPage,
  resetNewMerchantGroup,
  showModalConfirmRemoveOffer,
  successCheckMerchantGroupName,
  successGetMerchantGroupMerchants,
  successGetMerchantGroupOffers,
  successGetMerchantGroupProfileInfo,
  successGetMerchantGroupProfileMerchants,
  successGetMerchantGroupProfileOffers,
  successGetMerchantGroups,
  successUpdateCommissionMerchantGroupProfile,
  successUpdateMerchantGroupMerchants,
  updateAddOrRemoveMerchantsSelected,
  updateAddOrRemoveOffersSelected,
  updateCommissionMerchantGroupProfile,
  updateFiltersMerchantGroupMerchants,
  updateFiltersMerchantGroups,
  updateMerchantGroupMerchants,
  updateMerchantGroupOffers,
  updateMerchantGroupProfileId,
  updateNewGroupSelectedMerchants,
  updateNewMerchantGroupData,
} from './merchant-groups.actions';
import { MerchantGroupsState } from './merchant-groups.state';

export const initialMerchantGroupsState: MerchantGroupsState = {
  filters: FILTERS_MERCHANTS,
};

const _merchantGroupsReducer = createReducer(
  initialMerchantGroupsState,
  // ********************
  // MERCHANT GROUPS
  // ********************
  on(updateFiltersMerchantGroups, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(successGetMerchantGroups, (state, { merchantGroups }) => ({
    ...state,
    merchantGroups,
    isLoading: false,
  })),
  on(failedGetMerchantGroups, (state) => ({
    ...state,
    merchantGroups: null,
    isLoading: false,
    isFiltred: false,
  })),
  // ***********************
  // MERCHANT GROUP PROFILE
  // ***********************
  on(destroyMerchantGroupProfilePage, (state) => ({
    ...state,
    merchantGroupProfile: null,
    merchants: null,
    offers: null,
  })),
  on(updateMerchantGroupProfileId, (state, { id }) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      id,
    },
  })),
  on(getMerchantGroupProfileInfo, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      isLoadingInfo: true,
    },
  })),
  on(successGetMerchantGroupProfileInfo, (state, { info }) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      info,
      isLoadingInfo: false,
    },
  })),
  on(failedGetMerchantGroupProfileInfo, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      info: null,
      isLoadingInfo: false,
    },
  })),
  on(filterMerchantGroupProfileMerchants, (state, { merchantNameFilter }) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      merchantNameFilter,
    },
  })),
  on(getMerchantGroupProfileMerchants, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      isLoadingMerchants: true,
    },
  })),
  on(successGetMerchantGroupProfileMerchants, (state, { merchants }) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      merchants,
      isLoadingMerchants: false,
    },
  })),
  on(failedGetMerchantGroupProfileMerchants, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      merchants: null,
      isLoadingMerchants: false,
    },
  })),
  on(getMerchantGroupProfileOffers, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      isLoadingOffers: true,
    },
  })),
  on(successGetMerchantGroupProfileOffers, (state, { offers }) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      offers,
      isLoadingOffers: false,
    },
  })),
  on(failedGetMerchantGroupProfileOffers, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      offers: null,
      isLoadingOffers: false,
    },
  })),
  on(updateCommissionMerchantGroupProfile, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      isLoadingUpdateCommission: true,
    },
  })),
  on(
    successUpdateCommissionMerchantGroupProfile,
    failedUpdateCommissionMerchantGroupProfile,
    cancelUpdateCommissionMerchantGroupProfile,
    (state) => ({
      ...state,
      merchantGroupProfile: {
        ...state?.merchantGroupProfile,
        isLoadingUpdateCommission: false,
      },
    })
  ),
  // ADD OR REMOVE MERCHANTS
  on(initAddOrRemoveMerchantsPage, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      addOrRemoveMerchantsSelected: state?.merchantGroupProfile?.merchants,
    },
  })),
  on(
    updateAddOrRemoveMerchantsSelected,
    (state, { merchantSelected, remove = false }) => {
      let merchantsList: Merchant[] =
        deepCopy(state?.merchantGroupProfile?.addOrRemoveMerchantsSelected) ??
        [];
      if (remove)
        merchantsList = merchantsList.filter(
          (merchant) => merchant?.id !== merchantSelected?.id
        );
      else merchantsList.push(merchantSelected);
      return {
        ...state,
        merchantGroupProfile: {
          ...state?.merchantGroupProfile,
          addOrRemoveMerchantsSelected: merchantsList,
        },
      };
    }
  ),
  on(updateMerchantGroupMerchants, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      isLoadingUpdateMerchants: true,
    },
  })),
  on(
    successUpdateMerchantGroupMerchants,
    cancelRemoveMerchantGroupMerchants,
    failedUpdateMerchantGroupMerchants,
    (state) => ({
      ...state,
      merchantGroupProfile: {
        ...state?.merchantGroupProfile,
        isLoadingUpdateMerchants: false,
      },
    })
  ),
  // ADD OR REMOVE OFFERS
  on(initAddOrRemoveOffersPage, (state) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      addOrRemoveOffersSelected: state?.merchantGroupProfile?.offers,
      offerIdsToRemove: [],
      feesToAdd: [],
      isLoadingUpdateOffers: false,
    },
  })),
  on(
    updateAddOrRemoveOffersSelected,
    (state, { offerSelected, remove = false }) => {
      let offersList: MerchantGroupOffer[] =
        deepCopy(state?.merchantGroupProfile?.addOrRemoveOffersSelected) ?? [];
      if (remove)
        offersList = offersList.filter(
          (offer) => offer?.offer_id !== offerSelected?.offer_id
        );
      else offersList.push(offerSelected);
      return {
        ...state,
        merchantGroupProfile: {
          ...state?.merchantGroupProfile,
          addOrRemoveOffersSelected: offersList,
        },
      };
    }
  ),
  on(updateMerchantGroupOffers, (state, { feesToAdd }) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      feesToAdd,
      isLoadingUpdateOffers: true,
    },
  })),
  on(showModalConfirmRemoveOffer, (state, { offerIdsToRemove }) => ({
    ...state,
    merchantGroupProfile: {
      ...state?.merchantGroupProfile,
      offerIdsToRemove,
    },
  })),
  on(
    cancelRemoveMerchantGroupOffers,
    failedAddMerchantGroupOffers,
    (state) => ({
      ...state,
      merchantGroupProfile: {
        ...state?.merchantGroupProfile,
        isLoadingUpdateOffers: false,
      },
    })
  ),
  // ********************
  // NEW MERCHANT GROUPS
  // ********************
  on(destroyNewMerchantGroupPages, (state) => ({
    ...state,
    newMerchantGroupDetailsValidations: null,
    newMerchantGroupData: null,
    merchants: null,
    offers: null,
  })),
  on(checkMerchantGroupName, (state) => ({
    ...state,
    newMerchantGroupDetailsValidations: {
      isLoadingMerchantName: true,
    },
  })),
  on(successCheckMerchantGroupName, (state) => ({
    ...state,
    newMerchantGroupDetailsValidations: {
      isLoadingMerchantName: false,
      isValidMerchantName: true,
    },
  })),
  on(failedCheckMerchantGroupName, (state) => ({
    ...state,
    newMerchantGroupDetailsValidations: {
      isLoadingMerchantName: false,
      isValidMerchantName: false,
    },
  })),
  on(resetNewMerchantGroup, (state) => ({
    ...state,
    newMerchantGroupDetailsValidations: null,
    merchants: null,
    offers: null,
    newMerchantGroupData: null,
  })),
  on(updateNewMerchantGroupData, (state, { newMerchantGroupData }) => ({
    ...state,
    newMerchantGroupData: {
      ...state?.newMerchantGroupData,
      ...newMerchantGroupData,
    },
  })),
  on(
    updateNewGroupSelectedMerchants,
    (state, { merchantSelected, remove = false }) => {
      let merchantsList: Merchant[] =
        deepCopy(state?.newMerchantGroupData?.merchantsSelected) ?? [];
      if (remove)
        merchantsList = merchantsList.filter(
          (merchant) => merchant?.id !== merchantSelected?.id
        );
      else merchantsList.push(merchantSelected);
      return {
        ...state,
        newMerchantGroupData: {
          ...state?.newMerchantGroupData,
          merchantsSelected: merchantsList,
        },
      };
    }
  ),
  on(
    updateFiltersMerchantGroupMerchants,
    (state, { filters, filtersOptions }) => {
      const FiltersValidationsOptions: FiltersValidationsOptions =
        setFilterValidation(state?.merchants, filters, filtersOptions);
      return {
        ...state,
        merchants: {
          ...state?.merchants,
          isFiltred: FiltersValidationsOptions?.isFiltred,
          filters: FiltersValidationsOptions?.filters,
        },
      };
    }
  ),
  on(successGetMerchantGroupMerchants, (state, { merchants }) => ({
    ...state,
    merchants: {
      ...state?.merchants,
      data: merchants,
      isLoading: false,
    },
  })),
  on(failedGetMerchantGroupMerchants, (state) => ({
    ...state,
    merchants: {
      ...state?.merchants,
      data: null,
      isLoading: false,
      isFiltred: false,
    },
  })),
  on(getMerchantGroupOffers, (state, { filters, filtersOptions }) => ({
    ...state,
    offers: {
      ...state?.offers,
      isLoading: true,
      filters,
      isFiltred: filtersOptions?.isFiltred,
    },
  })),
  on(successGetMerchantGroupOffers, (state, { offers }) => ({
    ...state,
    offers: {
      ...state?.offers,
      data: offers,
      isLoading: false,
    },
  })),
  on(failedGetMerchantGroupOffers, (state) => ({
    ...state,
    offers: {
      ...state?.offers,
      data: null,
      isLoading: false,
      isFiltred: false,
    },
  }))
);

export function MerchantGroupsReducers(
  state: MerchantGroupsState | undefined,
  action: Action
) {
  return _merchantGroupsReducer(state, action);
}
