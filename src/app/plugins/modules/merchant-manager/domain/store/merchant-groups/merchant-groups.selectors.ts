import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MerchantGroupsState } from './merchant-groups.state';

export const getMerchantGroupsFeatureState =
  createFeatureSelector<MerchantGroupsState>(
    MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.STORE_NAME
  );

// ********************
// MERCHANT GROUPS
// ********************

export const selectMerchantGroupsFilters = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.filters
);

export const selectMerchantGroups = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroups
);

export const selectIsMerchantGroupsFiltred = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.isFiltred
);

export const selectIsLoadingMerchantGroups = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.isLoading
);

export const selectIsFiltredGroupMerchants = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchants?.isFiltred
);
export const selectIsLoadingGroupMerchants = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchants?.isLoading
);

export const selectGroupMerchants = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchants?.data
);
export const selectGroupMerchantsFilters = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchants?.filters
);

export const selectGroupOffers = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.offers?.data
);
export const selectGroupOffersFilters = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.offers?.filters
);
export const selectIsFiltredGroupOffers = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.offers?.isFiltred
);
export const selectIsLoadingGroupOffers = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.offers?.isLoading
);

// ***********************
// MERCHANT GROUP PROFILE
// ***********************

export const selectMerchantGroupId = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.id
);

export const selectMerchantGroupInfo = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.info
);

export const selectIsLoadingMerchantGroupInfo = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.isLoadingInfo
);

export const selectMerchantGroupMerchants = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.merchants
);

export const selectIsLoadingMerchantGroupMerchants = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.merchantGroupProfile?.isLoadingMerchants
);

export const selectMerchantNameFilterMerchantGroupMerchants = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.merchantGroupProfile?.merchantNameFilter
);

export const selectMerchantGroupOffers = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.offers
);

export const selectIsLoadingMerchantGroupOffers = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.isLoadingOffers
);

export const selectIsLoadingMerchantGroupUpdateCommission = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.merchantGroupProfile?.isLoadingUpdateCommission
);

// Add Or Remove Merchants
export const selectAddOrRemoveMerchantsSelected = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.merchantGroupProfile?.addOrRemoveMerchantsSelected
);
export const selectIsLoadingUpdateMerchantsAddOrRemoveMerchants =
  createSelector(
    getMerchantGroupsFeatureState,
    (state: MerchantGroupsState) =>
      state?.merchantGroupProfile?.isLoadingUpdateMerchants
  );
// Add Or Remove Offers
export const selectAddOrRemoveOffersSelected = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.merchantGroupProfile?.addOrRemoveOffersSelected
);
export const selectAddOrRemoveOffersOfferIdsToRemove = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.offerIdsToRemove
);
export const selectAddOrRemoveOffersFeesToAdd = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.merchantGroupProfile?.feesToAdd
);
export const selectIsLoadingAddOrRemoveOffersUpdateOffers = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.merchantGroupProfile?.isLoadingUpdateOffers
);

// ********************
// NEW MERCHANT GROUPS
// ********************

export const selectNewMerchantGroupData = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.newMerchantGroupData
);

export const selectIsLoadingMerchantName = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.newMerchantGroupDetailsValidations?.isLoadingMerchantName
);

export const selectNewGroupMerchantsDetails = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.newMerchantGroupData?.details
);

export const selectNewGroupMerchantsSelected = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.newMerchantGroupData?.merchantsSelected
);

export const selectIsValidMerchantName = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) =>
    state?.newMerchantGroupDetailsValidations?.isValidMerchantName
);

export const selectNewGroupSelectedOfferFees = createSelector(
  getMerchantGroupsFeatureState,
  (state: MerchantGroupsState) => state?.newMerchantGroupData?.fees
);
