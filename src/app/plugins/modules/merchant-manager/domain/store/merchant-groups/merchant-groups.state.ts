import { TablePageActions } from '@app/core/models/page.model';
import {
  MerchantGroup,
  MerchantGroupFees,
  MerchantGroupFilters,
  MerchantGroupOffer,
  MerchantGroupProfile,
  NewMerchantGroup,
  NewMerchantGroupDetailsValidations,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import {
  Merchant,
  MerchantFilters,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { OffersFilters } from '@app/plugins/modules/merchant-manager/core/models/offer.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface MerchantGroupsState extends TablePageActions {
  // Merchants Groups
  merchantGroups?: TableResponse<MerchantGroup>;
  filters?: MerchantGroupFilters;
  merchants?: MerchantGroupMerchantsState;
  offers?: MerchantGroupOffersState;

  // Merchant group profile
  merchantGroupProfile?: MerchantGroupProfileState;

  // New Merchant group
  newMerchantGroupDetailsValidations?: NewMerchantGroupDetailsValidations;
  newMerchantGroupData?: NewMerchantGroup;
}

export interface MerchantGroupProfileState {
  id?: string;
  info: MerchantGroupProfile;
  isLoadingInfo?: boolean;
  merchants?: Merchant[];
  isLoadingMerchants?: boolean;
  merchantNameFilter?: string;
  addOrRemoveMerchantsSelected?: Merchant[];
  isLoadingUpdateMerchants?: boolean;
  offers?: MerchantGroupOffer[];
  isLoadingOffers?: boolean;
  isLoadingUpdateCommission?: boolean;
  addOrRemoveOffersSelected?: MerchantGroupOffer[];
  offerIdsToRemove?: string[];
  feesToAdd: MerchantGroupFees[];
  isLoadingUpdateOffers?: boolean;
}

export interface MerchantGroupMerchantsState extends TablePageActions {
  data?: TableResponse<Merchant>;
  filters?: MerchantFilters;
}

export interface MerchantGroupOffersState extends TablePageActions {
  data?: MerchantGroupOffer[];
  filters?: OffersFilters;
}
