import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import { Loan } from '@app/plugins/modules/merchant-manager/core/models/loan.model';
import {
  Merchant,
  MerchantFilters,
  MerchantGroupAndOffersProfile,
  MerchantProfile,
  MerchantSummary,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

// ********************
// MERCHANTS
// ********************

export const initMerchantsPage = createAction(
  '[Merchants Page] Init Merchants Page'
);

export const updateFiltersMerchants = createAction(
  '[Merchants Filters Component] Update Filters Merchants',
  props<{ filters: MerchantFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetMerchants = createAction(
  '[Merchants Effects] Success Get Merchants',
  props<{ merchants?: TableResponse<Merchant> }>()
);

export const failedGetMerchants = createAction(
  '[Merchants Effects] Failed Get Merchants'
);

export const downloadMerchants = createAction(
  '[Merchants Page] Downloand Merchants'
);

export const successDownloadMerchants = createAction(
  '[Merchants Effects] success Downloand Merchants',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadMerchants = createAction(
  '[Merchants Effects] failed Downloand Merchants'
);

// ********************
// MERCHANT PROFILE
// ********************

export const updateMerchantId = createAction(
  '[Merchant Page] Update Merchants Id',
  props<{ merchantId: string }>()
);

export const getMerchantProfile = createAction(
  '[Merchant Page] Get Merchant Profile'
);

export const successGetMerchantProfile = createAction(
  '[Merchant Effects] Success Get Merchant Profile',
  props<{ merchant?: MerchantProfile }>()
);

export const failedGetMerchantProfile = createAction(
  '[Merchant Effects] Failed Get Merchant Profile'
);

export const destroyMerchantProfilePage = createAction(
  '[Merchant Profile Page] Destroy Merchant Profile Page'
);

export const getMerchantSummary = createAction(
  '[Merchant Page] Get Summary Merchant Profile'
);

export const successGetMerchantSummary = createAction(
  '[Merchant Effects] Success Get Summary Merchant Profile',
  props<{ summary?: MerchantSummary }>()
);

export const failedGetMerchantSummary = createAction(
  '[Merchant Effects] Failed Get Summary Merchant Profile'
);

export const getMerchantPurchases = createAction(
  '[Merchant Page] Get Purchases Merchant Profile'
);

export const successGetMerchantPurchases = createAction(
  '[Merchant Effects] Success Get Purchases Merchant Profile',
  props<{ purchases?: Loan[] }>()
);

export const failedGetMerchantPurchases = createAction(
  '[Merchant Effects] Failed Get Purchases Merchant Profile'
);

export const getMerchantGroupAndOffers = createAction(
  '[Merchant Page] Get Group And Offers Merchant Profile'
);

export const successGetMerchantGroupAndOffers = createAction(
  '[Merchant Effects] Success Get Group And Offers Merchant Profile',
  props<{ groupAndOffers?: MerchantGroupAndOffersProfile }>()
);

export const failedGetMerchantGroupAndOffers = createAction(
  '[Merchant Effects] Failed Get Group And Offers Merchant Profile'
);
