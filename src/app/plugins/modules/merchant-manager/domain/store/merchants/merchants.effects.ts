import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { FILTERS_MERCHANT_PROFILE_TABLES } from '@app/plugins/modules/merchant-manager/core/constants/filters.constant';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantsApi } from '@app/plugins/modules/merchant-manager/data/api/merchants.api';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as merchantsActions from './merchants.actions';
import * as merchantsSelectors from './merchants.selectors';
@Injectable()
export class MerchantsEffects {
  constructor(
    private _actions$: Actions,
    private _merchantApi: MerchantsApi,
    private _store: Store,
    private _utilsService: UtilsService
  ) {}

  initMerchantsPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantsActions.initMerchantsPage),
      map(() => merchantsActions.updateFiltersMerchants({ filters: FILTERS }))
    )
  );

  getMerchants$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantsActions.updateFiltersMerchants),
      withLatestFrom(
        this._store.select(merchantsSelectors.selectMerchantsFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._merchantApi.getMerchants(filters).pipe(
          map((merchants: TableResponse<Merchant>) =>
            merchantsActions.successGetMerchants({ merchants })
          ),
          catchError((_) => of(merchantsActions.failedGetMerchants()))
        );
      })
    )
  );

  downloadMerchants$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantsActions.downloadMerchants),
      withLatestFrom(
        this._store.select(merchantsSelectors.selectMerchantsFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._merchantApi.downloadMerchants(fileFilters).pipe(
          map((fileResponse) =>
            merchantsActions.successDownloadMerchants({ fileResponse })
          ),
          catchError((_) => of(merchantsActions.failedDownloadMerchants()))
        );
      })
    )
  );

  successDownloadMerchants$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(merchantsActions.successDownloadMerchants),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.NAME
          )
        )
      ),
    { dispatch: false }
  );

  getMerchantProfile$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantsActions.getMerchantProfile),
      withLatestFrom(this._store.select(merchantsSelectors.selectmMerchantId)),
      switchMap(([_, merchantId]) =>
        this._merchantApi.getMerchantProfile(merchantId).pipe(
          map((merchant) =>
            merchantsActions.successGetMerchantProfile({ merchant })
          ),
          catchError((_) => of(merchantsActions.failedGetMerchantProfile()))
        )
      )
    )
  );

  getMerchantSummary$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantsActions.getMerchantSummary),
      withLatestFrom(this._store.select(merchantsSelectors.selectmMerchantId)),
      switchMap(([_, merchantId]) =>
        this._merchantApi.getMerchantSummary(merchantId).pipe(
          map((summary) =>
            merchantsActions.successGetMerchantSummary({
              summary,
            })
          ),
          catchError((_) => of(merchantsActions.failedGetMerchantSummary()))
        )
      )
    )
  );

  getMerchantPurchases$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantsActions.getMerchantPurchases),
      withLatestFrom(this._store.select(merchantsSelectors.selectmMerchantId)),
      switchMap(([_, merchant_id]) =>
        this._merchantApi
          .getMerchantPurchases({
            ...FILTERS_MERCHANT_PROFILE_TABLES,
            merchant_id,
          })
          .pipe(
            map(({ results }) =>
              merchantsActions.successGetMerchantPurchases({
                purchases: results,
              })
            ),
            catchError((_) => of(merchantsActions.failedGetMerchantPurchases()))
          )
      )
    )
  );

  getMerchantGroupAndOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(merchantsActions.getMerchantGroupAndOffers),
      withLatestFrom(this._store.select(merchantsSelectors.selectmMerchantId)),
      switchMap(([_, merchantId]) =>
        this._merchantApi.getMerchantGroupAndOffers(merchantId).pipe(
          map((groupAndOffers) =>
            merchantsActions.successGetMerchantGroupAndOffers({
              groupAndOffers,
            })
          ),
          catchError((_) =>
            of(merchantsActions.failedGetMerchantGroupAndOffers())
          )
        )
      )
    )
  );
}
