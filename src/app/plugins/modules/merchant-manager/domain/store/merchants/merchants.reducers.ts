import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
  destroyMerchantProfilePage,
  failedGetMerchantGroupAndOffers,
  failedGetMerchantProfile,
  failedGetMerchantPurchases,
  failedGetMerchants,
  failedGetMerchantSummary,
  getMerchantGroupAndOffers,
  getMerchantProfile,
  getMerchantPurchases,
  getMerchantSummary,
  successGetMerchantGroupAndOffers,
  successGetMerchantProfile,
  successGetMerchantPurchases,
  successGetMerchants,
  successGetMerchantSummary,
  updateFiltersMerchants,
  updateMerchantId,
} from './merchants.actions';
import { MerchantState } from './merchants.state';

export const initialMerchantState: MerchantState = {
  filters: FILTERS,
};

const _merchantReducer = createReducer(
  initialMerchantState,
  on(updateFiltersMerchants, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(successGetMerchants, (state, { merchants }) => ({
    ...state,
    merchants,
    isLoading: false,
  })),
  on(failedGetMerchants, (state) => ({
    ...state,
    merchants: null,
    isLoading: false,
    isFiltred: false,
  })),
  on(updateMerchantId, (state, { merchantId }) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      merchantId,
    },
  })),
  on(getMerchantProfile, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      isLoadingMerchantProfile: true,
    },
  })),
  on(successGetMerchantProfile, (state, { merchant }) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      merchant,
      isLoadingMerchantProfile: false,
    },
  })),
  on(failedGetMerchantProfile, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      merchant: null,
      isLoadingMerchantProfile: false,
    },
  })),
  on(destroyMerchantProfilePage, (state) => ({
    ...state,
    merchantProfile: null,
  })),
  on(getMerchantSummary, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      isLoadingSummary: true,
    },
  })),
  on(successGetMerchantSummary, (state, { summary }) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      summary,
      isLoadingSummary: false,
    },
  })),
  on(failedGetMerchantSummary, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      summary: null,
      isLoadingSummary: false,
    },
  })),
  on(getMerchantPurchases, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      isLoadingPurchases: true,
    },
  })),
  on(successGetMerchantPurchases, (state, { purchases }) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      purchases,
      isLoadingPurchases: false,
    },
  })),
  on(failedGetMerchantPurchases, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      purchases: null,
      isLoadingPurchases: false,
    },
  })),
  on(getMerchantGroupAndOffers, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      isLoadingGroupAndOffers: true,
    },
  })),
  on(successGetMerchantGroupAndOffers, (state, { groupAndOffers }) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      groupAndOffers,
      isLoadingGroupAndOffers: false,
    },
  })),
  on(failedGetMerchantGroupAndOffers, (state) => ({
    ...state,
    merchantProfile: {
      ...state?.merchantProfile,
      groupAndOffers: null,
      isLoadingGroupAndOffers: false,
    },
  }))
);

export function MerchantsReducers(
  state: MerchantState | undefined,
  action: Action
) {
  return _merchantReducer(state, action);
}
