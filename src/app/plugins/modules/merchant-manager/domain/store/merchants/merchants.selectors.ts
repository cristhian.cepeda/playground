import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MerchantState } from './merchants.state';

export const getMerchantsFeatureState = createFeatureSelector<MerchantState>(
  MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.STORE_NAME
);

export const selectMerchantsFilters = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.filters
);

export const selectMerchants = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchants
);

export const selectIsMerchantsFiltred = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.isFiltred
);

export const selectIsLoadingMerchants = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.isLoading
);

export const selectmMerchantId = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.merchantId
);

export const selectMerchantProfile = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.merchant
);
export const selectIsLoadingMerchantProfile = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.isLoadingMerchantProfile
);

export const selectMerchantSummary = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.summary
);
export const selectIsLoadingMerchantSummary = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.isLoadingSummary
);

export const selectMerchantPurchases = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.purchases
);
export const selectIsLoadingMerchantPurchases = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.isLoadingPurchases
);

export const selectMerchantGroupAndOffers = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.groupAndOffers
);
export const selectIsLoadingMerchantGroupAndOffers = createSelector(
  getMerchantsFeatureState,
  (state: MerchantState) => state?.merchantProfile?.isLoadingGroupAndOffers
);
