import { TablePageActions } from '@app/core/models/page.model';
import { Loan } from '@app/plugins/modules/merchant-manager/core/models/loan.model';
import {
  Merchant,
  MerchantFilters,
  MerchantGroupAndOffersProfile,
  MerchantProfile,
  MerchantSummary,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface MerchantState extends TablePageActions {
  merchants?: TableResponse<Merchant>;
  filters?: MerchantFilters;
  merchantProfile?: MerchantProfileState;
}

export interface MerchantProfileState {
  merchantId: string;
  merchant: MerchantProfile;
  isLoadingMerchantProfile?: boolean;
  summary?: MerchantSummary;
  isLoadingSummary?: boolean;
  purchases?: Loan[];
  isLoadingPurchases?: boolean;
  groupAndOffers: MerchantGroupAndOffersProfile;
  isLoadingGroupAndOffers?: boolean;
}
