import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  Movement,
  MovementFilters,
} from '@app/plugins/modules/merchant-manager/core/models/movement.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initMovementsPage = createAction(
  '[Movements Page] Init Movements Page',
  props<{ filters: MovementFilters }>()
);

export const updateFiltersMovements = createAction(
  '[Movements Filters Component] Update Filters Movements',
  props<{ filters: MovementFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetMovements = createAction(
  '[Movements Effects] Success Get Movements',
  props<{ movements?: TableResponse<Movement> }>()
);

export const failedGetMovements = createAction(
  '[Movements Effects] Failed Get Movements'
);

export const downloadMovements = createAction(
  '[Movements Page] Downloand Movements'
);

export const successDownloadMovements = createAction(
  '[Movements Effects] success Downloand Movements',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadMovements = createAction(
  '[Movements Effects] failed Downloand Movements'
);
