import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { MovementFilters } from '@app/plugins/modules/merchant-manager/core/models/movement.model';
import { MovementsApi } from '@app/plugins/modules/merchant-manager/data/api/movements.api';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as movementsActions from './movements.actions';
import * as movementsSelectors from './movements.selectors';
@Injectable()
export class MovementsEffects {
  constructor(
    private _actions$: Actions,
    private _movementApi: MovementsApi,
    private _store: Store,
    private _utilsService: UtilsService
  ) {}

  initMovementsPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(movementsActions.initMovementsPage),
      map(({ filters }) => {
        const movementsFilters: MovementFilters = {
          ...FILTERS,
          ...filters,
        };
        return movementsActions.updateFiltersMovements({
          filters: movementsFilters,
          filtersOptions: { isFiltred: !!filters },
        });
      })
    )
  );

  getMovements$ = createEffect(() =>
    this._actions$.pipe(
      ofType(movementsActions.updateFiltersMovements),
      withLatestFrom(
        this._store.select(movementsSelectors.selectMovementsFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._movementApi.getMovements(filters).pipe(
          map((movements) =>
            movementsActions.successGetMovements({ movements })
          ),
          catchError((_) => of(movementsActions.failedGetMovements()))
        );
      })
    )
  );

  downloadMovements$ = createEffect(() =>
    this._actions$.pipe(
      ofType(movementsActions.downloadMovements),
      withLatestFrom(
        this._store.select(movementsSelectors.selectMovementsFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._movementApi.downloadMovements(fileFilters).pipe(
          map((fileResponse) =>
            movementsActions.successDownloadMovements({ fileResponse })
          ),
          catchError((_) => of(movementsActions.failedDownloadMovements()))
        );
      })
    )
  );

  successDownloadMovements$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(movementsActions.successDownloadMovements),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            MERCHANT_MANAGER_FEATURE.PAGES.MOVEMENTS.NAME
          )
        )
      ),
    { dispatch: false }
  );
}
