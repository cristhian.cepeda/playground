import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
  failedGetMovements,
  successGetMovements,
  updateFiltersMovements,
} from './movements.actions';
import { MovementsState } from './movements.state';

export const initialMovementState: MovementsState = {
  filters: FILTERS,
};

const _movementReducer = createReducer(
  initialMovementState,
  on(updateFiltersMovements, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(successGetMovements, (state, { movements }) => ({
    ...state,
    movements,
    isLoading: false,
  })),
  on(failedGetMovements, (state) => ({
    ...state,
    movements: null,
    isLoading: false,
    isFiltred: false,
  }))
);

export function MovementsReducers(
  state: MovementsState | undefined,
  action: Action
) {
  return _movementReducer(state, action);
}
