import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MovementsState } from './movements.state';

export const getMovementsFeatureState = createFeatureSelector<MovementsState>(
  MERCHANT_MANAGER_FEATURE.PAGES.MOVEMENTS.STORE_NAME
);

export const selectMovementsFilters = createSelector(
  getMovementsFeatureState,
  (state: MovementsState) => state?.filters
);

export const selectMovements = createSelector(
  getMovementsFeatureState,
  (state: MovementsState) => state?.movements
);

export const selectIsMovementsFiltred = createSelector(
  getMovementsFeatureState,
  (state: MovementsState) => state?.isFiltred
);

export const selectIsLoadingMovements = createSelector(
  getMovementsFeatureState,
  (state: MovementsState) => state?.isLoading
);
