import { TablePageActions } from '@app/core/models/page.model';
import {
  Movement,
  MovementFilters,
} from '@app/plugins/modules/merchant-manager/core/models/movement.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface MovementsState extends TablePageActions {
  movements?: TableResponse<Movement>;
  filters?: MovementFilters;
}
