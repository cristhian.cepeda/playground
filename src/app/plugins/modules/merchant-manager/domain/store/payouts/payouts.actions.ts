import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  Payout,
  PayoutFilters,
} from '@app/plugins/modules/merchant-manager/core/models/payout.modes';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initPayoutsPage = createAction('[Payouts Page] Init Payouts Page');

export const updateFiltersPayouts = createAction(
  '[Payouts Filters Component] Update Filters Payouts',
  props<{ filters: PayoutFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetPayouts = createAction(
  '[Payouts Effects] Success Get Payouts',
  props<{ payouts?: TableResponse<Payout> }>()
);

export const failedGetPayouts = createAction(
  '[Payouts Effects] Failed Get Payouts'
);

export const downloadPayouts = createAction('[Payouts Page] Downloand Payouts');

export const successDownloadPayouts = createAction(
  '[Payouts Effects] success Downloand Payouts',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadPayouts = createAction(
  '[Payouts Effects] failed Downloand Payouts'
);
