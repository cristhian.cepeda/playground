import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { PayoutsApi } from '@app/plugins/modules/merchant-manager/data/api/payouts.api';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as payoutsActions from './payouts.actions';
import * as payoutsSelectors from './payouts.selectors';
@Injectable()
export class PayoutsEffects {
  constructor(
    private _actions$: Actions,
    private _payoutApi: PayoutsApi,
    private _store: Store,
    private _utilsService: UtilsService
  ) {}

  initPayoutsPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(payoutsActions.initPayoutsPage),
      map(() => payoutsActions.updateFiltersPayouts({ filters: FILTERS }))
    )
  );

  getPayouts$ = createEffect(() =>
    this._actions$.pipe(
      ofType(payoutsActions.updateFiltersPayouts),
      withLatestFrom(this._store.select(payoutsSelectors.selectPayoutsFilters)),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._payoutApi.getPayouts(filters).pipe(
          map((payouts) => payoutsActions.successGetPayouts({ payouts })),
          catchError((_) => of(payoutsActions.failedGetPayouts()))
        );
      })
    )
  );

  downloadPayouts$ = createEffect(() =>
    this._actions$.pipe(
      ofType(payoutsActions.downloadPayouts),
      withLatestFrom(this._store.select(payoutsSelectors.selectPayoutsFilters)),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._payoutApi.downloadPayouts(fileFilters).pipe(
          map((fileResponse) =>
            payoutsActions.successDownloadPayouts({ fileResponse })
          ),
          catchError((_) => of(payoutsActions.failedDownloadPayouts()))
        );
      })
    )
  );

  successDownloadPayouts$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(payoutsActions.successDownloadPayouts),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            MERCHANT_MANAGER_FEATURE.PAGES.PAYOUTS.NAME
          )
        )
      ),
    { dispatch: false }
  );
}
