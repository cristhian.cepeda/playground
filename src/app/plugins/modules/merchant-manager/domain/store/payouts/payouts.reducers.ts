import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
  failedGetPayouts,
  successGetPayouts,
  updateFiltersPayouts,
} from './payouts.actions';
import { PayoutsState } from './payouts.state';

export const initialPayoutState: PayoutsState = {
  filters: FILTERS,
};

const _payoutReducer = createReducer(
  initialPayoutState,
  on(updateFiltersPayouts, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(successGetPayouts, (state, { payouts }) => ({
    ...state,
    payouts,
    isLoading: false,
  })),
  on(failedGetPayouts, (state) => ({
    ...state,
    payouts: null,
    isLoading: false,
    isFiltred: false,
  }))
);

export function PayoutsReducers(
  state: PayoutsState | undefined,
  action: Action
) {
  return _payoutReducer(state, action);
}
