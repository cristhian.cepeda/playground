import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PayoutsState } from './payouts.state';

export const getPayoutsFeatureState = createFeatureSelector<PayoutsState>(
  MERCHANT_MANAGER_FEATURE.PAGES.PAYOUTS.STORE_NAME
);

export const selectPayoutsFilters = createSelector(
  getPayoutsFeatureState,
  (state: PayoutsState) => state?.filters
);

export const selectPayouts = createSelector(
  getPayoutsFeatureState,
  (state: PayoutsState) => state?.payouts
);

export const selectIsPayoutsFiltred = createSelector(
  getPayoutsFeatureState,
  (state: PayoutsState) => state?.isFiltred
);

export const selectIsLoadingPayouts = createSelector(
  getPayoutsFeatureState,
  (state: PayoutsState) => state?.isLoading
);
