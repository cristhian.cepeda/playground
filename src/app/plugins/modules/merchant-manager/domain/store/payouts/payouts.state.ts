import { TablePageActions } from '@app/core/models/page.model';
import {
  Payout,
  PayoutFilters,
} from '@app/plugins/modules/merchant-manager/core/models/payout.modes';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface PayoutsState extends TablePageActions {
  payouts?: TableResponse<Payout>;
  filters?: PayoutFilters;
}
