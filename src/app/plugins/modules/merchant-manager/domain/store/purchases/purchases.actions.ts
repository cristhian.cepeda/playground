// ********************
// PURCHASES
// ********************

import { HttpResponse } from '@angular/common/http';
import { FiltersOptions } from '@app/core/models/filters.model';
import {
  Purchase,
  PurchaseFilters,
} from '@app/plugins/modules/merchant-manager/core/models/purchase.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

export const initPurchasesPage = createAction(
  '[Purchases Page] Init Purchases Page'
);

export const updateFiltersPurchases = createAction(
  '[Purchases Filters Component] Update Filters Purchases',
  props<{ filters: PurchaseFilters; filtersOptions?: FiltersOptions }>()
);

export const successGetPurchases = createAction(
  '[Purchases Effects] Success Get Purchases',
  props<{ purchases?: TableResponse<Purchase> }>()
);

export const failedGetPurchases = createAction(
  '[Purchases Effects] Failed Get Purchases'
);

export const downloadPurchases = createAction(
  '[Purchases Page] Downloand Purchases'
);

export const successDownloadPurchases = createAction(
  '[Purchases Effects] success Downloand Purchases',
  props<{ fileResponse: HttpResponse<string> }>()
);

export const failedDownloadPurchases = createAction(
  '[Purchases Effects] failed Downloand Purchases'
);
