import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import { UtilsService } from '@app/core/services/utils/utils.service';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { PurchasesApi } from '@app/plugins/modules/merchant-manager/data/api/purchases.api';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, withLatestFrom } from 'rxjs';
import * as purchasesActions from './purchases.actions';
import * as purchasesSelectors from './purchases.selectors';
@Injectable()
export class PurchasesEffects {
  constructor(
    private _actions$: Actions,
    private _purchaseApi: PurchasesApi,
    private _store: Store,
    private _utilsService: UtilsService
  ) {}

  initPurchasesPage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(purchasesActions.initPurchasesPage),
      map(() => purchasesActions.updateFiltersPurchases({ filters: FILTERS }))
    )
  );

  getPurchases$ = createEffect(() =>
    this._actions$.pipe(
      ofType(purchasesActions.updateFiltersPurchases),
      withLatestFrom(
        this._store.select(purchasesSelectors.selectPurchasesFilters)
      ),
      switchMap(([{ filtersOptions }, filters]) => {
        if (!filtersOptions?.isFiltred) this._utilsService.removeQueryParams();
        return this._purchaseApi.getPurchases(filters).pipe(
          map((purchases) =>
            purchasesActions.successGetPurchases({ purchases })
          ),
          catchError((_) => of(purchasesActions.failedGetPurchases()))
        );
      })
    )
  );

  downloadPurchases$ = createEffect(() =>
    this._actions$.pipe(
      ofType(purchasesActions.downloadPurchases),
      withLatestFrom(
        this._store.select(purchasesSelectors.selectPurchasesFilters)
      ),
      switchMap(([_, filters]) => {
        const fileFilters = this._utilsService.removePaginationFilter(filters);
        return this._purchaseApi.downloadPurchases(fileFilters).pipe(
          map((fileResponse) =>
            purchasesActions.successDownloadPurchases({ fileResponse })
          ),
          catchError((_) => of(purchasesActions.failedDownloadPurchases()))
        );
      })
    )
  );

  successDownloadPurchases$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(purchasesActions.successDownloadPurchases),
        map(({ fileResponse }) =>
          this._utilsService.downloadFile(
            fileResponse,
            MERCHANT_MANAGER_FEATURE.PAGES.PURCHASES.NAME
          )
        )
      ),
    { dispatch: false }
  );
}
