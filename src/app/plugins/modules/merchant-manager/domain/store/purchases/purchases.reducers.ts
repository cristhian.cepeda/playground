import { FILTERS } from '@app/core/constants/filters.constant';
import { FiltersValidationsOptions } from '@app/core/models/filters.model';
import { setFilterValidation } from '@app/core/models/utils.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
  failedGetPurchases,
  successGetPurchases,
  updateFiltersPurchases,
} from './purchases.actions';
import { PurchaseState } from './purchases.state';

export const initialPurchaseState: PurchaseState = {
  filters: FILTERS,
};

const _purchaseReducer = createReducer(
  initialPurchaseState,
  on(updateFiltersPurchases, (state, { filters, filtersOptions }) => {
    const FiltersValidationsOptions: FiltersValidationsOptions =
      setFilterValidation(state, filters, filtersOptions);
    return {
      ...state,
      isLoading: true,
      isFiltred: FiltersValidationsOptions?.isFiltred,
      filters: FiltersValidationsOptions?.filters,
    };
  }),
  on(successGetPurchases, (state, { purchases }) => ({
    ...state,
    purchases,
    isLoading: false,
  })),
  on(failedGetPurchases, (state) => ({
    ...state,
    purchases: null,
    isLoading: false,
    isFiltred: false,
  }))
);

export function PurchasesReducers(
  state: PurchaseState | undefined,
  action: Action
) {
  return _purchaseReducer(state, action);
}
