import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PurchaseState } from './purchases.state';

export const getPurchasesFeatureState = createFeatureSelector<PurchaseState>(
  MERCHANT_MANAGER_FEATURE.PAGES.PURCHASES.STORE_NAME
);

export const selectPurchasesFilters = createSelector(
  getPurchasesFeatureState,
  (state: PurchaseState) => state?.filters
);

export const selectPurchases = createSelector(
  getPurchasesFeatureState,
  (state: PurchaseState) => state?.purchases
);

export const selectIsPurchasesFiltred = createSelector(
  getPurchasesFeatureState,
  (state: PurchaseState) => state?.isFiltred
);

export const selectIsLoadingPurchases = createSelector(
  getPurchasesFeatureState,
  (state: PurchaseState) => state?.isLoading
);
