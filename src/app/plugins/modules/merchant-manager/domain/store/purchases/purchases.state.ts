import { TablePageActions } from '@app/core/models/page.model';
import {
  Purchase,
  PurchaseFilters,
} from '@app/plugins/modules/merchant-manager/core/models/purchase.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface PurchaseState extends TablePageActions {
  purchases?: TableResponse<Purchase>;
  filters?: PurchaseFilters;
}
