import { TestBed } from '@angular/core/testing';

import { MerchantGroupFacade } from './merchant-group.facade';

describe('MerchantGroupFacade', () => {
  let service: MerchantGroupFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MerchantGroupFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
