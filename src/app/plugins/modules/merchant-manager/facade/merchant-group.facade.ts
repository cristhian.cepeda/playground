import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { Project } from '@app/core/models/project.model';
import { deepCopy } from '@app/core/models/utils.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  MerchantGroup,
  MerchantGroupFees,
  MerchantGroupFilters,
  MerchantGroupOffer,
  MerchantGroupProfile,
  NewMerchantGroup,
  NewMerchantGroupDetails,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupsService } from '@app/plugins/modules/merchant-manager/domain/services/merchant-groups.service';
import * as merchantGroupsActions from '@app/plugins/modules/merchant-manager/domain/store/merchant-groups/merchant-groups.actions';
import * as merchantGroupsSeletors from '@app/plugins/modules/merchant-manager/domain/store/merchant-groups/merchant-groups.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { combineLatest, filter, map, Observable } from 'rxjs';
import { Merchant, MerchantFilters } from '../core/models/merchant.model';

@Injectable({
  providedIn: 'root',
})
export class MerchantGroupFacade {
  // ********************
  // PROJECT
  // ********************
  public project$: Observable<Project>;
  public projectFamily$: Observable<string>;

  // ********************
  // MERCHANT GROUPS
  // ********************
  public merchantGroups$: Observable<TableResponse<MerchantGroup>>;
  public isLoadingMerchantGroups$: Observable<boolean>;
  public isMerchantGroupsFiltred$: Observable<boolean>;

  public offers$: Observable<MerchantGroupOffer[]>;
  public isLoadingOffers$: Observable<boolean>;
  public isOffersFiltred$: Observable<boolean>;
  public isLoadingMerchants$: Observable<boolean>;
  public isMerchantsFiltred$: Observable<boolean>;
  public productOfferListItemFromOffers$: Observable<
    MerchantGroupProductOfferListItem[]
  >;

  // ***********************
  // MERCHANT GROUP PROFILE
  // ***********************
  // Profile
  public merchantGroupProfileId$: Observable<string>;
  public merchantGroupProfileInfo$: Observable<MerchantGroupProfile>;
  public isLoadingMerchantGroupProfileInfo$: Observable<boolean>;

  public merchantGroupProfileMerchants$: Observable<Merchant[]>;
  public isLoadingMerchantGroupProfileMerchants$: Observable<boolean>;

  public productOfferListItemFromOffersMerchantGroupProfile$: Observable<
    MerchantGroupProductOfferListItem[]
  >;
  public merchantGroupProfileOffers$: Observable<MerchantGroupOffer[]>;
  public isLoadingMerchantGroupProfileOffers$: Observable<boolean>;
  public isLoadingMerchantGroupUpdateCommission$: Observable<boolean>;

  // Add Or Remove Merchants
  public addOrRemoveMerchantsSelected$: Observable<Merchant[]>;
  public merchantsEditMerchantGroup$: Observable<TableResponse<Merchant>>;
  public isLoadingUpdateMerchants$: Observable<boolean>;

  // Add Or Remove Offers
  public addOrRemoveOffersSelected$: Observable<
    MerchantGroupProductOfferListItem[]
  >;
  public offerEditProductOfferListItem$: Observable<
    MerchantGroupProductOfferListItem[]
  >;
  public isLoadingUpdateOffers$: Observable<boolean>;

  // ********************
  // NEW MERCHANT GROUPS
  // ********************
  public isLoadingMerchantName$: Observable<boolean>;
  public isValidMerchantName$: Observable<boolean>;
  public newGroupMerchantDetails$: Observable<NewMerchantGroupDetails>;
  public merchantsNewMerchantGroup$: Observable<TableResponse<Merchant>>;
  public newGroupMerchantsSelected$: Observable<Merchant[]>;
  public offerFees$: Observable<MerchantGroupFees[]>;
  public newMerchantGroupData$: Observable<NewMerchantGroup>;

  constructor(
    private _store: Store,
    private _projectFacade: ProjectFacade,
    private _merchantGroupService: MerchantGroupsService
  ) {
    this._setSelectors();
  }

  // ********************
  // MERCHANT GROUPS
  // ********************

  public initMerchantGroupsPage(filters?: MerchantGroupFilters) {
    this._store.dispatch(
      merchantGroupsActions.initMerchantGroupsPage({ filters })
    );
  }

  public updateFiltersMerchantGroups(
    filters: MerchantGroupFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      merchantGroupsActions.updateFiltersMerchantGroups({
        filters,
        filtersOptions,
      })
    );
  }

  public getMerchantGroupOffers(
    filters?: MerchantGroupFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      merchantGroupsActions.getMerchantGroupOffers({
        filters,
        filtersOptions,
      })
    );
  }

  public updateFiltersMerchantGroupMerchants(
    filters: MerchantFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      merchantGroupsActions.updateFiltersMerchantGroupMerchants({
        filters,
        filtersOptions,
      })
    );
  }

  // ***********************
  // MERCHANT GROUP PROFILE
  // ***********************

  public destroyMerchantGroupProfilePage() {
    this._store.dispatch(
      merchantGroupsActions.destroyMerchantGroupProfilePage()
    );
  }

  public updateMerchantGroupProfileId(id: string) {
    this._store.dispatch(
      merchantGroupsActions.updateMerchantGroupProfileId({
        id,
      })
    );
  }

  public deleteMerchantGroup() {
    this._store.dispatch(merchantGroupsActions.deleteMerchantGroup());
  }

  public getMerchantGroupProfileInfo() {
    this._store.dispatch(merchantGroupsActions.getMerchantGroupProfileInfo());
  }

  public getMerchantGroupProfileMerchants() {
    this._store.dispatch(
      merchantGroupsActions.getMerchantGroupProfileMerchants()
    );
  }

  public filterMerchantGroupProfileMerchants(merchantNameFilter: string) {
    this._store.dispatch(
      merchantGroupsActions.filterMerchantGroupProfileMerchants({
        merchantNameFilter,
      })
    );
  }

  public getMerchantGroupProfileOffers() {
    this._store.dispatch(merchantGroupsActions.getMerchantGroupProfileOffers());
  }

  public updateCommissionMerchantGroupProfile(fees: MerchantGroupFees[]) {
    this._store.dispatch(
      merchantGroupsActions.updateCommissionMerchantGroupProfile({ fees })
    );
  }

  // ADD OR REMOVE MERCHANTS

  public initAddOrRemoveMerchantsPage() {
    this._store.dispatch(merchantGroupsActions.initAddOrRemoveMerchantsPage());
  }

  public updateAddOrRemoveMerchantsSelected(
    merchantSelected: Merchant,
    remove: boolean = false
  ) {
    this._store.dispatch(
      merchantGroupsActions.updateAddOrRemoveMerchantsSelected({
        merchantSelected,
        remove,
      })
    );
  }

  public updateMerchantGroupMerchants() {
    this._store.dispatch(merchantGroupsActions.updateMerchantGroupMerchants());
  }

  public confirmRemoveMerchantGroupMerchants() {
    this._store.dispatch(
      merchantGroupsActions.confirmRemoveMerchantGroupMerchants()
    );
  }
  public cancelRemoveMerchantGroupMerchants() {
    this._store.dispatch(
      merchantGroupsActions.cancelRemoveMerchantGroupMerchants()
    );
  }

  // ADD OR REMOVE OFFERS

  public initAddOrRemoveOffersPage() {
    this._store.dispatch(merchantGroupsActions.initAddOrRemoveOffersPage());
  }

  public updateAddOrRemoveOffersSelected(
    offerSelected: MerchantGroupOffer,
    remove: boolean = false
  ) {
    this._store.dispatch(
      merchantGroupsActions.updateAddOrRemoveOffersSelected({
        offerSelected,
        remove,
      })
    );
  }

  public updateMerchantGroupOffers(feesToAdd: MerchantGroupFees[]) {
    this._store.dispatch(
      merchantGroupsActions.updateMerchantGroupOffers({ feesToAdd })
    );
  }

  public removeMerchantGroupOffers() {
    this._store.dispatch(merchantGroupsActions.removeMerchantGroupOffers());
  }

  public cancelRemoveMerchantGroupOffers() {
    this._store.dispatch(
      merchantGroupsActions.cancelRemoveMerchantGroupOffers()
    );
  }

  // ********************
  // NEW MERCHANT GROUPS
  // ********************

  public destroyNewMerchantGroupPages() {
    this._store.dispatch(merchantGroupsActions.destroyNewMerchantGroupPages());
  }

  public checkMerchantGroupName(name: string) {
    this._store.dispatch(
      merchantGroupsActions.checkMerchantGroupName({ name })
    );
  }

  public resetNewMerchantGroup() {
    this._store.dispatch(merchantGroupsActions.resetNewMerchantGroup());
  }

  public updateNewMerchantGroupData(newMerchantGroupData: NewMerchantGroup) {
    this._store.dispatch(
      merchantGroupsActions.updateNewMerchantGroupData({
        newMerchantGroupData,
      })
    );
  }

  public updateNewGroupSelectedMerchants(
    merchantSelected: Merchant,
    remove: boolean = false
  ) {
    this._store.dispatch(
      merchantGroupsActions.updateNewGroupSelectedMerchants({
        merchantSelected,
        remove,
      })
    );
  }

  public initNewGroupMerchantsPage() {
    this._store.dispatch(merchantGroupsActions.initNewGroupMerchantsPage());
  }

  public createNewMerchantGroup() {
    this._store.dispatch(merchantGroupsActions.createNewMerchantGroup());
  }

  private _setSelectors() {
    // ********************
    // MERCHANT GROUPS
    // ********************
    this.merchantGroups$ = this._store.select(
      merchantGroupsSeletors.selectMerchantGroups
    );
    this.isLoadingMerchantGroups$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingMerchantGroups
    );
    this.isMerchantGroupsFiltred$ = this._store.select(
      merchantGroupsSeletors.selectIsMerchantGroupsFiltred
    );

    this.isLoadingMerchants$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingGroupMerchants
    );
    this.isMerchantsFiltred$ = this._store.select(
      merchantGroupsSeletors.selectIsFiltredGroupMerchants
    );

    this.offers$ = this._store.select(merchantGroupsSeletors.selectGroupOffers);

    this.isLoadingOffers$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingGroupOffers
    );
    this.isOffersFiltred$ = this._store.select(
      merchantGroupsSeletors.selectIsFiltredGroupOffers
    );

    this.productOfferListItemFromOffers$ = this.offers$.pipe(
      filter((offers) => !!offers),
      map((offers) =>
        this._merchantGroupService.setProductOfferListItemFromOffers(offers)
      )
    );
    // ***********************
    // MERCHANT GROUP PROFILE
    // ***********************
    this.merchantGroupProfileId$ = this._store.select(
      merchantGroupsSeletors.selectMerchantGroupId
    );
    this.merchantGroupProfileInfo$ = this._store.select(
      merchantGroupsSeletors.selectMerchantGroupInfo
    );
    this.isLoadingMerchantGroupProfileInfo$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingMerchantGroupInfo
    );
    this.merchantGroupProfileMerchants$ = combineLatest([
      this._store.select(merchantGroupsSeletors.selectMerchantGroupMerchants),
      this._store.select(
        merchantGroupsSeletors.selectMerchantNameFilterMerchantGroupMerchants
      ),
    ]).pipe(
      filter(([merchants]) => !!merchants),
      map(([merchants, merchantNameFilter = '']) =>
        merchants.filter((merchant) =>
          merchant?.display_name
            ?.toLowerCase()
            .includes(merchantNameFilter?.toLowerCase())
        )
      )
    );

    this.isLoadingMerchantGroupProfileMerchants$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingMerchantGroupMerchants
    );

    this.merchantGroupProfileOffers$ = this._store.select(
      merchantGroupsSeletors.selectMerchantGroupOffers
    );

    this.isLoadingMerchantGroupProfileOffers$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingMerchantGroupOffers
    );
    this.isLoadingMerchantGroupUpdateCommission$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingMerchantGroupUpdateCommission
    );

    this.productOfferListItemFromOffersMerchantGroupProfile$ =
      this.merchantGroupProfileOffers$.pipe(
        filter((offers) => !!offers),
        map((offers) =>
          this._merchantGroupService.setProductOfferListItemFromOffers(offers)
        )
      );

    // Add Or Remove Merchants
    this.addOrRemoveMerchantsSelected$ = this._store.select(
      merchantGroupsSeletors.selectAddOrRemoveMerchantsSelected
    );

    this.merchantsEditMerchantGroup$ = combineLatest([
      this._store.select(merchantGroupsSeletors.selectGroupMerchants),
      this._store.select(
        merchantGroupsSeletors.selectAddOrRemoveMerchantsSelected
      ),
    ]).pipe(
      filter(([merchantResponse]) => !!merchantResponse),
      map(([merchantResponse, merchantsSelected = []]) =>
        this._merchantGroupService.mapMerchantsWithMerchantsSeleted(
          merchantResponse,
          merchantsSelected
        )
      )
    );

    this.isLoadingUpdateMerchants$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingUpdateMerchantsAddOrRemoveMerchants
    );

    // Add Or Remove Offers
    this.addOrRemoveOffersSelected$ = this._store
      .select(merchantGroupsSeletors.selectAddOrRemoveOffersSelected)
      .pipe(
        filter((offers) => !!offers),
        map((offers) =>
          this._merchantGroupService.setProductOfferListItemFromOffers(offers)
        )
      );

    this.offerEditProductOfferListItem$ = combineLatest([
      this._store.select(merchantGroupsSeletors.selectGroupOffers),
      this._store.select(
        merchantGroupsSeletors.selectAddOrRemoveOffersSelected
      ),
    ]).pipe(
      filter(([offerResponse]) => !!offerResponse),
      map(([offerResponse, offersSelected = []]) => {
        let offers = deepCopy(offerResponse);
        offers = offers?.filter((offer) =>
          offersSelected.every(
            (offerSelected) => offer?.offer_id !== offerSelected?.offer_id
          )
        );
        return this._merchantGroupService.setProductOfferListItemFromOffers(
          offers
        );
      })
    );

    this.isLoadingUpdateOffers$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingAddOrRemoveOffersUpdateOffers
    );
    // ********************
    // NEW MERCHANT GROUPS
    // ********************
    this.newMerchantGroupData$ = this._store.select(
      merchantGroupsSeletors.selectNewMerchantGroupData
    );
    this.isLoadingMerchantName$ = this._store.select(
      merchantGroupsSeletors.selectIsLoadingMerchantName
    );
    this.isValidMerchantName$ = this._store.select(
      merchantGroupsSeletors.selectIsValidMerchantName
    );

    this.merchantsNewMerchantGroup$ = combineLatest([
      this._store.select(merchantGroupsSeletors.selectGroupMerchants),
      this._store.select(
        merchantGroupsSeletors.selectNewGroupMerchantsSelected
      ),
    ]).pipe(
      filter(([merchantResponse]) => !!merchantResponse),
      map(([merchantResponse, merchantsSelected = []]) =>
        this._merchantGroupService.mapMerchantsWithMerchantsSeleted(
          merchantResponse,
          merchantsSelected
        )
      )
    );

    this.newGroupMerchantsSelected$ = this._store.select(
      merchantGroupsSeletors.selectNewGroupMerchantsSelected
    );
    this.newGroupMerchantDetails$ = this._store.select(
      merchantGroupsSeletors.selectNewGroupMerchantsDetails
    );

    this.offerFees$ = this._store.select(
      merchantGroupsSeletors.selectNewGroupSelectedOfferFees
    );

    // ********************
    // PROJECT
    // ********************
    this.project$ = this._projectFacade.project$;
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
