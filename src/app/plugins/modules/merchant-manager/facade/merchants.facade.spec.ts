import { TestBed } from '@angular/core/testing';

import { MerchantsFacade } from './merchants.facade';

describe('MerchantsFacade', () => {
  let service: MerchantsFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MerchantsFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
