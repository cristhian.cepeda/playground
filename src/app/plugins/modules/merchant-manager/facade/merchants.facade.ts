import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import { Loan } from '@app/plugins/modules/merchant-manager/core/models/loan.model';
import {
  MerchantGroupAndOffersProfile,
  MerchantProfile,
  MerchantSummary,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import * as merchantsActions from '@app/plugins/modules/merchant-manager/domain/store/merchants/merchants.actions';
import * as merchantsSelectors from '@app/plugins/modules/merchant-manager/domain/store/merchants/merchants.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { filter, map, mergeMap, Observable } from 'rxjs';
import { Merchant, MerchantFilters } from '../core/models/merchant.model';
import { MerchantProfileService } from '../domain/services/merchant-profile.service';
import { MerchantService } from '../domain/services/merchant.service';

@Injectable({
  providedIn: 'root',
})
export class MerchantsFacade {
  public projectFamily$: Observable<string>;

  public merchants$: Observable<TableResponse<Merchant>>;
  public isLoadingMerchants$: Observable<boolean>;
  public isMerchantsFiltred$: Observable<boolean>;

  public merchantProfile$: Observable<MerchantProfile>;
  public merchantProfileItems$: Observable<SummaryGroup[]>;
  public isLoadingMerchantProfile$: Observable<boolean>;
  public merchantSummary$: Observable<MerchantSummary>;
  public isLoadingMerchantSummary$: Observable<boolean>;
  public merchantPurchases$: Observable<Loan[]>;
  public isLoadingMerchantPurchases$: Observable<boolean>;
  public merchantGroupAndOffers$: Observable<MerchantGroupAndOffersProfile>;
  public isLoadingMerchantGroupAndOffers$: Observable<boolean>;
  public merchantId$: Observable<string>;

  constructor(
    private _store: Store,
    private _merchantProfileService: MerchantProfileService,
    private _merchantService: MerchantService,
    private _projectFacade: ProjectFacade
  ) {
    this._setSelectors();
  }

  public initMechantsPage() {
    this._store.dispatch(merchantsActions.initMerchantsPage());
  }

  public updateFiltersMerchants(
    filters: MerchantFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      merchantsActions.updateFiltersMerchants({ filters, filtersOptions })
    );
  }

  public downloadMerchants() {
    this._store.dispatch(merchantsActions.downloadMerchants());
  }

  public updateMerchantId(merchantId: any) {
    this._store.dispatch(merchantsActions.updateMerchantId({ merchantId }));
  }

  public getMerchantProfile(): void {
    this._store.dispatch(merchantsActions.getMerchantProfile());
  }

  public getMerchantSummary(): void {
    this._store.dispatch(merchantsActions.getMerchantSummary());
  }

  public getMerchantPurchases(): void {
    this._store.dispatch(merchantsActions.getMerchantPurchases());
  }

  public getMerchantGroupAndOffers(): void {
    this._store.dispatch(merchantsActions.getMerchantGroupAndOffers());
  }

  public destroyMerchantProfilePage(): void {
    this._store.dispatch(merchantsActions.destroyMerchantProfilePage());
  }

  private _setSelectors() {
    this.merchants$ = this._store
      .select(merchantsSelectors.selectMerchants)
      .pipe(
        filter((merchants: TableResponse<Merchant>) => !!merchants),
        map((merchants: TableResponse<Merchant>) =>
          this._merchantService.mapMerchants(merchants)
        )
      );

    this.isLoadingMerchants$ = this._store.select(
      merchantsSelectors.selectIsLoadingMerchants
    );
    this.isMerchantsFiltred$ = this._store.select(
      merchantsSelectors.selectIsMerchantsFiltred
    );

    this.merchantProfile$ = this._store.select(
      merchantsSelectors.selectMerchantProfile
    );

    this.isLoadingMerchantProfile$ = this._store.select(
      merchantsSelectors.selectIsLoadingMerchantProfile
    );

    this.merchantSummary$ = this._store
      .select(merchantsSelectors.selectMerchantSummary)
      .pipe(
        filter((merchantSummary) => !!merchantSummary),
        map((merchantSummary) =>
          this._merchantProfileService.setMerchantSummary(merchantSummary)
        )
      );

    this.isLoadingMerchantSummary$ = this._store.select(
      merchantsSelectors.selectIsLoadingMerchantSummary
    );

    this.merchantPurchases$ = this._store.select(
      merchantsSelectors.selectMerchantPurchases
    );

    this.isLoadingMerchantPurchases$ = this._store.select(
      merchantsSelectors.selectIsLoadingMerchantPurchases
    );

    this.merchantGroupAndOffers$ = this._store
      .select(merchantsSelectors.selectMerchantGroupAndOffers)
      .pipe(
        filter((merchantGroupAndOffers) => !!merchantGroupAndOffers),
        map((merchantGroupAndOffers) =>
          this._merchantService.mapMerchantGroupAndOffers(
            merchantGroupAndOffers
          )
        )
      );

    this.isLoadingMerchantGroupAndOffers$ = this._store.select(
      merchantsSelectors.selectIsLoadingMerchantGroupAndOffers
    );

    this.merchantProfileItems$ = this.merchantProfile$.pipe(
      filter((merchantProfile: MerchantProfile) => !!merchantProfile),
      mergeMap((merchantProfile: MerchantProfile) =>
        this._merchantProfileService.getMerchantItemsByProfile(merchantProfile)
      )
    );

    this.merchantId$ = this.merchantProfile$.pipe(
      filter((merchant) => !!merchant),
      map((merchantProfile) => merchantProfile?.id)
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
