import { TestBed } from '@angular/core/testing';

import { MovementsFacade } from './movements.facade';

describe('MovementsFacade', () => {
  let service: MovementsFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MovementsFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
