import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import * as movementsActions from '@app/plugins/modules/merchant-manager/domain/store/movements/movements.actions';
import * as movementsSelectors from '@app/plugins/modules/merchant-manager/domain/store/movements/movements.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Movement, MovementFilters } from '../core/models/movement.model';

@Injectable({
  providedIn: 'root',
})
export class MovementsFacade {
  public projectFamily$: Observable<string>;

  public movements$: Observable<TableResponse<Movement>>;
  public isLoadingMovements$: Observable<boolean>;
  public isMovementsFiltred$: Observable<boolean>;

  constructor(private _store: Store, private _projectFacade: ProjectFacade) {
    this._setSelectors();
  }

  public initMovementsPage(filters?: MovementFilters) {
    this._store.dispatch(movementsActions.initMovementsPage({ filters }));
  }

  public updateFiltersMovements(
    filters: MovementFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      movementsActions.updateFiltersMovements({ filters, filtersOptions })
    );
  }

  public downloadMovements() {
    this._store.dispatch(movementsActions.downloadMovements());
  }

  private _setSelectors() {
    this.movements$ = this._store.select(movementsSelectors.selectMovements);
    this.isLoadingMovements$ = this._store.select(
      movementsSelectors.selectIsLoadingMovements
    );
    this.isMovementsFiltred$ = this._store.select(
      movementsSelectors.selectIsMovementsFiltred
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
