import { TestBed } from '@angular/core/testing';

import { PayoutsFacade } from './payouts.facade';

describe('PayoutsFacade', () => {
  let service: PayoutsFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PayoutsFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
