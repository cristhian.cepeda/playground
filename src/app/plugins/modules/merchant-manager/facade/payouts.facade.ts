import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import * as payoutsActions from '@app/plugins/modules/merchant-manager/domain/store/payouts/payouts.actions';
import * as payoutsSelectors from '@app/plugins/modules/merchant-manager/domain/store/payouts/payouts.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Payout, PayoutFilters } from '../core/models/payout.modes';

@Injectable({
  providedIn: 'root',
})
export class PayoutsFacade {
  public projectFamily$: Observable<string>;

  public payouts$: Observable<TableResponse<Payout>>;
  public isLoadingPayouts$: Observable<boolean>;
  public isPayoutsFiltred$: Observable<boolean>;

  constructor(private _store: Store, private _projectFacade: ProjectFacade) {
    this._setSelectors();
  }

  public initPayoutsPage() {
    this._store.dispatch(payoutsActions.initPayoutsPage());
  }

  public updateFiltersPayouts(
    filters: PayoutFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      payoutsActions.updateFiltersPayouts({ filters, filtersOptions })
    );
  }

  public downloadPayouts() {
    this._store.dispatch(payoutsActions.downloadPayouts());
  }

  private _setSelectors() {
    this.payouts$ = this._store.select(payoutsSelectors.selectPayouts);
    this.isLoadingPayouts$ = this._store.select(
      payoutsSelectors.selectIsLoadingPayouts
    );
    this.isPayoutsFiltred$ = this._store.select(
      payoutsSelectors.selectIsPayoutsFiltred
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
