import { TestBed } from '@angular/core/testing';

import { PurchasesFacade } from './purchases.facade';

describe('PurchasesFacade', () => {
  let service: PurchasesFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PurchasesFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
