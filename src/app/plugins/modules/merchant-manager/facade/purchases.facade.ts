import { Injectable } from '@angular/core';
import { FiltersOptions } from '@app/core/models/filters.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import * as purchasesActions from '@app/plugins/modules/merchant-manager/domain/store/purchases/purchases.actions';
import * as purchasesSelectors from '@app/plugins/modules/merchant-manager/domain/store/purchases/purchases.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Purchase, PurchaseFilters } from '../core/models/purchase.model';

@Injectable({
  providedIn: 'root',
})
export class PurchasesFacade {
  public projectFamily$: Observable<string>;

  public purchases$: Observable<TableResponse<Purchase>>;
  public isLoadingPurchases$: Observable<boolean>;
  public isPurchasesFiltred$: Observable<boolean>;

  constructor(private _store: Store, private _projectFacade: ProjectFacade) {
    this._setSelectors();
  }

  public initMechantsPage() {
    this._store.dispatch(purchasesActions.initPurchasesPage());
  }

  public updateFiltersPurchases(
    filters: PurchaseFilters,
    filtersOptions?: FiltersOptions
  ) {
    this._store.dispatch(
      purchasesActions.updateFiltersPurchases({ filters, filtersOptions })
    );
  }

  public downloadPurchases() {
    this._store.dispatch(purchasesActions.downloadPurchases());
  }

  private _setSelectors() {
    this.purchases$ = this._store.select(purchasesSelectors.selectPurchases);
    this.isLoadingPurchases$ = this._store.select(
      purchasesSelectors.selectIsLoadingPurchases
    );
    this.isPurchasesFiltred$ = this._store.select(
      purchasesSelectors.selectIsPurchasesFiltred
    );
    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
