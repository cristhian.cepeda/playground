import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseContentComponent } from '@app/presentation/layout/components/base-content/base-content.component';
import { MENU_CONFIG_DISABLED } from '@instance-config/menu.config';
import { MERCHANT_MANAGER_FEATURE } from '../core/constants/feature.constants';

const CHILDREN_ROUTES: Routes = [
  {
    path: MERCHANT_MANAGER_FEATURE.PAGES.OVERVIEW.PATH,
    loadChildren: () =>
      import('./pages/overview/overview.module').then(
        (m) => m.OverviewPageModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.OVERVIEW.TITLE',
      disabled: MENU_CONFIG_DISABLED.MERCHANT_MANAGER.OVERVIEW ?? false,
    },
  },
  {
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.PATH,
    loadChildren: () =>
      import('./pages/merchants/merchants.module').then(
        (m) => m.MerchantsModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.MERCHANTS.TITLE',
      disabled: MENU_CONFIG_DISABLED.MERCHANT_MANAGER.MERCHANTS ?? false,
    },
  },
  {
    path: MERCHANT_MANAGER_FEATURE.PAGES.PURCHASES.PATH,
    loadChildren: () =>
      import('./pages/purchases/purchases.module').then(
        (m) => m.PurchasesModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.PURCHASES.TITLE',
      disabled: MENU_CONFIG_DISABLED.MERCHANT_MANAGER.PURCHASES ?? false,
    },
  },
  {
    path: MERCHANT_MANAGER_FEATURE.PAGES.PAYOUTS.PATH,
    loadChildren: () =>
      import('./pages/payouts/payouts.module').then((m) => m.PayoutsModule),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.PAYOUTS.TITLE',
      disabled: MENU_CONFIG_DISABLED.MERCHANT_MANAGER.PAYOUTS ?? false,
    },
  },
  {
    path: MERCHANT_MANAGER_FEATURE.PAGES.MOVEMENTS.PATH,
    loadChildren: () =>
      import('./pages/movements/movements.module').then(
        (m) => m.MovementsModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.MOVEMENTS.TITLE',
      disabled: MENU_CONFIG_DISABLED.MERCHANT_MANAGER.MOVEMENTS ?? false,
    },
  },
  {
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.PATH,
    loadChildren: () =>
      import('./pages/merchant-groups/merchant-groups.module').then(
        (m) => m.MerchantGroupsModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.MERCHANT_GROUPS.TITLE',
      disabled: MENU_CONFIG_DISABLED.MERCHANT_MANAGER.MERCHANT_GROUPS ?? false,
    },
  },
];

const routes: Routes = [
  {
    path: '',
    component: BaseContentComponent,
    children: [
      {
        path: '',
        redirectTo: CHILDREN_ROUTES.filter(
          (route) => !route?.data?.disabled
        ).shift()?.path,
        pathMatch: 'full',
      },
      ...CHILDREN_ROUTES,
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantManagerRoutingModule {}
