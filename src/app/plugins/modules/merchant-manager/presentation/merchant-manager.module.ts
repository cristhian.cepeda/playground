import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MerchantManagerRoutingModule } from './merchant-manager-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MerchantManagerRoutingModule
  ]
})
export class MerchantManagerModule { }
