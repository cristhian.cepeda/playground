import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddOrRemoveMerchantsPage } from './add-or-remove-merchants.page';

const routes: Routes = [
  {
    path: '',
    component: AddOrRemoveMerchantsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddOrRemoveMerchantsPageoutingModule {}
