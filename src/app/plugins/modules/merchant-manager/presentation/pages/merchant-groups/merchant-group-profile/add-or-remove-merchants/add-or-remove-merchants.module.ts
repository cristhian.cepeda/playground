import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';

import { AddOrRemoveMerchantsPageoutingModule } from './add-or-remove-merchants-routing.module';
import { AddOrRemoveMerchantsPage } from './add-or-remove-merchants.page';
import { MerchantBoxComponent } from './components/merchant-box/merchant-box.component';
import { MerchantsTableFiltersComponent } from './components/merchants-table-filters/merchants-table-filters.component';
import { MerchantsTableComponent } from './components/merchants-table/merchants-table.component';
import { RemoveMerchantModalComponent } from './components/remove-merchant-modal/remove-merchant-modal.component';

@NgModule({
  declarations: [
    AddOrRemoveMerchantsPage,
    MerchantBoxComponent,
    MerchantsTableComponent,
    MerchantsTableFiltersComponent,
    RemoveMerchantModalComponent,
  ],
  imports: [CommonModule, AddOrRemoveMerchantsPageoutingModule, LayoutModule],
})
export class AddOrRemoveMerchantsPageModule {}
