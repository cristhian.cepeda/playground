import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrRemoveMerchantsPage } from './add-or-remove-merchants.page';

describe('AddOrRemoveMerchantsPage', () => {
  let component: AddOrRemoveMerchantsPage;
  let fixture: ComponentFixture<AddOrRemoveMerchantsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOrRemoveMerchantsPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddOrRemoveMerchantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
