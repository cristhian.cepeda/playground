import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable, Subscription, take } from 'rxjs';

@Component({
  selector: 'merchant-group-profile-merchant-box',
  templateUrl: './merchant-box.component.html',
  styleUrls: ['./merchant-box.component.scss'],
})
export class MerchantBoxComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public merchantGroupProfileId$: Observable<string>;
  public merchants$: Observable<Merchant[]>;
  public merchantsSelected$: Observable<Merchant[]>;
  public isLoadingUpdateMerchants$: Observable<boolean>;

  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public merchantGroupProfileUrl: string;

  private _merchantsSelectedSubscription: Subscription;

  constructor(private _merchantGroupFacade: MerchantGroupFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onRemoveItem(merchant: Merchant) {
    this._merchantGroupFacade.updateAddOrRemoveMerchantsSelected(
      merchant,
      true
    );
  }

  public onConfirm(): void {
    this._merchantsSelectedSubscription = this.merchantsSelected$
      .pipe(take(1))
      .subscribe((merchantsSelected) => {
        if (merchantsSelected?.length > 0) {
          this._merchantGroupFacade.updateMerchantGroupMerchants();
        }
      });
  }

  private _setInitialValues() {
    this.merchantGroupProfileUrl = MERCHANT_MANAGER_URLS.MERCHANT_GROUP_PROFILE;
    this.merchantGroupProfileId$ =
      this._merchantGroupFacade.merchantGroupProfileId$;
    this.merchantsSelected$ =
      this._merchantGroupFacade.addOrRemoveMerchantsSelected$;
    this.isLoadingUpdateMerchants$ =
      this._merchantGroupFacade.isLoadingUpdateMerchants$;
  }
}
