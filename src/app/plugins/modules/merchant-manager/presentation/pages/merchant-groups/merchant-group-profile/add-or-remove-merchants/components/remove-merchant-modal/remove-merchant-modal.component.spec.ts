import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveMerchantModalComponent } from './remove-merchant-modal.component';

describe('RemoveMerchantModalComponent', () => {
  let component: RemoveMerchantModalComponent;
  let fixture: ComponentFixture<RemoveMerchantModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveMerchantModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RemoveMerchantModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
