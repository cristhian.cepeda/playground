import { Component } from '@angular/core';
import { AppFacade } from '@app/facade/app/app.facade';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'merchant-group-profile-remove-merchant-modal',
  templateUrl: './remove-merchant-modal.component.html',
  styleUrls: ['./remove-merchant-modal.component.scss'],
})
export class RemoveMerchantModalComponent {
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _appFacade: AppFacade
  ) {}

  public onConfirm(): void {
    this._merchantGroupFacade.confirmRemoveMerchantGroupMerchants();
    this._appFacade.toggleModal();
  }

  public onCancel(): void {
    this._merchantGroupFacade.cancelRemoveMerchantGroupMerchants();
    this._appFacade.toggleModal();
  }
}
