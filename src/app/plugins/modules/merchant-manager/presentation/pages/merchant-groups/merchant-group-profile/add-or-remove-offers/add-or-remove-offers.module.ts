import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';

import { AddOrRemoveOffersPageRoutingModule } from './add-or-remove-offers-routing.module';
import { AddOrRemoveOffersPage } from './add-or-remove-offers.page';
import { CurrentOfferItemComponent } from './components/current-offer-item/current-offer-item.component';
import { CurrentOffersListComponent } from './components/current-offers-list/current-offers-list.component';
import { EmptyListComponent } from './components/empty-list/empty-list.component';
import { FilterComponent } from './components/filter/filter.component';
import { NewOffersListComponent } from './components/new-offers-list/new-offers-list.component';
import { RemoveOfferModalComponent } from './components/remove-offer-modal/remove-offer-modal.component';

@NgModule({
  declarations: [
    AddOrRemoveOffersPage,
    FilterComponent,
    NewOffersListComponent,
    CurrentOffersListComponent,
    CurrentOfferItemComponent,
    RemoveOfferModalComponent,
    EmptyListComponent,
  ],
  imports: [CommonModule, AddOrRemoveOffersPageRoutingModule, LayoutModule],
})
export class AddOrRemoveOffersPageModule {}
