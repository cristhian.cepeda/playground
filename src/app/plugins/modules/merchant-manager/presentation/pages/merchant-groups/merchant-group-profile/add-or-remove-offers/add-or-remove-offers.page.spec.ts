import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrRemoveOffersPage } from './add-or-remove-offers.page';

describe('AddOrRemoveOffersPage', () => {
  let component: AddOrRemoveOffersPage;
  let fixture: ComponentFixture<AddOrRemoveOffersPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOrRemoveOffersPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddOrRemoveOffersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
