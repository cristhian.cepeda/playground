import { Component } from '@angular/core';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';

@Component({
  selector: 'add-or-remove-offers',
  templateUrl: './add-or-remove-offers.page.html',
  styleUrls: ['./add-or-remove-offers.page.scss'],
})
export class AddOrRemoveOffersPage {
  constructor(private _merchantGroupFacade: MerchantGroupFacade) {
    this._merchantGroupFacade.initAddOrRemoveOffersPage();
  }
}
