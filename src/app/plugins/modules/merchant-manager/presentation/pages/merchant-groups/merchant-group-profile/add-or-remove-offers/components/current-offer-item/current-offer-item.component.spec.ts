import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentOfferItemComponent } from './current-offer-item.component';

describe('CurrentOfferItemComponent', () => {
  let component: CurrentOfferItemComponent;
  let fixture: ComponentFixture<CurrentOfferItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentOfferItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CurrentOfferItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
