import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { Offer } from '@app/plugins/modules/merchant-manager/core/models/offer.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';

@Component({
  selector: 'merchant-group-profile-current-offer-item',
  templateUrl: './current-offer-item.component.html',
  styleUrls: ['./current-offer-item.component.scss'],
})
export class CurrentOfferItemComponent {
  @Input() productOfferCard: MerchantGroupProductOfferListItem;
  @Input() isChecked: boolean = false;
  @Input() canUncheck: boolean = true;
  @Input() id: string | number;
  @Input() isLoading?: boolean;
  @Output() removeOffer: EventEmitter<Offer> = new EventEmitter<Offer>();

  constructor(private _merchantGroupFacade: MerchantGroupFacade) {}

  public onRemoveOffer(isChecked: boolean) {
    if (!isChecked && this.canUncheck) {
      this._merchantGroupFacade.updateAddOrRemoveOffersSelected(
        this.productOfferCard?.offer,
        true
      );
    }
  }
}
