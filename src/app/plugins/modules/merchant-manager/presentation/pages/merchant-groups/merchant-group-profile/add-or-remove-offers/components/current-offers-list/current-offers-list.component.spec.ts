import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentOffersListComponent } from './current-offers-list.component';

describe('CurrentOffersListComponent', () => {
  let component: CurrentOffersListComponent;
  let fixture: ComponentFixture<CurrentOffersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentOffersListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CurrentOffersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
