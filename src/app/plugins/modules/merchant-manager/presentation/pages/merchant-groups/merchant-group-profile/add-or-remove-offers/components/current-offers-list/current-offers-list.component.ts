import { Component, OnInit } from '@angular/core';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { Project } from '@app/core/models/project.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { CARD_CONTAINER_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import { Observable } from 'rxjs';

@Component({
  selector: 'merchant-group-profile-current-offers-list',
  templateUrl: './current-offers-list.component.html',
  styleUrls: ['./current-offers-list.component.scss'],
})
export class CurrentOffersListComponent implements OnInit {
  public CARD_CONTAINER_DESIGN_CLASS = CARD_CONTAINER_DESIGN_CLASS;
  public addOrRemoveOffersSelected$: Observable<
    MerchantGroupProductOfferListItem[]
  >;
  public project$: Observable<Project>;

  constructor(private _merchantGroupFacade: MerchantGroupFacade) {}

  ngOnInit(): void {
    this._setInitialValues();
  }

  private _setInitialValues() {
    this.project$ = this._merchantGroupFacade.project$;
    this.addOrRemoveOffersSelected$ =
      this._merchantGroupFacade.addOrRemoveOffersSelected$;
  }
}
