import { Component } from '@angular/core';
import { inOutAnimation } from '@app/core/ animations/ animations';

@Component({
  selector: 'add-or-remove-offers-empty-list',
  templateUrl: './empty-list.component.html',
  styleUrls: ['./empty-list.component.scss'],
  animations: inOutAnimation,
})
export class EmptyListComponent {}
