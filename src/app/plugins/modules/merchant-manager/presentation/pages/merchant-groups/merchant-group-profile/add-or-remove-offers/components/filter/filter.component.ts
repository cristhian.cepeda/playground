import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';

@Component({
  selector: 'merchant-group-profile-new-offers-list-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  public offers$: Observable<MerchantGroupProductOfferListItem[]>;
  public isOffersFiltred$: Observable<boolean>;

  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _merchantGroupFacade: MerchantGroupFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._merchantGroupFacade.getMerchantGroupOffers(this.form.value, {
      isFiltred: true,
    });
  }

  public onCleanFilters() {
    this.form.reset();
    this._merchantGroupFacade.getMerchantGroupOffers(this.form.value, {
      isFiltred: false,
    });
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      amount: [''],
      installments: [''],
    });
  }

  private _setInitialValues() {
    this.isOffersFiltred$ = this._merchantGroupFacade.isOffersFiltred$;
    this.offers$ = this._merchantGroupFacade.offerEditProductOfferListItem$;
  }
}
