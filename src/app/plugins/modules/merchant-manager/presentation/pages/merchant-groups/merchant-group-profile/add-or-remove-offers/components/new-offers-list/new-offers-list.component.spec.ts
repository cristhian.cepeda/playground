import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOffersListComponent } from './new-offers-list.component';

describe('OfferListComponent', () => {
  let component: NewOffersListComponent;
  let fixture: ComponentFixture<NewOffersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewOffersListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NewOffersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
