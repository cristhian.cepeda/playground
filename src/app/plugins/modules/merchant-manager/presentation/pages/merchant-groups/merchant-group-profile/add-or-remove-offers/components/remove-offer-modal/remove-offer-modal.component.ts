import { Component, Input } from '@angular/core';
import { AppFacade } from '@app/facade/app/app.facade';
import { Offer } from '@app/plugins/modules/merchant-manager/core/models/offer.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'merchant-group-profile-remove-offer-modal',
  templateUrl: './remove-offer-modal.component.html',
  styleUrls: ['./remove-offer-modal.component.scss'],
})
export class RemoveOfferModalComponent {
  @Input() offer: Offer;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _appFacade: AppFacade
  ) {}

  public onConfirm(): void {
    this._merchantGroupFacade.removeMerchantGroupOffers();
    this._appFacade.toggleModal();
  }

  public onCancel(): void {
    this._merchantGroupFacade.cancelRemoveMerchantGroupOffers();
    this._appFacade.toggleModal();
  }
}
