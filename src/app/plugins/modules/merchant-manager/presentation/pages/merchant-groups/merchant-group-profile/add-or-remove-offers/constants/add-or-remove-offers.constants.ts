import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const margin: string = '10px 0';
const width: string = '100%';
const height: string = '150px';

const skeletonTheme: SkeletonTheme = { width, height, margin };

export const SKELETON_OFFER_ITEM: Skeleton[] = [
  ...getSkeleton(1, 4, skeletonTheme),
];
