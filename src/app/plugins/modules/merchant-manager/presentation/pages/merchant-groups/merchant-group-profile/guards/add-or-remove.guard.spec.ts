import { TestBed } from '@angular/core/testing';

import { AddOrRemoveGuard } from './add-or-remove.guard';

describe('AddOrRemoveGuard', () => {
  let guard: AddOrRemoveGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AddOrRemoveGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
