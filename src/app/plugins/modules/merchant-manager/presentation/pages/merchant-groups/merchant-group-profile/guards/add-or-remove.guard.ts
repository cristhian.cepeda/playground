import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { combineLatest, map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AddOrRemoveGuard implements CanActivate {
  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _router: Router
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return combineLatest([
      this._merchantGroupFacade.merchantGroupProfileInfo$,
      this._merchantGroupFacade.merchantGroupProfileId$,
    ]).pipe(
      map(([merchantGroupProfile, id]) => {
        if (!!merchantGroupProfile) return true;
        return this._router.createUrlTree(
          [MERCHANT_MANAGER_URLS.MERCHANT_GROUP_PROFILE],
          { queryParams: { id } }
        );
      })
    );
  }
}
