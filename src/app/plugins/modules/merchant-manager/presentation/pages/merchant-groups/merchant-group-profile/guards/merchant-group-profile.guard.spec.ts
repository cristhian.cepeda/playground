import { TestBed } from '@angular/core/testing';

import { MerchantGroupProfileGuard } from './merchant-group-profile.guard';

describe('MerchantGroupProfileGuard', () => {
  let guard: MerchantGroupProfileGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MerchantGroupProfileGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
