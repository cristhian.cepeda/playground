import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanDeactivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MerchantGroupProfileGuard
  implements CanActivate, CanDeactivate<unknown>
{
  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._getMerchantGroupIdParam(route);
  }

  private _getMerchantGroupIdParam(
    route: ActivatedRouteSnapshot
  ): boolean | UrlTree {
    const merchantGroupId = route.queryParams?.id;
    if (!!merchantGroupId) {
      this._merchantGroupFacade.updateMerchantGroupProfileId(merchantGroupId);
      return true;
    }
    return this._router.createUrlTree([MERCHANT_MANAGER_URLS.MERCHANT_GROUPS]);
  }

  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this._merchantGroupFacade.destroyMerchantGroupProfilePage();
    return true;
  }
}
