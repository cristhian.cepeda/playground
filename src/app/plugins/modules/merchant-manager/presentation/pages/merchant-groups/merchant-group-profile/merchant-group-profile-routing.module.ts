import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { AddOrRemoveGuard } from './guards/add-or-remove.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('../merchant-group-profile/profile/profile.module').then(
        (m) => m.ProfileModule
      ),
    data: {
      breadcrumb:
        'MERCHANT_MANAGER.MERCHANT_GROUP_PROFILE.GROUP_PROFILE.BREADCRUMB',
    },
  },
  {
    canActivate: [AddOrRemoveGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES
      .MERCHANT_GROUP_PROFILE.SUB_PAGES.ADD_OR_REMOVE_MERCHANTS.PATH,
    loadChildren: () =>
      import(
        '../merchant-group-profile/add-or-remove-merchants/add-or-remove-merchants.module'
      ).then((m) => m.AddOrRemoveMerchantsPageModule),
    data: {
      breadcrumb:
        'MERCHANT_MANAGER.MERCHANT_GROUP_PROFILE.ADD_OR_REMOVE_MERCHANTS.BREADCRUMB',
    },
  },
  {
    canActivate: [AddOrRemoveGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES
      .MERCHANT_GROUP_PROFILE.SUB_PAGES.ADD_OR_REMOVE_OFFERS.PATH,
    loadChildren: () =>
      import(
        '../merchant-group-profile/add-or-remove-offers/add-or-remove-offers.module'
      ).then((m) => m.AddOrRemoveOffersPageModule),
    data: {
      breadcrumb:
        'MERCHANT_MANAGER.MERCHANT_GROUP_PROFILE.ADD_OR_REMOVE_OFFERS.BREADCRUMB',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantGroupProfilePageRoutingModule {}
