import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MerchantGroupProfilePageRoutingModule } from './merchant-group-profile-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, MerchantGroupProfilePageRoutingModule],
})
export class MerchantGroupProfilePageModule {}
