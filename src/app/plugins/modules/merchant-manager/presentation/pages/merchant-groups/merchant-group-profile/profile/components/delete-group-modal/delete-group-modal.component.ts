import { Component } from '@angular/core';
import { AppFacade } from '@app/facade/app/app.facade';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'merchant-group-profile-delete-group-modal',
  templateUrl: './delete-group-modal.component.html',
  styleUrls: ['./delete-group-modal.component.scss'],
})
export class DeleteGroupModalComponent {
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _appFacade: AppFacade
  ) {}

  public onConfirm(): void {
    this._merchantGroupFacade.deleteMerchantGroup();
  }

  public onCancel(): void {
    this._appFacade.toggleModal();
  }
}
