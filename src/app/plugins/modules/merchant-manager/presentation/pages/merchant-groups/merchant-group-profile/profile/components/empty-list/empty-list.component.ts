import { Component } from '@angular/core';
import { inOutAnimation } from '@app/core/ animations/ animations';

@Component({
  selector: 'merchant-group-profile-empty-list',
  templateUrl: './empty-list.component.html',
  styleUrls: ['./empty-list.component.scss'],
  animations: inOutAnimation,
})
export class EmptyListComponent {}
