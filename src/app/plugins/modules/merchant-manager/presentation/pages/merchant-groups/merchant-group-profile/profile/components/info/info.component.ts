import { Component, OnInit } from '@angular/core';
import { AppFacade } from '@app/facade/app/app.facade';
import { MerchantGroupProfile } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'merchant-group-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  public merchantGroupProfileInfo$: Observable<MerchantGroupProfile>;
  public isLoading$: Observable<boolean>;

  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _appFacade: AppFacade
  ) {}

  public onDeleteMerchantGroup() {
    this._appFacade.toggleModal();
  }

  ngOnInit(): void {
    this.merchantGroupProfileInfo$ =
      this._merchantGroupFacade.merchantGroupProfileInfo$;
    this.isLoading$ =
      this._merchantGroupFacade.isLoadingMerchantGroupProfileInfo$;
  }
}
