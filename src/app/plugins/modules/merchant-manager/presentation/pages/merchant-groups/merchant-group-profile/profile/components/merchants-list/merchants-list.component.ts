import { Component, OnInit } from '@angular/core';
import { Skeleton } from '@app/core/models/skeleton.model';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { CARD_CONTAINER_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import { ICON_POSITION } from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';
import { SKELETON_MERCHANTS_ITEM } from '../../constants/profile.constants';

@Component({
  selector: 'merchant-group-profile-merchants-list',
  templateUrl: './merchants-list.component.html',
  styleUrls: ['./merchants-list.component.scss'],
})
export class MerchantsListComponent implements OnInit {
  public merchants$: Observable<Merchant[]>;
  public isLoading$: Observable<boolean>;
  public merchantGroupProfileId$: Observable<string>;

  public CARD_CONTAINER_DESIGN_CLASS = CARD_CONTAINER_DESIGN_CLASS;
  public ICON_POSITION = ICON_POSITION;
  public merchantGroupAddOrRemoveMerchantUrl: string;
  public skeletons: Skeleton[] = SKELETON_MERCHANTS_ITEM;

  constructor(private _merchantGroupFacade: MerchantGroupFacade) {}

  ngOnInit(): void {
    this._merchantGroupFacade.getMerchantGroupProfileMerchants();
    this._setInitialValues();
  }

  public onFilter(value) {
    const name: string = value?.target?.value;
    this._merchantGroupFacade.filterMerchantGroupProfileMerchants(name);
  }

  private _setInitialValues() {
    this.merchantGroupAddOrRemoveMerchantUrl =
      MERCHANT_MANAGER_URLS.MERCHANT_GROUP_PROFILE_ADD_OR_REMOVE_MERCHANTS;
    this.merchantGroupProfileId$ =
      this._merchantGroupFacade.merchantGroupProfileId$;
    this.merchants$ = this._merchantGroupFacade.merchantGroupProfileMerchants$;
    this.isLoading$ =
      this._merchantGroupFacade.isLoadingMerchantGroupProfileMerchants$;
  }
}
