import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsOffersListComponent } from './products-offers-list.component';

describe('ProductsOffersListComponent', () => {
  let component: ProductsOffersListComponent;
  let fixture: ComponentFixture<ProductsOffersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductsOffersListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductsOffersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
