import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { Project } from '@app/core/models/project.model';
import { Skeleton } from '@app/core/models/skeleton.model';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { CHARGE_VALUE_TYPE } from '@app/plugins/modules/merchant-manager/core/models/charge.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { CARD_CONTAINER_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { combineLatest, filter, Observable, Subscription } from 'rxjs';
import { SKELETON_PRODUCT_OFFER_ITEM } from '../../constants/profile.constants';

@Component({
  selector: 'merchant-group-profile-products-offers-list',
  templateUrl: './products-offers-list.component.html',
  styleUrls: ['./products-offers-list.component.scss'],
})
export class ProductsOffersListComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public project$: Observable<Project>;
  public productOfferListItemFromOffers$: Observable<
    MerchantGroupProductOfferListItem[]
  >;
  public isLoading$: Observable<boolean>;
  public isLoadingUpdateCommission$: Observable<boolean>;
  public merchantGroupProfileId$: Observable<string>;

  public CARD_CONTAINER_DESIGN_CLASS = CARD_CONTAINER_DESIGN_CLASS;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public skeletons: Skeleton[] = SKELETON_PRODUCT_OFFER_ITEM;
  public canEdit: boolean;
  public merchantGroupAddOrRemoveOffersUrl: string;
  public form: UntypedFormGroup;

  private _offersAndProjectSubscription: Subscription;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _merchantGroupFacade: MerchantGroupFacade
  ) {
    super();
  }

  get offers() {
    return this.form.get('offers') as FormArray;
  }

  ngOnInit(): void {
    this._merchantGroupFacade.getMerchantGroupProfileOffers();
    this._setInitialValues();
    this._setForm();
    this._setFormValue();
  }

  public onEdit(): void {
    this.canEdit = true;
  }

  public onConfirm(): void {
    if (this.offers.invalid) return;
    this._merchantGroupFacade.updateCommissionMerchantGroupProfile(
      this.offers.value
    );
    this.canEdit = false;
  }

  private _setInitialValues() {
    this.merchantGroupAddOrRemoveOffersUrl =
      MERCHANT_MANAGER_URLS.MERCHANT_GROUP_PROFILE_ADD_OR_REMOVE_OFFERS;
    this.project$ = this._merchantGroupFacade.project$;
    this.merchantGroupProfileId$ =
      this._merchantGroupFacade.merchantGroupProfileId$;
    this.productOfferListItemFromOffers$ =
      this._merchantGroupFacade.productOfferListItemFromOffersMerchantGroupProfile$;
    this.isLoading$ =
      this._merchantGroupFacade.isLoadingMerchantGroupProfileOffers$;
    this.isLoadingUpdateCommission$ =
      this._merchantGroupFacade.isLoadingMerchantGroupUpdateCommission$;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      offers: this._formBuilder.array([]),
    });
  }

  private _setFormValue() {
    this._offersAndProjectSubscription = combineLatest([
      this._merchantGroupFacade
        .productOfferListItemFromOffersMerchantGroupProfile$,
      this._merchantGroupFacade.project$,
    ])
      .pipe(filter(([offers, project]) => !!offers && !!project))
      .subscribe(([offersList, project]) => {
        this._setForm();
        offersList.map((offer) =>
          this.offers.push(
            this._formBuilder.group({
              offer_id: [offer?.offerId],
              value: [
                parseFloat(offer?.commission),
                [Validators.min(0), Validators.max(100)],
              ],
              tax_id: [project?.default_tax_id],
              type: [CHARGE_VALUE_TYPE.PERCENTAGE],
            })
          )
        );
      });
  }
}
