import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const margin: string = '10px 0';
const width: string = '100%';

const skeletonProductOfferItemTheme: SkeletonTheme = {
  width,
  margin,
  height: '150px',
};
const skeletonMerchantItemTheme: SkeletonTheme = {
  width,
  margin,
  height: '35px',
};

export const SKELETON_PRODUCT_OFFER_ITEM: Skeleton[] = [
  ...getSkeleton(1, 4, skeletonProductOfferItemTheme),
];

export const SKELETON_MERCHANTS_ITEM: Skeleton[] = [
  ...getSkeleton(1, 8, skeletonMerchantItemTheme),
];
