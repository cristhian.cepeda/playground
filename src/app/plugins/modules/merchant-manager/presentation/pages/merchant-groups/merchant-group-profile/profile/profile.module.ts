import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { DeleteGroupModalComponent } from './components/delete-group-modal/delete-group-modal.component';
import { EmptyListComponent } from './components/empty-list/empty-list.component';
import { InfoComponent } from './components/info/info.component';
import { MerchantsListComponent } from './components/merchants-list/merchants-list.component';
import { ProductsOffersListComponent } from './components/products-offers-list/products-offers-list.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePage } from './profile.page';

@NgModule({
  declarations: [
    ProfilePage,
    InfoComponent,
    MerchantsListComponent,
    ProductsOffersListComponent,
    DeleteGroupModalComponent,
    EmptyListComponent,
  ],
  imports: [CommonModule, ProfileRoutingModule, LayoutModule],
})
export class ProfileModule {}
