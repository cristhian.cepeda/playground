import { Component, OnInit } from '@angular/core';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  constructor(private _merchantGroupFacade: MerchantGroupFacade) {}

  ngOnInit(): void {
    this._merchantGroupFacade.getMerchantGroupProfileInfo();
  }
}
