import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { MerchantGroupProfileGuard } from './merchant-group-profile/guards/merchant-group-profile.guard';
import { NewMerchantGroupGuard } from './new-group/guards/new-group.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./merchant-groups/merchant-groups.module').then(
        (m) => m.MerchantGroupsPageModule
      ),
  },
  {
    canActivate: [MerchantGroupProfileGuard],
    canDeactivate: [MerchantGroupProfileGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES
      .MERCHANT_GROUP_PROFILE.PATH,
    loadChildren: () =>
      import('./merchant-group-profile/merchant-group-profile.module').then(
        (m) => m.MerchantGroupProfilePageModule
      ),
  },
  {
    canDeactivate: [NewMerchantGroupGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP
      .PATH,
    loadChildren: () =>
      import('./new-group/new-group.module').then((m) => m.NewGroupModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantGroupsRoutingModule {}
