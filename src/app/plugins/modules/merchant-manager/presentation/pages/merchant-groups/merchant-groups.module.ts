import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { MerchantGroupsEffects } from '@app/plugins/modules/merchant-manager/domain/store/merchant-groups/merchant-groups.effects';
import { MerchantGroupsReducers } from '@app/plugins/modules/merchant-manager/domain/store/merchant-groups/merchant-groups.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MerchantGroupsRoutingModule } from './merchant-groups-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MerchantGroupsRoutingModule,
    StoreModule.forFeature(
      MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.STORE_NAME,
      MerchantGroupsReducers
    ),
    EffectsModule.forFeature([MerchantGroupsEffects]),
  ],
})
export class MerchantGroupsModule {}
