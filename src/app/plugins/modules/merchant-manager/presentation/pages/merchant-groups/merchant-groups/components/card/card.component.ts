import { Component, Input, OnInit } from '@angular/core';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { MerchantGroup } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';

@Component({
  selector: 'merchant-groups-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() merchantGroup: MerchantGroup;
  public merchantGroupProfileUrl: string;

  ngOnInit(): void {
    this.merchantGroupProfileUrl = MERCHANT_MANAGER_URLS.MERCHANT_GROUP_PROFILE;
  }
}
