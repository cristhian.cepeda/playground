import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { FILTERS } from '@app/core/constants/filters.constant';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { MerchantGroup } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
@Component({
  selector: 'merchant-groups-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public merchantGroups$: Observable<TableResponse<MerchantGroup>>;
  public isMerchantGroupsFiltred$: Observable<boolean>;

  public urlNewGroupDetails: string;
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _merchantGroupFacade: MerchantGroupFacade
  ) {}

  ngOnInit(): void {
    this.merchantGroups$ = this._merchantGroupFacade.merchantGroups$;
    this.isMerchantGroupsFiltred$ =
      this._merchantGroupFacade.isMerchantGroupsFiltred$;
    this.urlNewGroupDetails = MERCHANT_MANAGER_URLS.NEW_MERCHANT_GROUP_DETAILS;
    this._setForm();
  }

  public onFilter() {
    this._merchantGroupFacade.updateFiltersMerchantGroups(
      {
        ...this.form.value,
        offset: FILTERS.offset, // It's used for get first page
      },
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._merchantGroupFacade.updateFiltersMerchantGroups(this.form.value);
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
    });
  }
}
