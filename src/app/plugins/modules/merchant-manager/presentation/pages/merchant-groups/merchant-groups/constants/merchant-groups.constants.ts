import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const margin: string = '0 5px 5px 0';
const height: string = '200px';
const width: string = '100%';
const skeletonTheme: SkeletonTheme = { width, margin, height };

export const LIST_MERCHANT_GROUPS_STYLE = {
  display: 'grid',
  gap: '2rem',
  ['grid-template-columns']: `repeat(auto-fill, 280px)`,
};
export const SKELETON_MERCHANT_GROUPS: Skeleton[] = [
  ...getSkeleton(1, 4, skeletonTheme),
  ...getSkeleton(1, 4, skeletonTheme),
];
