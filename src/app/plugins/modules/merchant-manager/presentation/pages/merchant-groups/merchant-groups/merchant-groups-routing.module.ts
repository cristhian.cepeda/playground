import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MerchantGroupsPage } from './merchant-groups.page';

const routes: Routes = [
  {
    path: '',
    component: MerchantGroupsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantGroupsPageRoutingModule {}
