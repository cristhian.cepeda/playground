import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';

import { CardComponent } from './components/card/card.component';
import { FiltersComponent } from './components/filters/filters.component';
import { MerchantGroupsPageRoutingModule } from './merchant-groups-routing.module';
import { MerchantGroupsPage } from './merchant-groups.page';

@NgModule({
  declarations: [MerchantGroupsPage, FiltersComponent, CardComponent],
  imports: [CommonModule, MerchantGroupsPageRoutingModule, LayoutModule],
})
export class MerchantGroupsPageModule {}
