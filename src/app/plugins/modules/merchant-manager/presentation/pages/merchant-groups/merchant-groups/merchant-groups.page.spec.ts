import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantGroupsPage } from './merchant-groups.page';

describe('MerchantGroupsPage', () => {
  let component: MerchantGroupsPage;
  let fixture: ComponentFixture<MerchantGroupsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MerchantGroupsPage],
    }).compileComponents();

    fixture = TestBed.createComponent(MerchantGroupsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
