import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Skeleton } from '@app/core/models/skeleton.model';
import { FILTERS_MERCHANTS } from '@app/plugins/modules/merchant-manager/core/constants/filters.constant';
import {
  MerchantGroup,
  MerchantGroupFilters,
} from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, take } from 'rxjs';
import {
  LIST_MERCHANT_GROUPS_STYLE,
  SKELETON_MERCHANT_GROUPS,
} from './constants/merchant-groups.constants';

@Component({
  selector: 'merchant-manager-merchant-groups',
  templateUrl: './merchant-groups.page.html',
  styleUrls: ['./merchant-groups.page.scss'],
})
export class MerchantGroupsPage implements OnInit {
  public merchantGroups$: Observable<TableResponse<MerchantGroup>>;
  public isLoadingMerchantGroups$: Observable<boolean>;
  public skeleton: Skeleton[];
  public listMerchantGroupsStyle: any;
  public pageSize: number;

  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _activeRouted: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._getQueryParams();
  }

  public onChangePageFilter(filters: MerchantGroupFilters) {
    this._merchantGroupFacade.updateFiltersMerchantGroups(filters, {
      isNativationFilter: true,
    });
  }

  private _setInitialValues() {
    this.merchantGroups$ = this._merchantGroupFacade.merchantGroups$;
    this.isLoadingMerchantGroups$ =
      this._merchantGroupFacade.isLoadingMerchantGroups$;
    this.pageSize = FILTERS_MERCHANTS.limit;
    this.skeleton = SKELETON_MERCHANT_GROUPS;
    this.listMerchantGroupsStyle = LIST_MERCHANT_GROUPS_STYLE;
  }

  private _getQueryParams() {
    this._activeRouted.queryParamMap.pipe(take(1)).subscribe((params) => {
      const merchant_id = params.get('merchant_id');
      if (!merchant_id) {
        this._merchantGroupFacade.initMerchantGroupsPage();
        return;
      }
      this._merchantGroupFacade.initMerchantGroupsPage({ merchant_id });
    });
  }
}
