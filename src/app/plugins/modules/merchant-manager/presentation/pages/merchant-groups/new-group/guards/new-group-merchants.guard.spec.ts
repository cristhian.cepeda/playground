import { TestBed } from '@angular/core/testing';

import { NewGroupMerchantsGuard } from './new-group-merchants.guard';

describe('NewGroupMerchantsGuard', () => {
  let guard: NewGroupMerchantsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NewGroupMerchantsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
