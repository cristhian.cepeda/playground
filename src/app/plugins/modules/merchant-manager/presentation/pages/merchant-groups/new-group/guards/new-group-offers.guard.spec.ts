import { TestBed } from '@angular/core/testing';

import { NewGroupOffersGuard } from './new-group-offers.guard';

describe('NewGroupOffersGuard', () => {
  let guard: NewGroupOffersGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NewGroupOffersGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
