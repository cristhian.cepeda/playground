import { TestBed } from '@angular/core/testing';

import { NewGroupSuccessGuard } from './new-group-success.guard';

describe('NewGroupSuccessGuard', () => {
  let guard: NewGroupSuccessGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NewGroupSuccessGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
