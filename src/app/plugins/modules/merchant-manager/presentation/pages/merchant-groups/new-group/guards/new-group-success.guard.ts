import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { MerchantGroupFees } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NewGroupSuccessGuard implements CanActivate {
  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._merchantGroupFacade.offerFees$.pipe(
      map((offerFees: MerchantGroupFees[]) => {
        if (offerFees?.length > 0) return true;
        return this._router.createUrlTree([
          MERCHANT_MANAGER_URLS.NEW_MERCHANT_GROUP_OFFERS,
        ]);
      })
    );
  }
}
