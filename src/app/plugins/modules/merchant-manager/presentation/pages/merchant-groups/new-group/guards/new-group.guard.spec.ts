import { TestBed } from '@angular/core/testing';

import { NewMerchantGroupGuard } from './new-group.guard';

describe('NewGroupGuard', () => {
  let guard: NewMerchantGroupGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NewMerchantGroupGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
