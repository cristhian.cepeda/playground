import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NewMerchantGroupGuard implements CanDeactivate<unknown> {
  constructor(private _merchantGroupFacade: MerchantGroupFacade) {}
  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this._merchantGroupFacade.destroyNewMerchantGroupPages();
    return true;
  }
}
