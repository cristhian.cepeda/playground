import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewGroupDetailsPage } from './new-group-details.page';

const routes: Routes = [
  {
    path: '',
    component: NewGroupDetailsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewGroupDetailsRoutingModule {}
