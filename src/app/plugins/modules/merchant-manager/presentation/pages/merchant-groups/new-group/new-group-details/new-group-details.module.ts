import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { NewGroupDetailsRoutingModule } from './new-group-details-routing.module';
import { NewGroupDetailsPage } from './new-group-details.page';

@NgModule({
  declarations: [NewGroupDetailsPage],
  imports: [CommonModule, NewGroupDetailsRoutingModule, LayoutModule],
})
export class NewGroupDetailsModule {}
