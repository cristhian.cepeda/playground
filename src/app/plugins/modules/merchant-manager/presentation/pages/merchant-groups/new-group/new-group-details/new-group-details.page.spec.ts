import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGroupDetailsPage } from './new-group-details.page';

describe('NewGroupDetailsPage', () => {
  let component: NewGroupDetailsPage;
  let fixture: ComponentFixture<NewGroupDetailsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewGroupDetailsPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewGroupDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
