import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { NewMerchantGroupDetails } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { CustomValidators } from '@app/presentation/layout/mo-forms/validators/custom.validators';
import { filter, Observable, Subscription, take } from 'rxjs';

@Component({
  selector: 'merchant-manager-new-group-details',
  templateUrl: './new-group-details.page.html',
  styleUrls: ['./new-group-details.page.scss'],
})
export class NewGroupDetailsPage
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public isLoadingMerchantName$: Observable<boolean>;
  public isValidMerchantName$: Observable<boolean>;

  public urlMerchantGroup: string;
  public urlNewMerchantGroupMerchants: string;
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;

  private _enableNameSubcription: Subscription;
  private _newMerchantDetails: Subscription;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _merchantGroupFacade: MerchantGroupFacade,
    private _router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onConfirm(): void {
    if (this.form.invalid) return;
    const details: NewMerchantGroupDetails = this.form.value;
    this._merchantGroupFacade.updateNewMerchantGroupData({ details });
    this._router.navigateByUrl(
      MERCHANT_MANAGER_URLS.NEW_MERCHANT_GROUP_MERCHANTS
    );
  }

  public onCancel(): void {
    this._merchantGroupFacade.resetNewMerchantGroup();
  }

  public onCheckMerchantGroupName() {
    this._merchantGroupFacade.checkMerchantGroupName(
      this.form.get('name').value
    );
  }

  private _setInitialValues() {
    this.urlMerchantGroup = MERCHANT_MANAGER_URLS.MERCHANT_GROUPS;
    this.urlNewMerchantGroupMerchants =
      MERCHANT_MANAGER_URLS.NEW_MERCHANT_GROUP_MERCHANTS;
    this.isLoadingMerchantName$ =
      this._merchantGroupFacade.isLoadingMerchantName$;
    this.isValidMerchantName$ = this._merchantGroupFacade.isValidMerchantName$;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      name: ['', [Validators.required]],
      short_description: [
        '',
        [Validators.required, CustomValidators.maxWords(10)],
      ],
      description: ['', [Validators.required, CustomValidators.maxWords(200)]],
    });

    this._newMerchantDetails =
      this._merchantGroupFacade.newGroupMerchantDetails$
        .pipe(
          filter((details) => !!details),
          take(1)
        )
        .subscribe((details) => {
          this.form.get('name').setValue(details?.name);
          this.form
            .get('short_description')
            .setValue(details?.short_description);
          this.form.get('description').setValue(details?.description);
        });

    this._enableNameSubcription =
      this._merchantGroupFacade.isLoadingMerchantName$.subscribe(
        (isLoadingMerchantName) => {
          if (isLoadingMerchantName) this.form.get('name').disable();
          else this.form.get('name').enable();
        }
      );
  }
}
