import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantBoxComponent } from './merchant-box.component';

describe('MerchantBoxComponent', () => {
  let component: MerchantBoxComponent;
  let fixture: ComponentFixture<MerchantBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MerchantBoxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MerchantBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
