import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable, Subscription, take } from 'rxjs';

@Component({
  selector: 'new-group-merchant-box',
  templateUrl: './merchant-box.component.html',
  styleUrls: ['./merchant-box.component.scss'],
})
export class MerchantBoxComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public merchants$: Observable<Merchant[]>;
  public merchantsSelected$: Observable<Merchant[]>;

  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public urlMerchantGroup: string;
  public urlMerchantGroupOffer: string;

  private _merchantsSelectedSubscription: Subscription;

  constructor(
    private _merchantGroupFacade: MerchantGroupFacade,
    private _router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onRemoveItem(merchant: Merchant) {
    this._merchantGroupFacade.updateNewGroupSelectedMerchants(merchant, true);
  }

  public onCancel(): void {
    this._merchantGroupFacade.resetNewMerchantGroup();
  }

  public onConfirm(): void {
    this._merchantsSelectedSubscription = this.merchantsSelected$
      .pipe(take(1))
      .subscribe((merchantsSelected) => {
        if (merchantsSelected?.length > 0)
          this._router.navigateByUrl(this.urlMerchantGroupOffer);
      });
  }

  private _setInitialValues() {
    this.urlMerchantGroup = MERCHANT_MANAGER_URLS.MERCHANT_GROUPS;
    this.urlMerchantGroupOffer =
      MERCHANT_MANAGER_URLS.NEW_MERCHANT_GROUP_OFFERS;
    this.merchantsSelected$ =
      this._merchantGroupFacade.newGroupMerchantsSelected$;
  }
}
