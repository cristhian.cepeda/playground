import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantsTableFiltersComponent } from './merchants-table-filters.component';

describe('MerchantsTableFiltersComponent', () => {
  let component: MerchantsTableFiltersComponent;
  let fixture: ComponentFixture<MerchantsTableFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MerchantsTableFiltersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MerchantsTableFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
