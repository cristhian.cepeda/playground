import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { FILTERS } from '@app/core/constants/filters.constant';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'new-group-merchants-table-filters',
  templateUrl: './merchants-table-filters.component.html',
  styleUrls: ['./merchants-table-filters.component.scss'],
})
export class MerchantsTableFiltersComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public merchants$: Observable<TableResponse<Merchant>>;
  public isMerchantsFiltred$: Observable<boolean>;

  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;
  public CONTROLS_NAMES: { [key: string]: string };

  private _formSearch$: Subscription;
  private _formMCC$: Subscription;
  private _validatorByControl: {
    [key: string]: ValidatorFn | ValidatorFn[] | null;
  };

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _merchantGroupFacade: MerchantGroupFacade
  ) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._merchantGroupFacade.updateFiltersMerchantGroupMerchants(
      {
        ...this.form.value,
        offset: FILTERS.offset, // It's used for get first page
      },
      {
        isFiltred: true,
      }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._merchantGroupFacade.updateFiltersMerchantGroupMerchants(
      this.form.value
    );
  }

  private _setInitialValues() {
    this.merchants$ = this._merchantGroupFacade.merchantsNewMerchantGroup$;
    this.isMerchantsFiltred$ = this._merchantGroupFacade.isMerchantsFiltred$;
    this.CONTROLS_NAMES = {
      SEARCH: 'search',
      MCC: 'mcc',
    };

    this._validatorByControl = {
      [this.CONTROLS_NAMES.SEARCH]: [Validators.required],
      [this.CONTROLS_NAMES.MCC]: [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4),
      ],
    };
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      [this.CONTROLS_NAMES.SEARCH]: [''],
      [this.CONTROLS_NAMES.MCC]: [''],
    });

    this._formSearch$ = this.form
      .get(this.CONTROLS_NAMES.SEARCH)
      .valueChanges.subscribe((value) =>
        this._validateControl(this.CONTROLS_NAMES.SEARCH, value)
      );
    this._formMCC$ = this.form
      .get(this.CONTROLS_NAMES.MCC)
      .valueChanges.subscribe((value) =>
        this._validateControl(this.CONTROLS_NAMES.MCC, value)
      );
  }

  private _validateControl(controlName, value) {
    const controls = Object.entries(this.form?.controls).map(([name, val]) => ({
      name,
      val,
    }));

    const othersControlNames = controls.filter(
      (control) => control?.name != controlName
    );
    if (value) {
      othersControlNames.forEach((control) => {
        this.form.get(control?.name).setValidators(null);
        this.form.get(control?.name).setValue('');
        this.form.get(control?.name).disable();
      });

      return;
    }

    this.form
      .get(controlName)
      .setValidators(this._validatorByControl[controlName]);
    this.form.updateValueAndValidity();

    othersControlNames.forEach((control) => {
      if (this.form.get(control?.name)?.disabled)
        this.form.get(control?.name).enable();
    });
  }
}
