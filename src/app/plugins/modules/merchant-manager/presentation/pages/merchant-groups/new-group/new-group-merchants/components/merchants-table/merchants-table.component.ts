import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getMerchantsTablePerFamily,
  MERCHANT_TABLES,
} from '@app/plugins/modules/merchant-manager/core/constants/tables';
import {
  Merchant,
  MerchantFilters,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'new-group-merchants-table',
  templateUrl: './merchants-table.component.html',
  styleUrls: ['./merchants-table.component.scss'],
})
export class MerchantsTableComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  @ViewChild('addMerchantTemplate', { static: true })
  addMerchantTemplate: TemplateRef<ContextColumn>;

  public headers: TableHeader[];
  public merchantProfileUrl: string;

  public merchants$: Observable<TableResponse<Merchant>>;
  public isLoadingMerchants$: Observable<boolean>;

  private _projectFamilySubscription: Subscription;

  constructor(private _merchantGroupFacade: MerchantGroupFacade) {
    super();
  }

  ngOnInit(): void {
    this._merchantGroupFacade.initNewGroupMerchantsPage();
    this._setInitialValues();
  }

  public onChangeTableFilters(filters: MerchantFilters) {
    this._merchantGroupFacade.updateFiltersMerchantGroupMerchants(filters, {
      isNativationFilter: true,
    });
  }

  public onAddMerchant(merchant: Merchant) {
    this._merchantGroupFacade.updateNewGroupSelectedMerchants(merchant);
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.merchants$ = this._merchantGroupFacade.merchantsNewMerchantGroup$;
    this.isLoadingMerchants$ = this._merchantGroupFacade.isLoadingMerchants$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._merchantGroupFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getMerchantsTablePerFamily(
          projectFamily,
          MERCHANT_TABLES.MERCHANT_GROUP_MERCHANTS
        ).map((header) => {
          if (header.dataKey == 'id')
            header.template = this.addMerchantTemplate;
          return header;
        });
      });
  }
}
