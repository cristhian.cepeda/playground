import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewGroupMerchantsPage } from './new-group-merchants.page';

const routes: Routes = [
  {
    path: '',
    component: NewGroupMerchantsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewGroupMerchantsRoutingModule {}
