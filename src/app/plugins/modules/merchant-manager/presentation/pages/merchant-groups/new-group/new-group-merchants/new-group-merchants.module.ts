import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { NewGroupMerchantsRoutingModule } from './new-group-merchants-routing.module';
import { NewGroupMerchantsPage } from './new-group-merchants.page';
import { MerchantBoxComponent } from './components/merchant-box/merchant-box.component';
import { MerchantsTableComponent } from './components/merchants-table/merchants-table.component';
import { MerchantsTableFiltersComponent } from './components/merchants-table-filters/merchants-table-filters.component';

@NgModule({
  declarations: [NewGroupMerchantsPage, MerchantBoxComponent, MerchantsTableComponent, MerchantsTableFiltersComponent],
  imports: [CommonModule, NewGroupMerchantsRoutingModule, LayoutModule],
})
export class NewGroupMerchantsModule {}
