import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGroupMerchantsPage } from './new-group-merchants.page';

describe('NewGroupMerchantsPage', () => {
  let component: NewGroupMerchantsPage;
  let fixture: ComponentFixture<NewGroupMerchantsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewGroupMerchantsPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewGroupMerchantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
