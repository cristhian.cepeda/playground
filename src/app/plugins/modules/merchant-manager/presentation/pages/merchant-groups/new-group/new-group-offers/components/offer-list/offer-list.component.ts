import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { Project } from '@app/core/models/project.model';
import { Skeleton } from '@app/core/models/skeleton.model';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { CHARGE_VALUE_TYPE } from '@app/plugins/modules/merchant-manager/core/models/charge.model';
import { MerchantGroupFees } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { CARD_CONTAINER_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { combineLatest, filter, map, Observable, Subscription } from 'rxjs';
import { SKELETON_OFFER_ITEM } from '../../constants/new-group-offers.constants';

@Component({
  selector: 'new-group-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.scss'],
})
export class OfferListComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public project$: Observable<Project>;
  public productOfferListItemFromOffers$: Observable<
    MerchantGroupProductOfferListItem[]
  >;

  public isLoadingOffers$: Observable<boolean>;
  public enableConfirm$: Observable<boolean>;

  public CARD_CONTAINER_DESIGN_CLASS = CARD_CONTAINER_DESIGN_CLASS;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public skeletons: Skeleton[] = SKELETON_OFFER_ITEM;
  public urlMerchantGroup: string;
  public form: UntypedFormGroup;

  private _feesSeleted: MerchantGroupFees[] = [];
  private _offersSubscription: Subscription;
  private _offersFormSubscription: Subscription;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _merchantGroupFacade: MerchantGroupFacade
  ) {
    super();
  }

  get offers() {
    return <FormArray>this.form.get('offers');
  }

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onCancel(): void {
    this._merchantGroupFacade.resetNewMerchantGroup();
  }

  public onConfirm(): void {
    const fees: MerchantGroupFees[] = this._feesSeleted.filter(
      (fee) => !!fee.value
    );
    this._merchantGroupFacade.updateNewMerchantGroupData({ fees });
    if (fees.length > 0) {
      this._merchantGroupFacade.createNewMerchantGroup();
    }
  }

  private _setInitialValues() {
    this.urlMerchantGroup = MERCHANT_MANAGER_URLS.MERCHANT_GROUPS;
    this.project$ = this._merchantGroupFacade.project$;
    this.productOfferListItemFromOffers$ =
      this._merchantGroupFacade.productOfferListItemFromOffers$;
    this.isLoadingOffers$ = this._merchantGroupFacade.isLoadingOffers$;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      offers: this._formBuilder.array([]),
    });

    this.enableConfirm$ = this.offers.valueChanges.pipe(
      map((offers) => offers?.some((offer) => !!offer?.value))
    );

    this._offersSubscription = this.offers.valueChanges.subscribe(
      (offerFees) => {
        offerFees.map((offer) => {
          const feeFound = this._feesSeleted.some(
            (feeSeleted) => offer?.offer_id === feeSeleted?.offer_id
          );
          if (feeFound) {
            this._feesSeleted.map((fee) => {
              if (fee?.offer_id == offer?.offer_id) fee.value = offer?.value;
              return fee;
            });
          } else if (!feeFound && !!offer?.value) {
            this._feesSeleted.push(offer);
            return;
          }
        });
      }
    );

    this._offersFormSubscription = combineLatest([
      this._merchantGroupFacade.productOfferListItemFromOffers$,
      this._merchantGroupFacade.project$,
    ])
      .pipe(filter(([offers, project]) => !!offers && !!project))
      .subscribe(([offersList, project]) => {
        this.offers.clear();
        offersList.map((offer) => {
          const valueSeleted: string =
            this._feesSeleted
              .filter((feeSeleted) => offer?.offerId == feeSeleted?.offer_id)
              .map((fee) => fee?.value)[0] ?? '';

          this.offers.push(
            this._formBuilder.group({
              offer_id: [offer?.offerId],
              value: [valueSeleted, [Validators.min(0), Validators.max(100)]],
              tax_id: [project?.default_tax_id],
              type: [CHARGE_VALUE_TYPE.PERCENTAGE],
            })
          );
        });
      });
  }
}
