import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewGroupOffersPage } from './new-group-offers.page';

const routes: Routes = [
  {
    path: '',
    component: NewGroupOffersPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewGroupOffersRoutingModule {}
