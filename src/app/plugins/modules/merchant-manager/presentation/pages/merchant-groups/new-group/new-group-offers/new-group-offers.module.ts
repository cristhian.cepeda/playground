import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { NewGroupOffersRoutingModule } from './new-group-offers-routing.module';
import { NewGroupOffersPage } from './new-group-offers.page';
import { FilterComponent } from './components/filter/filter.component';
import { OfferListComponent } from './components/offer-list/offer-list.component';

@NgModule({
  declarations: [NewGroupOffersPage, FilterComponent, OfferListComponent],
  imports: [CommonModule, NewGroupOffersRoutingModule, LayoutModule],
})
export class NewGroupOffersModule {}
