import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGroupOffersPage } from './new-group-offers.page';

describe('NewGroupOffersPage', () => {
  let component: NewGroupOffersPage;
  let fixture: ComponentFixture<NewGroupOffersPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewGroupOffersPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewGroupOffersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
