import { Component } from '@angular/core';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';

@Component({
  selector: 'merchant-manager-new-group-offers',
  templateUrl: './new-group-offers.page.html',
  styleUrls: ['./new-group-offers.page.scss'],
})
export class NewGroupOffersPage {
  constructor(private _merchantGroupFacade: MerchantGroupFacade) {
    this._merchantGroupFacade.getMerchantGroupOffers();
  }
}
