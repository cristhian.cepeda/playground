import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { NewGroupMerchantsGuard } from './guards/new-group-merchants.guard';
import { NewGroupOffersGuard } from './guards/new-group-offers.guard';
import { NewGroupSuccessGuard } from './guards/new-group-success.guard';

const routes: Routes = [
  {
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP
      .SUB_PAGES.NEW_GROUP_DETAILS.PATH,
    loadChildren: () =>
      import('../new-group/new-group-details/new-group-details.module').then(
        (m) => m.NewGroupDetailsModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.NEW_GROUP.BREADCRUMB',
    },
  },
  {
    canActivate: [NewGroupMerchantsGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP
      .SUB_PAGES.NEW_GROUP_MERCHANTS.PATH,
    loadChildren: () =>
      import(
        '../new-group/new-group-merchants/new-group-merchants.module'
      ).then((m) => m.NewGroupMerchantsModule),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.NEW_GROUP.BREADCRUMB',
    },
  },
  {
    canActivate: [NewGroupOffersGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP
      .SUB_PAGES.NEW_GROUP_OFFERS.PATH,
    loadChildren: () =>
      import('../new-group/new-group-offers/new-group-offers.module').then(
        (m) => m.NewGroupOffersModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.NEW_GROUP.BREADCRUMB',
    },
  },
  {
    canActivate: [NewGroupSuccessGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANT_GROUPS.SUB_PAGES.NEW_GROUP
      .SUB_PAGES.NEW_GROUP_SUCCESS.PATH,
    loadChildren: () =>
      import('../new-group/new-group-success/new-group-success.module').then(
        (m) => m.NewGroupSuccessModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.NEW_GROUP.BREADCRUMB',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewGroupRoutingModule {}
