import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewGroupSuccessPage } from './new-group-success.page';

const routes: Routes = [
  {
    path: '',
    component: NewGroupSuccessPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewGroupSuccessRoutingModule {}
