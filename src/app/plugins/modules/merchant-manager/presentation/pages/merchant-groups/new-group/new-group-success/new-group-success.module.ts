import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { NewGroupSuccessRoutingModule } from './new-group-success-routing.module';
import { NewGroupSuccessPage } from './new-group-success.page';

@NgModule({
  declarations: [NewGroupSuccessPage],
  imports: [CommonModule, NewGroupSuccessRoutingModule, LayoutModule],
})
export class NewGroupSuccessModule {}
