import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGroupSuccessPage } from './new-group-success.page';

describe('NewGroupSuccessPage', () => {
  let component: NewGroupSuccessPage;
  let fixture: ComponentFixture<NewGroupSuccessPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewGroupSuccessPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewGroupSuccessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
