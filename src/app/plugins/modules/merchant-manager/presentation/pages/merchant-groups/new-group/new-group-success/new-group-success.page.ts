import { Component, OnInit } from '@angular/core';
import { NewMerchantGroup } from '@app/plugins/modules/merchant-manager/core/models/merchant-groups.model';
import { MerchantGroupFacade } from '@app/plugins/modules/merchant-manager/facade/merchant-group.facade';
import { CARD_CONTAINER_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';

@Component({
  selector: 'merchant-manager-new-group-success',
  templateUrl: './new-group-success.page.html',
  styleUrls: ['./new-group-success.page.scss'],
})
export class NewGroupSuccessPage implements OnInit {
  public newMerchantGroupData$: Observable<NewMerchantGroup>;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public CARD_CONTAINER_DESIGN_CLASS = CARD_CONTAINER_DESIGN_CLASS;

  constructor(private _merchantGroupFacade: MerchantGroupFacade) {}

  ngOnInit(): void {
    this.newMerchantGroupData$ =
      this._merchantGroupFacade.newMerchantGroupData$;
  }

  public onFinish(): void {
    this._merchantGroupFacade.resetNewMerchantGroup();
  }
}
