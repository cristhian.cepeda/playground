import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewGroupRoutingModule } from './new-group-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NewGroupRoutingModule
  ]
})
export class NewGroupModule { }
