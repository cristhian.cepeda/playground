import { Component } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'merchant-profile-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent {
  public merchantProfileItems$: Observable<SummaryGroup[]>;
  public isLoading$: Observable<boolean>;

  constructor(private _merchantsFacade: MerchantsFacade) {
    this.merchantProfileItems$ = this._merchantsFacade.merchantProfileItems$;
    this.isLoading$ = this._merchantsFacade.isLoadingMerchantProfile$;
  }
}
