import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupAndOffersComponent } from './group-and-offers.component';

describe('GroupAndOffersComponent', () => {
  let component: GroupAndOffersComponent;
  let fixture: ComponentFixture<GroupAndOffersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupAndOffersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GroupAndOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
