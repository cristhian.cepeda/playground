import { Component, OnInit } from '@angular/core';
import { Skeleton } from '@app/core/models/skeleton.model';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { MerchantGroupAndOffersProfile } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';
import { SKELETON_GROUP_AND_OFFER_PERCENTAGE } from '../../constants/group-and-offers.constants';

@Component({
  selector: 'merchant-profile-group-and-offers',
  templateUrl: './group-and-offers.component.html',
  styleUrls: ['./group-and-offers.component.scss'],
})
export class GroupAndOffersComponent implements OnInit {
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public merchantGroupAndOffers$: Observable<MerchantGroupAndOffersProfile>;
  public isLoading$: Observable<boolean>;
  public merchantId$: Observable<string>;
  public skeletonGroupAndOfferPercentage: Skeleton[];
  public merchantGroupsUrl: string;

  constructor(private _merchantsFacade: MerchantsFacade) {}

  ngOnInit(): void {
    this._merchantsFacade.getMerchantGroupAndOffers();
    this.merchantGroupAndOffers$ =
      this._merchantsFacade.merchantGroupAndOffers$;
    this.isLoading$ = this._merchantsFacade.isLoadingMerchantGroupAndOffers$;
    this.merchantId$ = this._merchantsFacade.merchantId$;
    this.skeletonGroupAndOfferPercentage = SKELETON_GROUP_AND_OFFER_PERCENTAGE;
    this.merchantGroupsUrl = MERCHANT_MANAGER_URLS.MERCHANT_GROUPS;
  }
}
