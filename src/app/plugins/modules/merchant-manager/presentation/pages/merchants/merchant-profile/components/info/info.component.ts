import { Component } from '@angular/core';
import { MerchantProfile } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'merchant-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent {
  public merchantProfile$: Observable<MerchantProfile>;
  public isLoading$: Observable<boolean>;

  constructor(private _merchantsFacade: MerchantsFacade) {
    this.merchantProfile$ = this._merchantsFacade.merchantProfile$;
    this.isLoading$ = this._merchantsFacade.isLoadingMerchantProfile$;
  }
}
