import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getMerchantsTablePerFamily,
  MERCHANT_TABLES,
} from '@app/plugins/modules/merchant-manager/core/constants/tables';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TABLE_DESIGN_CLASS } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'merchant-profile-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.scss'],
})
export class MovementsComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public designClass: TABLE_DESIGN_CLASS;
  public headers: TableHeader[];

  private _projectFamilySubscription: Subscription;

  constructor(private _merchantsFacade: MerchantsFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  private _setInitialValues() {
    this.designClass = TABLE_DESIGN_CLASS.ALTERNATIVE;
    this._mapHeaders();
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._merchantsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getMerchantsTablePerFamily(
          projectFamily,
          MERCHANT_TABLES.MERCHANT_PROFILE_MOVEMENTS
        );
      });
  }
}
