import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getMerchantsTablePerFamily,
  MERCHANT_TABLES,
} from '@app/plugins/modules/merchant-manager/core/constants/tables';
import {
  LOAN_MANAGER_URLS,
  MERCHANT_MANAGER_URLS,
} from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { Loan } from '@app/plugins/modules/merchant-manager/core/models/loan.model';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TABLE_DESIGN_CLASS } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'merchant-profile-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.scss'],
})
export class PurchasesComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public merchantPurchases$: Observable<Loan[]>;
  public isLoading$: Observable<boolean>;
  public merchantId$: Observable<string>;
  public designClass: TABLE_DESIGN_CLASS;
  public headers: TableHeader[];
  public loansUrl: string;
  public purchasesUrl: string;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  private _projectFamilySubscription: Subscription;

  constructor(private _merchantsFacade: MerchantsFacade) {
    super();
  }

  ngOnInit(): void {
    this._merchantsFacade.getMerchantPurchases();
    this._setInitialValues();
  }
  private _setInitialValues() {
    this._mapHeaders();
    this.merchantPurchases$ = this._merchantsFacade.merchantPurchases$;
    this.isLoading$ = this._merchantsFacade.isLoadingMerchantPurchases$;
    this.merchantId$ = this._merchantsFacade.merchantId$;
    this.designClass = TABLE_DESIGN_CLASS.ALTERNATIVE;
    this.loansUrl = LOAN_MANAGER_URLS.LOANS;
    this.purchasesUrl = MERCHANT_MANAGER_URLS.PURCHASES;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._merchantsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getMerchantsTablePerFamily(
          projectFamily,
          MERCHANT_TABLES.MERCHANT_PROFILE_PURCHASES
        );
      });
  }
}
