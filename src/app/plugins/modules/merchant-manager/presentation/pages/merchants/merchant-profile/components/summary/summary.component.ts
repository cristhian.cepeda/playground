import { Component, OnInit } from '@angular/core';
import { Skeleton } from '@app/core/models/skeleton.model';
import { MerchantSummary } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import { Observable } from 'rxjs';
import { SKELETON_SUMMERY_MERCHANT } from '../../constants/merchant.constants';

@Component({
  selector: 'merchant-profile-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
  public merchantSummary$: Observable<MerchantSummary>;
  public isLoading$: Observable<boolean>;
  public skeletons: Skeleton[] = SKELETON_SUMMERY_MERCHANT;

  constructor(private _merchantsFacade: MerchantsFacade) {}

  ngOnInit(): void {
    this._merchantsFacade.getMerchantSummary();
    this.merchantSummary$ = this._merchantsFacade.merchantSummary$;
    this.isLoading$ = this._merchantsFacade.isLoadingMerchantSummary$;
  }
}
