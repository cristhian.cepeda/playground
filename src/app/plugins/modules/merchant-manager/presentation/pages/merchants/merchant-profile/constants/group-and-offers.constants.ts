import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const margin: string = '0 5px 5px 0';
const width: string = '120px';
const height: string = '110px';

const skeletonTheme: SkeletonTheme = { width, height, margin };

export const SKELETON_GROUP_AND_OFFER_PERCENTAGE: Skeleton[] = [
  ...getSkeleton(1, 1, skeletonTheme),
];
