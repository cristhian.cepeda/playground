import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const margin: string = '0 0 0 0';
const width: string = '100%';
const height: string = '163px';

const skeletonTheme: SkeletonTheme = { width, height, margin };

export const SKELETON_SUMMERY_MERCHANT: Skeleton[] = [
  ...getSkeleton(1, 4, skeletonTheme),
];
