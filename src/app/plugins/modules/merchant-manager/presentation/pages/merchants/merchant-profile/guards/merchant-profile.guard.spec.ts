import { TestBed } from '@angular/core/testing';

import { MerchantProfileGuard } from './merchant-profile.guard';

describe('MerchantProfileGuard', () => {
  let guard: MerchantProfileGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MerchantProfileGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
