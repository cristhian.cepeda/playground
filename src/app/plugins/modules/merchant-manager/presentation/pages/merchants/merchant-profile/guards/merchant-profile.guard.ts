import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MerchantProfileGuard implements CanActivate {
  constructor(
    private _merchantFacade: MerchantsFacade,
    private _router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._getMerchantIdParam(route);
  }

  private _getMerchantIdParam(
    route: ActivatedRouteSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    const merchantId = route.queryParams?.id;
    if (!!merchantId) {
      this._merchantFacade.updateMerchantId(merchantId);
      return true;
    }
    return this._router.createUrlTree([MERCHANT_MANAGER_URLS.MERCHANTS]);
  }
}
