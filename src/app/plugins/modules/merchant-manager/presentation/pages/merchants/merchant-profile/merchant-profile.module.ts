import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';

import { DetailsComponent } from './components/details/details.component';
import { GroupAndOffersComponent } from './components/group-and-offers/group-and-offers.component';
import { InfoComponent } from './components/info/info.component';
import { MovementsComponent } from './components/movements/movements.component';
import { PurchasesComponent } from './components/purchases/purchases.component';
import { SummaryComponent } from './components/summary/summary.component';
import { MerchantProfilePageRoutingModule } from './merchant-profile-routing.module';
import { MerchantProfilePage } from './merchant-profile.page';

@NgModule({
  declarations: [
    MerchantProfilePage,
    InfoComponent,
    SummaryComponent,
    DetailsComponent,
    PurchasesComponent,
    MovementsComponent,
    GroupAndOffersComponent,
  ],
  imports: [CommonModule, MerchantProfilePageRoutingModule, LayoutModule],
})
export class MerchantProfilePageModule {}
