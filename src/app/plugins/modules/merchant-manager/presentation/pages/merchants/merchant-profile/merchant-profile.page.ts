import { Component, OnDestroy } from '@angular/core';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';

@Component({
  selector: 'merchant-profile',
  templateUrl: './merchant-profile.page.html',
  styleUrls: ['./merchant-profile.page.scss'],
})
export class MerchantProfilePage implements OnDestroy {
  constructor(private _merchantsFacade: MerchantsFacade) {
    this._merchantsFacade.getMerchantProfile();
  }

  ngOnDestroy(): void {
    this._merchantsFacade.destroyMerchantProfilePage();
  }
}
