import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { MerchantProfileGuard } from './merchant-profile/guards/merchant-profile.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./merchants/merchants.module').then((m) => m.MerchantsModule),
  },
  {
    canActivate: [MerchantProfileGuard],
    path: MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.SUB_PAGES.MERCHANT_PROFILE
      .PATH,
    loadChildren: () =>
      import('./merchant-profile/merchant-profile.module').then(
        (m) => m.MerchantProfilePageModule
      ),
    data: {
      breadcrumb: 'MERCHANT_MANAGER.MERCHANT_PROFILE.TITLE',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantsRoutingModule {}
