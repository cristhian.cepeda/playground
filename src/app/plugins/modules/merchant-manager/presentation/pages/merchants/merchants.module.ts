import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { MerchantsEffects } from '@app/plugins/modules/merchant-manager/domain/store/merchants/merchants.effects';
import { MerchantsReducers } from '@app/plugins/modules/merchant-manager/domain/store/merchants/merchants.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MerchantsRoutingModule } from './merchants-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MerchantsRoutingModule,
    StoreModule.forFeature(
      MERCHANT_MANAGER_FEATURE.PAGES.MERCHANTS.STORE_NAME,
      MerchantsReducers
    ),
    EffectsModule.forFeature([MerchantsEffects]),
  ],
})
export class MerchantsModule {}
