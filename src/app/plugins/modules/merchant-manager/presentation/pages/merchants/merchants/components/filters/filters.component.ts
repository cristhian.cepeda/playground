import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { Merchant } from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { MERCHANT_STATUS_OPTIONS } from '../../constants/status.constants';
@Component({
  selector: 'merchants-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public merchants$: Observable<TableResponse<Merchant>>;
  public isMerchantsFiltred$: Observable<boolean>;

  public statusOptions: SelectOption<string>[];
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;
  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _merchantsFacade: MerchantsFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._merchantsFacade.updateFiltersMerchants(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._merchantsFacade.updateFiltersMerchants(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at'];
    this.merchants$ = this._merchantsFacade.merchants$;
    this.isMerchantsFiltred$ = this._merchantsFacade.isMerchantsFiltred$;
    this.statusOptions = MERCHANT_STATUS_OPTIONS;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      status: [''],
      created_at: [''],
      sales__lte: [''],
      sales__gte: [''],
      payout__lte: [''],
      payout__gte: [''],
    });
  }
}
