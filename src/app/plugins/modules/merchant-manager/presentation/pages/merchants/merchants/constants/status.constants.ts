import { MERCHANT_STATUS } from '@app/plugins/modules/merchant-manager/core/constants/merchants.constants';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

export const MERCHANT_STATUS_OPTIONS: SelectOption<string>[] = [
  {
    key: MERCHANT_STATUS.ACTIVE,
    value: MERCHANT_STATUS.ACTIVE,
  },
  {
    key: MERCHANT_STATUS.INACTIVE,
    value: MERCHANT_STATUS.INACTIVE,
  },
  {
    key: MERCHANT_STATUS.ARCHIVED,
    value: MERCHANT_STATUS.ARCHIVED,
  },
  {
    key: MERCHANT_STATUS.UNREGISTERED,
    value: MERCHANT_STATUS.UNREGISTERED,
  },
  {
    key: MERCHANT_STATUS.UNSIGNED,
    value: MERCHANT_STATUS.UNSIGNED,
  },
];
