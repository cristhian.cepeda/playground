import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { FiltersComponent } from './components/filters/filters.component';
import { MerchantsRoutingModule } from './merchants-routing.module';
import { MerchantsPage } from './merchants.page';

@NgModule({
  declarations: [MerchantsPage, FiltersComponent],
  imports: [CommonModule, MerchantsRoutingModule, LayoutModule],
})
export class MerchantsModule {}
