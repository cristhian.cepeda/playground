import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { MerchantsPage } from './merchants.page';

describe('MerchantsPage', () => {
  let component: MerchantsPage;
  let fixture: ComponentFixture<MerchantsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [MerchantsPage],
    }).compileComponents();

    fixture = TestBed.createComponent(MerchantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
