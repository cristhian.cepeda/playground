import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getMerchantsTablePerFamily,
  MERCHANT_TABLES,
} from '@app/plugins/modules/merchant-manager/core/constants/tables';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import {
  Merchant,
  MerchantFilters,
} from '@app/plugins/modules/merchant-manager/core/models/merchant.model';
import { MerchantsFacade } from '@app/plugins/modules/merchant-manager/facade/merchants.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'merchant-manager-merchants-page',
  templateUrl: './merchants.page.html',
  styleUrls: ['./merchants.page.scss'],
})
export class MerchantsPage extends AutoUnsubscribeOnDetroy implements OnInit {
  @ViewChild('merchantStatusTemplate', { static: true })
  merchantStatusTemplate: TemplateRef<ContextColumn>;

  public headers: TableHeader[];
  public merchantProfileUrl: string;

  public merchants$: Observable<TableResponse<Merchant>>;
  public isLoadingMerchants$: Observable<boolean>;

  private _projectFamilySubscription: Subscription;

  constructor(private _merchantsFacade: MerchantsFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangeTableFilters(filters: MerchantFilters) {
    this._merchantsFacade.updateFiltersMerchants(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._merchantsFacade.downloadMerchants();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.merchantProfileUrl = MERCHANT_MANAGER_URLS.MERCHANT_PROFILE;
    this._merchantsFacade.initMechantsPage();
    this.merchants$ = this._merchantsFacade.merchants$;
    this.isLoadingMerchants$ = this._merchantsFacade.isLoadingMerchants$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._merchantsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getMerchantsTablePerFamily(
          projectFamily,
          MERCHANT_TABLES.MERCHANTS
        ).map((header) => {
          if (header.dataKey == 'status')
            header.template = this.merchantStatusTemplate;
          return header;
        });
      });
  }
}
