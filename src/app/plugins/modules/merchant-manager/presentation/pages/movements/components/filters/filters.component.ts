import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { Movement } from '@app/plugins/modules/merchant-manager/core/models/movement.model';
import { MovementsFacade } from '@app/plugins/modules/merchant-manager/facade/movements.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { MOVEMENTS_TYPE_OPTIONS } from '../../constants/types.constants';
@Component({
  selector: 'movements-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public movements$: Observable<TableResponse<Movement>>;
  public isMovementsFiltred$: Observable<boolean>;

  public typeOptions: SelectOption<string>[];
  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;
  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _movementsFacade: MovementsFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._movementsFacade.updateFiltersMovements(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._movementsFacade.updateFiltersMovements(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at'];
    this.movements$ = this._movementsFacade.movements$;
    this.isMovementsFiltred$ = this._movementsFacade.isMovementsFiltred$;
    this.typeOptions = MOVEMENTS_TYPE_OPTIONS;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      created_at: [''],
      type: [''],
    });
  }
}
