import { MOVEMENT_TYPE } from '@app/plugins/modules/merchant-manager/core/models/movement.model';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

export const MOVEMENTS_TYPE_OPTIONS: SelectOption<string>[] = [
  {
    key: MOVEMENT_TYPE.IN,
    value: MOVEMENT_TYPE.IN,
  },
  {
    key: MOVEMENT_TYPE.OUT,
    value: MOVEMENT_TYPE.OUT,
  },
];
