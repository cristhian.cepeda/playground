import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { MovementsEffects } from '@app/plugins/modules/merchant-manager/domain/store/movements/movements.effects';
import { MovementsReducers } from '@app/plugins/modules/merchant-manager/domain/store/movements/movements.reducers';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FiltersComponent } from './components/filters/filters.component';
import { MovementsRoutingModule } from './movements-routing.module';
import { MovementsPage } from './movements.page';

@NgModule({
  declarations: [MovementsPage, FiltersComponent],
  imports: [
    CommonModule,
    MovementsRoutingModule,
    LayoutModule,
    StoreModule.forFeature(
      MERCHANT_MANAGER_FEATURE.PAGES.MOVEMENTS.STORE_NAME,
      MovementsReducers
    ),
    EffectsModule.forFeature([MovementsEffects]),
  ],
})
export class MovementsModule {}
