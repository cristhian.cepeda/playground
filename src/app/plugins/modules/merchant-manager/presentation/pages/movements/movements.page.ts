import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getMerchantsTablePerFamily,
  MERCHANT_TABLES,
} from '@app/plugins/modules/merchant-manager/core/constants/tables';
import {
  Movement,
  MovementFilters,
} from '@app/plugins/modules/merchant-manager/core/models/movement.model';
import { MovementsFacade } from '@app/plugins/modules/merchant-manager/facade/movements.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription, take } from 'rxjs';

@Component({
  selector: 'merchant-manager-movements',
  templateUrl: './movements.page.html',
  styleUrls: ['./movements.page.scss'],
})
export class MovementsPage extends AutoUnsubscribeOnDetroy implements OnInit {
  @ViewChild('typeTemplate', { static: true })
  typeTemplate: TemplateRef<ContextColumn>;

  public movements$: Observable<TableResponse<Movement>>;
  public isLoadingMovements$: Observable<boolean>;
  public headers: TableHeader[];

  private _projectFamilySubscription: Subscription;

  constructor(
    private _movementsFacade: MovementsFacade,
    private _activeRouted: ActivatedRoute
  ) {
    super();
  }
  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangeTableFilters(filters: MovementFilters) {
    this._movementsFacade.updateFiltersMovements(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._movementsFacade.downloadMovements();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.movements$ = this._movementsFacade.movements$;
    this.isLoadingMovements$ = this._movementsFacade.isLoadingMovements$;

    this._activeRouted.queryParamMap.pipe(take(1)).subscribe((params) => {
      const search = params.get('reference');
      if (search) {
        this._movementsFacade.initMovementsPage({ search });
        return;
      }
      this._movementsFacade.initMovementsPage();
    });
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._movementsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getMerchantsTablePerFamily(
          projectFamily,
          MERCHANT_TABLES.MOVEMENTS
        ).map((header) => {
          if (header.dataKey == 'type') header.template = this.typeTemplate;
          return header;
        });
      });
  }
}
