import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { Payout } from '@app/plugins/modules/merchant-manager/core/models/payout.modes';
import { PayoutsFacade } from '@app/plugins/modules/merchant-manager/facade/payouts.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
@Component({
  selector: 'payouts-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit {
  public payouts$: Observable<TableResponse<Payout>>;
  public isPayoutsFiltred$: Observable<boolean>;

  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  private _controlsWithDateRanges: string[];

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _payoutsFacade: PayoutsFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._payoutsFacade.updateFiltersPayouts(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._payoutsFacade.updateFiltersPayouts(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      date: [''],
    });
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['date'];
    this.payouts$ = this._payoutsFacade.payouts$;
    this.isPayoutsFiltred$ = this._payoutsFacade.isPayoutsFiltred$;
  }
}
