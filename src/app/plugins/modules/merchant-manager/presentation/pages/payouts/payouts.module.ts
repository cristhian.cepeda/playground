import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';

import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { PayoutsEffects } from '@app/plugins/modules/merchant-manager/domain/store/payouts/payouts.effects';
import { PayoutsReducers } from '@app/plugins/modules/merchant-manager/domain/store/payouts/payouts.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FiltersComponent } from './components/filters/filters.component';
import { PayoutsRoutingModule } from './payouts-routing.module';
import { PayoutsPage } from './payouts.page';

@NgModule({
  declarations: [PayoutsPage, FiltersComponent],
  imports: [
    CommonModule,
    PayoutsRoutingModule,
    LayoutModule,
    StoreModule.forFeature(
      MERCHANT_MANAGER_FEATURE.PAGES.PAYOUTS.STORE_NAME,
      PayoutsReducers
    ),
    EffectsModule.forFeature([PayoutsEffects]),
  ],
})
export class PayoutsModule {}
