import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getMerchantsTablePerFamily,
  MERCHANT_TABLES,
} from '@app/plugins/modules/merchant-manager/core/constants/tables';
import { MERCHANT_MANAGER_URLS } from '@app/plugins/modules/merchant-manager/core/constants/urls.constants';
import {
  Payout,
  PayoutFilters,
} from '@app/plugins/modules/merchant-manager/core/models/payout.modes';
import { PayoutsFacade } from '@app/plugins/modules/merchant-manager/facade/payouts.facade';
import {
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'merchant-manager-payouts',
  templateUrl: './payouts.page.html',
  styleUrls: ['./payouts.page.scss'],
})
export class PayoutsPage extends AutoUnsubscribeOnDetroy implements OnInit {
  public payouts$: Observable<TableResponse<Payout>>;
  public isLoadingPayouts$: Observable<boolean>;
  public headers: TableHeader[];
  public movementsUrl: string;

  private _projectFamilySubscription: Subscription;

  constructor(private _payoutsFacade: PayoutsFacade) {
    super();
  }
  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangeTableFilters(filters: PayoutFilters) {
    this._payoutsFacade.updateFiltersPayouts(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._payoutsFacade.downloadPayouts();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this._payoutsFacade.initPayoutsPage();
    this.payouts$ = this._payoutsFacade.payouts$;
    this.isLoadingPayouts$ = this._payoutsFacade.isLoadingPayouts$;
    this.movementsUrl = MERCHANT_MANAGER_URLS.MOVEMENTS;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._payoutsFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getMerchantsTablePerFamily(
          projectFamily,
          MERCHANT_TABLES.PAYOUTS
        );
      });
  }
}
