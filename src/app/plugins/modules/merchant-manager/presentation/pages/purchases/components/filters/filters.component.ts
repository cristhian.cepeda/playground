import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  clearFiltersWithDateRanges,
  setFiltersWithDateRanges,
} from '@app/core/models/utils.model';
import { Purchase } from '@app/plugins/modules/merchant-manager/core/models/purchase.model';
import { PurchasesFacade } from '@app/plugins/modules/merchant-manager/facade/purchases.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'purchases-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public purchases$: Observable<TableResponse<Purchase>>;
  public isPurchasesFiltred$: Observable<boolean>;

  public form: UntypedFormGroup;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  private _controlsWithDateRanges: string[];
  private _form$: Subscription;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _purchasesFacade: PurchasesFacade
  ) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onFilter() {
    this._purchasesFacade.updateFiltersPurchases(
      setFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges),
      { isFiltred: true }
    );
  }

  public onCleanFilters() {
    this.form.reset();
    this._purchasesFacade.updateFiltersPurchases(
      clearFiltersWithDateRanges(this.form.value, this._controlsWithDateRanges)
    );
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      search: [''],
      created_at: [''],
      ticket__lte: [''],
      ticket__gte: [''],
      downpayment__lte: [''],
      downpayment__gte: [''],
    });

    this._form$ = this.form.get('search').valueChanges.subscribe((search) => {
      const controls: string[] = [
        'created_at',
        'ticket__lte',
        'ticket__gte',
        'downpayment__lte',
        'downpayment__gte',
      ];
      if (search) {
        controls.forEach((control) => {
          this.form.get(control).setValue('');
          this.form.get(control).disable();
        });
        return;
      }
      controls.forEach((control) => {
        this.form.get(control).enable();
      });
    });
  }

  private _setInitialValues() {
    this._controlsWithDateRanges = ['created_at'];
    this.purchases$ = this._purchasesFacade.purchases$;
    this.isPurchasesFiltred$ = this._purchasesFacade.isPurchasesFiltred$;
  }
}
