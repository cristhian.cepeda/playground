import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MERCHANT_MANAGER_FEATURE } from '@app/plugins/modules/merchant-manager/core/constants/feature.constants';
import { PurchasesEffects } from '@app/plugins/modules/merchant-manager/domain/store/purchases/purchases.effects';
import { PurchasesReducers } from '@app/plugins/modules/merchant-manager/domain/store/purchases/purchases.reducers';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FiltersComponent } from './components/filters/filters.component';
import { PurchasesRoutingModule } from './purchases-routing.module';
import { PurchasesPage } from './purchases.page';

@NgModule({
  declarations: [PurchasesPage, FiltersComponent],
  imports: [
    CommonModule,
    PurchasesRoutingModule,
    LayoutModule,
    StoreModule.forFeature(
      MERCHANT_MANAGER_FEATURE.PAGES.PURCHASES.STORE_NAME,
      PurchasesReducers
    ),
    EffectsModule.forFeature([PurchasesEffects]),
  ],
})
export class PurchasesModule {}
