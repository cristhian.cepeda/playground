import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  getMerchantsTablePerFamily,
  MERCHANT_TABLES,
} from '@app/plugins/modules/merchant-manager/core/constants/tables';
import {
  Purchase,
  PurchaseFilters,
} from '@app/plugins/modules/merchant-manager/core/models/purchase.model';
import { PurchasesFacade } from '@app/plugins/modules/merchant-manager/facade/purchases.facade';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'merchant-manager-purchases-page',
  templateUrl: './purchases.page.html',
  styleUrls: ['./purchases.page.scss'],
})
export class PurchasesPage extends AutoUnsubscribeOnDetroy implements OnInit {
  @ViewChild('purchaseStatusTemplate', { static: true })
  purchaseStatusTemplate: TemplateRef<ContextColumn>;

  public purchases$: Observable<TableResponse<Purchase>>;
  public isLoadingPurchases$: Observable<boolean>;
  public headers: TableHeader[];

  private _projectFamilySubscription: Subscription;

  constructor(private _purchasesFacade: PurchasesFacade) {
    super();
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  public onChangeTableFilters(filters: PurchaseFilters) {
    this._purchasesFacade.updateFiltersPurchases(filters, {
      isNativationFilter: true,
    });
  }

  public onDownload(): void {
    this._purchasesFacade.downloadPurchases();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this._purchasesFacade.initMechantsPage();
    this.purchases$ = this._purchasesFacade.purchases$;
    this.isLoadingPurchases$ = this._purchasesFacade.isLoadingPurchases$;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._purchasesFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getMerchantsTablePerFamily(
          projectFamily,
          MERCHANT_TABLES.PURCHASES
        ).map((header) => {
          if (header.dataKey == 'status')
            header.template = this.purchaseStatusTemplate;
          return header;
        });
      });
  }
}
