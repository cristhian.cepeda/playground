const NUMBER_REGEX = '[0-9]+';
const ID_REGEX = '[0-9a-z-]+';

export const MOCK_OFFERS_BY_PRODUCT_FILTER = {
  limit: NUMBER_REGEX,
  offset: NUMBER_REGEX,
  product_reference: ID_REGEX,
};
