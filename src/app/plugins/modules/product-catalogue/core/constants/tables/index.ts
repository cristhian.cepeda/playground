import { PRODUCT_FAMILY } from '@app/core/constants/product.constants';
import { DEFAULT_FAMILY } from '@app/core/i18n/constants/translate.constants';
import { TableFamilyConfig } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PRODUCT_PROFILE_OFFER_BNPL } from './tables-bnpl/product.table';
import { TABLE_PRODUCT_PROFILE_OFFER_CREDIT_CARD } from './tables-credit-card/product.table';
import { TABLE_PRODUCT_PROFILE_OFFER_DEFAULT } from './tables-default/product.table';
import { TABLE_PRODUCT_PROFILE_OFFER_MCA } from './tables-mca/product.table';
import { TABLE_PRODUCT_PROFILE_OFFER_MICRO_CREDIT } from './tables-micro-credit/product.table';

export enum PRODUCT_CATALOGUE {
  PRODUCT_PROFILE_OFFER = 'product-profile-offer',
}

export const TABLES_BY_PRODUCT_FAMILY: TableFamilyConfig = {
  [DEFAULT_FAMILY]: {
    [PRODUCT_CATALOGUE.PRODUCT_PROFILE_OFFER]:
      TABLE_PRODUCT_PROFILE_OFFER_DEFAULT,
  },
  [PRODUCT_FAMILY.BNPL]: {
    [PRODUCT_CATALOGUE.PRODUCT_PROFILE_OFFER]: TABLE_PRODUCT_PROFILE_OFFER_BNPL,
  },
  [PRODUCT_FAMILY.CREDIT_CARD]: {
    [PRODUCT_CATALOGUE.PRODUCT_PROFILE_OFFER]:
      TABLE_PRODUCT_PROFILE_OFFER_CREDIT_CARD,
  },
  [PRODUCT_FAMILY.MCA]: {
    [PRODUCT_CATALOGUE.PRODUCT_PROFILE_OFFER]: TABLE_PRODUCT_PROFILE_OFFER_MCA,
  },
  [PRODUCT_FAMILY.MICRO_CREDIT]: {
    [PRODUCT_CATALOGUE.PRODUCT_PROFILE_OFFER]:
      TABLE_PRODUCT_PROFILE_OFFER_MICRO_CREDIT,
  },
};

export const getProductCatalogueTablePerFamily = (
  family: string,
  table: PRODUCT_CATALOGUE
) => {
  return !!family
    ? TABLES_BY_PRODUCT_FAMILY[family][table]
    : TABLES_BY_PRODUCT_FAMILY[DEFAULT_FAMILY][table];
};
