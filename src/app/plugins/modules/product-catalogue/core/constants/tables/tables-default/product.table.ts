import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export const TABLE_PRODUCT_PROFILE_OFFER_DEFAULT: TableHeader[] = [
  {
    label: 'PRODUCT_CATALOGUE.PRODUCTS_PROFILE.OFFERS.TABLE.OFFER_NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'name',
  },
  {
    label: 'PRODUCT_CATALOGUE.PRODUCTS_PROFILE.OFFERS.TABLE.OFFER_REFERENCE',
    size: 4,
    typeColumn: TYPE_COLUMN.LABEL,
    dataKey: 'reference',
  },
  {
    label: 'PRODUCT_CATALOGUE.PRODUCTS_PROFILE.OFFERS.TABLE.CREATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'created_at',
  },
  {
    label: 'PRODUCT_CATALOGUE.PRODUCTS_PROFILE.OFFERS.TABLE.UPDATED_AT',
    size: 4,
    typeColumn: TYPE_COLUMN.DATE,
    dataKey: 'updated_at',
  },
  {
    label: 'PRODUCT_CATALOGUE.PRODUCTS_PROFILE.OFFERS.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    dataKey: 'status',
    template: null,
  },
];
