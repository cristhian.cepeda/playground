import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { TABLE_PRODUCT_PROFILE_OFFER_DEFAULT } from '../tables-default/product.table';

export const TABLE_PRODUCT_PROFILE_OFFER_MICRO_CREDIT: TableHeader[] =
  TABLE_PRODUCT_PROFILE_OFFER_DEFAULT;
