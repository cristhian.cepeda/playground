import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const PRODUCT_CATALOGUE_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Product Catalogue',
  _MENU: {
    PRODUCTS_AND_OFFERS: {
      TITLE: 'Products and offers',
      TOOLTIP_MESSAGE:
        'View all configured products and offers in your credit operation.',
    },
  },
};
export default PRODUCT_CATALOGUE_FEATURE_DETAILS_TRANSLATE;
