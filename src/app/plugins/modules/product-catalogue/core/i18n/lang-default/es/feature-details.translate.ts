import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';

const PRODUCT_CATALOGUE_FEATURE_DETAILS_TRANSLATE: FeatureDetailsTranslate = {
  _NAME: 'Catalogo de producto',
  _MENU: {
    PRODUCTS_AND_OFFERS: {
      TITLE: 'Productos y ofertas',
      TOOLTIP_MESSAGE:
        'Visualiza todos los productos y ofertas configurados en tu operación de crédito.',
    },
  },
};
export default PRODUCT_CATALOGUE_FEATURE_DETAILS_TRANSLATE;
