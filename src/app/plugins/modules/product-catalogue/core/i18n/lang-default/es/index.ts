import { FeatureDetailsTranslate } from '@app/core/i18n/models/app/feature-details-translate';
import PRODUCT_CATALOGUE_FEATURE_DETAILS_TRANSLATE from './feature-details.translate';
import PRODUCTS_AND_OFFERS_TRANSLATE from './products_and_offers.translate';

const SPANISH_DEFAULT_PRODUCT_CATALOGUE: FeatureDetailsTranslate = {
  ...PRODUCT_CATALOGUE_FEATURE_DETAILS_TRANSLATE,
  ...PRODUCTS_AND_OFFERS_TRANSLATE,
};
export default SPANISH_DEFAULT_PRODUCT_CATALOGUE;
