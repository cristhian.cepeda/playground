import ENGLISH_DEFAULT_PRODUCT_CATALOGUE from '../../lang-default/en';

const ENGLISH_MICRO_CREDIT_PRODUCT_CATALOGUE = {
  ...ENGLISH_DEFAULT_PRODUCT_CATALOGUE,
};
export default ENGLISH_MICRO_CREDIT_PRODUCT_CATALOGUE;
