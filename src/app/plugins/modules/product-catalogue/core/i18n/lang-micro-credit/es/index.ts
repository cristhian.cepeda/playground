import SPANISH_DEFAULT_PRODUCT_CATALOGUE from '../../lang-default/es';

const SPANISH_MICRO_CREDIT_PRODUCT_CATALOGUE = {
  ...SPANISH_DEFAULT_PRODUCT_CATALOGUE,
};
export default SPANISH_MICRO_CREDIT_PRODUCT_CATALOGUE;
