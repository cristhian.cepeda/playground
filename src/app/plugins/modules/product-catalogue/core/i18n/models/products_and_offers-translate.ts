export interface ProductsAndOffersPageTranslate {
  PRODUCTS_AND_OFFERS: ProductsAndOffersTranslate;
  PRODUCTS_PROFILE: ProductsProfileTranslate;
  OFFER_PROFILE: OfferProfileTranslate;
  OFFER_OPTIONS: OfferOptionsTranslate;
}
export interface ProductsAndOffersTranslate {
  TITLE: string;
  DESCRIPTION: string;
  PRODUCTS_COUNTER: string;
  OFFERS_COUNTER: string;
  PRODUCTS: ProductsAndOffersMapTranslate;
}

export interface ProductsAndOffersMapTranslate {
  TITLE: string;
  DESCRIPTION: string;
  BTN_CREATE_NEW_PRODUCT: string;
  PRODUCT_CARD: ProductsCardTranslate;
}
export interface ProductsCardTranslate {
  EMPTY: string;
  OFFERS_COUNTER: string;
  PRODUCT_ID: string;
  BTN_VIEW_MORE: string;
  BTN_CREATE_NEW_OFFER: string;
  BTN_VIEW_PRODUCT_DETAILS: string;
}

export interface ProductsProfileTranslate {
  TITLE: string;
  DESCRIPTION: string;
  INFO: ProductsProfileInfoTranslate;
  OFFERS: ProductsProfileOffersTranslate;
  DETAILS: ProductsProfileDetailsTranslate;
}
export interface ProductsProfileInfoTranslate {
  PRODUCT_ID: string;
  OFFERS_COUNTER: string;
  PRODUCTS: string;
  CREATED_AT: string;
  UPDATED_AT: string;
}

export interface ProductsProfileOffersTranslate {
  TITLE: string;
  DESCRIPTION: string;
  EMPTY: string;
  BTN_CREATE_NEW_OFFER: string;
  TABLE: ProductsProfileOffersTableTranslate;
}

export interface ProductsProfileOffersTableTranslate {
  OFFER_NAME: string;
  OFFER_REFERENCE: string;
  CREATED_AT: string;
  UPDATED_AT: string;
  STATUS: string;
}

export interface ProductsProfileDetailsTranslate {
  TITLE: string;
  MAXIMUM_NUMBER_OF_ACTIVE_LOANS: string;
  LOAN_AMOUNTS: string;
  CLOSING_TOLERANCE: string;
  DECIMALS: string;
  AMOUNT_REQUESTED_STEP: string;
  NEAREST_MULTIPLE_OF: string;
}

export interface OfferProfileTranslate {
  TITLE: string;
  DESCRIPTION: string;
  INFO: OfferProfileInfoTranslate;
  GENERAL_SETTINGS: OfferProfileGeneralSettingsTranslate;
  REPAYMENT_LOGIC: OfferProfileRepaymentLogicTranslate;
  CHARGES: OfferProfileChargesTranslate;
}
export interface OfferProfileInfoTranslate {
  OFFER_ID: string;
  CREATED_AT: string;
  UPDATED_AT: string;
}

export interface OfferProfileGeneralSettingsTranslate {
  TITLE: string;
  MAXIMUM_ACTIVE_CREDITS: string;
  DOWNPAYMENT: string;
}

export interface OfferProfileRepaymentLogicTranslate {
  TITLE: string;
  TYPE: string;
  PERIODICITY: string;
  CALENDAR_LOGIC: string;
  FIXED_OR_RELATIVE: string;
  FIXED_OR_RELATIVE_DAY_OF_WEEK: string;
  FIXED_OR_RELATIVE_DAY_OF_MONTH: string;
  NUMBER_OF_INSTALLMENTS: string;
  TERM_OF_CREDIT: string;
  TYPE_OF_INSTALLMENT: string;
  MISSED_REPAYMENT_LOGIC: string;
  GRACE_PERIOD: string;
  GRACE_PERIOD_NUMBER_OF_DAYS: string;
}

export interface OfferProfileChargesTranslate {
  TITLE: string;
  CAPITAL: OfferProfileCapitalTranslate;
  INTEREST: OfferProfileChargesInterestTranslate;
  COSTS: OfferProfileChargesCostTranslate;
  COLLECTION_COSTS: OfferProfileChargesCollectionCostTranslate;
  DEFAULT_INTEREST: OfferProfileChargesDefaultInterestTranslate;
}

export interface OfferProfileCapitalTranslate {
  TITLE: string;
  DOWN_PAYMENT: string;
}
export interface OfferProfileChargesInterestTranslate {
  TITLE: string;
  VALUE: string;
  BASE: string;
  TAX: string;
  TAX_FEE: string;
  PERIODICITY: string;
  PERIODICITY_DAY_OF_WEEK: string;
  PERIODICITY_DAY_OF_MONTH: string;
  GRACE_PERIOD: string;
  GRACE_PERIOD_NUMBER_OF_DAYS: string;
}

export interface OfferProfileChargesCostTranslate {
  TITLE: string;
  PLATFORM: string;
  ADMINISTRATIVE: string;
  FIXED_OR_PERCENT: string;
  FIXED_OR_PERCENT_PERCENTAGE: string;
  TAX: string;
  TAX_FEE: string;
}
export interface OfferProfileChargesCollectionCostTranslate {
  TITLE: string;
  TRIGGER: string;
  GRACE_PERIOD: string;
  GRACE_PERIOD_TIME_UNIT: string;
  GRACE_PERIOD_PERCENTAGE: string;
  FIXED_OR_PERCENT: string;
  FIXED_OR_PERCENT_PERCENTAGE: string;
  FIXED_OR_PERCENT_BASE: string;
  TAX: string;
  TAX_FEE: string;
}

export interface OfferProfileChargesDefaultInterestTranslate {
  TITLE: string;
  TRIGGER: string;
  INTEREST_VALUE: string;
  BASE: string;
  TAX: string;
  TAX_FEE: string;
  PERIODICITY: string;
  PERIODICITY_DAY_OF_WEEK: string;
  PERIODICITY_DAY_OF_MONTH: string;
  FIXED_OR_RELATIVE: string;
  GRACE_PERIOD: string;
  GRACE_PERIOD_NUMBER_OF_DAYS: string;
}
export interface OfferOptionsTranslate {
  FIXED: string;
  RELATIVE: string;
  PERCENT: string;
}
