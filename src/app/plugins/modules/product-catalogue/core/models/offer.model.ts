import { Filters } from '@app/core/models/filters.model';
import { CHARGE_TYPE, CHARGE_VALUE_TYPE } from './charge.model';

export interface Offer {
  name?: string;
  status?: OFFER_STATUS;
  reference?: string;
  product_id?: string;
  product_reference?: string;
  product_name?: string;
  downpayment?: string;
  number_of_installments?: string;
  offer_value?: string;
  id?: string;
  is_new?: boolean;
  minimum_loan_amount?: string;
  maximum_loan_amount?: string;
  maximum_number_active_loans?: number;
  created_at?: Date;
  updated_at?: Date;
  // TODO VALIDATE
  action?: OFFER_ACTION;
}

export interface OffersFilters extends Filters {
  product_reference?: string;
  amount?: string;
  installments?: string;
  merchant_group_id?: string;
}

export interface OfferRepaymentLogic {
  id: string;
  reference: string;
  repayment_type: string;
  repayment_frequency_type: OFFER_FREQUENCY_TYPE;
  repayment_frequency_day_type: string;
  repayment_frequency_day: number;
  repayment_frequency_week_day: string;
  installments_number: number;
  loan_term_value: string;
  loan_term_periodicity: OFFER_FREQUENCY_TYPE;
  repayment_installment_type: string;
  repayment_missed_payment_logic: string;
  repayment_grace_days: number;
}

export interface OfferCharge {
  id: string;
  reference: string;
  name: string;
  type: CHARGE_TYPE;
  priority: number;
  value_detail_value: string;
  value_detail_type: CHARGE_VALUE_TYPE;
  value_detail_base: string;
  tax_value: string;
  tax_type: CHARGE_VALUE_TYPE;
  tax_name: string;
  grace_period_frequency_type: OFFER_FREQUENCY_TYPE;
  grace_period_frequency_week_day: string;
  installment_frequency_type: string;
  installment_frequency_week_day: string;
  installment_frequency_day: number;
  grace_period_frequency_quantity: number;
}

export enum OFFER_STATUS {
  DRAFT = 'draft',
  ENABLED = 'enabled',
  DISABLED = 'disabled',
}

export enum OFFER_STATUS_ICONS {
  DRAFT = 'uil-edit',
  ENABLED = 'uil-wifi',
  DISABLED = 'uil-eye-slash',
}

export enum OFFER_ACTION {
  IN_USE = 'in-use',
  INACTIVE = 'inactive',
  IDLE = 'idle',
}

export enum OFFER_OPTION {
  FIXED = 'Fixed',
  RELATIVE = 'Relative',
  PERCENT = 'Percent',
}

export enum OFFER_FREQUENCY_TYPE {
  DAILY = 'daily',
  WEEKLY = 'weekly',
  MONTHLY = 'monthly',
  UNIQUE = 'unique',
}

export interface OfferSpecs {
  generalSettings?: OfferGeneralSettings;
  repaymentLogic?: DemoOfferRepaymentLogic;
  charges?: OfferCharges;
}

export interface OfferGeneralSettings {
  name?: string;
  description?: string;
  max_active_credits?: number;
  limitless?: string;
  file?: File;
  file_name?: string;
}
export interface DemoOfferRepaymentLogic {
  repayment_type?: string;
  periodicity?: string;
  calendar_logic?: string;
  fixed_or_relative?: string;
  day_of_week?: string;
  number_of_installments?: number;
  type_of_installment?: string;
  missed_repayment_logic?: string;
  grace_period?: string;
  grace_period_days?: number;
}

export type OfferChargeType =
  | 'interest'
  | 'cost'
  | 'collectionCost'
  | 'defaultInterest'
  | 'capital';

export interface OfferCharges {
  capital?: OfferCapitalCharge;
  interest?: OfferInterestCharge;
  cost?: OfferCostCharge;
  collectionCost?: OfferCollectionCostCharge;
  defaultInterest?: OfferDefaultInterestCharge;
}

export interface OfferCapitalCharge {
  name?: string;
  priorization?: number;
}

export interface OfferInterestCharge {
  name?: string;
  interest?: string;
  type_of_interest?: string;
  tax?: string;
  tax_fee?: number;
  periodicity?: string;
  fixed_or_relative?: string;
  grace_period?: string;
  grace_period_days?: number;
  priorization?: number;
}
export interface OfferCostCharge {
  type_of_cost?: string;
  name?: string;
  description?: string;
  fixed_or_percent?: string;
  fixed_or_percent_value?: number;
  tax?: string;
  tax_fee?: number;
  priorization?: number;
}
export interface OfferCollectionCostCharge {
  name?: string;
  trigger?: string;
  grace_period?: string;
  grace_period_time_unit?: string;
  grace_period_time_number?: number;
  fixed_or_percent?: string;
  fixed_or_percent_value?: number;
  base_definition?: string;
  tax?: string;
  tax_fee?: number;
  priorization?: number;
}
export interface OfferDefaultInterestCharge {
  name?: string;
  trigger?: string;
  interest?: string;
  type_of_interest?: string;
  tax?: string;
  tax_fee?: number;
  periodicity?: string;
  fixed_or_relative?: string;
  grace_period?: string;
  grace_period_days?: number;
  priorization?: number;
}
