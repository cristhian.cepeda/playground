import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FILTERS } from '@app/core/constants/filters.constant';
import {
  Offer,
  OfferCharge,
  OfferRepaymentLogic,
  OffersFilters,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class OfferApi {
  constructor(private _http: HttpClient) {}

  public getOffers(
    filters?: OffersFilters
  ): Observable<TableResponse<Offer> | Offer[]> {
    let params = new HttpParams();
    const filtersParam = filters ?? FILTERS;
    params = params.appendAll({ ...filtersParam });
    return this._http.get<TableResponse<Offer> | Offer[]>(
      API_URLS.PRODUCTS_AND_OFFERS.GET_OFFERS,
      { params }
    );
  }

  public getOffer(reference: string): Observable<Offer> {
    return this._http.get<Offer>(
      API_URLS.PRODUCTS_AND_OFFERS.GET_OFFER.replace(':id', reference)
    );
  }

  public getOfferRepaymentLogic(
    reference: string
  ): Observable<OfferRepaymentLogic> {
    return this._http.get<OfferRepaymentLogic>(
      API_URLS.PRODUCTS_AND_OFFERS.GET_OFFER_REPAYMENTS.replace(
        ':id',
        reference
      )
    );
  }

  public getOfferCharges(reference: string): Observable<OfferCharge[]> {
    return this._http.get<OfferCharge[]>(
      API_URLS.PRODUCTS_AND_OFFERS.GET_OFFER_CONDITIONS.replace(
        ':id',
        reference
      )
    );
  }
}
