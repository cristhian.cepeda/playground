import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { Observable } from 'rxjs';
import { API_URLS } from '../constants/api.constants';

@Injectable({
  providedIn: 'root',
})
export class ProductApi {
  constructor(private _http: HttpClient) {}

  public getProducts(): Observable<Product[]> {
    return this._http.get<Product[]>(API_URLS.PRODUCTS_AND_OFFERS.GET_PRODUCTS);
  }

  public getProduct(productId: string): Observable<Product> {
    return this._http.get<Product>(
      API_URLS.PRODUCTS_AND_OFFERS.GET_PRODUCT.replace(':id', productId)
    );
  }
}
