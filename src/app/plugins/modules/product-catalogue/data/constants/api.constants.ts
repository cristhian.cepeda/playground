export const API_URLS = {
  PRODUCTS_AND_OFFERS: {
    GET_PRODUCTS: '/products/products-offers',
    GET_PRODUCT: '/products/:id',
    GET_OFFERS: '/offers',
    GET_OFFER: '/offers/:id',
    GET_OFFER_REPAYMENTS: '/offers/:id/repayments',
    GET_OFFER_CONDITIONS: '/offers/:id/conditions',
  },
};
