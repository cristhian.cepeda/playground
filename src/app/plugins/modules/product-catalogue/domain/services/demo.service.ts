import { Injectable } from '@angular/core';
import { STORAGE } from '@app/data/constants/storage.constants';
import { StorageService } from '@app/data/storage/storage.service';
import {
  OfferCharges,
  OfferChargeType,
  OfferSpecs,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DemoService {
  constructor(private _storageService: StorageService) {}

  public getCreateNewOfferData(): Observable<OfferSpecs> {
    return this._storageService.getItem(STORAGE.CREATE_NEW_OFFER);
  }

  public cleanOfferData() {
    return this._storageService.removeItem(STORAGE.CREATE_NEW_OFFER);
  }

  public updatePrioritizedCharges(charges: OfferCharges) {
    this.getCreateNewOfferData().subscribe((offerData) => {
      const offerSpecs: OfferSpecs = {
        charges,
      };
      offerData = { ...offerData, ...offerSpecs };
      this._storageService.setItem(STORAGE.CREATE_NEW_OFFER, offerData);
    });
  }

  public updateCreateNewOfferData(offerSpecsData: OfferSpecs) {
    this.getCreateNewOfferData().subscribe((offerData) => {
      const offerSpecs: OfferSpecs = {
        ...offerSpecsData,
        charges: {
          ...offerSpecsData?.charges,
          ...offerData?.charges,
        },
      };
      offerData = { ...offerData, ...offerSpecs };
      this._storageService.setItem(STORAGE.CREATE_NEW_OFFER, offerData);
    });
  }

  public removeOfferChargesByType(chargeType: OfferChargeType) {
    this.getCreateNewOfferData().subscribe((offerData) => {
      const charges: OfferCharges = offerData?.charges;
      delete charges[chargeType];
      const offerSpecs: OfferSpecs = {
        charges: {
          ...offerData?.charges,
        },
      };
      offerData = { ...offerData, ...offerSpecs };
      this._storageService.setItem(STORAGE.CREATE_NEW_OFFER, offerData);
    });
  }
}
