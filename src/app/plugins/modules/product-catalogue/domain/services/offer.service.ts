import { Injectable } from '@angular/core';
import {
  SummaryDropItem,
  SummaryGroup,
} from '@app/core/models/summary-group.model';
import { IKeyValue } from '@app/core/models/utils.model';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { SnakeToCapitalPipe } from '@app/presentation/layout/pipes/snake-to-capital.pipe';
import { TranslateService } from '@ngx-translate/core';
import { map, Observable } from 'rxjs';
import {
  OfferProfileChargesTranslate,
  OfferProfileGeneralSettingsTranslate,
  OfferProfileRepaymentLogicTranslate,
} from '../../core/i18n/models/products_and_offers-translate';
import { CHARGE_TYPE, CHARGE_VALUE_TYPE } from '../../core/models/charge.model';
import {
  Offer,
  OfferCharge,
  OfferRepaymentLogic,
  OFFER_FREQUENCY_TYPE,
  OFFER_OPTION,
} from '../../core/models/offer.model';

@Injectable({
  providedIn: 'root',
})
export class OfferService {
  constructor(
    private _translateService: TranslateService,
    private _currency: CustomCurrencyPipe,
    private _skaneToCapital: SnakeToCapitalPipe
  ) {}

  public getOfferProfileGeneralSettingsItems(
    offer: Offer
  ): Observable<SummaryGroup[]> {
    return this._translateService
      .get('PRODUCT_CATALOGUE.OFFER_PROFILE.GENERAL_SETTINGS')
      .pipe(
        map((GENERAL_SETTINGS: OfferProfileGeneralSettingsTranslate) => {
          return [
            {
              key: GENERAL_SETTINGS.MAXIMUM_ACTIVE_CREDITS,
              value: offer?.maximum_number_active_loans,
            },
            {
              key: GENERAL_SETTINGS.DOWNPAYMENT,
              value: `${parseFloat(offer?.downpayment)}%`,
            },
          ].filter((generalSettings) => generalSettings?.value);
        })
      );
  }

  public getOfferProfileRepaymentLogic(
    offerRepaymentLogic: OfferRepaymentLogic
  ): Observable<SummaryGroup[]> {
    return this._translateService
      .get('PRODUCT_CATALOGUE.OFFER_PROFILE.REPAYMENT_LOGIC')
      .pipe(
        map((REPLAYMENT_LOGIC: OfferProfileRepaymentLogicTranslate) => {
          return [
            {
              key: REPLAYMENT_LOGIC.TYPE,
              value: this._skaneToCapital.transform(
                offerRepaymentLogic?.repayment_type
              ),
            },
            {
              key: REPLAYMENT_LOGIC.PERIODICITY,
              value: this._skaneToCapital.transform(
                offerRepaymentLogic?.repayment_frequency_type
              ),
            },
            {
              key: REPLAYMENT_LOGIC.CALENDAR_LOGIC,
              value: this._skaneToCapital.transform(
                offerRepaymentLogic?.repayment_frequency_day_type
              ),
            },
            ...this._getOfferProfileRepaymentLogicFixedOrRelative(
              REPLAYMENT_LOGIC,
              offerRepaymentLogic
            ),
            {
              key: REPLAYMENT_LOGIC.NUMBER_OF_INSTALLMENTS,
              value: offerRepaymentLogic?.installments_number,
            },
            {
              key: REPLAYMENT_LOGIC.TERM_OF_CREDIT,
              value: `${parseFloat(
                offerRepaymentLogic?.loan_term_value
              )} ${this._getPeriodicityLabelByFrequencyType(
                offerRepaymentLogic?.loan_term_periodicity
              ).toLowerCase()}`,
            },
            {
              key: REPLAYMENT_LOGIC.TYPE_OF_INSTALLMENT,
              value: this._skaneToCapital.transform(
                offerRepaymentLogic?.repayment_installment_type
              ),
            },
            {
              key: REPLAYMENT_LOGIC.MISSED_REPAYMENT_LOGIC,
              value: this._skaneToCapital.transform(
                offerRepaymentLogic?.repayment_missed_payment_logic
              ),
            },
            ...this._getOfferProfileGracePeriod(
              REPLAYMENT_LOGIC.GRACE_PERIOD,
              offerRepaymentLogic?.repayment_grace_days
            ),
          ].filter((repaymentLogic) => repaymentLogic?.value);
        })
      );
  }

  public getOfferProfileChargesItems(
    offerCharges: OfferCharge[]
  ): Observable<SummaryDropItem[]> {
    return this._translateService
      .get('PRODUCT_CATALOGUE.OFFER_PROFILE.CHARGES')
      .pipe(
        map((CHARGES: OfferProfileChargesTranslate) => {
          const charges: SummaryDropItem[] = offerCharges.map<SummaryDropItem>(
            (charge) => {
              const mapPerChargeType: { [key: string]: Function } = {
                [CHARGE_TYPE.COLLECTION_COST]: () =>
                  this._getOfferProfileCollectionCostCharge(CHARGES, charge),
                [CHARGE_TYPE.COST]: () =>
                  this._getOfferProfileCostCharge(CHARGES, charge),
                [CHARGE_TYPE.DEFAULT_INTEREST]: () =>
                  this._getOfferProfileDefaultInterestCharge(CHARGES, charge),
                [CHARGE_TYPE.INTEREST]: () =>
                  this._getOfferProfileInterestCharge(CHARGES, charge),
              };

              return mapPerChargeType[charge?.type]
                ? mapPerChargeType[charge?.type]()
                : () => {};
            }
          );
          return charges.filter(
            (charges) => charges && charges?.items && charges?.items?.length > 0
          );
        })
      );
  }

  // ****************************************
  // CHARGES IN ORDER OF COLLECTION PRIORITY
  // ****************************************

  private _getOfferProfileCollectionCostCharge(
    CHARGES: OfferProfileChargesTranslate,
    offerCharge: OfferCharge
  ): SummaryDropItem {
    return {
      title: this._skaneToCapital.transform(offerCharge?.name),
      items: [
        ...this._getOfferProfileGracePeriod(
          CHARGES.COLLECTION_COSTS.GRACE_PERIOD,
          offerCharge?.grace_period_frequency_quantity,
          offerCharge?.grace_period_frequency_type
        ),
        ...this._getOfferProfileFixedOrPercent(
          CHARGES.COLLECTION_COSTS.FIXED_OR_PERCENT,
          offerCharge?.value_detail_type,
          offerCharge?.value_detail_value,
          CHARGES.COLLECTION_COSTS.FIXED_OR_PERCENT_BASE,
          offerCharge?.value_detail_base
        ),
        ...this._getOfferProfileFixedOrPercentageValue(
          CHARGES.COLLECTION_COSTS.TAX,
          offerCharge?.tax_type,
          offerCharge?.tax_value
        ),
      ],
    };
  }
  private _getOfferProfileCostCharge(
    CHARGES: OfferProfileChargesTranslate,
    offerCharge: OfferCharge
  ): SummaryDropItem {
    return {
      title: this._skaneToCapital.transform(offerCharge?.name),
      items: [
        ...this._getOfferProfileFixedOrPercent(
          CHARGES.COLLECTION_COSTS.FIXED_OR_PERCENT,
          offerCharge?.value_detail_type,
          offerCharge?.value_detail_value,
          CHARGES.COLLECTION_COSTS.FIXED_OR_PERCENT_BASE,
          offerCharge?.value_detail_base
        ),
        ...this._getOfferProfileFixedOrPercentageValue(
          CHARGES.COLLECTION_COSTS.TAX,
          offerCharge?.tax_type,
          offerCharge?.tax_value
        ),
      ],
    };
  }
  private _getOfferProfileDefaultInterestCharge(
    CHARGES: OfferProfileChargesTranslate,
    offerCharge: OfferCharge
  ): SummaryDropItem {
    return {
      title: this._skaneToCapital.transform(offerCharge?.name),
      items: [
        ...this._getOfferProfileFixedOrPercentageValue(
          CHARGES.DEFAULT_INTEREST.INTEREST_VALUE,
          offerCharge?.value_detail_type,
          offerCharge?.value_detail_value
        ),
        {
          key: CHARGES.DEFAULT_INTEREST.BASE,
          value: this._skaneToCapital.transform(offerCharge?.value_detail_base),
        },
        ...this._getOfferProfileFixedOrPercentageValue(
          CHARGES.DEFAULT_INTEREST.TAX,
          offerCharge?.tax_type,
          offerCharge?.tax_value
        ),
        ...this._getOfferProfileChargePeriodicity(
          offerCharge,
          CHARGES.DEFAULT_INTEREST.PERIODICITY,
          CHARGES.DEFAULT_INTEREST.PERIODICITY_DAY_OF_WEEK,
          CHARGES.DEFAULT_INTEREST.PERIODICITY_DAY_OF_MONTH
        ),
        ...this._getOfferProfileChargeFixedOrRelative(CHARGES, offerCharge),
        ...this._getOfferProfileGracePeriod(
          CHARGES.DEFAULT_INTEREST.GRACE_PERIOD,
          offerCharge?.grace_period_frequency_quantity,
          offerCharge?.grace_period_frequency_type
        ),
      ],
    };
  }

  private _getOfferProfileInterestCharge(
    CHARGES: OfferProfileChargesTranslate,
    offerCharge: OfferCharge
  ): SummaryDropItem {
    return {
      title: this._skaneToCapital.transform(offerCharge?.name),
      items: [
        ...this._getOfferProfileFixedOrPercentageValue(
          CHARGES.INTEREST.VALUE,
          offerCharge?.value_detail_type,
          offerCharge?.value_detail_value
        ),
        {
          key: CHARGES.INTEREST.BASE,
          value: this._skaneToCapital.transform(offerCharge?.value_detail_base),
        },
        ...this._getOfferProfileFixedOrPercentageValue(
          CHARGES.INTEREST.TAX,
          offerCharge?.tax_type,
          offerCharge?.tax_value
        ),
        ...this._getOfferProfileChargePeriodicity(
          offerCharge,
          CHARGES.INTEREST.PERIODICITY,
          CHARGES.INTEREST.PERIODICITY_DAY_OF_WEEK,
          CHARGES.INTEREST.PERIODICITY_DAY_OF_MONTH
        ),
        ...this._getOfferProfileGracePeriod(
          CHARGES.INTEREST.GRACE_PERIOD,
          offerCharge?.grace_period_frequency_quantity,
          offerCharge?.grace_period_frequency_type
        ),
      ],
    };
  }

  private _getOfferProfileChargeFixedOrRelative(
    CHARGES: OfferProfileChargesTranslate,
    offerCharge: OfferCharge
  ): SummaryGroup[] {
    let value: string = OFFER_OPTION.FIXED;
    if (
      offerCharge?.installment_frequency_type == OFFER_FREQUENCY_TYPE.DAILY ||
      (offerCharge?.installment_frequency_type == OFFER_FREQUENCY_TYPE.WEEKLY &&
        !offerCharge?.installment_frequency_week_day) ||
      (offerCharge?.installment_frequency_type ==
        OFFER_FREQUENCY_TYPE.MONTHLY &&
        !offerCharge?.installment_frequency_day)
    )
      value = OFFER_OPTION.RELATIVE;

    return [
      {
        key: CHARGES.DEFAULT_INTEREST.FIXED_OR_RELATIVE,
        value,
      },
    ];
  }

  private _getOfferProfileChargePeriodicity(
    offerCharge: OfferCharge,
    title: string,
    weekDayTitle: string,
    monthDayTitle: string
  ): SummaryGroup[] {
    return [
      {
        key: title,
        value: this._skaneToCapital.transform(
          offerCharge?.grace_period_frequency_type
        ),
        extraData: this._getFrequencyType(
          offerCharge?.grace_period_frequency_quantity,
          offerCharge?.grace_period_frequency_type,
          offerCharge?.grace_period_frequency_week_day,
          weekDayTitle,
          monthDayTitle
        ),
      },
    ];
  }

  // ********************
  // REPAYMENT LOGIC
  // ********************
  private _getOfferProfileRepaymentLogicFixedOrRelative(
    REPLAYMENT_LOGIC: OfferProfileRepaymentLogicTranslate,
    offerRepaymentLogic: OfferRepaymentLogic
  ): SummaryGroup[] {
    if (!offerRepaymentLogic?.repayment_frequency_day)
      return [
        {
          key: REPLAYMENT_LOGIC.FIXED_OR_RELATIVE,
          value: OFFER_OPTION.RELATIVE,
        },
      ];

    return [
      {
        key: REPLAYMENT_LOGIC.FIXED_OR_RELATIVE,
        value: OFFER_OPTION.FIXED,
        extraData: this._getFrequencyType(
          offerRepaymentLogic?.repayment_frequency_day,
          offerRepaymentLogic?.repayment_frequency_type,
          offerRepaymentLogic?.repayment_frequency_week_day,
          REPLAYMENT_LOGIC.FIXED_OR_RELATIVE_DAY_OF_WEEK,
          REPLAYMENT_LOGIC.FIXED_OR_RELATIVE_DAY_OF_MONTH
        ),
      },
    ];
  }

  // ********************
  // GLOBAL
  // ********************

  private _getOfferProfileGracePeriod(
    title: string,
    period: number,
    frequencyType?: OFFER_FREQUENCY_TYPE
  ): SummaryGroup[] {
    const hasGracePeriod: boolean = period && period > 0;
    return hasGracePeriod
      ? [
          {
            key: title,
            value: `${period} 
            ${this._getPeriodicityLabelByFrequencyType(frequencyType)}
         `,
          },
        ]
      : [];
  }

  private _getOfferProfileFixedOrPercent(
    title: string,
    type: CHARGE_VALUE_TYPE,
    value: string,
    baseTitle?: string,
    baseValue?: string
  ): SummaryGroup[] {
    return [
      {
        key: title,
        value:
          type == CHARGE_VALUE_TYPE.PERCENTAGE
            ? `${parseFloat(value)}%`
            : this._currency.transform(value),
        extraData:
          baseTitle && baseValue
            ? [
                {
                  key: baseTitle,
                  value: this._skaneToCapital.transform(baseValue),
                },
              ]
            : [],
      },
    ];
  }

  private _getFrequencyType(
    day: number,
    type: OFFER_FREQUENCY_TYPE,
    weekDay: string,
    weekDayTitle: string,
    monthDayTitle: string
  ): IKeyValue[] {
    const repaymentFrequencyType: { [key: string]: IKeyValue[] } = {
      [OFFER_FREQUENCY_TYPE.WEEKLY]: [
        {
          key: weekDayTitle,
          value: this._translateService.instant(
            `APP.DATES.DATE_OF_WEEK.${weekDay?.toUpperCase()}`
          ),
        },
      ],
      [OFFER_FREQUENCY_TYPE.MONTHLY]: [
        {
          key: monthDayTitle,
          value: String(day),
        },
      ],
    };
    return repaymentFrequencyType[type] ?? [];
  }

  private _getOfferProfileFixedOrPercentageValue(
    title: string,
    type: CHARGE_VALUE_TYPE,
    value: string
  ): SummaryGroup[] {
    return value
      ? [
          {
            key: title,
            value:
              type == CHARGE_VALUE_TYPE.PERCENTAGE
                ? `${parseFloat(value)}%`
                : this._currency.transform(value),
          },
        ]
      : [];
  }

  private _getPeriodicityLabelByFrequencyType(
    frequencyType: OFFER_FREQUENCY_TYPE
  ): string {
    const FREQUENCY_TYPE: { [key: string]: string } = {
      [OFFER_FREQUENCY_TYPE.DAILY]:
        this._translateService.instant('APP.DATES.DAYS'),
      [OFFER_FREQUENCY_TYPE.WEEKLY]:
        this._translateService.instant('APP.DATES.WEEKS'),
      [OFFER_FREQUENCY_TYPE.MONTHLY]:
        this._translateService.instant('APP.DATES.MONTHS'),
    };
    return (
      FREQUENCY_TYPE[frequencyType] ??
      this._translateService.instant('APP.DATES.DAYS')
    );
  }
}
