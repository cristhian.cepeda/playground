import { TestBed } from '@angular/core/testing';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { Offer } from '../../core/models/offer.model';
import { Product } from '../../core/models/product.model';
import { PRODUCTS } from '../../data/mocks/products.mock';
import { ProductsService } from './products.service';

describe('ProductsService', () => {
  let service: ProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [CustomCurrencyPipe],
    });
    service = TestBed.inject(ProductsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('generateProductsTemplates', () => {
    it('should be return an array of products', () => {
      // Arrange
      const products = PRODUCTS;
      // Act
      const resultProducts: Product[] =
        service.generateProductsTemplates(products);
      // Assert
      expect(resultProducts[0].style).toBeDefined();
    });
  });

  describe('updateProductsOffers', () => {
    it('should be return an array of products', () => {
      // Arrange
      const products: Product[] = PRODUCTS;
      const productReference: string = PRODUCTS[0].reference;
      const offers: Offer[] = PRODUCTS[0].offers;
      // Act

      const resultProducts: Product[] = service.updateProductsOffers(
        products,
        productReference,
        offers
      );
      // Assert
      expect(resultProducts[0].offersDisplayed).toBeTrue();
    });
  });
});
