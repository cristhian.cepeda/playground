import { Injectable } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { deepCopy } from '@app/core/models/utils.model';
import { Offer } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import {
  CLOSING_TOLERANCE_LABEL,
  Product,
  ProductCardStyle,
  ProductsAndOfferCounter,
  ROUND_WAY_LABEL_BY_VALUE,
} from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { OFFER_CARD_TEMPLATE } from '@app/plugins/modules/product-catalogue/presentation/pages/products-and-offers/products-and-offers/enums/offer-card-enum';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { TranslateService } from '@ngx-translate/core';
import { map, Observable } from 'rxjs';
import { ProductsProfileDetailsTranslate } from '../../core/i18n/models/products_and_offers-translate';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  public productCardStyles: ProductCardStyle[];

  constructor(
    private _customCurrencyPipe: CustomCurrencyPipe,
    private _translateService: TranslateService
  ) {
    this._setProductCardStyles();
  }

  public generateProductsTemplates(products: Product[]): Product[] {
    const iter = this._getProductCardStyleIterator();
    const productsCopy = deepCopy(products);
    const productsWithStyle: Product[] = productsCopy.map((product) => {
      product.style = iter.next().value;
      return product;
    });
    return productsWithStyle;
  }

  public getProductsAndOffersCounter(
    products: Product[]
  ): ProductsAndOfferCounter {
    let productsCounter: number = 0;
    let offersCounter: number = 0;
    productsCounter = products?.length;
    products?.forEach((product) => (offersCounter += product.number_of_offers));
    return { productsCounter, offersCounter };
  }

  public updateProductsOffers(
    products: Product[],
    productReference: string,
    offers: Offer[]
  ): Product[] {
    const productsUpdate = deepCopy(products);
    const updatedProducts: Product[] = productsUpdate.map(
      (product: Product) => {
        if (product.reference === productReference && offers) {
          product.offers = [...offers];
          product.offersDisplayed = true;
        }
        return product;
      }
    );
    return updatedProducts;
  }

  public getGeneralSettingsItemsByProduct(
    product: Product
  ): Observable<SummaryGroup[]> {
    // TODO IMPLEMENT WHEN BACK ADD IT
    const maxNumberActiveLoans = product?.maximum_number_active_loans
      ? product.maximum_number_active_loans
      : 'Limitless';
    ////

    const minLoanAmount = this._customCurrencyPipe.transform(
      product.minimum_loan_amount
    );
    const maxLoanAmount = this._customCurrencyPipe.transform(
      product.maximum_loan_amount
    );

    const closingToleranceLabel = product.closing_tolerance_amount
      ? CLOSING_TOLERANCE_LABEL.FIXED_AMOUNT
      : CLOSING_TOLERANCE_LABEL.PERCENTAGE;

    const closingToleranceValue = this._customCurrencyPipe.transform(
      product?.closing_tolerance_amount ?? product?.closing_tolerance_percentage
    );
    const rounWayLabel = ROUND_WAY_LABEL_BY_VALUE[product?.decimal_round_way];
    const amountRequestedStep = this._customCurrencyPipe.transform(
      product.amount_requested_step
    );

    return this._translateService
      .get('PRODUCT_CATALOGUE.PRODUCTS_PROFILE.DETAILS')
      .pipe(
        map((DETAILS: ProductsProfileDetailsTranslate) => {
          const summaryItems: SummaryGroup[] = [
            // TODO IMPLEMENT WHEN BACK ADD IT
            // {
            //   key: `${DETAILS.MAXIMUM_NUMBER_OF_ACTIVE_LOANS}:`,
            //   value: maxNumberActiveLoans,
            // },
            {
              key: `${DETAILS.LOAN_AMOUNTS}:`,
              value: `Min. ${minLoanAmount}; Max. ${maxLoanAmount}`,
            },
            {
              key: `${DETAILS.CLOSING_TOLERANCE}:`,
              value: `${closingToleranceLabel} ${closingToleranceValue}`,
            },
            {
              key: `${DETAILS.DECIMALS}:`,
              value: `${parseFloat(
                String(product?.decimal_places)
              )} ${DETAILS.DECIMALS.toLowerCase()}; ${rounWayLabel}`,
            },
            {
              key: `${DETAILS.AMOUNT_REQUESTED_STEP}:`,
              value: `${DETAILS.NEAREST_MULTIPLE_OF} ${amountRequestedStep}`,
            },
          ];

          return summaryItems;
        })
      );
  }

  private _setProductCardStyles() {
    this.productCardStyles = [
      {
        icon: 'offer-icon-1.svg',
        template: OFFER_CARD_TEMPLATE.GREEN,
      },
      {
        icon: 'offer-icon-2.svg',
        template: OFFER_CARD_TEMPLATE.LIGHT_GREEN,
      },
      {
        icon: 'offer-icon-1.svg',
        template: OFFER_CARD_TEMPLATE.DARK_GREEN,
      },
      {
        icon: 'offer-icon-2.svg',
        template: OFFER_CARD_TEMPLATE.ADDITIONAL,
      },
    ];
  }

  // Generator function
  // Reference: https://www.angularfix.com/2022/04/applying-color-with-index-in-foreach.html
  private *_getProductCardStyleIterator(): IterableIterator<ProductCardStyle> {
    while (true) yield* this.productCardStyles;
  }
}
