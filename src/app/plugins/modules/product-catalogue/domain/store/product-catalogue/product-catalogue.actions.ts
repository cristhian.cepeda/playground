import {
  Offer,
  OfferCharge,
  OfferRepaymentLogic,
  OffersFilters,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { createAction, props } from '@ngrx/store';

// PRODUCTS AND OFFERS
export const updateProducts = createAction(
  '[Product and offers Page] Update Products',
  props<{ products: Product[] }>()
);

export const getProducts = createAction(
  '[Product and offers Page] Get Products '
);
export const successGetProducts = createAction(
  '[Product and offers Effects] Success Get Products ',
  props<{ products: Product[] }>()
);
export const failedGetProducts = createAction(
  '[Product and offers Effects] Failed Get Products '
);

// ********************
// PRODUCT PROFILE
// ********************

export const initProductProfilePage = createAction(
  '[Product Profile Page] Init Product Profile Page'
);
export const destroyProductProfilePage = createAction(
  '[Product Profile Page] Destroy Product Profile Page'
);
export const updateProductReference = createAction(
  '[Product Profile Guard] Update Product Reference',
  props<{ reference: string }>()
);

export const getProduct = createAction('[Product Profile Page] Get Product');
export const successGetProduct = createAction(
  '[Product and offers Effects] Success Get Product',
  props<{ product: Product }>()
);
export const failedGetProduct = createAction(
  '[Product and offers Effects] Failed Get Product'
);

// Offers
export const updateFiltersProductOffersTable = createAction(
  '[Product Profile Page] Update Filters Offers',
  props<{ filters: OffersFilters }>()
);

export const getProductOffersTable = createAction(
  '[Product and offers Page] Get Offers Table'
);

export const successGetProductOffersTable = createAction(
  '[Product and offers Effects] Success Get Offers Table',
  props<{ offers: TableResponse<Offer> }>()
);
export const failedGetProductOffersTable = createAction(
  '[Product and offers Effects] Failed Get Offers Table'
);

export const getProductOffers = createAction(
  '[Product and offers Page] Get Offers ',
  props<{ productReference: string }>()
);

export const successGetProductOffers = createAction(
  '[Product and offers Effects] Success Get Offers ',
  props<{ productReference: string; offers: Offer[] }>()
);
export const failedGetProductOffers = createAction(
  '[Product and offers Effects] Failed Get Offers '
);

// ********************
// OFFER PROFILE
// ********************

export const updateOfferReference = createAction(
  '[Offer Profile Guard] Update Offer Reference',
  props<{ reference: string }>()
);

export const getOffer = createAction('[Offer Profile Page] Get Offer');
export const successGetOffer = createAction(
  '[Product and offers Effects] Success Get Offer',
  props<{ offer: Offer }>()
);
export const failedGetOffer = createAction(
  '[Product and offers Effects] Failed Get Offer'
);
export const getOfferRepaymentLogic = createAction(
  '[Offer Profile Page] Get Offer Repayment Logic'
);
export const successGetOfferRepaymentLogic = createAction(
  '[Product and offers Effects] Success Get Offer Repayment Logic',
  props<{ offerRepaymentLogic: OfferRepaymentLogic }>()
);
export const failedGetOfferRepaymentLogic = createAction(
  '[Product and offers Effects] Failed Get Offer Repayment Logic'
);
export const getOfferCharges = createAction(
  '[Offer Profile Page] Get Offer Charges'
);
export const successGetOfferCharges = createAction(
  '[Product and offers Effects] Success Get Offer Charges',
  props<{ offerCharges: OfferCharge[] }>()
);
export const failedGetOfferCharges = createAction(
  '[Product and offers Effects] Failed Get Offer Charges'
);

export const destroyOfferProfilePage = createAction(
  '[Offer Profile Page] Destroy Offer Profile Page'
);
// TODO VALIDATE
export const updateNewProduct = createAction(
  '[Create New Product - General Settings Form] update new product',
  props<{ newProduct: Product }>()
);

export const nextStep = createAction(
  '[Create New Product - General Settings Form] next step'
);

export const createNewProduct = createAction(
  '[Create New Product - Summary] create new product'
);
export const updateNewOfferStep = createAction(
  '[Create New Offer - Specs] update step',
  props<{ newOfferStep: number }>()
);
