import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FILTERS } from '@app/core/constants/filters.constant';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import {
  Offer,
  OffersFilters,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { OfferApi } from '@app/plugins/modules/product-catalogue/data/api/offer.api';
import { ProductApi } from '@app/plugins/modules/product-catalogue/data/api/product.api';
import { ProductsService } from '@app/plugins/modules/product-catalogue/domain/services/products.service';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of, withLatestFrom } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as productCatalogueActions from './product-catalogue.actions';
import * as productCatalogueSelectors from './product-catalogue.selectors';
@Injectable()
export class ProductCatalogueEffects {
  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _productApi: ProductApi,
    private _offerApi: OfferApi,
    private _router: Router,
    private _productsService: ProductsService
  ) {}

  getProducts$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.getProducts),
      switchMap(() =>
        this._productApi.getProducts().pipe(
          map((products) =>
            productCatalogueActions.successGetProducts({ products })
          ),
          catchError((_) => of(productCatalogueActions.failedGetProducts()))
        )
      )
    )
  );

  // ********************
  // PRODUCT PROFILE
  // ********************

  initProductProfilePage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.initProductProfilePage),
      withLatestFrom(
        this._store.select(productCatalogueSelectors.selectProductReference)
      ),
      map(([_, productReference]) => {
        const filters: OffersFilters = {
          ...FILTERS,
          product_reference: productReference,
        };
        this._store.dispatch(productCatalogueActions.getProduct());
        return productCatalogueActions.updateFiltersProductOffersTable({
          filters,
        });
      })
    )
  );

  getProduct$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.getProduct),
      withLatestFrom(
        this._store.select(productCatalogueSelectors.selectProductReference)
      ),
      switchMap(([_, reference]) =>
        this._productApi.getProduct(reference).pipe(
          map((product) =>
            productCatalogueActions.successGetProduct({ product })
          ),
          catchError((_) => of(productCatalogueActions.failedGetProduct()))
        )
      )
    )
  );

  getOffersTable$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.updateFiltersProductOffersTable),
      withLatestFrom(
        this._store.select(productCatalogueSelectors.selectProductOffersFilters)
      ),
      switchMap(([_, filters]) => {
        return this._offerApi.getOffers(filters).pipe(
          map((offers: TableResponse<Offer>) =>
            productCatalogueActions.successGetProductOffersTable({ offers })
          ),
          catchError((_) =>
            of(productCatalogueActions.failedGetProductOffersTable())
          )
        );
      })
    )
  );

  getOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.getProductOffers),
      switchMap(({ productReference }) => {
        const filters = {
          product_reference: productReference,
        };
        return this._offerApi.getOffers(filters).pipe(
          map((offers: Offer[]) =>
            productCatalogueActions.successGetProductOffers({
              productReference,
              offers,
            })
          ),
          catchError((_) =>
            of(productCatalogueActions.failedGetProductOffers())
          )
        );
      })
    )
  );

  successGetOffers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.successGetProductOffers),
      withLatestFrom(
        this._store.select(productCatalogueSelectors.selectProducts)
      ),
      map(([{ productReference, offers }, products]) => {
        const updatedProducts: Product[] =
          this._productsService.updateProductsOffers(
            products,
            productReference,
            offers
          );

        return productCatalogueActions.updateProducts({
          products: updatedProducts,
        });
      })
    )
  );

  // ********************
  // OFFER PROFILE
  // ********************

  getOffer$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.getOffer),
      withLatestFrom(
        this._store.select(productCatalogueSelectors.selectOfferReference)
      ),
      switchMap(([_, reference]) =>
        this._offerApi.getOffer(reference).pipe(
          map((offer) => productCatalogueActions.successGetOffer({ offer })),
          catchError((_) => of(productCatalogueActions.failedGetOffer()))
        )
      )
    )
  );

  getOfferRepaymentLogic$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.getOfferRepaymentLogic),
      withLatestFrom(
        this._store.select(productCatalogueSelectors.selectOfferReference)
      ),
      switchMap(([_, reference]) =>
        this._offerApi.getOfferRepaymentLogic(reference).pipe(
          map((offerRepaymentLogic) =>
            productCatalogueActions.successGetOfferRepaymentLogic({
              offerRepaymentLogic,
            })
          ),
          catchError((_) =>
            of(productCatalogueActions.failedGetOfferRepaymentLogic())
          )
        )
      )
    )
  );

  getOfferCharges$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.getOfferCharges),
      withLatestFrom(
        this._store.select(productCatalogueSelectors.selectOfferReference)
      ),
      switchMap(([_, reference]) =>
        this._offerApi.getOfferCharges(reference).pipe(
          map((offerCharges) =>
            productCatalogueActions.successGetOfferCharges({
              offerCharges,
            })
          ),
          catchError((_) => of(productCatalogueActions.failedGetOfferCharges()))
        )
      )
    )
  );

  // TODO VALIDATE
  public createNewProductActionEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(productCatalogueActions.updateNewProduct),
      switchMap(() => {
        this._router.navigateByUrl(
          PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.PRODUCT
            .CREATE_NEW_PRODUCT_SUMMARY
        );
        return of(productCatalogueActions.nextStep());
      })
    )
  );
}
