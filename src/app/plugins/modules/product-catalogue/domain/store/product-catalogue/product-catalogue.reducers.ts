import { Action, createReducer, on } from '@ngrx/store';
import {
  destroyOfferProfilePage,
  destroyProductProfilePage,
  failedGetOffer,
  failedGetOfferCharges,
  failedGetOfferRepaymentLogic,
  failedGetProduct,
  failedGetProductOffersTable,
  failedGetProducts,
  getOffer,
  getOfferCharges,
  getOfferRepaymentLogic,
  getProduct,
  getProductOffersTable,
  getProducts,
  successGetOffer,
  successGetOfferCharges,
  successGetOfferRepaymentLogic,
  successGetProduct,
  successGetProductOffersTable,
  successGetProducts,
  updateFiltersProductOffersTable,
  updateNewOfferStep,
  updateNewProduct,
  updateOfferReference,
  updateProductReference,
  updateProducts,
} from './product-catalogue.actions';
import { ProductCatalogueState } from './product-catalogue.state';

export const initialProductCatalogueState: ProductCatalogueState = {
  isLoadingProducts: false,

  // TODO VALIDATE
  newProduct: null,
  newOfferStep: 0,
};

const _productCatalogueReducer = createReducer(
  initialProductCatalogueState,
  on(getProducts, (state) => ({
    ...state,
    isLoadingProducts: true,
  })),
  on(successGetProducts, (state, { products }) => ({
    ...state,
    products,
    isLoadingProducts: false,
  })),
  on(failedGetProducts, (state) => ({
    ...state,
    products: null,
    isLoadingProducts: false,
  })),
  on(updateProducts, (state, { products }) => ({
    ...state,
    products,
  })),

  // ********************
  // PRODUCT PROFILE
  // ********************
  on(destroyProductProfilePage, (state) => ({
    ...state,
    product: null,
  })),
  on(updateProductReference, (state, { reference }) => ({
    ...state,
    product: {
      ...state?.product,
      productReference: reference,
    },
  })),
  on(getProduct, (state) => ({
    ...state,
    product: {
      ...state?.product,
      isLoadingProduct: true,
    },
  })),
  on(successGetProduct, (state, { product }) => ({
    ...state,
    product: {
      ...state?.product,
      data: product,
      isLoadingData: false,
    },
  })),
  on(failedGetProduct, (state) => ({
    ...state,
    product: {
      ...state?.product,
      data: null,
      isLoadingProduct: false,
    },
  })),
  on(updateFiltersProductOffersTable, (state, { filters }) => ({
    ...state,
    product: {
      ...state?.product,
      offersFilters: filters,
      isLoadingOffers: true,
    },
  })),
  on(getProductOffersTable, (state) => ({
    ...state,
    product: {
      ...state?.product,
      isLoadingOffers: true,
    },
  })),
  on(successGetProductOffersTable, (state, { offers }) => ({
    ...state,
    product: {
      ...state?.product,
      offers,
      isLoadingOffers: false,
    },
  })),
  on(failedGetProductOffersTable, (state) => ({
    ...state,
    product: {
      ...state?.product,
      offers: null,
      isLoadingOffers: false,
    },
  })),

  // ********************
  // OFFER PROFILE
  // ********************
  on(destroyOfferProfilePage, (state) => ({
    ...state,
    offer: null,
  })),
  on(updateOfferReference, (state, { reference }) => ({
    ...state,
    offer: {
      ...state?.offer,
      offerReference: reference,
    },
  })),
  on(getOffer, (state) => ({
    ...state,
    offer: {
      ...state?.offer,
      isLoadingData: true,
    },
  })),
  on(successGetOffer, (state, { offer }) => ({
    ...state,
    offer: {
      ...state?.offer,
      data: offer,
      isLoadingData: false,
    },
  })),
  on(failedGetOffer, (state) => ({
    ...state,
    offer: {
      ...state?.offer,
      data: null,
      isLoadingData: false,
    },
  })),
  on(getOfferRepaymentLogic, (state) => ({
    ...state,
    offer: {
      ...state?.offer,
      isLoadingOfferRepaymentLogic: true,
    },
  })),
  on(successGetOfferRepaymentLogic, (state, { offerRepaymentLogic }) => ({
    ...state,
    offer: {
      ...state?.offer,
      offerRepaymentLogic,
      isLoadingOfferRepaymentLogic: false,
    },
  })),
  on(failedGetOfferRepaymentLogic, (state) => ({
    ...state,
    offer: {
      ...state?.offer,
      offerRepaymentLogic: null,
      isLoadingOfferRepaymentLogic: false,
    },
  })),
  on(getOfferCharges, (state) => ({
    ...state,
    offer: {
      ...state?.offer,
      isLoadingOfferCharges: true,
    },
  })),
  on(successGetOfferCharges, (state, { offerCharges }) => ({
    ...state,
    offer: {
      ...state?.offer,
      offerCharges,
      isLoadingOfferCharges: false,
    },
  })),
  on(failedGetOfferCharges, (state) => ({
    ...state,
    offer: {
      ...state?.offer,
      offerCharges: null,
      isLoadingOfferCharges: false,
    },
  })),

  // TODO VALIDATE
  on(updateNewProduct, (state, { newProduct }) => ({
    ...state,
    newProduct,
  })),
  on(updateNewOfferStep, (state, { newOfferStep }) => ({
    ...state,
    newOfferStep,
  }))
);

export function ProductCatalogueReducers(
  state: ProductCatalogueState | undefined,
  action: Action
) {
  return _productCatalogueReducer(state, action);
}
