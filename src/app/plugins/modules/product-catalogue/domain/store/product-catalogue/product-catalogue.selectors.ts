import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProductCatalogueState } from './product-catalogue.state';

export const getProductCatalogueFeatureState =
  createFeatureSelector<ProductCatalogueState>(
    PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.STORE_NAME
  );

export const selectProducts = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.products
);
export const selectIsLoadingProducts = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.isLoadingProducts
);

// ********************
// PRODUCT PROFILE
// ********************
export const selectProductReference = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.product?.productReference
);
export const selectProduct = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.product?.data
);
export const selectIsLoadingProduct = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.product?.isLoadingData
);
export const selectProductOffersFilters = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.product?.offersFilters
);
export const selectProductOffers = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.product?.offers
);
export const selectIsLoadingProductOffers = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.product?.isLoadingOffers
);

// ********************
// OFFER PROFILE
// ********************

export const selectOfferReference = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.offer?.offerReference
);
export const selectOffer = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.offer?.data
);
export const selectIsLoadingOffer = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.offer?.isLoadingData
);
export const selectOfferRepaymentLogic = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.offer?.offerRepaymentLogic
);
export const selectIsLoadingOfferRepaymentLogic = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.offer?.isLoadingOfferRepaymentLogic
);
export const selectOfferCharges = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.offer?.offerCharges
);
export const selectIsLoadingOfferCharges = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.offer?.isLoadingOfferCharges
);

// TODO VALIDATE
export const selectNewProduct = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.newProduct
);

export const selectNewOfferStep = createSelector(
  getProductCatalogueFeatureState,
  (state: ProductCatalogueState) => state?.newOfferStep
);
