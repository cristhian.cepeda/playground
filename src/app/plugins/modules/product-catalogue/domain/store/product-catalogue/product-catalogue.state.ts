import {
  Offer,
  OfferCharge,
  OfferRepaymentLogic,
  OffersFilters,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';

export interface ProductCatalogueState {
  products?: Product[];
  isLoadingProducts: boolean;
  product?: ProductState;
  offer?: OfferState;

  // TODO VALIDATE
  newProduct?: Product;
  newOfferStep?: number;
}
export interface ProductState {
  productReference?: string;
  data?: Product;
  isLoadingData?: boolean;
  offersFilters?: OffersFilters;
  offers?: TableResponse<Offer>;
  isLoadingOffers?: boolean;
}

export interface OfferState {
  offerReference?: string;
  data?: Offer;
  isLoadingData?: boolean;
  offerRepaymentLogic?: OfferRepaymentLogic;
  isLoadingOfferRepaymentLogic?: boolean;
  offerCharges?: OfferCharge[];
  isLoadingOfferCharges?: boolean;
}
