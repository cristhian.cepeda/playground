import { TestBed } from '@angular/core/testing';
import * as productCatalogueActions from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.actions';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { OfferFacade } from './offer.facade';

describe('OfferFacade', () => {
  let facade: OfferFacade;
  let store: MockStore;
  const initialState: any = {};
  let spy: jasmine.Spy<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomCurrencyPipe, provideMockStore({ initialState })],
    });
    facade = TestBed.inject(OfferFacade);
    store = TestBed.inject(MockStore);
    spy = spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be call updateNewOfferStep action', () => {
    const newOfferStep: number = 1;
    facade.updateNewOfferStep(newOfferStep);
    expect(spy).toHaveBeenCalledWith(
      productCatalogueActions.updateNewOfferStep({ newOfferStep })
    );
  });
});
