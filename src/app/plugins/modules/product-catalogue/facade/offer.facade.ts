import { Injectable } from '@angular/core';
import {
  OFFER_SPECS_STEPS,
  OFFER_SPECS_TAPS,
} from '@app/core/constants/offer.constants';
import {
  SummaryDropItem,
  SummaryGroup,
} from '@app/core/models/summary-group.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  DemoOfferRepaymentLogic,
  Offer,
  OfferCharge,
  OfferCharges,
  OfferChargeType,
  OfferCollectionCostCharge,
  OfferCostCharge,
  OfferDefaultInterestCharge,
  OfferGeneralSettings,
  OfferInterestCharge,
  OfferRepaymentLogic,
  OfferSpecs,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { DemoService } from '@app/plugins/modules/product-catalogue/domain/services/demo.service';
import * as productCatalogueActions from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.actions';
import * as productCatalogueSelectors from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.selectors';
import { TableResponse } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Store } from '@ngrx/store';
import { filter, map, mergeMap, Observable } from 'rxjs';
import { OfferService } from '../domain/services/offer.service';
@Injectable({ providedIn: 'root' })
export class OfferFacade {
  public projectFamily$: Observable<string>;

  public offers$: Observable<TableResponse<Offer>>;
  public isLoadingOffers$: Observable<boolean>;

  public offer$: Observable<Offer>;
  public isLoadingOffer$: Observable<boolean>;
  public offerProfileGeneralSettingsItems$: Observable<SummaryGroup[]>;

  public offerRepaymentLogic$: Observable<OfferRepaymentLogic>;
  public isLoadingOfferRepaymentLogic$: Observable<boolean>;
  public offerProfileRepaymentLogicItems$: Observable<SummaryGroup[]>;

  public offerCharges$: Observable<OfferCharge[]>;
  public isLoadingOfferCharges$: Observable<boolean>;
  public offerProfileChargesItems$: Observable<SummaryDropItem[]>;

  public newOfferStep$: Observable<number>;

  constructor(
    private _store: Store,
    private _projectFacade: ProjectFacade,
    private _offerService: OfferService,
    private _demoService: DemoService
  ) {
    this._setSelectors();
  }

  public getProductOffersTable() {
    this._store.dispatch(productCatalogueActions.getProductOffersTable());
  }

  public getProductOffers(productReference: string) {
    this._store.dispatch(
      productCatalogueActions.getProductOffers({ productReference })
    );
  }

  public updateOfferReference(offerReference: string) {
    this._store.dispatch(
      productCatalogueActions.updateOfferReference({
        reference: offerReference,
      })
    );
  }

  public getOffer() {
    this._store.dispatch(productCatalogueActions.getOffer());
  }

  public getOfferRepaymentLogic() {
    this._store.dispatch(productCatalogueActions.getOfferRepaymentLogic());
  }

  public getOfferCharges() {
    this._store.dispatch(productCatalogueActions.getOfferCharges());
  }

  // TODO VALIDATE

  public updateNewOfferStep(newOfferStep: number) {
    this._store.dispatch(
      productCatalogueActions.updateNewOfferStep({ newOfferStep })
    );
  }

  public cleanOfferData() {
    this._demoService.cleanOfferData();
    this.updateNewOfferStep(
      OFFER_SPECS_TAPS.indexOf(OFFER_SPECS_STEPS.GENERAL_SETTINGS)
    );
  }

  // General Settings
  public sendGeneralSettings(generalSettings: OfferGeneralSettings) {
    this._demoService.updateCreateNewOfferData({ generalSettings });
  }

  // Repayment Logic
  public sendRepaymentLogic(repaymentLogic: DemoOfferRepaymentLogic) {
    this._demoService.updateCreateNewOfferData({ repaymentLogic });
  }

  // Offer Specs
  public getOfferSpecs(): Observable<OfferSpecs> {
    return this._demoService
      .getCreateNewOfferData()
      .pipe(map((offer) => offer));
  }

  // Charges
  public getCharges(): Observable<OfferCharges> {
    return this._demoService
      .getCreateNewOfferData()
      .pipe(map((offer) => offer?.charges));
  }

  public removeOfferChargesByType(chargeType: OfferChargeType) {
    this._demoService.removeOfferChargesByType(chargeType);
  }

  public sendCharges(charges: OfferCharges) {
    this._demoService.updatePrioritizedCharges(charges);
  }

  public sendInterestCharge(interest: OfferInterestCharge) {
    this._demoService.updateCreateNewOfferData({ charges: { interest } });
  }

  public sendCostCharge(cost: OfferCostCharge) {
    this._demoService.updateCreateNewOfferData({ charges: { cost } });
  }

  public sendCollectionCostCharge(collectionCost: OfferCollectionCostCharge) {
    this._demoService.updateCreateNewOfferData({ charges: { collectionCost } });
  }

  public sendDefaultInterestCharge(
    defaultInterest: OfferDefaultInterestCharge
  ) {
    this._demoService.updateCreateNewOfferData({
      charges: { defaultInterest },
    });
  }

  private _setSelectors() {
    this.newOfferStep$ = this._store.select(
      productCatalogueSelectors.selectNewOfferStep
    );
    this.offers$ = this._store.select(
      productCatalogueSelectors.selectProductOffers
    );
    this.isLoadingOffers$ = this._store.select(
      productCatalogueSelectors.selectIsLoadingProductOffers
    );
    this.offer$ = this._store.select(productCatalogueSelectors.selectOffer);
    this.isLoadingOffer$ = this._store.select(
      productCatalogueSelectors.selectIsLoadingOffer
    );
    this.offerProfileGeneralSettingsItems$ = this.offer$.pipe(
      filter((offer: Offer) => !!offer),
      mergeMap((offer: Offer) =>
        this._offerService.getOfferProfileGeneralSettingsItems(offer)
      )
    );
    this.offerRepaymentLogic$ = this._store.select(
      productCatalogueSelectors.selectOfferRepaymentLogic
    );
    this.isLoadingOfferRepaymentLogic$ = this._store.select(
      productCatalogueSelectors.selectIsLoadingOfferRepaymentLogic
    );
    this.offerProfileRepaymentLogicItems$ = this.offerRepaymentLogic$.pipe(
      filter(
        (offerRepaymentLogic: OfferRepaymentLogic) => !!offerRepaymentLogic
      ),
      mergeMap((offerRepaymentLogic: OfferRepaymentLogic) =>
        this._offerService.getOfferProfileRepaymentLogic(offerRepaymentLogic)
      )
    );

    this.offerCharges$ = this._store.select(
      productCatalogueSelectors.selectOfferCharges
    );
    this.isLoadingOfferCharges$ = this._store.select(
      productCatalogueSelectors.selectIsLoadingOfferCharges
    );
    this.offerProfileChargesItems$ = this.offerCharges$.pipe(
      filter(
        (offerCharges: OfferCharge[]) =>
          !!offerCharges && offerCharges?.length > 0
      ),
      mergeMap((offerCharges: OfferCharge[]) =>
        this._offerService.getOfferProfileChargesItems(offerCharges)
      )
    );

    this.projectFamily$ = this._projectFacade.projectFamily$;
  }
}
