import { TestBed } from '@angular/core/testing';
import * as productCatalogueActions from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.actions';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { Product } from '../core/models/product.model';
import { ProductFacade } from './product.facade';

describe('ProductFacade', () => {
  let facade: ProductFacade;
  let store: MockStore;
  const initialState: any = {};
  let spy: jasmine.Spy<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [CustomCurrencyPipe, provideMockStore({ initialState })],
    });
    facade = TestBed.inject(ProductFacade);
    store = TestBed.inject(MockStore);
    spy = spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  it('should be call updateNewProduct action', () => {
    const newProduct: Product = {} as Product;
    facade.onUpdateNewProduct(newProduct);
    expect(spy).toHaveBeenCalledWith(
      productCatalogueActions.updateNewProduct({ newProduct })
    );
  });
});
