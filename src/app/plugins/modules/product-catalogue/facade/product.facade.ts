import { Injectable } from '@angular/core';
import { Project } from '@app/core/models/project.model';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { ProjectFacade } from '@app/facade/project/project.facade';
import {
  Product,
  ProductsAndOfferCounter,
} from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { ProductsService } from '@app/plugins/modules/product-catalogue/domain/services/products.service';
import * as productCatalogueActions from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.actions';
import * as productCatalogueSelectors from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.selectors';
import { Store } from '@ngrx/store';
import { filter, map, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProductFacade {
  public projectFamily$: Observable<string>;
  public project$: Observable<Project>;

  public products$: Observable<Product[]>;
  public productsAndOffersCounter$: Observable<ProductsAndOfferCounter>;

  public isLoadingProducts$: Observable<boolean>;
  public productProfile$: Observable<Product>;
  public isLoadingProductProfile$: Observable<boolean>;
  public productProfileItems$: Observable<SummaryGroup[]>;

  public newProduct$: Observable<Product>;
  constructor(
    private _store: Store,
    private _productsService: ProductsService,
    private _projectFacade: ProjectFacade
  ) {
    this._setSelectors();
  }

  public updateProductReference(productReference: string): void {
    this._store.dispatch(
      productCatalogueActions.updateProductReference({
        reference: productReference,
      })
    );
  }

  public getProducts() {
    this._store.dispatch(productCatalogueActions.getProducts());
  }

  public initProductProfilePage() {
    this._store.dispatch(productCatalogueActions.initProductProfilePage());
  }

  public destroyProductProfilePage() {
    this._store.dispatch(productCatalogueActions.destroyProductProfilePage());
  }

  public onUpdateNewProduct(newProduct: Product) {
    this._store.dispatch(
      productCatalogueActions.updateNewProduct({ newProduct })
    );
  }

  private _setSelectors() {
    this.project$ = this._projectFacade.project$;
    this.projectFamily$ = this._projectFacade.projectFamily$;

    this.products$ = this._store
      .select(productCatalogueSelectors.selectProducts)
      .pipe(
        filter((products: Product[]) => !!products),
        map((products: Product[]) =>
          this._productsService.generateProductsTemplates(products)
        )
      );
    this.productsAndOffersCounter$ = this._store
      .select(productCatalogueSelectors.selectProducts)
      .pipe(
        filter((products: Product[]) => !!products),
        map((products: Product[]) =>
          this._productsService.getProductsAndOffersCounter(products)
        )
      );
    this.isLoadingProducts$ = this._store.select(
      productCatalogueSelectors.selectIsLoadingProducts
    );
    this.productProfile$ = this._store.select(
      productCatalogueSelectors.selectProduct
    );
    this.productProfileItems$ = this.productProfile$.pipe(
      filter(
        (productProfile: Product) => !!productProfile && !!productProfile.name
      ),
      mergeMap((productProfile: Product) =>
        this._productsService.getGeneralSettingsItemsByProduct(productProfile)
      )
    );
    this.isLoadingProductProfile$ = this._store.select(
      productCatalogueSelectors.selectIsLoadingProduct
    );

    this.newProduct$ = this._store.select(
      productCatalogueSelectors.selectNewProduct
    );
  }
}
