import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { OfferStepsComponent } from './offer-steps/offer-steps.component';
import { OfferSummaryComponent } from './offer-summary/offer-summary.component';
const OFFER_STEPS_ROUTES =
  PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES.CREATE_NEW_OFFER
    .SUB_PAGES;

const routes: Routes = [
  {
    path: '',
    component: OfferStepsComponent,
    children: [
      {
        path: '',
        redirectTo: OFFER_STEPS_ROUTES.OFFER_SPECS.PATH,
        pathMatch: 'full',
      },
      {
        path: OFFER_STEPS_ROUTES.OFFER_SPECS.PATH,
        loadChildren: () =>
          import('./offer-specs/offer-specs.module').then(
            (m) => m.OfferSpecsModule
          ),
      },
      {
        path: OFFER_STEPS_ROUTES.OFFER_SUMMARY.PATH,
        component: OfferSummaryComponent,
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewOfferRoutingModule {}
