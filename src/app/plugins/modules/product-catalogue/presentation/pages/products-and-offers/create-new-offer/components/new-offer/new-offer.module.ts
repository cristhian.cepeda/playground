import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ComponentsModule } from '@app/plugins/modules/product-catalogue/presentation/pages/products-and-offers/products-and-offers/components/components.module';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { NewOfferRoutingModule } from './new-offer-routing.module';
import { OfferSpecsModule } from './offer-specs/offer-specs.module';
import { OfferStepsComponent } from './offer-steps/offer-steps.component';
import { OfferSummaryComponent } from './offer-summary/offer-summary.component';
import { ProductSummaryComponent } from './product-summary/product-summary.component';

@NgModule({
  declarations: [
    OfferStepsComponent,
    ProductSummaryComponent,
    OfferSummaryComponent,
  ],
  imports: [
    CommonModule,
    NewOfferRoutingModule,
    OfferSpecsModule,
    LayoutModule,
    ComponentsModule,
  ],
})
export class NewOfferModule {}
