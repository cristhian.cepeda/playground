import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OfferInterestCharge } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AllChargesComponent } from './all-charges.component';

describe('AllChargesComponent', () => {
  let component: AllChargesComponent;
  let fixture: ComponentFixture<AllChargesComponent>;
  let store: MockStore;
  let facade: OfferFacade;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AllChargesComponent],
      providers: [OfferFacade, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
    facade = TestBed.inject(OfferFacade);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onSendInterestCharge', () => {
    it('should be set values', () => {
      const interest: OfferInterestCharge = {};
      const spy = spyOn(facade, 'sendInterestCharge');
      component.onSendInterestCharge(interest);
      expect(spy).toHaveBeenCalledOnceWith(interest);
      expect(component.interestCharge).toEqual(interest);
      expect(component.isOpenInterest).toBeFalse();
    });
  });
});
