import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  OfferCollectionCostCharge,
  OfferCostCharge,
  OfferDefaultInterestCharge,
  OfferInterestCharge,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { Subscription } from 'rxjs';

@Component({
  selector: 'offer-specs-all-charges',
  templateUrl: './all-charges.component.html',
  styleUrls: ['./all-charges.component.scss'],
})
export class AllChargesComponent implements OnInit, OnDestroy {
  public isOpenInterest: boolean;
  public interestCharge?: OfferInterestCharge;

  public isOpenCost: boolean;
  public costCharge?: OfferCostCharge;

  public isOpenCollectionCost: boolean;
  public collectionCostCharge?: OfferCollectionCostCharge;

  public isOpenDefaultInterest: boolean;
  public defaultInterestCharge?: OfferDefaultInterestCharge;

  private _chargesSubscription: Subscription;

  constructor(private _offerFacade: OfferFacade) {}

  ngOnInit(): void {
    this._setinitialValues();
  }

  ngOnDestroy(): void {
    this._chargesSubscription.unsubscribe();
  }

  public onSendInterestCharge(interest?: OfferInterestCharge) {
    this._offerFacade.sendInterestCharge(interest);
    this.interestCharge = interest;
    this.isOpenInterest = false;
  }

  public onRemoveInterestCharge() {
    this._offerFacade.removeOfferChargesByType('interest');
    this.interestCharge = null;
    this.isOpenInterest = false;
  }

  public onToggleInterest(isOpenInterest: boolean): void {
    this.isOpenInterest = isOpenInterest;
  }

  public onSendCostCharge(costCharge?: OfferCostCharge) {
    this._offerFacade.sendCostCharge(costCharge);
    this.costCharge = costCharge;
    this.isOpenCost = false;
  }

  public onRemoveCostCharge() {
    this._offerFacade.removeOfferChargesByType('cost');
    this.costCharge = null;
    this.isOpenCost = false;
  }

  public onToggleCost(isOpenCost: boolean): void {
    this.isOpenCost = isOpenCost;
  }

  public onSendCollectionCost(collectionCost?: OfferCollectionCostCharge) {
    this._offerFacade.sendCollectionCostCharge(collectionCost);
    this.collectionCostCharge = collectionCost;
    this.isOpenCollectionCost = false;
  }

  public onRemoveCollectionCost() {
    this._offerFacade.removeOfferChargesByType('collectionCost');
    this.collectionCostCharge = null;
    this.isOpenCollectionCost = false;
  }

  public onToggleCollectionCost(isOpenCollectionCost: boolean): void {
    this.isOpenCollectionCost = isOpenCollectionCost;
  }

  public onSendDefaultInterestCharge(
    defaultInterest?: OfferDefaultInterestCharge
  ) {
    this._offerFacade.sendDefaultInterestCharge(defaultInterest);
    this.defaultInterestCharge = defaultInterest;
    this.isOpenDefaultInterest = false;
  }

  public onRemoveDefaultInterestCharge() {
    this._offerFacade.removeOfferChargesByType('defaultInterest');
    this.defaultInterestCharge = null;
    this.isOpenDefaultInterest = false;
  }

  public onToggleDefaultInterest(isOpenDefaultInterest: boolean): void {
    this.isOpenDefaultInterest = isOpenDefaultInterest;
  }

  private _setinitialValues() {
    this.isOpenInterest = false;
    this.isOpenCost = false;
    this.isOpenCollectionCost = false;
    this.isOpenDefaultInterest = false;
    this._chargesSubscription = this._offerFacade
      .getCharges()
      .subscribe((charges) => {
        this.interestCharge = charges?.interest;
        this.costCharge = charges?.cost;
        this.collectionCostCharge = charges?.collectionCost;
        this.defaultInterestCharge = charges?.defaultInterest;
      });
  }
}
