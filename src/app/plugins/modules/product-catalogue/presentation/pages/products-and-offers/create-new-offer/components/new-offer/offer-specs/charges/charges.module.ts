import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllChargesComponent } from './all-charges/all-charges.component';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { InterestComponent } from './interest/interest.component';
import { CostComponent } from './cost/cost.component';
import { CollectionCostsComponent } from './collection-costs/collection-costs.component';
import { DefaultInterestComponent } from './default-interest/default-interest.component';

const COMPONENTS = [
  AllChargesComponent,
  InterestComponent,
  CostComponent,
  CollectionCostsComponent,
  DefaultInterestComponent,
];

@NgModule({
  declarations: COMPONENTS,
  exports: COMPONENTS,
  imports: [CommonModule, LayoutModule],
})
export class ChargesModule {}
