import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { CollectionCostsComponent } from './collection-costs.component';

describe('CollectionCostsComponent', () => {
  let component: CollectionCostsComponent;
  let fixture: ComponentFixture<CollectionCostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LayoutModule],
      declarations: [CollectionCostsComponent],
      providers: [UntypedFormBuilder],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionCostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
