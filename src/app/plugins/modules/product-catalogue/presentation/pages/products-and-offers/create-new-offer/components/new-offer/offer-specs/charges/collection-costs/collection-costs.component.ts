import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { OFFER_PRIORIZATION_DEFAULT } from '@app/core/constants/offer.constants';
import { OfferCollectionCostCharge } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { SETTINGS_ITEM_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'collection-costs',
  templateUrl: './collection-costs.component.html',
  styleUrls: ['./collection-costs.component.scss'],
})
export class CollectionCostsComponent implements OnInit {
  @Input() collectionCostCharge: OfferCollectionCostCharge;
  @Output() submitCollectionCost: EventEmitter<OfferCollectionCostCharge> =
    new EventEmitter<OfferCollectionCostCharge>();
  @Output() removeCollectionCost: EventEmitter<void> = new EventEmitter<void>();

  public form: UntypedFormGroup;

  public SETTINGS_ITEM_DESIGN_CLASS = SETTINGS_ITEM_DESIGN_CLASS;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public INPUT_TYPE = INPUT_TYPE;

  constructor(private _formBuilder: UntypedFormBuilder) {}

  ngOnInit(): void {
    this._setForm();
  }

  public onSubmit() {
    if (this.form.invalid) return;
    this.submitCollectionCost.emit(this.form.value);
  }

  public onCancel() {
    this.form.reset();
  }

  public onRemoveCharges() {
    this.form.reset();
    this.form
      .get('name')
      .setValue(OFFER_PRIORIZATION_DEFAULT.COLLECTION_COSTS.NAME);
    this.removeCollectionCost.emit();
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      trigger: [this.collectionCostCharge?.trigger ?? '', Validators.required],
      grace_period: [
        this.collectionCostCharge?.grace_period ?? '',
        Validators.required,
      ],
      grace_period_time_unit: [
        this.collectionCostCharge?.grace_period_time_unit ?? '',
        Validators.required,
      ],
      grace_period_time_number: [
        this.collectionCostCharge?.grace_period_time_number ?? '',
        Validators.required,
      ],
      fixed_or_percent: [
        this.collectionCostCharge?.fixed_or_percent ?? '',
        Validators.required,
      ],
      fixed_or_percent_value: [
        this.collectionCostCharge?.fixed_or_percent_value ?? '',
        Validators.required,
      ],
      base_definition: [
        this.collectionCostCharge?.base_definition ?? '',
        Validators.required,
      ],
      tax: [this.collectionCostCharge?.tax ?? '', Validators.required],
      tax_fee: [this.collectionCostCharge?.tax_fee ?? '', Validators.required],
      name: [
        this.collectionCostCharge?.name ??
          OFFER_PRIORIZATION_DEFAULT.COLLECTION_COSTS.NAME,
        Validators.required,
      ],
    });

    this._setDynamicFormValidations();
  }

  private _setDynamicFormValidations() {
    this.form
      .get('grace_period')
      .valueChanges.subscribe((grace_period: boolean) => {
        const controls = [
          this.form.get('grace_period_time_unit'),
          this.form.get('grace_period_time_number'),
        ];
        if (String(grace_period) !== 'Yes') {
          controls.forEach((control) => {
            control.removeValidators(Validators.required);
            control.setValue(null);
          });
        } else {
          controls.forEach((control) => {
            control.addValidators(Validators.required);
          });
        }
        this.form.updateValueAndValidity();
      });

    this.form
      .get('fixed_or_percent')
      .valueChanges.subscribe((fixed_or_percent: boolean) => {
        const control = this.form.get('base_definition');
        if (String(fixed_or_percent) !== 'Percent') {
          control.removeValidators(Validators.required);
          control.setValue(null);
        } else {
          control.addValidators(Validators.required);
        }
        this.form.updateValueAndValidity();
      });

    this.form.get('tax').valueChanges.subscribe((tax: boolean) => {
      const control = this.form.get('tax_fee');
      if (String(tax) !== 'Yes') {
        control.removeValidators(Validators.required);
        control.setValue(null);
      } else {
        control.addValidators(Validators.required);
      }
      this.form.updateValueAndValidity();
    });
  }
}
