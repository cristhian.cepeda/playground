import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { OfferCostCharge } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { SETTINGS_ITEM_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  COLOR_TEMPLATE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';

@Component({
  selector: 'cost',
  templateUrl: './cost.component.html',
  styleUrls: ['./cost.component.scss'],
})
export class CostComponent implements OnInit {
  @Input() costCharge: OfferCostCharge;
  @Output() submitCost: EventEmitter<OfferCostCharge> =
    new EventEmitter<OfferCostCharge>();
  @Output() removeCost: EventEmitter<void> = new EventEmitter<void>();

  public form: UntypedFormGroup;

  public SETTINGS_ITEM_DESIGN_CLASS = SETTINGS_ITEM_DESIGN_CLASS;
  public COLOR_TEMPLATE = COLOR_TEMPLATE;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public INPUT_TYPE = INPUT_TYPE;
  public typeOfCostOptions: SelectOption<string | number>[];

  constructor(private _formBuilder: UntypedFormBuilder) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onSubmit() {
    if (this.form.invalid) return;
    this.submitCost.emit(this.form.value);
  }

  public onCancel() {
    this.form.reset();
  }

  public onRemoveCharges() {
    this.form.reset();
    this.removeCost.emit();
  }

  private _setInitialValues() {
    this.typeOfCostOptions = [
      {
        key: 'Platform',
        value: 'Platform',
      },
      {
        key: 'Administrative',
        value: 'Administrative',
      },
      {
        key: 'Other',
        value: 'Other',
      },
    ];
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      name: [this.costCharge?.name ?? '', Validators.required],
      fixed_or_percent: [
        this.costCharge?.fixed_or_percent ?? '',
        Validators.required,
      ],
      fixed_or_percent_value: [
        this.costCharge?.fixed_or_percent_value ?? '',
        Validators.required,
      ],
      tax: [this.costCharge?.tax ?? '', Validators.required],
      tax_fee: [this.costCharge?.tax_fee ?? '', Validators.required],
    });

    this._setDynamicFormValidations();
  }

  private _setDynamicFormValidations() {
    this.form.get('tax').valueChanges.subscribe((tax: boolean) => {
      const control = this.form.get('tax_fee');
      if (String(tax) !== 'Yes') {
        control.removeValidators(Validators.required);
        control.setValue(null);
      } else {
        control.addValidators(Validators.required);
      }
      this.form.updateValueAndValidity();
    });
  }
}
