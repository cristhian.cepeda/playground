import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';

import { DefaultInterestComponent } from './default-interest.component';

describe('DefaultInterestComponent', () => {
  let component: DefaultInterestComponent;
  let fixture: ComponentFixture<DefaultInterestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DefaultInterestComponent],
      providers: [UntypedFormBuilder],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
