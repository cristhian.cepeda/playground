import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { OFFER_PRIORIZATION_DEFAULT } from '@app/core/constants/offer.constants';
import { OfferInterestCharge } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { SETTINGS_ITEM_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'interest',
  templateUrl: './interest.component.html',
  styleUrls: ['./interest.component.scss'],
})
export class InterestComponent implements OnInit {
  @Input() offerInterestCharge: OfferInterestCharge;
  @Output() submitInterest: EventEmitter<OfferInterestCharge> =
    new EventEmitter<OfferInterestCharge>();
  @Output() removeInterest: EventEmitter<void> = new EventEmitter<void>();

  public form: UntypedFormGroup;
  public fixedOrRelativeDesingClass: SETTINGS_ITEM_DESIGN_CLASS;

  public SETTINGS_ITEM_DESIGN_CLASS = SETTINGS_ITEM_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  constructor(private _formBuilder: UntypedFormBuilder) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onSubmit() {
    if (this.form.invalid) return;
    this.submitInterest.emit(this.form.value);
  }

  public onCancel() {
    this.form.reset();
  }

  public onRemoveCharges() {
    this.form.reset();
    this.form.get('name').setValue(OFFER_PRIORIZATION_DEFAULT.INTERESTS.NAME);
    this.removeInterest.emit();
  }

  private _setInitialValues() {
    this.fixedOrRelativeDesingClass = SETTINGS_ITEM_DESIGN_CLASS.DEFAULT;
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      interest: [this.offerInterestCharge?.interest ?? '', Validators.required],
      type_of_interest: [
        this.offerInterestCharge?.type_of_interest ?? '',
        Validators.required,
      ],
      tax: [this.offerInterestCharge?.tax ?? '', Validators.required],
      tax_fee: [this.offerInterestCharge?.tax_fee ?? '', Validators.required],
      periodicity: [
        this.offerInterestCharge?.periodicity ?? '',
        Validators.required,
      ],
      fixed_or_relative: [
        this.offerInterestCharge?.fixed_or_relative ?? '',
        Validators.required,
      ],
      grace_period: [
        this.offerInterestCharge?.grace_period ?? '',
        Validators.required,
      ],
      grace_period_days: [
        this.offerInterestCharge?.grace_period_days ?? '',
        Validators.required,
      ],
      name: [
        this.offerInterestCharge?.name ??
          OFFER_PRIORIZATION_DEFAULT.INTERESTS.NAME,
        Validators.required,
      ],
    });

    this._setDynamicFormValidations();
  }

  private _setDynamicFormValidations() {
    this.form
      .get('periodicity')
      .valueChanges.subscribe((periodicity: boolean) => {
        const control = this.form.get('fixed_or_relative');
        if (String(periodicity) === 'Daily') {
          control.disable();
          control.removeValidators(Validators.required);
          control.setValue(null);
          this.fixedOrRelativeDesingClass = SETTINGS_ITEM_DESIGN_CLASS.DISABLED;
        } else {
          this.fixedOrRelativeDesingClass = SETTINGS_ITEM_DESIGN_CLASS.DEFAULT;
          control.enable();
          control.addValidators(Validators.required);
        }
        this.form.updateValueAndValidity();
      });

    this.form.get('tax').valueChanges.subscribe((tax: boolean) => {
      const control = this.form.get('tax_fee');
      if (String(tax) !== 'Yes') {
        control.removeValidators(Validators.required);
        control.setValue(null);
      } else {
        control.addValidators(Validators.required);
      }
      this.form.updateValueAndValidity();
    });

    this.form
      .get('grace_period')
      .valueChanges.subscribe((grace_period: boolean) => {
        const control = this.form.get('grace_period_days');
        if (String(grace_period) !== 'Yes') {
          control.removeValidators(Validators.required);
          control.setValue(null);
        } else {
          control.addValidators(Validators.required);
        }
        this.form.updateValueAndValidity();
      });
  }
}
