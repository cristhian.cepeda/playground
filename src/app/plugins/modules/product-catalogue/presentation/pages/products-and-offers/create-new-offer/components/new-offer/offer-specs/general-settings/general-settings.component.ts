import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { OfferGeneralSettings } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'offer-specs-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss'],
})
export class GeneralSettingsComponent implements OnInit {
  @Input() offerGeneralSettings: OfferGeneralSettings;
  @Output() submitGeneralSettings: EventEmitter<OfferGeneralSettings> =
    new EventEmitter<OfferGeneralSettings>();

  public form: UntypedFormGroup;

  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public INPUT_TYPE = INPUT_TYPE;

  constructor(private _formBuilder: UntypedFormBuilder) {}

  ngOnInit(): void {
    this._setForm();
  }

  public onSubmit() {
    if (this.form.invalid) return;
    this.submitGeneralSettings.emit(this.form.value);
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      name: [this.offerGeneralSettings?.name ?? '', Validators.required],
      description: [
        this.offerGeneralSettings?.description ?? '',
        Validators.required,
      ],
      max_active_credits: [
        this.offerGeneralSettings?.max_active_credits ?? null,
        Validators.required,
      ], // Si no tiene valor, es limitless
      limitless: [
        this.offerGeneralSettings?.limitless ?? false,
        Validators.required,
      ],
      file: [this.offerGeneralSettings?.file ?? null, Validators.required],
      file_name: [
        this.offerGeneralSettings?.file_name ?? '',
        Validators.required,
      ],
    });

    this.form.controls['file'].valueChanges.subscribe((data) => {
      this.form.controls['file_name'].setValue(data?.name ? data?.name : '');
    });

    this.form
      .get('max_active_credits')
      .valueChanges.subscribe((max_active_credits: number) => {
        if (max_active_credits) {
          this.form.get('limitless').setValue(false);
        }
      });

    this.form.get('limitless').valueChanges.subscribe((limitless: boolean) => {
      const control = this.form.get('max_active_credits');
      if (limitless) {
        control.removeValidators(Validators.required);
        control.setValue(null);
      } else {
        control.addValidators(Validators.required);
      }
      this.form.updateValueAndValidity();
    });
  }
}
