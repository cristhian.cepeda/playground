import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';
import { RepaymentLogicComponent } from './repayment-logic/repayment-logic.component';
import { PriorizationComponent } from './priorization/priorization.component';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { SpecsComponent } from './specs/specs.component';
import { ChargesModule } from './charges/charges.module';
import { OfferSpecsRountingModule } from './offer-specs-routing.module';

const COMPONENTS = [
  GeneralSettingsComponent,
  RepaymentLogicComponent,
  PriorizationComponent,
  SpecsComponent,
];
@NgModule({
  declarations: COMPONENTS,
  exports: COMPONENTS,
  imports: [
    CommonModule,
    OfferSpecsRountingModule,
    ChargesModule,
    LayoutModule,
  ],
})
export class OfferSpecsModule {}
