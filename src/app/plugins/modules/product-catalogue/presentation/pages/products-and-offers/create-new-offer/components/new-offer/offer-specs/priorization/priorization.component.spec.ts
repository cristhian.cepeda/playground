import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PriorizationComponent } from './priorization.component';

describe('PriorizationComponent', () => {
  let component: PriorizationComponent;
  let fixture: ComponentFixture<PriorizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PriorizationComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
