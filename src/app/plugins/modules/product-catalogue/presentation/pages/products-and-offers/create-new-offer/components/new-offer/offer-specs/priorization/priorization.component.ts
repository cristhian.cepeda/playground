import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  OFFER_PRIORIZATION_DEFAULT,
  OFFER_PRIORIZATION_DEFAULT_DYNAMIC,
  OFFER_PRIORIZATION_TYPE_DYNAMIC,
} from '@app/core/constants/offer.constants';
import { OfferCharges } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'offer-specs-priorization',
  templateUrl: './priorization.component.html',
  styleUrls: ['./priorization.component.scss'],
})
export class PriorizationComponent implements OnInit, OnChanges {
  @Input() charges: OfferCharges;
  @Output() goBack: EventEmitter<void> = new EventEmitter<void>();
  @Output() continue: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteCharge: EventEmitter<void> = new EventEmitter<void>();

  public chargeList: any[] = [];
  public isDisabledRestartOrder: boolean = true;

  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['charges']) {
      this._setDefaulPriorization();
    }
  }

  ngOnInit(): void {}

  public onContinue() {
    this.continue.emit(this.chargeList);
  }

  public onUpdatePriorization(
    newPriorization: number,
    chargeIndex: number
  ): void {
    this.isDisabledRestartOrder = false;
    this.chargeList = this.chargeList.map((charge, index) => {
      if (index === chargeIndex) charge.priorization = newPriorization;
      return charge;
    });
  }

  public onDeleteCharge(chargeIndex: number) {
    this.isDisabledRestartOrder = false;
    this.chargeList.splice(chargeIndex, 1);
    this._sortChargesByPriority();
  }

  public onRestartOrder() {
    if (!this.isDisabledRestartOrder) {
      this.isDisabledRestartOrder = true;
      this._setDefaulPriorization();
    }
  }

  public onGoBack() {
    this.goBack.emit();
  }

  private _setDefaulPriorization() {
    this.chargeList = this.charges
      ? Object.keys(this.charges).map((key) => this.charges[key])
      : [];

    const capitalChargeIndex = this.chargeList.findIndex(
      (charge) => charge.name === OFFER_PRIORIZATION_DEFAULT.CAPITAL.NAME
    );

    if (capitalChargeIndex === -1) {
      this.chargeList.push({ name: OFFER_PRIORIZATION_DEFAULT.CAPITAL.NAME });
    }

    this._sortChargesByPriority();
  }
  private _sortChargesByPriority() {
    this.chargeList = this.chargeList
      .sort((a, b) => {
        if (
          OFFER_PRIORIZATION_DEFAULT_DYNAMIC[a?.name] >
          OFFER_PRIORIZATION_DEFAULT_DYNAMIC[b?.name]
        )
          return 1;
        if (
          OFFER_PRIORIZATION_DEFAULT_DYNAMIC[a?.name] <
          OFFER_PRIORIZATION_DEFAULT_DYNAMIC[b?.name]
        )
          return -1;
        return 0;
      })
      .map((charge) => ({
        ...charge,
        chargeType: OFFER_PRIORIZATION_TYPE_DYNAMIC[charge?.name],
        priorization:
          OFFER_PRIORIZATION_DEFAULT_DYNAMIC[charge?.name] <=
          this.chargeList.length
            ? OFFER_PRIORIZATION_DEFAULT_DYNAMIC[charge?.name]
            : this.chargeList.length,
        canBeDeleted: charge?.name !== OFFER_PRIORIZATION_DEFAULT.CAPITAL.NAME,
      }));
  }
}
