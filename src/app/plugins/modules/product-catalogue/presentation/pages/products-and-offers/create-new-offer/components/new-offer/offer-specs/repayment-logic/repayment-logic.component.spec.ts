import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';

import { RepaymentLogicComponent } from './repayment-logic.component';

describe('RepaymentLogicComponent', () => {
  let component: RepaymentLogicComponent;
  let fixture: ComponentFixture<RepaymentLogicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RepaymentLogicComponent],
      providers: [UntypedFormBuilder],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepaymentLogicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
