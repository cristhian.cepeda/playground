import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { DemoOfferRepaymentLogic } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import {
  CARD_CONTAINER_DESIGN_CLASS,
  SETTINGS_ITEM_DESIGN_CLASS,
} from '@app/presentation/layout/enums/layout.enum';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'offer-specs-repayment-logic',
  templateUrl: './repayment-logic.component.html',
  styleUrls: ['./repayment-logic.component.scss'],
})
export class RepaymentLogicComponent implements OnInit {
  @Input() offerRepaymentLogic: DemoOfferRepaymentLogic;
  @Output() submitRepayment: EventEmitter<DemoOfferRepaymentLogic> =
    new EventEmitter<DemoOfferRepaymentLogic>();

  public form: UntypedFormGroup;
  public termOfCredit: {
    designClass?: SETTINGS_ITEM_DESIGN_CLASS;
    text?: string;
  };

  public CARD_CONTAINER_DESIGN_CLASS = CARD_CONTAINER_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SETTINGS_ITEM_DESIGN_CLASS = SETTINGS_ITEM_DESIGN_CLASS;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;

  constructor(private _formBuilder: UntypedFormBuilder) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setForm();
  }

  public onSubmit() {
    if (this.form.invalid) return;
    this.submitRepayment.emit(this.form.value);
  }

  private _setInitialValues() {
    this.termOfCredit = {
      designClass: SETTINGS_ITEM_DESIGN_CLASS.ALTERNATIVE_DARK,
      text: '-',
    };
  }

  private _setForm() {
    this.form = this._formBuilder.group({
      repayment_type: [
        this.offerRepaymentLogic?.repayment_type ?? 'Installment',
        Validators.required,
      ],
      periodicity: [
        this.offerRepaymentLogic?.periodicity ?? '',
        Validators.required,
      ],
      calendar_logic: [
        this.offerRepaymentLogic?.calendar_logic ?? '',
        Validators.required,
      ],
      fixed_or_relative: [
        this.offerRepaymentLogic?.fixed_or_relative ?? '',
        Validators.required,
      ],
      day_of_week: [
        this.offerRepaymentLogic?.day_of_week ?? '',
        Validators.required,
      ],
      number_of_installments: [
        this.offerRepaymentLogic?.number_of_installments ?? '',
        Validators.required,
      ],
      type_of_installment: [
        this.offerRepaymentLogic?.type_of_installment ?? '',
        Validators.required,
      ],
      missed_repayment_logic: [
        this.offerRepaymentLogic?.missed_repayment_logic ?? '',
        Validators.required,
      ],
      grace_period: [
        this.offerRepaymentLogic?.grace_period ?? '',
        Validators.required,
      ],
      grace_period_days: [
        this.offerRepaymentLogic?.grace_period_days ?? '',
        Validators.required,
      ],
    });

    this._setDynamicFormValidations();
  }

  private _setDynamicFormValidations() {
    this.form
      .get('fixed_or_relative')
      .valueChanges.subscribe((fixed_or_relative: boolean) => {
        const control = this.form.get('day_of_week');
        if (String(fixed_or_relative) !== 'Fixed') {
          control.removeValidators(Validators.required);
          control.setValue(null);
        } else {
          control.addValidators(Validators.required);
        }
        this.form.updateValueAndValidity();
      });

    this.form
      .get('grace_period')
      .valueChanges.subscribe((grace_period: boolean) => {
        const control = this.form.get('grace_period_days');
        if (String(grace_period) !== 'Yes') {
          control.removeValidators(Validators.required);
          control.setValue(null);
        } else {
          control.addValidators(Validators.required);
        }
        this.form.updateValueAndValidity();
      });

    // TermOfCredit Validation
    combineLatest([
      this.form.get('periodicity').valueChanges,
      this.form.get('number_of_installments').valueChanges,
    ]).subscribe(([periodicity, installments]) => {
      this._setTermOfCredit(periodicity, installments);
    });
  }

  private _setTermOfCredit(periodicity: string, installments: number) {
    if (!periodicity || !installments)
      this.termOfCredit = {
        designClass: SETTINGS_ITEM_DESIGN_CLASS.ALTERNATIVE_DARK,
        text: '-',
      };
    else {
      const text = {
        ['Weekly']: `${installments} Weeks`,
        ['Bi-weekly']: `${installments * 2} Weeks`,
        ['Monthly']: `${installments} Months`,
      };
      this.termOfCredit = {
        designClass: SETTINGS_ITEM_DESIGN_CLASS.PRIMARY,
        text: text[periodicity],
      };
    }
  }
}
