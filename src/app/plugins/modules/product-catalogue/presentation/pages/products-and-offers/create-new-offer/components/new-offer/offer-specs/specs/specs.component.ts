import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  OFFER_SPECS_STEPS,
  OFFER_SPECS_TAPS,
} from '@app/core/constants/offer.constants';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import {
  DemoOfferRepaymentLogic,
  OfferCharges,
  OfferGeneralSettings,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { first, Observable } from 'rxjs';

@Component({
  selector: 'offer-specs',
  templateUrl: './specs.component.html',
  styleUrls: ['./specs.component.scss'],
})
export class SpecsComponent implements OnInit {
  public newOfferStep$: Observable<number>;
  public offerGeneralSettings: OfferGeneralSettings;
  public offerRepaymentLogic: DemoOfferRepaymentLogic;
  public charges: OfferCharges;

  public tabs: string[];

  constructor(private _offerFacade: OfferFacade, private _router: Router) {}

  ngOnInit(): void {
    this.newOfferStep$ = this._offerFacade.newOfferStep$;
    this.tabs = OFFER_SPECS_TAPS;

    this._offerFacade.getOfferSpecs().subscribe((offerSpecs) => {
      this.offerGeneralSettings = offerSpecs?.generalSettings;
      this.offerRepaymentLogic = offerSpecs?.repaymentLogic;
    });
  }

  public onSendGeneralSettings(generalSettingData: OfferGeneralSettings) {
    this._offerFacade.sendGeneralSettings(generalSettingData);
    this._offerFacade.updateNewOfferStep(
      this.tabs.indexOf(OFFER_SPECS_STEPS.REPAYMENT_LOGIC)
    );
  }
  public onSendRepaymentLogic(repaymentLogic: DemoOfferRepaymentLogic) {
    this._offerFacade.sendRepaymentLogic(repaymentLogic);
    this._offerFacade.updateNewOfferStep(
      this.tabs.indexOf(OFFER_SPECS_STEPS.CHARGES)
    );
  }

  public onTabChange(tabIndex) {
    this._offerFacade.updateNewOfferStep(tabIndex);
    const priorizationIndex = this.tabs.indexOf(OFFER_SPECS_STEPS.PRIORIZATION);
    if (tabIndex === priorizationIndex)
      this._offerFacade.getCharges().subscribe((charges) => {
        this.charges = charges;
      });
  }

  public onGoBack() {
    this.newOfferStep$
      .pipe(first())
      .subscribe((currentStep) =>
        this._offerFacade.updateNewOfferStep(--currentStep)
      );
  }

  public onContinuePriorization(prioritizedCharge) {
    const charges = this._mapPrioritizedCharge(prioritizedCharge);
    this._offerFacade.sendCharges(charges);
    this._router.navigateByUrl(
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.OFFER.CREATE_NEW_OFFER_SUMMARY
    );
  }
  private _mapPrioritizedCharge(prioritizedCharge): any {
    let chargeSorted = {};
    prioritizedCharge
      .sort((a, b) => a.priorization - b.priorization)
      .forEach((charge) => {
        chargeSorted = { ...chargeSorted, [charge.chargeType]: charge };
      });
    return chargeSorted;
  }
}
