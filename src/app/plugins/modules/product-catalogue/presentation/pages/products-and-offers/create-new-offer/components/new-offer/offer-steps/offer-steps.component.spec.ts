import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferStepsComponent } from './offer-steps.component';

describe('OfferStepsComponent', () => {
  let component: OfferStepsComponent;
  let fixture: ComponentFixture<OfferStepsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OfferStepsComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
