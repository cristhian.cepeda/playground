import { Component, OnInit } from '@angular/core';
import { IKeyValue } from '@app/core/models/utils.model';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import { PRODUCT } from '@app/plugins/modules/product-catalogue/data/mocks/products.mock';

@Component({
  selector: 'offer-steps',
  templateUrl: './offer-steps.component.html',
  styleUrls: ['./offer-steps.component.scss'],
})
export class OfferStepsComponent implements OnInit {
  public steps: IKeyValue[];
  public product;

  constructor() {}

  ngOnInit(): void {
    const OFFER_STEPS_ROUTES = PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS;
    this.product = PRODUCT;

    this.steps = [
      {
        key: '1. Offer specs',
        value: OFFER_STEPS_ROUTES.OFFER.CREATE_NEW_OFFER_SPECS,
      },
      {
        key: '2. Offer summary',
        value: OFFER_STEPS_ROUTES.OFFER.CREATE_NEW_OFFER_SUMMARY,
      },
    ];
  }
}
