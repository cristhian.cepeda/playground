import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { MODatePipe } from '@app/presentation/layout/pipes/date.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { OfferSummaryComponent } from './offer-summary.component';

describe('OfferSummaryComponent', () => {
  let component: OfferSummaryComponent;
  let fixture: ComponentFixture<OfferSummaryComponent>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [OfferSummaryComponent],
      providers: [
        CustomCurrencyPipe,
        MODatePipe,
        provideMockStore({ initialState }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
