import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import {
  DemoOfferRepaymentLogic,
  OfferCharges,
  OfferCollectionCostCharge,
  OfferCostCharge,
  OfferDefaultInterestCharge,
  OfferGeneralSettings,
  OfferInterestCharge,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { MODatePipe } from '@app/presentation/layout/pipes/date.pipe';
import { Subscription } from 'rxjs';

@Component({
  selector: 'offer-summary',
  templateUrl: './offer-summary.component.html',
  styleUrls: ['./offer-summary.component.scss'],
})
export class OfferSummaryComponent implements OnInit, OnDestroy {
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public generalSettingItems: SummaryGroup[];
  public repaymentLogics: SummaryGroup[];
  public chargesCollection: SummaryGroup[];
  public offerName: string;

  private _getOfferSpecsSubscription: Subscription;

  constructor(
    private _offerFacade: OfferFacade,
    private _currency: CustomCurrencyPipe,
    private _date: MODatePipe,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.setInitialValues();
  }

  ngOnDestroy(): void {
    this._getOfferSpecsSubscription.unsubscribe();
  }

  public onCreateOffer() {
    this._router.navigateByUrl(
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.OFFER.CREATE_NEW_OFFER_SUCCESS
    );
  }

  public onDiscard() {
    this._router.navigateByUrl(PRODUCT_CATALOGUE_URLS.ROOT_PATH);
  }
  public onBack() {
    this._router.navigateByUrl(
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.OFFER.CREATE_NEW_OFFER_SPECS
    );
  }

  private setInitialValues(): void {
    this.chargesCollection = [];
    this._getOfferSpecsSubscription = this._offerFacade
      .getOfferSpecs()
      .subscribe((offerSpecs) => {
        const { generalSettings, repaymentLogic, charges } = offerSpecs;
        this.offerName = generalSettings?.name;
        this._setGeneralSettingsItems(generalSettings);
        this._setRepaymentLogicItems(repaymentLogic);
        this._setChargesItems(charges);
      });
  }

  private _setGeneralSettingsItems(generalSettings: OfferGeneralSettings) {
    this.generalSettingItems = [
      {
        key: 'Maximum number of the active credits:',
        value: generalSettings?.limitless
          ? 'Limitless'
          : String(generalSettings?.max_active_credits),
      },
      {
        key: 'Offer documents:',
        value: generalSettings?.file_name,
      },
    ];
  }

  private _setRepaymentLogicItems(repaymentLogic: DemoOfferRepaymentLogic) {
    this.repaymentLogics = [
      {
        key: 'Repayment type:',
        value: repaymentLogic?.repayment_type,
      },
      {
        key: 'Periodicity:',
        value: repaymentLogic?.periodicity,
      },
      {
        key: 'Calendar logic:',
        value: repaymentLogic?.calendar_logic,
      },
      {
        key: 'Fixed or relative:',
        value: repaymentLogic?.fixed_or_relative,
        extraData: repaymentLogic?.day_of_week
          ? [
              {
                key: 'Day of the week:',
                value: repaymentLogic?.day_of_week,
              },
            ]
          : [],
      },
      {
        key: 'Number of installments:',
        value: String(repaymentLogic?.number_of_installments),
      },
      {
        key: 'Term of credit',
        value: this._setTermOfCredit(
          repaymentLogic?.periodicity,
          repaymentLogic?.number_of_installments
        ),
      },
      {
        key: 'Type of installment:',
        value: repaymentLogic?.type_of_installment,
      },
      {
        key: 'Missed repayment logic:',
        value: repaymentLogic?.missed_repayment_logic,
      },
      {
        key: 'Grace period:',
        value: repaymentLogic?.grace_period,
        extraData: repaymentLogic?.grace_period_days
          ? [
              {
                key: 'Number of days:',
                value: String(repaymentLogic?.grace_period_days),
              },
            ]
          : [],
      },
    ];
  }

  private _setChargesItems(charges: OfferCharges) {
    const { interest, cost, collectionCost, defaultInterest, capital } =
      charges;
    const interestCharge = this._setInterestChargeItems(interest);
    const costCharge = this._setCostChargeItems(cost);
    const collectionCostCharge =
      this._setCollectionCostChargeItems(collectionCost);
    const defaultInterestCharge =
      this._setDefaultInterestChargeItems(defaultInterest);
    const capitalCharge = this._setCapitalChargeItems();

    const chargesItems = {
      interestCharge,
      costCharge,
      collectionCostCharge,
      defaultInterestCharge,
      capitalCharge,
    };

    this._sortChargesByPriority(charges, chargesItems);
  }

  private _sortChargesByPriority(charges: OfferCharges, chargesItems) {
    const { interest, cost, collectionCost, defaultInterest, capital } =
      charges;

    const {
      interestCharge,
      costCharge,
      collectionCostCharge,
      defaultInterestCharge,
      capitalCharge,
    } = chargesItems;

    [
      {
        priorization: interest?.priorization,
        items: interestCharge,
      },
      { priorization: cost?.priorization, items: costCharge },
      {
        priorization: collectionCost?.priorization,
        items: collectionCostCharge,
      },
      {
        priorization: defaultInterest?.priorization,
        items: defaultInterestCharge,
      },
      {
        priorization: capital?.priorization,
        items: capitalCharge,
      },
    ]
      .sort((a, b) => a.priorization - b.priorization)
      .map(({ items }) => {
        this.chargesCollection = [...this.chargesCollection, ...items];
      });
  }

  private _setCapitalChargeItems(): SummaryGroup[] {
    return [
      {
        key: 'Capital',
        type: 'title',
      },
      {
        key: 'Down payment:',
        value: 'No',
      },
    ];
  }

  private _setInterestChargeItems(
    interest: OfferInterestCharge
  ): SummaryGroup[] {
    return interest
      ? [
          {
            key: 'Interest',
            type: 'title',
          },
          {
            key: 'Interest value:',
            value: `${interest?.interest}%`,
          },
          {
            key: 'Tax:',
            value: interest?.tax,
            extraData: interest?.tax_fee
              ? [
                  {
                    key: 'Tax fee:',
                    value: `${interest?.tax_fee}%`,
                  },
                ]
              : [],
          },
          {
            key: 'Periodicity:',
            value: interest?.periodicity,
          },
          {
            key: 'Fixed or relative::',
            value: interest?.fixed_or_relative,
          },
          {
            key: 'Grace period:',
            value: interest?.grace_period,
            extraData: interest?.grace_period_days
              ? [
                  {
                    key: 'Number of days:',
                    value: String(interest?.grace_period_days),
                  },
                ]
              : [],
          },
        ]
      : [];
  }

  private _setCostChargeItems(cost?: OfferCostCharge): SummaryGroup[] {
    return cost
      ? [
          {
            key: 'Costs',
            type: 'title',
          },
          {
            key: cost?.name,
            type: 'subtitle',
          },
          {
            key: 'Fixed or percent',
            value: cost?.fixed_or_percent,
            extraData: cost?.fixed_or_percent_value
              ? [
                  {
                    key: `${cost?.fixed_or_percent}:`,
                    value: `${
                      cost?.fixed_or_percent === 'Fixed'
                        ? this._currency.transform(cost?.fixed_or_percent_value)
                        : cost?.fixed_or_percent_value + '%'
                    }`,
                  },
                ]
              : [],
          },
          {
            key: 'Tax:',
            value: cost?.tax,
            extraData: cost?.tax_fee
              ? [
                  {
                    key: 'Tax fee:',
                    value: `${cost?.tax_fee}%`,
                  },
                ]
              : [],
          },
        ]
      : [];
  }

  private _setCollectionCostChargeItems(
    collectionCost?: OfferCollectionCostCharge
  ): SummaryGroup[] {
    return collectionCost
      ? [
          {
            key: 'Collection costs',
            type: 'title',
          },
          {
            key: 'Trigger:',
            value: collectionCost?.trigger,
          },
          {
            key: 'Grace period:',
            value: collectionCost?.grace_period,
            extraData:
              collectionCost?.grace_period === 'Yes'
                ? [
                    {
                      key: 'Time unit:',
                      value: String(collectionCost?.grace_period_time_unit),
                    },
                    {
                      key: 'How many:',
                      value: String(collectionCost?.grace_period_time_number),
                    },
                  ]
                : [],
          },
          {
            key: 'Fixed or percent',
            value: collectionCost?.fixed_or_percent,
            extraData: [
              {
                key: `${collectionCost?.fixed_or_percent}:`,
                value: `${
                  collectionCost?.fixed_or_percent === 'Fixed'
                    ? this._currency.transform(
                        collectionCost?.fixed_or_percent_value
                      )
                    : collectionCost?.fixed_or_percent_value + '%'
                }`,
              },
              {
                key: 'Base:',
                value: collectionCost?.base_definition,
              },
            ],
          },

          {
            key: 'Tax:',
            value: collectionCost?.tax,
            extraData: collectionCost?.tax_fee
              ? [
                  {
                    key: 'Tax fee:',
                    value: `${collectionCost?.tax_fee}%`,
                  },
                ]
              : [],
          },
        ]
      : [];
  }

  private _setDefaultInterestChargeItems(
    defaultInterest?: OfferDefaultInterestCharge
  ): SummaryGroup[] {
    return defaultInterest
      ? [
          {
            key: 'Default interest',
            type: 'title',
          },
          {
            key: 'Trigger:',
            value: defaultInterest?.trigger,
          },
          {
            key: 'Interest value:',
            value: `${defaultInterest?.interest}%`,
          },
          {
            key: 'Tax:',
            value: defaultInterest?.tax,
            extraData: defaultInterest?.tax_fee
              ? [
                  {
                    key: 'Tax fee:',
                    value: `${defaultInterest?.tax_fee}%`,
                  },
                ]
              : [],
          },
          {
            key: 'Periodicity:',
            value: defaultInterest?.periodicity,
          },
          {
            key: 'Fixed or relative::',
            value: defaultInterest?.fixed_or_relative,
          },
          {
            key: 'Grace period:',
            value: defaultInterest?.grace_period,
            extraData: defaultInterest?.grace_period_days
              ? [
                  {
                    key: 'Number of days:',
                    value: String(defaultInterest?.grace_period_days),
                  },
                ]
              : [],
          },
        ]
      : [];
  }

  private _setTermOfCredit(periodicity: string, installments: number): string {
    const text = {
      ['Weekly']: `${installments} Weeks`,
      ['Bi-weekly']: `${installments * 2} Weeks`,
      ['Monthly']: `${installments} Months`,
    };
    return text[periodicity];
  }
}
