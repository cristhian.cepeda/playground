import { Component, Input } from '@angular/core';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';

@Component({
  selector: 'product-summary',
  templateUrl: './product-summary.component.html',
  styleUrls: ['./product-summary.component.scss'],
})
export class ProductSummaryComponent {
  @Input() product: Product;
}
