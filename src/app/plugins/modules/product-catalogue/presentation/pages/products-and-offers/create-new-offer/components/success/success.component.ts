import { Component, OnDestroy, OnInit } from '@angular/core';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Subscription } from 'rxjs';
@Component({
  selector: 'success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss'],
})
export class SuccessComponent implements OnInit, OnDestroy {
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public BUTTON_TYPE = BUTTON_TYPE;

  public productAndOffersUrl: string;
  public offerName: string;

  constructor(private _offerFacade: OfferFacade) {}
  private _getOfferSpecsSubscription: Subscription;

  ngOnInit(): void {
    this.setInitialValues();
  }

  ngOnDestroy(): void {
    this._getOfferSpecsSubscription.unsubscribe();
  }

  public onCleanOfferData() {
    this._offerFacade.cleanOfferData();
  }

  private setInitialValues(): void {
    this.productAndOffersUrl = PRODUCT_CATALOGUE_URLS.ROOT_PATH;

    this._getOfferSpecsSubscription = this._offerFacade
      .getOfferSpecs()
      .subscribe((offerSpecs) => {
        const { generalSettings } = offerSpecs;
        this.offerName = generalSettings?.name;
      });
  }
}
