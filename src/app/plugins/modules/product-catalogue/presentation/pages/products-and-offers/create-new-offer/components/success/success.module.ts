import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuccessRoutingModule } from './success-routing.module';
import { SuccessComponent } from './success.component';
import { LayoutModule } from '@app/presentation/layout/layout.module';

@NgModule({
  declarations: [SuccessComponent],
  imports: [CommonModule, SuccessRoutingModule, LayoutModule],
})
export class SuccessModule {}
