import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { CreateNewOfferPage } from './create-new-offer.page';

const routes: Routes = [
  {
    path: '',
    component: CreateNewOfferPage,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./components/new-offer/new-offer.module').then(
            (m) => m.NewOfferModule
          ),
      },
      {
        path: PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
          .CREATE_NEW_OFFER.SUB_PAGES.SUCCESS.PATH,
        loadChildren: () =>
          import('./components/success/success.module').then(
            (m) => m.SuccessModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateNewOfferPageRoutingModule {}
