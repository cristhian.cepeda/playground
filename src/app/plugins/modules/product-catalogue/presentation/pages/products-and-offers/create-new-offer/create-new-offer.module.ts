import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { CreateNewOfferPageRoutingModule } from './create-new-offer-routing.module';
import { CreateNewOfferPage } from './create-new-offer.page';

@NgModule({
  declarations: [CreateNewOfferPage],
  imports: [CommonModule, CreateNewOfferPageRoutingModule, LayoutModule],
})
export class CreateNewOfferPageModule {}
