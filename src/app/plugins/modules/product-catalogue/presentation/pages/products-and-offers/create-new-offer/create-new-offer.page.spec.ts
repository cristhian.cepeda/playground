import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewOfferPage } from './create-new-offer.page';

describe('CreateNewOfferPage', () => {
  let component: CreateNewOfferPage;
  let fixture: ComponentFixture<CreateNewOfferPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateNewOfferPage],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
