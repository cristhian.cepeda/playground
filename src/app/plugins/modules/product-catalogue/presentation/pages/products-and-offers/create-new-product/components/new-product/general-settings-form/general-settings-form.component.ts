import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import {
  CLOSING_TOLERANCE_LABEL,
  CLOSING_TOLERANCE_TYPE,
  ROUND_WAY,
  ROUND_WAY_LABEL,
} from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  COLOR_TEMPLATE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { SelectOption } from '@app/presentation/layout/mo-forms/interfaces/form.interface';
import { filter } from 'rxjs';

@Component({
  selector: 'general-settings-form',
  templateUrl: './general-settings-form.component.html',
  styleUrls: ['./general-settings-form.component.scss'],
})
export class GeneralSettingsFormComponent implements OnInit {
  public form: UntypedFormGroup;
  public nameOptions: SelectOption<string | number>[] = [
    {
      key: 'COP',
      value: 'Option 1',
    },
  ];
  public COLOR_TEMPLATE = COLOR_TEMPLATE;
  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public INPUT_TYPE = INPUT_TYPE;
  public urlCreateNewProductSummary =
    PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.PRODUCT
      .CREATE_NEW_PRODUCT_SUMMARY;
  public CLOSING_TOLERANCE_TYPE = CLOSING_TOLERANCE_TYPE;
  public CLOSING_TOLERANCE_LABEL = CLOSING_TOLERANCE_LABEL;
  public ROUND_WAY = ROUND_WAY;
  public ROUND_WAY_LABEL = ROUND_WAY_LABEL;
  public nextUrl =
    PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.PRODUCT
      .CREATE_NEW_PRODUCT_SUMMARY;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _productFacade: ProductFacade
  ) {}

  ngOnInit(): void {
    this._setForm();
  }

  public onClick(): void {
    // if (this.form.invalid) return;
    this._productFacade.onUpdateNewProduct(this.form.value);
  }

  private _setForm(): void {
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      currency: ['', Validators.required],
      description: ['', Validators.required],
      limitless: [false, Validators.required],
      max_number_active_loans: ['', Validators.required],
      max_loan_amount: ['', Validators.required],
      min_loan_amount: ['', Validators.required],
      closing_tolerance_type: ['', Validators.required],
      closing_tolerance_value: ['', Validators.required],
      decimal_round: ['', Validators.required],
      round_to: ['', Validators.required],
      amount_requested_step: ['', Validators.required],
      requiere_down_payment: [null, Validators.required],
    });

    this.form
      .get('max_number_active_loans')
      .valueChanges.pipe(
        filter((max_number_active_loans) => !!max_number_active_loans)
      )
      .subscribe((max_number_active_loans: number) => {
        this.form.get('limitless').setValue(false);
      });

    this.form.get('limitless').valueChanges.subscribe((limitless: boolean) => {
      const control = this.form.get('max_number_active_loans');
      if (limitless) {
        control.removeValidators(Validators.required);
        control.setValue('');
      } else {
        control.addValidators(Validators.required);
      }
      this.form.updateValueAndValidity();
    });
  }
}
