import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { GeneralSettingsFormComponent } from './general-settings-form/general-settings-form.component';
import { ProductStepsComponent } from './product-steps/product-steps.component';
import { SummaryComponent } from './summary/summary.component';

const PRODUCTS_STEPS_ROUTES =
  PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
    .CREATE_NEW_PRODUCT.SUB_PAGES;

const routes: Routes = [
  {
    path: '',
    component: ProductStepsComponent,
    children: [
      {
        path: '',
        redirectTo: PRODUCTS_STEPS_ROUTES.FORM.PATH,
        pathMatch: 'full',
      },
      {
        path: PRODUCTS_STEPS_ROUTES.FORM.PATH,
        component: GeneralSettingsFormComponent,
      },
      {
        path: PRODUCTS_STEPS_ROUTES.SUMMARY.PATH,
        component: SummaryComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewProductRoutingModule {}
