import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { GeneralSettingsFormComponent } from './general-settings-form/general-settings-form.component';
import { NewProductRoutingModule } from './new-product-routing.module';
import { ProductStepsComponent } from './product-steps/product-steps.component';
import { SummaryComponent } from './summary/summary.component';

@NgModule({
  declarations: [
    ProductStepsComponent,
    GeneralSettingsFormComponent,
    SummaryComponent,
  ],
  imports: [CommonModule, NewProductRoutingModule, LayoutModule],
})
export class NewProductModule {}
