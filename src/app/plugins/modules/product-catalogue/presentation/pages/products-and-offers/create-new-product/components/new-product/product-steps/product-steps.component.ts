import { Component, OnInit } from '@angular/core';
import { IKeyValue } from '@app/core/models/utils.model';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';

@Component({
  selector: 'app-product-steps',
  templateUrl: './product-steps.component.html',
  styleUrls: ['./product-steps.component.scss'],
})
export class ProductStepsComponent implements OnInit {
  public tabs = ['1. General settings', '2. Summary'];
  public steps: IKeyValue[];

  constructor() {}

  ngOnInit(): void {
    const PRODUCTS_STEPS_ROUTES = PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS;

    this.steps = [
      {
        key: '1. General settings',
        value: PRODUCTS_STEPS_ROUTES.PRODUCT.CREATE_NEW_PRODUCT_FORM,
      },
      {
        key: '2. Summary',
        value: PRODUCTS_STEPS_ROUTES.PRODUCT.CREATE_NEW_PRODUCT_SUMMARY,
      },
    ];
  }
}
