import { Component, OnInit } from '@angular/core';
import {
  CLOSING_TOLERANCE_LABEL,
  Product,
  ROUND_WAY_LABEL_BY_VALUE,
} from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { filter, map, Observable } from 'rxjs';

@Component({
  selector: 'summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public BUTTON_TYPE = BUTTON_TYPE;
  public summaryItems$: Observable<{ value: string | number; name: string }[]>;

  constructor(private _productFacade: ProductFacade) {}

  ngOnInit(): void {
    this._setSummaryItems();
  }

  private _setSummaryItems(): void {
    this.summaryItems$ = this._productFacade.newProduct$.pipe(
      filter((product: Product) => !!product),
      map((product: Product) => {
        // TODO VALIDATE WHEN IT'S LIMITLESS
        // TODO MOVE TO A SERVICE WHEN DEFINED
        const maxNumberActiveLoans = product?.maximum_number_active_loans
          ? 'Limitless'
          : product.maximum_number_active_loans;
        ////
        const customCurrency = new CustomCurrencyPipe();
        const minLoanAmount = customCurrency.transform(
          product.minimum_loan_amount
        );
        const maxLoanAmount = customCurrency.transform(
          product.maximum_loan_amount
        );

        const closingToleranceLabel = product.closing_tolerance_amount
          ? CLOSING_TOLERANCE_LABEL.FIXED_AMOUNT
          : CLOSING_TOLERANCE_LABEL.PERCENTAGE;

        const closingToleranceValue = customCurrency.transform(
          product?.closing_tolerance_amount ??
            product?.closing_tolerance_percentage
        );
        const rounWayLabel =
          ROUND_WAY_LABEL_BY_VALUE[product?.decimal_round_way];
        const amountRequestedStep = customCurrency.transform(
          product.amount_requested_step
        );

        const summaryItems = [
          {
            name: 'Maximum number of active loans:',
            value: maxNumberActiveLoans,
          },
          {
            name: 'Loan amounts:',
            value: `Min. ${minLoanAmount}; Max. ${maxLoanAmount}`,
          },
          {
            name: 'Closing tolerance:',
            value: `${closingToleranceLabel} ${closingToleranceValue}`,
          },
          {
            name: 'Decimals:',
            value: `${product.decimal_places} decimals; ${rounWayLabel}`,
          },
          {
            name: 'Amount requested step:',
            value: `Nearest multiple of ${amountRequestedStep}`,
          },
        ];

        return summaryItems;
      })
    );
  }
}
