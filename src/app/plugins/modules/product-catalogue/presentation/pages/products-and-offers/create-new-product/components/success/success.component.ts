import { Component, OnInit } from '@angular/core';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss'],
})
export class SuccessComponent implements OnInit {
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public BUTTON_TYPE = BUTTON_TYPE;

  public urlProductCatalog: string;
  public urlCreateNewOffer: string;

  constructor() {}

  ngOnInit(): void {
    this.urlCreateNewOffer =
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.OFFER.CREATE_NEW_OFFER;
    this.urlProductCatalog = PRODUCT_CATALOGUE_URLS.ROOT_PATH;
  }
}
