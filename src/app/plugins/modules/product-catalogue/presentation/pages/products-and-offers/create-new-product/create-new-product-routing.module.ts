import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { SuccessComponent } from './components/success/success.component';
import { CreateNewProductPage } from './create-new-product.page';

const routes: Routes = [
  {
    path: '',
    component: CreateNewProductPage,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./components/new-product/new-product.module').then(
            (m) => m.NewProductModule
          ),
      },
      {
        path: PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
          .CREATE_NEW_PRODUCT.SUB_PAGES.SUCCESS.PATH,
        component: SuccessComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateNewProductRoutingModule {}
