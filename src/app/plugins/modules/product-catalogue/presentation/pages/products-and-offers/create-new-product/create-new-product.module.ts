import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { SuccessComponent } from './components/success/success.component';
import { CreateNewProductRoutingModule } from './create-new-product-routing.module';
import { CreateNewProductPage } from './create-new-product.page';

@NgModule({
  declarations: [CreateNewProductPage, SuccessComponent],
  imports: [CommonModule, CreateNewProductRoutingModule, LayoutModule],
})
export class CreateNewProductPageModule {}
