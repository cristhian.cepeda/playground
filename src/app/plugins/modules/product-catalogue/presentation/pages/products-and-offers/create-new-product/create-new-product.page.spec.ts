import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateNewProductPage } from './create-new-product.page';

describe('CreateNewProductComponent', () => {
  let component: CreateNewProductPage;
  let fixture: ComponentFixture<CreateNewProductPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateNewProductPage],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewProductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
