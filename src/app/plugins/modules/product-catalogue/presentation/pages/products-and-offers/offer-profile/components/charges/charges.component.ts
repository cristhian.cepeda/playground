import { Component, OnInit } from '@angular/core';
import { SummaryDropItem } from '@app/core/models/summary-group.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'offer-profile-charges',
  templateUrl: './charges.component.html',
  styleUrls: ['./charges.component.scss'],
})
export class ChargesComponent implements OnInit {
  public isLoading$: Observable<boolean>;
  public chargesItems$: Observable<SummaryDropItem[]>;
  constructor(private _offerFacade: OfferFacade) {}

  ngOnInit(): void {
    this._offerFacade.getOfferCharges();

    this.isLoading$ = this._offerFacade.isLoadingOfferCharges$;
    this.chargesItems$ = this._offerFacade.offerProfileChargesItems$;
  }
}
