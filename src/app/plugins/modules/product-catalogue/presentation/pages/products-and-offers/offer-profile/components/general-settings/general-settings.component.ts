import { Component, OnInit } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'offer-profile-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss'],
})
export class GeneralSettingsComponent implements OnInit {
  public isLoading$: Observable<boolean>;
  public generalSettingsItems$: Observable<SummaryGroup[]>;

  constructor(private _offerFacade: OfferFacade) {}

  ngOnInit(): void {
    this.isLoading$ = this._offerFacade.isLoadingOffer$;
    this.generalSettingsItems$ =
      this._offerFacade.offerProfileGeneralSettingsItems$;
  }
}
