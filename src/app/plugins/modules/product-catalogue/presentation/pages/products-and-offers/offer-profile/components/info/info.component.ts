import { Component, OnInit } from '@angular/core';
import {
  Offer,
  OFFER_STATUS_ICONS,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'offer-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  public offer$: Observable<Offer>;
  public isLoadingOffer$: Observable<boolean>;

  public OFFER_STATUS_ICONS = OFFER_STATUS_ICONS;

  constructor(private _offerFacade: OfferFacade) {}

  ngOnInit(): void {
    this.offer$ = this._offerFacade.offer$;
    this.isLoadingOffer$ = this._offerFacade.isLoadingOffer$;
  }
}
