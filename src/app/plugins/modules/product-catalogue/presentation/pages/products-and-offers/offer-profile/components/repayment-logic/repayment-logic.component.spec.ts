import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepaymentLogicComponent } from './repayment-logic.component';

describe('RepaymentLogicComponent', () => {
  let component: RepaymentLogicComponent;
  let fixture: ComponentFixture<RepaymentLogicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepaymentLogicComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepaymentLogicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
