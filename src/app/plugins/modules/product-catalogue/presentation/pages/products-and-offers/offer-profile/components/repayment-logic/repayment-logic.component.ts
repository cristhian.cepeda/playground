import { Component, OnInit } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'offer-profile-repayment-logic',
  templateUrl: './repayment-logic.component.html',
  styleUrls: ['./repayment-logic.component.scss'],
})
export class RepaymentLogicComponent implements OnInit {
  public isLoading$: Observable<boolean>;
  public repaymentLogicItems$: Observable<SummaryGroup[]>;
  constructor(private _offerFacade: OfferFacade) {}

  ngOnInit(): void {
    this._offerFacade.getOfferRepaymentLogic();

    this.isLoading$ = this._offerFacade.isLoadingOfferRepaymentLogic$;
    this.repaymentLogicItems$ =
      this._offerFacade.offerProfileRepaymentLogicItems$;
  }
}
