import { TestBed } from '@angular/core/testing';

import { OfferProfileGuard } from './offer-profile.guard';

describe('OfferProfileGuard', () => {
  let guard: OfferProfileGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(OfferProfileGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
