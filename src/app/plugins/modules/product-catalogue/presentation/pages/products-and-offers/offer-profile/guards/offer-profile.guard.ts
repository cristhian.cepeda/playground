import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OfferProfileGuard implements CanActivate {
  constructor(private _offerFacade: OfferFacade, private _router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._getOfferIdParam(route);
  }

  private _getOfferIdParam(route: ActivatedRouteSnapshot): boolean | UrlTree {
    const offerReference = route.queryParams?.reference;
    if (!!offerReference) {
      this._offerFacade.updateOfferReference(offerReference);
      return true;
    }
    return this._router.createUrlTree([PRODUCT_CATALOGUE_URLS.ROOT_PATH]);
  }
}
