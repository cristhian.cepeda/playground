import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OfferProfilePage } from './offer-profile.page';

const routes: Routes = [
  {
    path: '',
    component: OfferProfilePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfferProfileRoutingModule {}
