import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { OfferProfileRoutingModule } from './offer-profile-routing.module';
import { OfferProfilePage } from './offer-profile.page';
import { InfoComponent } from './components/info/info.component';
import { GeneralSettingsComponent } from './components/general-settings/general-settings.component';
import { RepaymentLogicComponent } from './components/repayment-logic/repayment-logic.component';
import { ChargesComponent } from './components/charges/charges.component';

@NgModule({
  declarations: [OfferProfilePage, InfoComponent, GeneralSettingsComponent, RepaymentLogicComponent, ChargesComponent],
  imports: [CommonModule, OfferProfileRoutingModule, LayoutModule],
})
export class OfferProfileModule {}
