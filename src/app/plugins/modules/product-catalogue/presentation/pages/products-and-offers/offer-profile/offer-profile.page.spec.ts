import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferProfilePage } from './offer-profile.page';

describe('OfferProfilePage', () => {
  let component: OfferProfilePage;
  let fixture: ComponentFixture<OfferProfilePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferProfilePage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OfferProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
