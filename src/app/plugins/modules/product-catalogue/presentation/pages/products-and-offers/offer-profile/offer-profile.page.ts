import { Component, OnInit } from '@angular/core';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';

@Component({
  selector: 'offer-profile',
  templateUrl: './offer-profile.page.html',
  styleUrls: ['./offer-profile.page.scss'],
})
export class OfferProfilePage implements OnInit {
  constructor(private _offerFacade: OfferFacade) {}

  ngOnInit(): void {
    this._offerFacade.getOffer();
  }
}
