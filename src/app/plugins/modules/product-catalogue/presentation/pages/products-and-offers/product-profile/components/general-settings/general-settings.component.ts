import { Component, OnInit } from '@angular/core';
import { SummaryGroup } from '@app/core/models/summary-group.model';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'product-profile-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss'],
})
export class GeneralSettingsComponent implements OnInit {
  public productProfileItems$: Observable<SummaryGroup[]>;
  public isLoading$: Observable<boolean>;

  constructor(private _productFacade: ProductFacade) {}

  ngOnInit(): void {
    this.productProfileItems$ = this._productFacade.productProfileItems$;
    this.isLoading$ = this._productFacade.isLoadingProductProfile$;
  }
}
