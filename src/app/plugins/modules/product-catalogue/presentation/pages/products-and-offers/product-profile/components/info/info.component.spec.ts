import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PROJECT } from '@app/data/mocks/data/project.mock';
import { PRODUCT } from '@app/plugins/modules/product-catalogue/data/mocks/products.mock';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { InfoComponent } from './info.component';

describe('InfoComponent', () => {
  let component: InfoComponent;
  let fixture: ComponentFixture<InfoComponent>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LayoutModule, TranslateModule.forRoot()],
      declarations: [InfoComponent],
      providers: [ProductFacade, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    store.setState({
      project: {
        data: PROJECT,
      },
      productCatalogue: {
        product: PRODUCT,
        isLoadingProduct: false,
      },
    });
    expect(component).toBeTruthy();
  });
});
