import { Component, OnInit } from '@angular/core';
import { Project } from '@app/core/models/project.model';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'product-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  public productProfile$: Observable<Product>;
  public isLoadingProductProfile$: Observable<boolean>;
  public project$: Observable<Project>;

  constructor(private _productFacade: ProductFacade) {}

  ngOnInit(): void {
    this.project$ = this._productFacade.project$;
    this.productProfile$ = this._productFacade.productProfile$;
    this.isLoadingProductProfile$ =
      this._productFacade.isLoadingProductProfile$;
  }
}
