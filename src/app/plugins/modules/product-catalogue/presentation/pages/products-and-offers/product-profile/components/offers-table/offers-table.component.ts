import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { CAN_CREATE_NEW_OFFER } from '@app/plugins/modules/product-catalogue/core/constants/offers.constants';
import {
  getProductCatalogueTablePerFamily,
  PRODUCT_CATALOGUE,
} from '@app/plugins/modules/product-catalogue/core/constants/tables';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import {
  Offer,
  OFFER_STATUS_ICONS,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'product-profile-offers-table',
  templateUrl: './offers-table.component.html',
  styleUrls: ['./offers-table.component.scss'],
})
export class OffersTableComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  @ViewChild('statusTemplate', { static: true })
  statusTemplate: TemplateRef<ContextColumn>;

  public offers$: Observable<TableResponse<Offer>>;
  public isLoadingOffers$: Observable<boolean>;

  public offerProfileUrl: string;
  public headers: TableHeader[];
  public OFFER_STATUS_ICONS = OFFER_STATUS_ICONS;

  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public CAN_CREATE_NEW_OFFER = CAN_CREATE_NEW_OFFER;
  public urlCreateNewOffer: string;

  private _projectFamilySubscription: Subscription;

  constructor(private _offerFacade: OfferFacade) {
    super();
    this.offers$ = this._offerFacade.offers$;
    this.isLoadingOffers$ = this._offerFacade.isLoadingOffers$;
  }

  ngOnInit(): void {
    this._setInitialValues();
  }

  private _setInitialValues() {
    this._mapHeaders();
    this.offerProfileUrl =
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.OFFER.OFFER_PROFILE;
    this.urlCreateNewOffer =
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.OFFER.CREATE_NEW_OFFER;
  }

  private _mapHeaders() {
    this._projectFamilySubscription =
      this._offerFacade.projectFamily$.subscribe((projectFamily) => {
        this.headers = getProductCatalogueTablePerFamily(
          projectFamily,
          PRODUCT_CATALOGUE.PRODUCT_PROFILE_OFFER
        ).map((header) => {
          if (header.dataKey == 'status') header.template = this.statusTemplate;
          return header;
        });
      });
  }
}
