import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ProductFacade } from '../../../../../facade/product.facade';
import { ProductProfileGuard } from './product-profile.guard';

describe('ProductProfileGuard', () => {
  let guard: ProductProfileGuard;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        CustomCurrencyPipe,
        ProductFacade,
        provideMockStore({ initialState }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    guard = TestBed.inject(ProductProfileGuard);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
