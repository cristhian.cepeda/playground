import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductProfileGuard implements CanActivate {
  constructor(private _productFacade: ProductFacade, private _router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._getProductReferenceParam(route);
  }

  private _getProductReferenceParam(
    route: ActivatedRouteSnapshot
  ): boolean | UrlTree {
    const productReference = route.queryParams?.id;
    if (!!productReference) {
      this._productFacade.updateProductReference(productReference);
      return true;
    }
    return this._router.createUrlTree([PRODUCT_CATALOGUE_URLS.ROOT_PATH]);
  }
}
