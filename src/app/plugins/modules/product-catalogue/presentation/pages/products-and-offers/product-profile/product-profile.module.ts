import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { InfoComponent } from './components/info/info.component';
import { OffersTableComponent } from './components/offers-table/offers-table.component';
import { ProductProfilePageRoutingModule } from './product-profile-routing.module';
import { ProductProfilePage } from './product-profile.page';

import { GeneralSettingsComponent } from './components/general-settings/general-settings.component';

@NgModule({
  declarations: [
    ProductProfilePage,
    InfoComponent,
    OffersTableComponent,
    GeneralSettingsComponent,
  ],
  imports: [CommonModule, ProductProfilePageRoutingModule, LayoutModule],
})
export class ProductProfilePageModule {}
