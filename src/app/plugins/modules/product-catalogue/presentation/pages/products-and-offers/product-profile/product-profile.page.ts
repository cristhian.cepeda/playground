import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';

@Component({
  selector: 'product-profile',
  templateUrl: './product-profile.page.html',
  styleUrls: ['./product-profile.page.scss'],
})
export class ProductProfilePage implements OnInit, OnDestroy {
  constructor(private _productFacade: ProductFacade) {}

  ngOnInit(): void {
    this._productFacade.initProductProfilePage();
  }

  ngOnDestroy(): void {
    this._productFacade.destroyProductProfilePage();
  }
}
