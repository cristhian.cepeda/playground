import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { OfferProfileGuard } from './offer-profile/guards/offer-profile.guard';
import { ProductProfileGuard } from './product-profile/guards/product-profile.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./products-and-offers/products-and-offers.module').then(
        (m) => m.ProductsAndOffersPageModule
      ),
  },
  {
    canActivate: [ProductProfileGuard],
    path: PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
      .PRODUCT_PROFILE.PATH,
    loadChildren: () =>
      import('./product-profile/product-profile.module').then(
        (m) => m.ProductProfilePageModule
      ),
    data: {
      breadcrumb: 'PRODUCT_CATALOGUE.PRODUCTS_PROFILE.TITLE',
    },
  },
  {
    canActivate: [OfferProfileGuard],
    path: PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
      .OFFER_PROFILE.PATH,
    loadChildren: () =>
      import('./offer-profile/offer-profile.module').then(
        (m) => m.OfferProfileModule
      ),
    data: {
      breadcrumb: 'PRODUCT_CATALOGUE.OFFER_PROFILE.TITLE',
    },
  },
  {
    path: PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
      .CREATE_NEW_OFFER.PATH,
    loadChildren: () =>
      import('./create-new-offer/create-new-offer.module').then(
        (m) => m.CreateNewOfferPageModule
      ),
    data: {
      breadcrumb:
        PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
          .CREATE_NEW_OFFER.NAME,
    },
  },
  {
    path: PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
      .CREATE_NEW_PRODUCT.PATH,
    loadChildren: () =>
      import('./create-new-product/create-new-product.module').then(
        (m) => m.CreateNewProductPageModule
      ),
    data: {
      breadcrumb:
        PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.SUB_PAGES
          .CREATE_NEW_PRODUCT.NAME,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsAndOffersRoutingModule {}
