import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { ProductCatalogueEffects } from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.effects';
import { ProductCatalogueReducers } from '@app/plugins/modules/product-catalogue/domain/store/product-catalogue/product-catalogue.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ProductsAndOffersRoutingModule } from './products-and-offers-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ProductsAndOffersRoutingModule,
    StoreModule.forFeature(
      PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.STORE_NAME,
      ProductCatalogueReducers
    ),
    EffectsModule.forFeature([ProductCatalogueEffects]),
  ],
})
export class ProductsAndOffersModule {}
