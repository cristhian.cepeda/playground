import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { CountersComponent } from './counters/counters.component';
import { DetailsComponent } from './details/details.component';
import { OfferItemComponent } from './offer-item/offer-item.component';
import { OffersComponent } from './offers/offers.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ProductCardsLoaderComponent } from './product-cards-loader/product-cards-loader.component';

const COMPONENTS = [
  OffersComponent,
  OfferItemComponent,
  ProductCardComponent,
  ProductCardsLoaderComponent,
  CountersComponent,
  DetailsComponent,
];

@NgModule({
  declarations: COMPONENTS,
  exports: COMPONENTS,
  imports: [CommonModule, LayoutModule, RouterModule],
})
export class ComponentsModule {}
