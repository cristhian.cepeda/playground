import { Component, OnInit } from '@angular/core';
import { ProductsAndOfferCounter } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'products-and-offers-counters',
  templateUrl: './counters.component.html',
  styleUrls: ['./counters.component.scss'],
})
export class CountersComponent implements OnInit {
  public productsAndOffersCounter$: Observable<ProductsAndOfferCounter>;

  constructor(private _productFacade: ProductFacade) {}

  ngOnInit(): void {
    this.productsAndOffersCounter$ =
      this._productFacade.productsAndOffersCounter$;
  }
}
