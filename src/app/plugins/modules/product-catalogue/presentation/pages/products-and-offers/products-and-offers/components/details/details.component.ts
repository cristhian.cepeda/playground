import { Component, OnInit } from '@angular/core';
import { CAN_CREATE_NEW_PRODUCT } from '@app/plugins/modules/product-catalogue/core/constants/products.constants';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'products-and-offers-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public CAN_CREATE_NEW_PRODUCT: boolean;
  public urlCreateNewProduct: string;
  constructor() {}

  ngOnInit(): void {
    this.urlCreateNewProduct =
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.PRODUCT.CREATE_NEW_PRODUCT_FORM;
    this.CAN_CREATE_NEW_PRODUCT = CAN_CREATE_NEW_PRODUCT;
  }
}
