import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OFFER_STATUS } from '@app/plugins/modules/product-catalogue/core/models/offer.model';

import { OfferItemComponent } from './offer-item.component';

describe('OfferItemComponent', () => {
  let component: OfferItemComponent;
  let fixture: ComponentFixture<OfferItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OfferItemComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferItemComponent);
    component = fixture.componentInstance;
    component.offer = {
      status: OFFER_STATUS.ENABLED,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
