import { Component, Input, OnInit } from '@angular/core';
import {
  Offer,
  OFFER_STATUS_ICONS,
} from '@app/plugins/modules/product-catalogue/core/models/offer.model';

@Component({
  selector: 'layout-offer-item',
  templateUrl: './offer-item.component.html',
  styleUrls: ['./offer-item.component.scss'],
})
export class OfferItemComponent implements OnInit {
  @Input() offer: Offer;
  public statusLabel: string;

  public OFFER_STATUS_ICONS = OFFER_STATUS_ICONS;

  ngOnInit(): void {
    this.statusLabel = this.offer?.status;
  }
}
