import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Skeleton } from '@app/core/models/skeleton.model';
import { Offer } from '@app/plugins/modules/product-catalogue/core/models/offer.model';
import { SKELETON_OFFERS_PRODUCT_CARDS } from '../../constants/product-card.constants';

@Component({
  selector: 'offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss'],
})
export class OffersComponent implements OnInit {
  @Input() offers?: Offer[];
  @Input() offersDisplayed?: boolean;
  @Input() numberOfOffers?: number;
  @Output() showMoreOffers = new EventEmitter<void>();

  public showMore: boolean;
  public isLoadingOffer: boolean;
  public offersSkeletons: Skeleton[];

  constructor() {}

  ngOnInit(): void {
    this.isLoadingOffer = false;
    this.offersSkeletons = SKELETON_OFFERS_PRODUCT_CARDS;
    this.showMore = this.numberOfOffers > 2 && !this.offersDisplayed;
  }

  public onShowMoreOffers() {
    this.isLoadingOffer = true;
    this.showMoreOffers.emit();
  }
}
