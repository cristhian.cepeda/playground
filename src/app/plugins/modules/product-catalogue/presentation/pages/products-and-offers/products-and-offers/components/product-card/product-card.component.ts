import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CAN_CREATE_NEW_OFFER } from '@app/plugins/modules/product-catalogue/core/constants/offers.constants';
import { PRODUCT_CATALOGUE_URLS } from '@app/plugins/modules/product-catalogue/core/constants/urls.constants';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() data: Product;
  @Output() showMoreOffers = new EventEmitter<string>();

  public CAN_CREATE_NEW_OFFER = CAN_CREATE_NEW_OFFER;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public SIZES = SIZES;
  public urlCreateNewOffer: string;
  public iconPath: string;

  constructor(private _router: Router) {}

  ngOnInit(): void {
    this.iconPath = 'app/presentation/assets/img/icons/';
    this.urlCreateNewOffer =
      PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.OFFER.CREATE_NEW_OFFER;
    this.CAN_CREATE_NEW_OFFER = CAN_CREATE_NEW_OFFER;
  }

  public onShowMoreOffers() {
    this.showMoreOffers.emit(this.data?.reference);
  }

  public onGoToProductProfile() {
    const id = this.data.reference;
    this._router.navigate(
      [PRODUCT_CATALOGUE_URLS.PRODUCTS_AND_OFFERS.PRODUCT.PRODUCT_PROFILE],
      { queryParams: { id } }
    );
  }
}
