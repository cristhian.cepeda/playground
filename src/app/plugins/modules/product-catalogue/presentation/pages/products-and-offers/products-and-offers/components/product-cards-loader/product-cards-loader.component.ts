import { Component } from '@angular/core';
import { Skeleton } from '@app/core/models/skeleton.model';
import { SKELETON_PRODUCT_CARDS } from '../../constants/product-card.constants';

@Component({
  selector: 'product-cards-loader',
  templateUrl: './product-cards-loader.component.html',
  styleUrls: ['./product-cards-loader.component.scss'],
})
export class ProductCardsLoaderComponent {
  public skeletons: Skeleton[];

  constructor() {
    this.skeletons = SKELETON_PRODUCT_CARDS;
  }
}
