import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';

const skeletonProductCardTheme: SkeletonTheme = {
  width: '243px',
  height: '442px',
};
const skeletonOffersProductCardTheme: SkeletonTheme = {
  width: '215px',
  height: '48.25px',
};

export const SKELETON_PRODUCT_CARDS: Skeleton[] = [
  ...getSkeleton(1, 4, skeletonProductCardTheme),
];

export const SKELETON_OFFERS_PRODUCT_CARDS: Skeleton[] = [
  ...getSkeleton(1, 3, skeletonOffersProductCardTheme),
];
