export enum OFFER_CARD_TEMPLATE {
  GREEN = 'template-green',
  DARK_GREEN = 'template-dark-green',
  LIGHT_GREEN = 'template-light-green',
  ADDITIONAL = 'template-additional',
}
