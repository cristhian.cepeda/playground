import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsAndOffersPage } from './products-and-offers.page';

const routes: Routes = [{ path: '', component: ProductsAndOffersPage }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsAndOffersPageRoutingModule {}
