import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { ComponentsModule } from './components/components.module';
import { ProductsAndOffersPageRoutingModule } from './products-and-offers-routing.module';
import { ProductsAndOffersPage } from './products-and-offers.page';

@NgModule({
  declarations: [ProductsAndOffersPage],
  imports: [
    CommonModule,
    ProductsAndOffersPageRoutingModule,
    LayoutModule,
    ComponentsModule,
  ],
})
export class ProductsAndOffersPageModule {}
