import { Component, OnInit } from '@angular/core';
import { Product } from '@app/plugins/modules/product-catalogue/core/models/product.model';
import { OfferFacade } from '@app/plugins/modules/product-catalogue/facade/offer.facade';
import { ProductFacade } from '@app/plugins/modules/product-catalogue/facade/product.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'pc-products-and-offers-page',
  templateUrl: './products-and-offers.page.html',
  styleUrls: ['./products-and-offers.page.scss'],
})
export class ProductsAndOffersPage implements OnInit {
  public products$: Observable<Product[]>;
  public isLoadingProducts$: Observable<boolean>;

  constructor(
    private _productFacade: ProductFacade,
    private _offerFacade: OfferFacade
  ) {}

  ngOnInit(): void {
    this._productFacade.getProducts();
    this.products$ = this._productFacade.products$;
    this.isLoadingProducts$ = this._productFacade.isLoadingProducts$;
  }

  public onShowMoreOffers(productReference: string) {
    this._offerFacade.getProductOffers(productReference);
  }
}
