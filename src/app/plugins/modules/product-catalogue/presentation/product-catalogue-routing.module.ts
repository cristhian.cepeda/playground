import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PRODUCT_CATALOGUE_FEATURE } from '@app/plugins/modules/product-catalogue/core/constants/feature.constants';
import { BaseContentComponent } from '@app/presentation/layout/components/base-content/base-content.component';
import { MENU_CONFIG_DISABLED } from '@instance-config/menu.config';

const CHILDREN_ROUTES: Routes = [
  {
    path: PRODUCT_CATALOGUE_FEATURE.PAGES.PRODUCTS_AND_OFFERS.PATH,
    loadChildren: () =>
      import('./pages/products-and-offers/products-and-offers.module').then(
        (m) => m.ProductsAndOffersModule
      ),
    data: {
      breadcrumb: 'PRODUCT_CATALOGUE.PRODUCTS_AND_OFFERS.TITLE',
      disabled:
        MENU_CONFIG_DISABLED.PRODUCT_CATALOGUE.PRODUCTS_AND_OFFERS ?? false,
    },
  },
];

const routes: Routes = [
  {
    path: '',
    component: BaseContentComponent,
    children: [
      {
        path: '',
        redirectTo: CHILDREN_ROUTES.filter(
          (route) => !route?.data?.disabled
        ).shift()?.path,
        pathMatch: 'full',
      },
      ...CHILDREN_ROUTES,
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductCatalogueRoutingModule {}
