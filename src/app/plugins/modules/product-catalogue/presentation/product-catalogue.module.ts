import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ProductCatalogueRoutingModule } from './product-catalogue-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ProductCatalogueRoutingModule],
})
export class ProductCatalogueModule {}
