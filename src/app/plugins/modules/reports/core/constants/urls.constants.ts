import { REPORTS_FEATURE } from './feature.constants';

export const REPORTS_URLS = {
  REPORTS: `/${REPORTS_FEATURE.PATH}`,
};
