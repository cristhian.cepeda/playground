import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { REPORT_IFRAME_URL } from '../../../core/constants/reports.constants';

@Component({
  selector: 'reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {
  public reportIframeUrl: SafeResourceUrl;

  constructor(private _sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.reportIframeUrl =
      this._sanitizer.bypassSecurityTrustResourceUrl(REPORT_IFRAME_URL);
  }
}
