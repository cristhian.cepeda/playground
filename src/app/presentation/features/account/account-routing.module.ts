import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseContentComponent } from '@app/presentation/layout/components/base-content/base-content.component';

const routes: Routes = [
  {
    path: '',
    component: BaseContentComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./pages/my-account/my-account.module').then(
            (m) => m.MyAccountPageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
