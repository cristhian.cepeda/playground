import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AccountRoutingModule } from './account-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, AccountRoutingModule],
})
export class AccountModule {}
