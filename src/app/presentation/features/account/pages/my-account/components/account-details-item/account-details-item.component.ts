import { Component, Input } from '@angular/core';
import { AccountItem } from '@app/core/models/account.model';

@Component({
  selector: 'account-details-item',
  templateUrl: './account-details-item.component.html',
  styleUrls: ['./account-details-item.component.scss'],
})
export class AccountDetailsItemComponent {
  @Input() title: string;
  @Input() item: AccountItem;
}
