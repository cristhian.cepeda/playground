import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import {
  ITEM_BY_MO_PRODUCT,
  ITEM_BY_PRODUCT_FAMILY,
  MO_PRODUCTS,
} from '@app/core/constants/product.constants';
import { Project } from '@app/core/models/project.model';
import { Skeleton } from '@app/core/models/skeleton.model';
import { MyAccountFacade } from '@app/facade/account/my-account.facade';
import { INPUT_TYPE } from '@app/presentation/layout/mo-forms/enums/fields.type';
import { filter, Observable, Subscription, take } from 'rxjs';
import {
  SKELETON_ACCOUNT_DETAILS,
  SKELETON_ACCOUNT_DETAILS_ITEMS,
} from '../../constants/account-details.constant';

@Component({
  selector: 'account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss'],
})
export class AccountDetailsComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public project$: Observable<Project>;
  public isLoadingProject$: Observable<boolean>;
  public form: UntypedFormGroup;
  public INPUT_TYPE = INPUT_TYPE;
  public ITEM_BY_PRODUCT_FAMILY = ITEM_BY_PRODUCT_FAMILY;
  public ITEM_BY_MO_PRODUCT = ITEM_BY_MO_PRODUCT;
  public MO_PRODUCTS = MO_PRODUCTS;
  public skeletonAccountDetails: Skeleton[];
  public skeletonAccountDetailsItems: Skeleton[];

  private _projectSubscription: Subscription;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _myAccountFacade: MyAccountFacade
  ) {
    super();
  }

  ngOnInit(): void {
    this.project$ = this._myAccountFacade.project$;
    this.isLoadingProject$ = this._myAccountFacade.isLoadingProject$;
    this.skeletonAccountDetails = SKELETON_ACCOUNT_DETAILS;
    this.skeletonAccountDetailsItems = SKELETON_ACCOUNT_DETAILS_ITEMS;
    this._setForm();
  }

  private _setForm() {
    this._projectSubscription = this.project$
      .pipe(
        filter((project) => !!project),
        take(1)
      )
      .subscribe((project) => {
        this.form = this._formBuilder.group({
          name: [project?.name, Validators.required],
          description: [
            project?.description,
            Validators.compose([Validators.required, Validators.email]),
          ],
        });
      });
  }
}
