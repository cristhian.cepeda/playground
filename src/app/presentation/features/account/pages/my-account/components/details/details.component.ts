import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AutoUnsubscribeOnDetroy } from '@app/core/classes/auto-unsubscribe.class';
import { MyAccountFacade } from '@app/facade/account/my-account.facade';
import { INPUT_TYPE } from '@app/presentation/layout/mo-forms/enums/fields.type';
import { filter, Subscription } from 'rxjs';
@Component({
  selector: 'account-your-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent
  extends AutoUnsubscribeOnDetroy
  implements OnInit
{
  public INPUT_TYPE = INPUT_TYPE;
  public form: UntypedFormGroup;
  private _userSubscription: Subscription;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _myAccountFacade: MyAccountFacade
  ) {
    super();
  }

  ngOnInit(): void {
    this._setForm();
  }

  private _setForm(): void {
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
    });

    this._userSubscription = this._myAccountFacade.user$
      .pipe(filter((user) => !!user))
      .subscribe((user) => {
        this.form
          .get('name')
          .setValue(`${user?.first_name} ${user?.last_name}`);
        this.form.get('email').setValue(user?.email);
      });
  }
}
