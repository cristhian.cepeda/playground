import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { GROUPS } from '@app/core/constants/groups.constants';
import { Filters } from '@app/core/models/filters.model';
import { User, UserFilters, USER_STATUS } from '@app/core/models/user.model';
import { UserFacade } from '@app/facade/user/user.facade';
import { TABLE_DESIGN_CLASS } from '@app/presentation/layout/mo-tables/enums/table.enum';
import {
  ContextColumn,
  TableHeader,
  TableResponse,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { Observable } from 'rxjs';
import { USERS_HEADERS } from '../../constants/table.constant';

@Component({
  selector: 'users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss'],
})
export class UsersTableComponent implements OnInit {
  @ViewChild('nameTemplate', { static: true })
  nameTemplate: TemplateRef<ContextColumn>;

  @ViewChild('statusTemplate', { static: true })
  statusTemplate: TemplateRef<ContextColumn>;

  @ViewChild('rolTemplate', { static: true })
  rolTemplate: TemplateRef<ContextColumn>;

  public userList$: Observable<TableResponse<User>>;
  public isLoadingUsersList$: Observable<boolean>;
  public headers: TableHeader[];
  public TABLE_DESIGN_CLASS = TABLE_DESIGN_CLASS;
  public USER_STATUS = USER_STATUS;

  constructor(private _userFacade: UserFacade) {}

  ngOnInit(): void {
    this._userFacade.initUsersList();
    this._setInitialValues();
    this._mapHeaders();
  }

  public onChangeTableFilters(filters: Filters) {
    const userFilters: UserFilters = {
      ...filters,
      group_name: GROUPS.ADMINS,
    };
    this._userFacade.updateFiltersUserList(userFilters);
  }

  private _setInitialValues() {
    this.userList$ = this._userFacade.userList$;
    this.isLoadingUsersList$ = this._userFacade.isLoadingUsersList$;
  }

  private _mapHeaders() {
    this.headers = USERS_HEADERS.map((header) => {
      const HEADER_TEMPLATE: { [key: string]: TemplateRef<ContextColumn> } = {
        ['name']: this.nameTemplate,
        ['is_active']: this.statusTemplate,
        ['is_superuser']: this.rolTemplate,
      };
      header.template = HEADER_TEMPLATE[header.dataKey];
      return header;
    });
  }
}
