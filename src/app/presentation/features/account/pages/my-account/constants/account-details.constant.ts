import { getSkeleton, Skeleton } from '@app/core/models/skeleton.model';

const height: string = '45px';

const skeletonThemeAccountDeteails = {
  minWidth: 300,
  maxWidth: 500,
  margin: '0',
  height,
};

const skeletonThemeAccountDeteailsItems = {
  minWidth: 80,
  maxWidth: 120,
  margin: '0 5px 0 0',
  height,
};

export const SKELETON_ACCOUNT_DETAILS: Skeleton[] = getSkeleton(
  1,
  2,
  skeletonThemeAccountDeteails
);

export const SKELETON_ACCOUNT_DETAILS_ITEMS: Skeleton[] = getSkeleton(
  2,
  5,
  skeletonThemeAccountDeteailsItems
);
