import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';

export const USERS_HEADERS = [
  {
    label: 'MY_ACCOUNT.ACCOUNT.TABLE_CARD.TABLE.NAME',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    template: null,
    dataKey: 'name',
  },
  {
    label: 'MY_ACCOUNT.ACCOUNT.TABLE_CARD.TABLE.STATUS',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    template: null,
    dataKey: 'is_active',
  },
  {
    label: 'MY_ACCOUNT.ACCOUNT.TABLE_CARD.TABLE.ROL',
    size: 4,
    typeColumn: TYPE_COLUMN.CUSTOM_TEMPLATE,
    template: null,
    dataKey: 'is_superuser',
  },
];
