import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@app/presentation/layout/layout.module';
import { AccountDetailsItemComponent } from './components/account-details-item/account-details-item.component';
import { AccountDetailsComponent } from './components/account-details/account-details.component';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { MyAccountPageRoutingModule } from './my-account-routing.module';
import { MyAccountPage } from './my-account.page';
import { DetailsComponent } from './components/details/details.component';

@NgModule({
  declarations: [
    MyAccountPage,
    UsersTableComponent,
    AccountDetailsComponent,
    AccountDetailsItemComponent,
    DetailsComponent,
  ],
  imports: [CommonModule, MyAccountPageRoutingModule, LayoutModule],
})
export class MyAccountPageModule {}
