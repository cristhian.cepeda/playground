import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FEATURES } from '@app/core/constants/feature.constants';
import { BasicSlidesComponent } from '@app/presentation/layout/components/basic-slides/basic-slides.component';
import { LoginGuard } from './pages/login/guards/login.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: FEATURES.AUTH.PAGES.LOGIN.PATH,
        pathMatch: 'full',
      },
      {
        canActivate: [LoginGuard],
        path: FEATURES.AUTH.PAGES.LOGIN.PATH,
        loadChildren: () =>
          import('./pages/login/login.module').then((m) => m.LoginPageModule),
      },
      {
        path: FEATURES.AUTH.PAGES.SET_PASS.PATH,
        component: BasicSlidesComponent,
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./pages/set-pass/set-pass.module').then(
                (m) => m.SetPassModule
              ),
          },
        ],
      },
      {
        path: FEATURES.AUTH.PAGES.FORGOT_PASSWORD.PATH,
        component: BasicSlidesComponent,
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./pages/forgot-password/forgot-password.module').then(
                (m) => m.ForgotPasswordModule
              ),
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
