import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordValidationsItemComponent } from './password-validations-item.component';

describe('PasswordValidationsItemComponent', () => {
  let component: PasswordValidationsItemComponent;
  let fixture: ComponentFixture<PasswordValidationsItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PasswordValidationsItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PasswordValidationsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
