import { Component, Input } from '@angular/core';

@Component({
  selector: 'password-validations-item',
  templateUrl: './password-validations-item.component.html',
  styleUrls: ['./password-validations-item.component.scss'],
})
export class PasswordValidationsItemComponent {
  @Input() text: string;
  @Input() isValid!: boolean;
}
