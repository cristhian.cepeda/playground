import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { CreatePasswordRoutingModule } from './create-password-routing.module';
import { CreatePasswordPage } from './create-password.page';
import { PasswordValidationsItemComponent } from './components/password-validations-item/password-validations-item.component';

@NgModule({
  declarations: [CreatePasswordPage, PasswordValidationsItemComponent],
  imports: [CommonModule, CreatePasswordRoutingModule, LayoutModule],
})
export class CreatePasswordModule {}
