import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { EmailRoutingModule } from './email-routing.module';
import { EmailPage } from './email.page';

@NgModule({
  declarations: [EmailPage],
  imports: [CommonModule, EmailRoutingModule, LayoutModule],
})
export class EmailModule {}
