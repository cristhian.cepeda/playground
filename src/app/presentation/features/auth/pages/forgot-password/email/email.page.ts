import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import {
  CreateOTP,
  OTP_CATEGORY,
  OTP_CHANNEL,
} from '@app/core/models/auth.model';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';

@Component({
  selector: 'forgot-password-email',
  templateUrl: './email.page.html',
  styleUrls: ['./email.page.scss'],
})
export class EmailPage implements OnInit {
  public isLoading$: Observable<boolean>;

  public form: UntypedFormGroup;
  public urlForgotPassword: string;

  public BUTTON_TYPE = BUTTON_TYPE;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _authFacade: AuthFacade
  ) {}

  ngOnInit(): void {
    this.isLoading$ = this._authFacade.isLoadingCreateOTP$;
    this._setupForm();
  }

  public onSubmit() {
    if (this.form.invalid) return;
    const createOTP: CreateOTP = {
      ...this.form.value,
      channel: OTP_CHANNEL.EMAIL,
      category: OTP_CATEGORY.FORGOT_PASSWORD,
    };
    this._authFacade.createOTP(createOTP);
  }

  private _setupForm() {
    this.form = this._formBuilder.group({
      email: ['', [Validators.required]],
    });
  }
}
