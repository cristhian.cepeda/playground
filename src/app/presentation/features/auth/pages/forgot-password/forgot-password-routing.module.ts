import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FEATURES } from '@app/core/constants/feature.constants';

const routes: Routes = [
  {
    path: '',
    redirectTo: FEATURES.AUTH.PAGES.FORGOT_PASSWORD.SUB_PAGES.EMAIL.PATH,
    pathMatch: 'full',
  },
  {
    path: FEATURES.AUTH.PAGES.FORGOT_PASSWORD.SUB_PAGES.EMAIL.PATH,
    loadChildren: () =>
      import('./email/email.module').then((m) => m.EmailModule),
  },
  {
    path: FEATURES.AUTH.PAGES.FORGOT_PASSWORD.SUB_PAGES.VERIFICATION_CODE.PATH,
    loadChildren: () =>
      import('./verification-code/verification-code.module').then(
        (m) => m.VerificationCodeModule
      ),
  },
  {
    path: FEATURES.AUTH.PAGES.FORGOT_PASSWORD.SUB_PAGES.CREATE_PASS.PATH,
    loadChildren: () =>
      import('./create-password/create-password.module').then(
        (m) => m.CreatePasswordModule
      ),
  },
  {
    path: FEATURES.AUTH.PAGES.FORGOT_PASSWORD.SUB_PAGES.SUCCESS.PATH,
    loadChildren: () =>
      import('./success/success.module').then((m) => m.SuccessModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgotPasswordRoutingModule {}
