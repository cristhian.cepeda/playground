import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { SuccessRoutingModule } from './success-routing.module';
import { SuccessPage } from './success.page';

@NgModule({
  declarations: [SuccessPage],
  imports: [CommonModule, SuccessRoutingModule, LayoutModule],
})
export class SuccessModule {}
