import { Component } from '@angular/core';
import { AuthFacade } from '@app/facade/auth/auth.facade';

import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';

@Component({
  selector: 'forgot-password-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage {
  public forgotPasswordEmail$: Observable<string>;
  public SIZES = SIZES;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;

  constructor(private _authFacade: AuthFacade) {
    this.forgotPasswordEmail$ = this._authFacade.forgotPasswordEmail$;
  }

  public goToLogin() {
    this._authFacade.cleanForgotPasswordData();
  }
}
