import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { CheckOTP, OTP_CATEGORY } from '@app/core/models/auth.model';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'verification-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.scss'],
})
export class CodeComponent implements OnInit {
  public isLoading$: Observable<boolean>;
  public form: UntypedFormGroup;
  public urlForgotPassword: string;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _authFacade: AuthFacade
  ) {}

  ngOnInit(): void {
    this._setupForm();
    this.isLoading$ = this._authFacade.isLoadingCheckingOTP$;
  }

  public onSubmit() {
    if (this.form.invalid) return;
    const checkOTP: CheckOTP = {
      ...this.form.value,
      category: OTP_CATEGORY.FORGOT_PASSWORD,
    };
    this._authFacade.checkOTP(checkOTP);
  }

  public onResendCode() {
    this._authFacade.resendOTP();
  }

  private _setupForm() {
    this.form = this._formBuilder.group({
      otp_value: ['', [Validators.required]],
    });
  }
}
