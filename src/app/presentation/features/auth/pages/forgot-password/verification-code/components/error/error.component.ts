import { Component } from '@angular/core';
import { AuthFacade } from '@app/facade/auth/auth.facade';

import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';

@Component({
  selector: 'verification-code-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent {
  public SIZES = SIZES;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;

  constructor(private _authFacade: AuthFacade) {}

  public goToLogin() {
    this._authFacade.cleanForgotPasswordData();
  }
}
