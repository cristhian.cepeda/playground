import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { VerificationCodeRoutingModule } from './verification-code-routing.module';
import { VerificationCodePage } from './verification-code.page';
import { CodeComponent } from './components/code/code.component';
import { ErrorComponent } from './components/error/error.component';

@NgModule({
  declarations: [VerificationCodePage, CodeComponent, ErrorComponent],
  imports: [CommonModule, VerificationCodeRoutingModule, LayoutModule],
})
export class VerificationCodeModule {}
