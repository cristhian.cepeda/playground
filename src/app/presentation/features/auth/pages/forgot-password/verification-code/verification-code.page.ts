import { Component, OnInit } from '@angular/core';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'forgot-password-verification-code',
  templateUrl: './verification-code.page.html',
  styleUrls: ['./verification-code.page.scss'],
})
export class VerificationCodePage implements OnInit {
  public showOTPError$: Observable<boolean>;

  constructor(private _authFacade: AuthFacade) {}

  ngOnInit(): void {
    this.showOTPError$ = this._authFacade.showOTPError$;
  }
}
