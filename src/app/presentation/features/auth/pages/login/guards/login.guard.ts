import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { APP_CONSTANTS } from '@app/core/constants/app.constants';
import { FEATURES } from '@app/core/constants/feature.constants';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import { combineLatest, map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  constructor(private _authFacade: AuthFacade, private _router: Router) {}
  canActivate(): Observable<UrlTree | boolean> {
    return this._validSession();
  }

  private _validSession(): Observable<UrlTree | boolean> {
    return combineLatest([
      this._authFacade.isAuthenticated$,
      this._authFacade.isFirstLogin$,
    ]).pipe(
      map(([isAuthenticated, isFirstLogin]) => {
        if (!isAuthenticated) return true;
        const url: string = isFirstLogin
          ? APP_CONSTANTS.ROUTES.AUTH.SET_PASS.CREATE_PASSWORD
          : FEATURES.MY_ACCOUNT.PATH;
        return this._router.createUrlTree([url]);
      })
    );
  }
}
