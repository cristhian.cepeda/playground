import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginPageRoutingModule } from './login-routing.module';
import { LoginPage } from './login.page';
import { LayoutModule } from '@app/presentation/layout/layout.module';

@NgModule({
  declarations: [LoginPage],
  imports: [CommonModule, LoginPageRoutingModule, LayoutModule],
})
export class LoginPageModule {}
