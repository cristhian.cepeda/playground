import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let store: MockStore;
  let _authFacade: AuthFacade;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginPage],
      providers: [UntypedFormBuilder, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
    _authFacade = TestBed.inject(AuthFacade);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onSubmit', () => {
    it('should be login has been called', () => {
      component.form.controls.username.setValue('test');
      component.form.controls.password.setValue('test');
      const spy = spyOn(_authFacade, 'login').and.callThrough();
      component.onSubmit();
      expect(spy).toHaveBeenCalled();
    });

    it('should be return when form is invalid', () => {
      component.form.controls.username.setValue('');
      component.form.controls.password.setValue('');
      const result = component.onSubmit();
      expect(result).toBeUndefined();
    });
  });
});
