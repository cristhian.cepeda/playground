import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { APP_CONSTANTS } from '@app/core/constants/app.constants';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  ICON_POSITION,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';

@Component({
  selector: 'auth-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public isLoadingLogin$: Observable<boolean>;
  public form: UntypedFormGroup;
  public urlForgotPassword: string;

  public BUTTON_TYPE = BUTTON_TYPE;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public ICON_POSITION = ICON_POSITION;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _authFacade: AuthFacade
  ) {}
  ngOnInit(): void {
    this.isLoadingLogin$ = this._authFacade.isLoadingLogin$;
    this.urlForgotPassword = APP_CONSTANTS.ROUTES.AUTH.FORGOT_PASSWORD._PATH;

    this._setupForm();
  }

  public onSubmit() {
    if (this.form.invalid) return;
    this._authFacade.login(this.form.value);
  }

  private _setupForm() {
    this.form = this._formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }
}
