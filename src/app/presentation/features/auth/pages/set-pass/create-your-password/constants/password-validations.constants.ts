export const PASSWORD_REGEX = {
  NUMBER: /[\d+]+/,
  LETTERS: /[A-Za-zá-úÁ-Ú\s.]+/,
  MINIMUN_CHARACTERS: /.{8,}/,
};
