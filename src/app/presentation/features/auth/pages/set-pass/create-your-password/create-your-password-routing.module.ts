import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateYourPasswordPage } from './create-your-password.page';

const routes: Routes = [{ path: '', component: CreateYourPasswordPage }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateYourPasswordRoutingModule {}
