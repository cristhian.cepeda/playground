import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from '@app/presentation/layout/layout.module';
import { PasswordValidationsItemComponent } from './components/password-validations-item/password-validations-item.component';
import { CreateYourPasswordRoutingModule } from './create-your-password-routing.module';
import { CreateYourPasswordPage } from './create-your-password.page';

@NgModule({
  declarations: [CreateYourPasswordPage, PasswordValidationsItemComponent],
  imports: [CommonModule, CreateYourPasswordRoutingModule, LayoutModule],
})
export class CreateYourPasswordModule {}
