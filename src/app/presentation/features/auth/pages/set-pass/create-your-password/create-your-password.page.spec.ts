import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateYourPasswordPage } from './create-your-password.page';

describe('CreateYourPasswordPage', () => {
  let component: CreateYourPasswordPage;
  let fixture: ComponentFixture<CreateYourPasswordPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateYourPasswordPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateYourPasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
