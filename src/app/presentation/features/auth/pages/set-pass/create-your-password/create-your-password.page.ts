import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import {
  BUTTON_DESIGN_CLASS,
  BUTTON_TYPE,
  INPUT_TYPE,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { CustomValidators } from '@app/presentation/layout/mo-forms/validators/custom.validators';
import { Observable } from 'rxjs';
import { PASSWORD_REGEX } from './constants/password-validations.constants';

@Component({
  selector: 'auth-create-your-password',
  templateUrl: './create-your-password.page.html',
  styleUrls: ['./create-your-password.page.scss'],
})
export class CreateYourPasswordPage implements OnInit {
  public isLoading$: Observable<boolean>;
  public form: UntypedFormGroup;
  public urlForgotPassword: string;

  public BUTTON_TYPE = BUTTON_TYPE;
  public INPUT_TYPE = INPUT_TYPE;
  public SIZES = SIZES;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;

  // PASSWORD VALIDATIONS
  public hasLetters: boolean;
  public hasNumber: boolean;
  public hasMinCharacters: boolean;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _authFacade: AuthFacade
  ) {}

  ngOnInit(): void {
    this._setInitialValues();
    this._setupForm();
  }

  public onValidatePassword(event) {
    const value = event?.target?.value;
    this.hasLetters = RegExp(PASSWORD_REGEX.LETTERS).test(value);
    this.hasNumber = RegExp(PASSWORD_REGEX.NUMBER).test(value);
    this.hasMinCharacters = RegExp(PASSWORD_REGEX.MINIMUN_CHARACTERS).test(
      value
    );
  }

  public onSubmit() {
    if (
      this.form.invalid ||
      !this.hasLetters ||
      !this.hasNumber ||
      !this.hasMinCharacters
    )
      return;
    this._authFacade.setPassChangePassword(this.form.value);
  }

  private _setInitialValues() {
    this.isLoading$ = this._authFacade.isLoadingChangePassword$;
  }

  private _setupForm() {
    this.form = this._formBuilder.group({
      password: [
        '',
        [Validators.required, CustomValidators.isMatch('confirm_password')],
      ],
      confirm_password: [
        '',
        [Validators.required, CustomValidators.isMatch('password')],
      ],
    });
  }
}
