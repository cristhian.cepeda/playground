import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FEATURES } from '@app/core/constants/feature.constants';

const routes: Routes = [
  {
    path: '',
    redirectTo: FEATURES.AUTH.PAGES.SET_PASS.SUB_PAGES.CREATE_PASS.PATH,
    pathMatch: 'full',
  },
  {
    path: FEATURES.AUTH.PAGES.SET_PASS.SUB_PAGES.CREATE_PASS.PATH,
    loadChildren: () =>
      import('./create-your-password/create-your-password.module').then(
        (m) => m.CreateYourPasswordModule
      ),
  },
  {
    path: FEATURES.AUTH.PAGES.SET_PASS.SUB_PAGES.SUCCESS.PATH,
    loadChildren: () =>
      import('./success/success.module').then((m) => m.SuccessModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetPassRoutingModule {}
