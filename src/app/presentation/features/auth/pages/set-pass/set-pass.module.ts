import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetPassRoutingModule } from './set-pass-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SetPassRoutingModule
  ]
})
export class SetPassModule { }
