import { Component, OnInit } from '@angular/core';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import {
  BUTTON_DESIGN_CLASS,
  SIZES,
} from '@app/presentation/layout/mo-forms/enums/fields.type';
import { Observable } from 'rxjs';

@Component({
  selector: 'auth-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {
  public emailSession$: Observable<string>;

  public SIZES = SIZES;
  public BUTTON_DESIGN_CLASS = BUTTON_DESIGN_CLASS;

  constructor(private _authFacade: AuthFacade) {}

  ngOnInit(): void {
    this.emailSession$ = this._authFacade.getSessionEmail();
  }

  onLogout() {
    this._authFacade.logout();
  }
}
