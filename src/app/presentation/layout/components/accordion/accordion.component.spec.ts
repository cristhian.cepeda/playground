import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { AccordionComponent } from './accordion.component';

describe('AccordionComponent', () => {
  let component: AccordionComponent;
  let fixture: ComponentFixture<AccordionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AccordionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onToggle', () => {
    it('should be toggle accordeon on click accordeon header and emit true value', (doneFn) => {
      // Arrange
      component.open = false;
      let resultOpen: boolean = false;
      const accordeonHeaderDebug: DebugElement = fixture.debugElement.query(
        By.css('div.accordion__header')
      );
      component.toggle.subscribe((open: boolean) => {
        resultOpen = open;
        doneFn();
      });

      // Act
      accordeonHeaderDebug.triggerEventHandler('click', null);
      fixture.detectChanges();

      // Assert
      expect(component.open).toBeTruthy();
      expect(resultOpen).toBeTruthy();
    });

    it('should be toggle accordeon on click accordeon header and emit false value', (doneFn) => {
      // Arrange
      component.open = true;
      let resultOpen: boolean = true;
      const accordeonHeaderDebug: DebugElement = fixture.debugElement.query(
        By.css('div.accordion__header')
      );
      component.toggle.subscribe((open: boolean) => {
        resultOpen = open;
        doneFn();
      });

      // Act
      accordeonHeaderDebug.triggerEventHandler('click', null);
      fixture.detectChanges();

      // Assert
      expect(component.open).toBeFalse();
      expect(resultOpen).toBeFalse();
    });
  });
});
