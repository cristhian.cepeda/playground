import { Component, OnInit } from '@angular/core';
import { DEFAULT_LANGUAGE } from '@app/core/i18n/constants/translate.constants';
import { AuthenticationService } from '@app/domain/services/auth/authentication.service';
import { AppFacade } from '@app/facade/app/app.facade';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private _authenticationService: AuthenticationService,
    private _appFacade: AppFacade
  ) {
    this._appFacade.updateLanguage(DEFAULT_LANGUAGE);
  }

  ngOnInit() {
    this._authenticationService.checkSession();
  }
}
