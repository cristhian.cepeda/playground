import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseContentComponent } from './base-content.component';

describe('BaseContentComponent', () => {
  let component: BaseContentComponent;
  let fixture: ComponentFixture<BaseContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BaseContentComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
