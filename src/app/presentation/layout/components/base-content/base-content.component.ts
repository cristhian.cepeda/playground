import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'layout-base-content',
  templateUrl: './base-content.component.html',
  styleUrls: ['./base-content.component.scss']
})
export class BaseContentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
