import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthFacade } from '@app/facade/auth/auth.facade';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { BaseLoggedComponent } from './base-logged.component';

describe('BaseLoggedComponent', () => {
  let component: BaseLoggedComponent;
  let fixture: ComponentFixture<BaseLoggedComponent>;
  let store: MockStore;
  const initialState: any = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BaseLoggedComponent],
      providers: [provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be logout has been called', () => {
    const _authFacade = TestBed.inject(AuthFacade);
    const spy = spyOn(_authFacade, 'logout');
    component.onLogout();
    expect(spy).toHaveBeenCalled();
  });
});
