import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicSlidesComponent } from './basic-slides.component';

describe('BasicSlidesComponent', () => {
  let component: BasicSlidesComponent;
  let fixture: ComponentFixture<BasicSlidesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasicSlidesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicSlidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
