import { Component, Input } from '@angular/core';

@Component({
  selector: 'layout-basic-slides',
  templateUrl: './basic-slides.component.html',
  styleUrls: ['./basic-slides.component.scss'],
})
export class BasicSlidesComponent {
  @Input() title?: string;
  @Input() description?: string;
  @Input() logo?: string =
    'app/presentation/assets/img/icons/logo/mo-logo-white.svg';
  @Input() background?: string =
    'app/presentation/assets/img/backgrounds/login-background.png';
  @Input() onlyLogo?: boolean = true;
  @Input() useAsContent?: boolean;
}
