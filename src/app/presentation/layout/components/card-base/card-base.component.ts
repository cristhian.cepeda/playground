import { Component } from '@angular/core';

@Component({
  selector: 'layout-card-base',
  templateUrl: './card-base.component.html',
  styleUrls: ['./card-base.component.scss'],
})
export class CardBaseComponent {}
