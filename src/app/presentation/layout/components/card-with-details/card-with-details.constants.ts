import { getSkeleton, SkeletonTheme } from '@app/core/models/skeleton.model';

const margin: string = '0 5px 5px 0';
const width: string = '100%';

const skeletonTheme: SkeletonTheme = { width, margin };

export const SKELETON_HEADER = getSkeleton(1, 1, skeletonTheme);
export const SKELETON_BODY = getSkeleton(1, 1, skeletonTheme);
