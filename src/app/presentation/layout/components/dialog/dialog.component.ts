import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Dialog } from '@app/core/classes/dialog.class';
import { DialogButtons, DialogParams } from '@app/core/models/dialog.model';

@Component({
  selector: 'layout-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent extends Dialog {
  public infoButton: DialogButtons;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogParams
  ) {
    super(dialogRef, data);
    const defaultButtons = {
      showButtons: true,
      showButtonClose: true,
      showButtonCancel: false,
      showButtonOk: true,
      textCancel: 'Cancel',
      textOk: 'Accept',
    };
    this.infoButton = { ...defaultButtons, ...data.infoButton };
  }
}
