import { Location } from '@angular/common';
import { Component, Input } from '@angular/core';
import { ICON_COLORS } from '../icon/icon.constants';
@Component({
  selector: 'layout-go-back',
  templateUrl: './go-back.component.html',
  styleUrls: ['./go-back.component.scss'],
})
export class GoBackComponent {
  @Input() goBackText: string = 'APP.MENUS.GO_BACK';
  public ICON_COLORS = ICON_COLORS;

  constructor(private _location: Location) {}

  public goBack() {
    this._location.back();
  }
}
