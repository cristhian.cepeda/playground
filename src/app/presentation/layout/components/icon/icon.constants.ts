export enum ICON_COLORS {
  ERROR = 'error',
  SUCCESS = 'success',
  PRIMARY = 'primary',
  WARNING = 'warning',
  FAILURE = 'failure',
  NEUTRAL = 'neutral',
  DARK_NEUTRAL = 'dark-neutral',
  COMMON_BG = 'common-bg',
  TRANSPARENT = 'transparent',
}
