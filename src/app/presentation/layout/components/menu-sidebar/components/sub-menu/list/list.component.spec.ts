import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubMenuListComponent } from './list.component';

describe('ListComponent', () => {
  let component: SubMenuListComponent;
  let fixture: ComponentFixture<SubMenuListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SubMenuListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubMenuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
