import { Component } from '@angular/core';

@Component({
  selector: 'sub-menu-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class SubMenuListComponent {}
