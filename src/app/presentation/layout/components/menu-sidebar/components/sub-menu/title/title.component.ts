import { Component, Input } from '@angular/core';

@Component({
  selector: 'sub-menu-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss'],
})
export class SubMenuTitleComponent {
  @Input() label: string;
}
