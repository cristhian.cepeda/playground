import { Component, Input } from '@angular/core';

@Component({
  selector: 'menu-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss'],
})
export class TitleComponent {
  @Input() label: string;
}
