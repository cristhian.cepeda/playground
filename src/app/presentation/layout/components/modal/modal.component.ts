import { Component, Input } from '@angular/core';
import { AppFacade } from '@app/facade/app/app.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'layout-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {
  @Input() isBigModal: boolean;
  @Input() showClose: boolean;
  public showModal$: Observable<boolean>;
  public isClosing: boolean;

  constructor(private _appFacade: AppFacade) {
    this.showModal$ = this._appFacade.showModal$;
  }

  public onClose() {
    this.isClosing = true;
    setTimeout(() => {
      this._appFacade.toggleModal();
      this.isClosing = false;
    }, 900);
  }
}
