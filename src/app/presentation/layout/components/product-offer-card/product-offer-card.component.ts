import { Component, Input, OnInit } from '@angular/core';
import { MerchantGroupProductOfferCard } from '@app/core/models/merchant-group-product-offer-card.model';
import { Skeleton } from '@app/core/models/skeleton.model';
import { CARD_CONTAINER_DESIGN_CLASS } from '@app/presentation/layout/enums/layout.enum';
import { SKELETON_TITLE } from './product-offer-card.constants';

@Component({
  selector: 'layout-product-offer-card',
  templateUrl: './product-offer-card.component.html',
  styleUrls: ['./product-offer-card.component.scss'],
})
export class ProductOfferCardComponent implements OnInit {
  @Input() showIsNewTag: boolean = false;
  @Input() productOfferCard: MerchantGroupProductOfferCard;
  @Input() isLoading?: boolean = true;
  @Input() showDetails?: boolean = false;
  @Input() listDetails?: boolean = false;
  @Input() amountLabel?: string;
  @Input() numberOfInstallmentsLabel?: string;
  @Input() skeletonLoader?: Skeleton[] = SKELETON_TITLE;

  public CARD_CONTAINER_DESIGN_CLASS = CARD_CONTAINER_DESIGN_CLASS;

  constructor() {}

  ngOnInit(): void {}
}
