import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductOfferListItemComponent } from './product-offer-list-item.component';

describe('ProductOfferListItemComponent', () => {
  let component: ProductOfferListItemComponent;
  let fixture: ComponentFixture<ProductOfferListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductOfferListItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductOfferListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
