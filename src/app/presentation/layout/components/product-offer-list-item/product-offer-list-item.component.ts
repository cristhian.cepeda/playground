import {
  Component,
  forwardRef,
  Input,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { MerchantGroupProductOfferListItem } from '@app/core/models/merchant-group-product-offer-card.model';
import { INPUT_TYPE } from '@app/presentation/layout/mo-forms/enums/fields.type';
import { MoInputComponent } from '../../mo-forms/components/mo-input/mo-input.component';

@Component({
  selector: 'layout-product-offer-list-item',
  templateUrl: './product-offer-list-item.component.html',
  styleUrls: ['./product-offer-list-item.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ProductOfferListItemComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ProductOfferListItemComponent),
      multi: true,
    },
  ],
})
export class ProductOfferListItemComponent
  implements OnInit, ControlValueAccessor, Validator
{
  @Input() showIsNewTag: boolean = false;
  @Input() canEdit: boolean = false;
  @Input() hasCheck: boolean = false;
  @Input() isChecked: boolean = false;
  @Input() listDetails?: boolean = false;
  @Input() showDetails: boolean = false;
  @Input() id: string | number;
  @Input() amountLabel?: string;
  @Input() numberOfInstallmentsLabel?: string;
  @Input() productOfferListItem: MerchantGroupProductOfferListItem;
  @Input() isLoading?: boolean;
  @Input() tax?: string;

  @ViewChildren('inputComissionEl', { read: MoInputComponent })
  inputComissionEl?: QueryList<MoInputComponent>;

  private _onChange = (_?: string | null) => {};
  private _onTouch = () => {};
  public value?: string;
  public INPUT_TYPE = INPUT_TYPE;

  ngOnInit(): void {
    this._setInitialValues();
  }

  writeValue(value: string): void {
    this.value = value ?? '';
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouch = fn;
  }

  setDisabledState?(isChecked: boolean): void {
    this.isChecked = isChecked;
  }

  validate(): ValidationErrors {
    if (this.isChecked && this.value?.length === 0) {
      return { required: 'Value is required' };
    }

    return null;
  }

  public onChange() {
    this._onTouch();
    this._onChange(this.value);
  }

  public onParseFloatPercentage(value: string): string {
    return value ? `${String(parseFloat(value))}%` : '-';
  }

  public onFocus(isChecked) {
    if (isChecked)
      setTimeout(() => {
        this.inputComissionEl?.first?.focus();
      });
    else {
      this.value = '';
      this.onChange();
    }
  }

  private _setInitialValues() {
    this.value = '';
    this.isChecked = this.isChecked || !this.hasCheck || !this.canEdit;
  }
}
