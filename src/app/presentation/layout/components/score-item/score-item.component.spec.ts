import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutModule } from '../../layout.module';

import { ScoreItemComponent } from './score-item.component';

describe('ScoreItemComponent', () => {
  let component: ScoreItemComponent;
  let fixture: ComponentFixture<ScoreItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LayoutModule],
      declarations: [ScoreItemComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ScoreItemComponent);
    component = fixture.componentInstance;
    component.date = new Date();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
