import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReplaySubject } from 'rxjs';
import { StepComponent } from '../step/step.component';
import { StepperComponent } from './stepper.component';

describe('StepperComponent', () => {
  let component: StepperComponent;
  let fixture: ComponentFixture<StepperComponent>;
  const eventSubjet: any = new ReplaySubject<RouterEvent>(1);
  const routerMock = {
    navigate: jasmine.createSpy('navigate'),
    events: eventSubjet.asObservable(),
    url: '/test',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [StepperComponent, StepComponent],
      providers: [
        {
          provide: Router,
          useValue: routerMock,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperComponent);
    component = fixture.componentInstance;
    component.steps = [
      {
        key: 'step1',
        value: '/test',
      },
      {
        key: 'step1',
        value: '/test1',
      },
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be _currentIndexStep to equal "0"', fakeAsync(() => {
    const ne = new NavigationEnd(0, '/test', '/test');
    eventSubjet.next(ne);
    tick();
    fixture.detectChanges();
    expect((component as any)._currentIndexStep).toEqual(0);
  }));

  it('should be _currentIndexStep to equal "1"', fakeAsync(() => {
    const ne = new NavigationEnd(0, '/test1', '/test1');
    eventSubjet.next(ne);
    tick();
    fixture.detectChanges();
    expect((component as any)._currentIndexStep).toEqual(1);
  }));

  describe('getState', () => {
    it('should be getState return "selected"', fakeAsync(() => {
      const ne = new NavigationEnd(0, '/test', '/test');
      eventSubjet.next(ne);
      tick();
      fixture.detectChanges();
      expect(component.getState(0)).toEqual('selected');
    }));

    it('should be getState return "completed"', fakeAsync(() => {
      const ne = new NavigationEnd(0, '/test', '/test');
      eventSubjet.next(ne);
      tick();
      fixture.detectChanges();
      expect(component.getState(-1)).toEqual('completed');
    }));

    it('should be getState return "disabled"', fakeAsync(() => {
      const ne = new NavigationEnd(0, '/test', '/test');
      eventSubjet.next(ne);
      tick();
      fixture.detectChanges();
      expect(component.getState(1)).toEqual('disabled');
    }));
  });
});
