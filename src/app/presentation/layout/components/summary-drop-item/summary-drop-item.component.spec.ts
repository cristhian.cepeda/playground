import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryDropItemComponent } from './summary-drop-item.component';

describe('SummaryDropItemComponent', () => {
  let component: SummaryDropItemComponent;
  let fixture: ComponentFixture<SummaryDropItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryDropItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SummaryDropItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
