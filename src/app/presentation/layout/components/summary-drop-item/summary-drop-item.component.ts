import { Component, Input } from '@angular/core';
import { expandCollapseAnimation } from '@app/core/ animations/ animations';

@Component({
  selector: 'layout-summary-drop-item',
  templateUrl: './summary-drop-item.component.html',
  styleUrls: ['./summary-drop-item.component.scss'],
  animations: expandCollapseAnimation,
})
export class SummaryDropItemComponent {
  @Input() title: string;
  public active: boolean;

  public onShowItem(): void {
    this.active = !this.active;
  }
}
