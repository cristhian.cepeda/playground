import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryItemsCategoryComponent } from './summary-items-category.component';

describe('SummaryItemsCategoryComponent', () => {
  let component: SummaryItemsCategoryComponent;
  let fixture: ComponentFixture<SummaryItemsCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryItemsCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryItemsCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
