import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarySubItemComponent } from './summary-sub-item.component';

describe('SummarySubItemComponent', () => {
  let component: SummarySubItemComponent;
  let fixture: ComponentFixture<SummarySubItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummarySubItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarySubItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
