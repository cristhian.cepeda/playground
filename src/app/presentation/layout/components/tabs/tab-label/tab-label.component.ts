import { Component, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'layout-tab-label',
  templateUrl: './tab-label.component.html',
  styleUrls: ['./tab-label.component.scss'],
})
export class TabLabelComponent {
  @ViewChild(TemplateRef)
  public labelContent: TemplateRef<any>;
}
