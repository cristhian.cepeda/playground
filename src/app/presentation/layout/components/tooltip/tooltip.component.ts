import { Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'layout-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
})
export class TooltipComponent<U> {
  @ViewChild('defaultTooltipTemplate', { static: true })
  defaultTooltipTemplate: TemplateRef<any | string>;
  @Input('tooltipTemplate') tooltipTemplate: TemplateRef<any> | string;
  @Input('tooltipData') tooltipData: U;
  @Input() tooltipDirection: 'top' | 'bottom' | 'left' | 'right' = 'top';
  @Input() tooltipDelayInOut: number = 300;
  @Input() tooltipIcon: string =
    '/app/presentation/assets/img/icons/general/tooltip.svg';
  @Input() tooltipMessage: string;
  @Input() tooltipOffset: number = 20;
}
