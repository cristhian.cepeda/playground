import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  Renderer2,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[MOTooltip]',
})
export class TooltipDirective<U> {
  @Input('MOTooltip') tooltipTemplate: TemplateRef<any> | string;
  @Input('tooltipData') tooltipData: U;
  @Input() tooltipDirection: 'top' | 'bottom' | 'left' | 'right' = 'top';
  @Input() tooltipDelayInOut: number = 300;
  @Input() offset: number = 20;
  public tooltip: HTMLElement;
  public windowWidth = window.innerWidth;
  public windowHeight = window.innerHeight;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private _viewContainerRef: ViewContainerRef
  ) {}

  @HostListener('click') onClick() {
    if (!this.tooltip) {
      this.show();
      return;
    }
    this.hide();
  }

  @HostListener('mouseenter') onMouseEnter() {
    if (!this.tooltip) {
      this.show();
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.tooltip) {
      this.hide();
    }
  }

  show() {
    this.create();
    this.setPosition();
    this.renderer.addClass(this.tooltip, 'ng-tooltip-show');
  }

  hide() {
    this.renderer.removeClass(this.tooltip, 'ng-tooltip-show');
    window.setTimeout(() => {
      this.renderer.removeChild(document.body, this.tooltip);
      this.tooltip = null;
    }, this.tooltipDelayInOut / 2);
  }

  public create() {
    // Create element div to make tooltip
    this.tooltip = this.renderer.createElement('div');

    if (this.tooltipTemplate instanceof TemplateRef) {
      // Case teplate
      // Create template as element
      const embeddedViewRef = this._viewContainerRef.createEmbeddedView(
        this.tooltipTemplate,
        {
          data: this.tooltipData,
        }
      );
      embeddedViewRef.detectChanges();

      // Add template to div tooltip
      embeddedViewRef.rootNodes.forEach((template) => {
        this.renderer.appendChild(this.tooltip, template);
      });
    } else {
      // Case String
      this.renderer.appendChild(
        this.tooltip,
        this.renderer.createText(this.tooltipTemplate)
      );
    }

    this.renderer.appendChild(document.body, this.tooltip);

    // add class to position and style
    this.renderer.addClass(this.tooltip, 'ng-tooltip');
    this.renderer.addClass(this.tooltip, `ng-tooltip-${this.tooltipDirection}`);

    // add class to animations
    this._setAnimationInOut();
  }

  private _setAnimationInOut(): void {
    this.renderer.setStyle(
      this.tooltip,
      '-webkit-transition',
      `opacity ${this.tooltipDelayInOut}ms`
    );
    this.renderer.setStyle(
      this.tooltip,
      '-moz-transition',
      `opacity ${this.tooltipDelayInOut}ms`
    );
    this.renderer.setStyle(
      this.tooltip,
      '-o-transition',
      `opacity ${this.tooltipDelayInOut}ms`
    );
    this.renderer.setStyle(
      this.tooltip,
      'transition',
      `opacity ${this.tooltipDelayInOut}ms`
    );
  }

  public setPosition() {
    const hostPosition = this.el.nativeElement.getBoundingClientRect();
    const tooltipPosition = this.tooltip.getBoundingClientRect();

    const scrollPosition =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;

    let top, left;

    if (this.tooltipDirection === 'top') {
      // get margin
      const tooltipStyle = getComputedStyle(this.tooltip);
      const tooltipMarginTop = Number(tooltipStyle.marginTop.replace('px', ''));
      const tooltipMarginBottom = Number(
        tooltipStyle.marginBottom.replace('px', '')
      );
      const tooltipMarginLeft = Number(
        tooltipStyle.marginLeft.replace('px', '')
      );
      const tooltipMarginRight = Number(
        tooltipStyle.marginRight.replace('px', '')
      );

      top =
        hostPosition.top -
        tooltipPosition.height -
        this.offset -
        tooltipMarginBottom -
        tooltipMarginTop;

      left = hostPosition.left + hostPosition.width / 2;
      left =
        left -
        (tooltipPosition.width + tooltipMarginLeft + tooltipMarginRight) / 2;
    }

    if (this.tooltipDirection === 'bottom') {
      const tooltipStyle = getComputedStyle(this.tooltip);
      const tooltipMarginLeft = Number(
        tooltipStyle.marginLeft.replace('px', '')
      );
      const tooltipMarginRight = Number(
        tooltipStyle.marginRight.replace('px', '')
      );
      const tooltipMarginTop = Number(tooltipStyle.marginTop.replace('px', ''));
      const tooltipMarginBottom = Number(
        tooltipStyle.marginBottom.replace('px', '')
      );

      top =
        hostPosition.bottom +
        this.offset +
        tooltipMarginBottom +
        tooltipMarginTop;

      left = hostPosition.left + hostPosition.width / 2;
      left =
        left -
        (tooltipPosition.width + tooltipMarginLeft + tooltipMarginRight) / 2;
    }

    if (this.tooltipDirection === 'left' && this.windowWidth > 990) {
      const tooltipStyle = getComputedStyle(this.tooltip);
      const tooltipMarginTop = Number(tooltipStyle.marginTop.replace('px', ''));
      const tooltipMarginBottom = Number(
        tooltipStyle.marginBottom.replace('px', '')
      );
      const tooltipMarginLeft = Number(
        tooltipStyle.marginLeft.replace('px', '')
      );
      const tooltipMarginRight = Number(
        tooltipStyle.marginRight.replace('px', '')
      );

      top =
        hostPosition.top +
        (hostPosition.height -
          tooltipPosition.height -
          tooltipMarginBottom -
          tooltipMarginTop) /
          2;
      left =
        hostPosition.left -
        tooltipPosition.width -
        this.offset -
        tooltipMarginRight -
        tooltipMarginLeft;
    } else if (this.tooltipDirection === 'left' && this.windowWidth <= 990) {
      const tooltipStyle = getComputedStyle(this.tooltip);
      const tooltipMarginTop = Number(tooltipStyle.marginTop.replace('px', ''));
      const tooltipMarginBottom = Number(
        tooltipStyle.marginBottom.replace('px', '')
      );
      const tooltipMarginLeft = Number(
        tooltipStyle.marginLeft.replace('px', '')
      );
      const tooltipMarginRight = Number(
        tooltipStyle.marginRight.replace('px', '')
      );

      top =
        hostPosition.top -
        tooltipPosition.height -
        this.offset -
        tooltipMarginBottom -
        tooltipMarginTop;

      left = hostPosition.left + hostPosition.width / 2;
      left =
        left -
        (tooltipPosition.width + tooltipMarginLeft + tooltipMarginRight) / 2;
    }

    if (this.tooltipDirection === 'right' && this.windowWidth > 990) {
      const tooltipStyle = getComputedStyle(this.tooltip);
      const tooltipMarginTop = Number(tooltipStyle.marginTop.replace('px', ''));
      const tooltipMarginBottom = Number(
        tooltipStyle.marginBottom.replace('px', '')
      );
      const tooltipMarginLeft = Number(
        tooltipStyle.marginLeft.replace('px', '')
      );
      const tooltipMarginRight = Number(
        tooltipStyle.marginRight.replace('px', '')
      );

      top =
        hostPosition.top +
        (hostPosition.height -
          tooltipPosition.height -
          tooltipMarginBottom -
          tooltipMarginTop) /
          2;
      left =
        hostPosition.right +
        this.offset +
        tooltipMarginLeft +
        tooltipMarginRight;
    } else if (this.tooltipDirection === 'right' && this.windowWidth <= 990) {
      const tooltipStyle = getComputedStyle(this.tooltip);
      const tooltipMarginTop = Number(tooltipStyle.marginTop.replace('px', ''));
      const tooltipMarginBottom = Number(
        tooltipStyle.marginBottom.replace('px', '')
      );
      const tooltipMarginLeft = Number(
        tooltipStyle.marginLeft.replace('px', '')
      );
      const tooltipMarginRight = Number(
        tooltipStyle.marginRight.replace('px', '')
      );

      top =
        hostPosition.top -
        tooltipPosition.height -
        this.offset -
        tooltipMarginBottom -
        tooltipMarginTop;

      left = hostPosition.left + hostPosition.width / 2;
      left =
        left -
        (tooltipPosition.width + tooltipMarginLeft + tooltipMarginRight) / 2;
    }

    this.renderer.setStyle(this.tooltip, 'top', `${top + scrollPosition}px`);
    this.renderer.setStyle(this.tooltip, 'left', `${left}px`);
  }
}
