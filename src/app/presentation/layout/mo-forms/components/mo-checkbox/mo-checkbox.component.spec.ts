import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { setCheckboxValue } from '@app/core/testing';
import { MoCheckboxComponent } from './mo-checkbox.component';

describe('MoCheckboxComponent', () => {
  let component: MoCheckboxComponent;
  let fixture: ComponentFixture<MoCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [MoCheckboxComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoCheckboxComponent);
    fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('writeValue', () => {
    it('should be selected true', () => {
      const selected: boolean = true;
      component.writeValue(selected);
      expect(component.isSelected).toEqual(true);
    });

    it('should be selected false', () => {
      const selected: boolean = false;
      component.writeValue(selected);
      expect(component.isSelected).toEqual(false);
    });
  });

  describe('registerOnChange', () => {
    it('should be _onChange to equal fn', () => {
      const fn = () => {};
      component.registerOnChange(fn);
      expect((component as any)._onChange).toEqual(fn);
    });
  });

  describe('registerOnTouched', () => {
    it('should be _onTouch to equal fn', () => {
      const fn = () => {};
      component.registerOnTouched(fn);
      expect((component as any)._onTouch).toEqual(fn);
    });
  });

  describe('setDisabledState', () => {
    it('should be isDisabled to equal false', () => {
      component.setDisabledState(false);
      expect(component.isDisabled).toBeFalse();
    });

    it('should be isDisabled to equal true', () => {
      component.setDisabledState(true);
      expect(component.isDisabled).toBeTrue();
    });
  });

  it('should be is selected true when element click event', () => {
    setCheckboxValue(fixture, 'checkbox', true, true);
    fixture.detectChanges();
    expect(component.isSelected).toBeTrue();
  });

  it('should be is selected false when element click event', () => {
    setCheckboxValue(fixture, 'checkbox', false, true);
    fixture.detectChanges();
    expect(component.isSelected).toBeFalse();
  });
});
