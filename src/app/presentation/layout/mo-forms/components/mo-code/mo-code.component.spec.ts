import { DebugElement, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import {
  ComponentFixture,
  discardPeriodicTasks,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MODatePipe } from '@app/presentation/layout/pipes/date.pipe';
import { MoInputComponent } from '../mo-input/mo-input.component';
import { MoCodeComponent } from './mo-code.component';

describe('MoCodeComponent', () => {
  let component: MoCodeComponent;
  let fixture: ComponentFixture<MoCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [MoInputComponent, MODatePipe],
      declarations: [MoCodeComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoCodeComponent);
    component = fixture.componentInstance;
    fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);
    fixture.debugElement.injector.get(NG_VALIDATORS);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be onChangeValue fn has called with code mock', () => {
    component.code = 123;
    const spy = spyOn(component as any, '_onChange');
    component.onChangeValue();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith(123);
  });

  describe('writeValue', () => {
    it('should be values = "[NaN]" with value null', () => {
      const value: string = null;
      component.writeValue(value);
      expect(component.values).toEqual([NaN]);
    });

    it('should be values = "[1,2,3]" with value 123', () => {
      const value: string = '123';
      component.length = 3;
      component.writeValue(value);
      expect(component.values).toEqual([1, 2, 3]);
    });
  });

  describe('registerOnChange', () => {
    it('should be _onChange to equal fn', () => {
      const fn = () => {};
      component.registerOnChange(fn);
      expect((component as any)._onChange).toEqual(fn);
    });
  });

  describe('registerOnTouched', () => {
    it('should be _onTouch to equal fn', () => {
      const fn = () => {};
      component.registerOnTouched(fn);
      expect((component as any)._onTouch).toEqual(fn);
    });
  });

  describe('setDisabledState', () => {
    it('should be isDisabled to equal false', () => {
      component.setDisabledState(false);
      expect(component.isDisabled).toBeFalse();
    });

    it('should be isDisabled to equal true', () => {
      component.setDisabledState(true);
      expect(component.isDisabled).toBeTrue();
    });
  });

  describe('ngOnChanges', () => {
    it('should be currentSecond to equal "0" when onChanges call', () => {
      component.isSentCode = true;
      component.ngOnChanges({
        isSentCode: new SimpleChange(false, true, true),
      });
      expect(component.currentSecond).toEqual(0);
    });
  });

  describe('onInput', () => {
    beforeEach(() => {
      component.value = '';
      component.isDisabled = false;
      component.required = true;
    });

    it('should be expect nothing when type a number', () => {
      component.length = 3;
      const inputDebugElement: DebugElement[] = fixture.debugElement.queryAll(
        By.css('mo-input')
      );
      const eventObj = {
        target: { value: '1' },
        data: 'text',
        key: '1',
      };
      fixture.detectChanges();
      inputDebugElement[0].triggerEventHandler('keydown', eventObj);
      expect().nothing();
    });

    it('should be expect nothing when type a letter', () => {
      component.length = 3;
      const inputDebugElement: DebugElement[] = fixture.debugElement.queryAll(
        By.css('mo-input')
      );
      const eventObj = {
        target: { value: 'A' },
        data: 'text',
        key: 'A',
      };
      fixture.detectChanges();
      inputDebugElement[0].triggerEventHandler('keydown', eventObj);
      expect().nothing();
    });

    it('should be expect nothing when push a "backspace" key', () => {
      component.length = 3;
      component.ngOnInit();
      fixture.detectChanges();
      const inputDebugElement: DebugElement[] = fixture.debugElement.queryAll(
        By.css('mo-input')
      );
      fixture.detectChanges();
      inputDebugElement[1].triggerEventHandler('keydown', {
        target: {},
        key: 'Backspace',
      });
      fixture.detectChanges();
      expect().nothing();
    });
  });

  describe('onInput', () => {
    beforeEach(() => {
      component.length = 3;
      component.ngOnInit();
      fixture.detectChanges();
    });

    it('should be expect values[0] equal "1"', () => {
      const inputDebugElement: DebugElement[] = fixture.debugElement.queryAll(
        By.css('mo-input')
      );
      const eventObj = {
        target: { value: '1' },
      };
      fixture.detectChanges();
      inputDebugElement[0].triggerEventHandler('input', eventObj);
      expect(component.values[0] as any).toEqual('1');
    });

    it('should be expect values[0] equal undefined', () => {
      component.length = 0;
      component.ngOnInit();
      const inputDebugElement: DebugElement[] = fixture.debugElement.queryAll(
        By.css('mo-input')
      );
      const eventObj = {
        target: { value: null },
      };
      fixture.detectChanges();
      inputDebugElement[0].triggerEventHandler('input', eventObj);
      expect(component.values[0] as any).toBeUndefined();
    });

    it('isolated: should be return undefined', () => {
      component.values = undefined;
      const result = component.onInput({ target: { value: null } }, 0);
      expect(result).toBeUndefined();
    });
  });

  describe('showWatch', () => {
    it('should be isDisabledResendCode to equal "true"', () => {
      component.isDisabledResendCode = false;
      component.showWatch();
      expect(component.isDisabledResendCode).toBeTrue();
    });

    it('should be time change to "00:58"', fakeAsync(() => {
      component.currentSecond = 1;
      fixture.detectChanges();
      component.showWatch();
      tick(1500);
      fixture.detectChanges();
      expect(component.time).toEqual('00:58');
      discardPeriodicTasks();
    }));

    it('should be time change to "00:08"', fakeAsync(() => {
      component.currentSecond = 1;
      fixture.detectChanges();
      component.showWatch();
      tick(1000 * 51 + 500);
      fixture.detectChanges();
      expect(component.time).toEqual('00:08');
      discardPeriodicTasks();
    }));

    it('should be clear interval', fakeAsync(() => {
      component.currentSecond = 60;
      fixture.detectChanges();
      component.showWatch();
      tick(1000);
      fixture.detectChanges();
      expect(component.isDisabledResendCode).toBeFalse();
      discardPeriodicTasks();
    }));
  });

  describe('resendCode', () => {
    it('shoul be emit resend "true"', () => {
      component.isDisabledResendCode = false;
      spyOn(component.resend, 'emit');
      component.resendCode();
      expect(component.resend.emit).toHaveBeenCalledWith(true);
    });

    it('shoul be not emit resend "true"', () => {
      component.isDisabledResendCode = true;
      spyOn(component.resend, 'emit');
      component.resendCode();
      expect(component.resend.emit).not.toHaveBeenCalled();
    });
  });
});
