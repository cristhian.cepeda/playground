import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  Validators,
} from '@angular/forms';
import { APP_CONSTANTS } from '@app/core/constants/app.constants';
import { createTimer, fancyTimeFormat } from '@app/core/models/utils.model';
import { INPUT_TYPE } from '../../enums/fields.type';
import { MoInputComponent } from '../mo-input/mo-input.component';

@Component({
  selector: 'mo-input-code',
  templateUrl: './mo-code.component.html',
  styleUrls: ['./mo-code.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MoCodeComponent),
      multi: true,
    },
  ],
})
export class MoCodeComponent
  implements OnInit, ControlValueAccessor, Validators, AfterContentChecked
{
  @Input() id = '';
  @Input() label = '';
  @Input() reSendCodeText = 'Resend code';
  @Input() required = true;
  @Input() length: number = 4;
  @Input() autoSend: boolean = true;
  @Input() seconds: number = APP_CONSTANTS.OTP_TIME_SECOND;
  @Input() isDisabled?: boolean;
  @Output() resend: EventEmitter<boolean>;
  @Output() send: EventEmitter<string>;

  public value?: string;
  public intervalWatch: any;
  public isDisabledResendCode: boolean;
  public inputs?: Array<number>;
  public values: Array<number>;
  public currentSecond: number;
  public time: string;
  public code?: number;
  private _onChange = (_: any) => {};
  private _onTouch = () => {};
  public INPUT_TYPE = INPUT_TYPE;

  @ViewChildren('inputsEl', { read: MoInputComponent })
  inputCode?: QueryList<MoInputComponent>;

  constructor(private cdref: ChangeDetectorRef) {
    this.value = '';
    this.currentSecond = 0;
    this.values = [];
    this.resend = new EventEmitter(false);
    this.send = new EventEmitter();
  }

  ngOnInit() {
    this.values = Array(this.length).fill('');
    this.inputs = Array(this.length);
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  public onKeydown(event: any, index: number) {
    const isDeleteKey = event.key === 'Backspace';
    const value = event.target['value'];
    if (isDeleteKey && !value) {
      const prevIndex = index - 1;
      if (prevIndex >= 0) {
        this.inputCode?.toArray()[prevIndex].focus();
      }
    }
  }

  public onInput(event: any, index: number) {
    const value = event.target['value'];
    if (!this.values) return;
    this.values[index] = value;
    this.value = this.values.reduce(
      (code?: string, number?: number) => `${code}${number}`,
      ''
    );

    this._onTouch();
    this._onChange(this.value);

    if (this.autoSend) this._onSendCode();

    if (value.length >= 1 && value != '') {
      const nextIndex = index + 1;
      if (nextIndex && nextIndex < this.length) {
        this.inputCode?.toArray()[nextIndex].focus();
      }
    }
  }

  public onResendCode() {
    if (!this.isDisabledResendCode) {
      this.resend.emit(true);
      this._restartTimer();
    }
  }

  private _startTimer() {
    createTimer(this.seconds).subscribe(
      (currentSecond) => {
        this.time = fancyTimeFormat(currentSecond);
      },
      () => {},
      () => {
        this._setResendCodeStatus();
      }
    );
  }

  private _restartTimer(): void {
    this._setResendCodeStatus(true);
    this._startTimer();
  }

  private _setResendCodeStatus(isDisabledResendCode: boolean = false) {
    this.isDisabledResendCode = isDisabledResendCode;
  }

  private _onSendCode() {
    if (this.value.length === this.length) this.send.emit(this.value);
  }

  writeValue(value: string): void {
    if (!value) {
      this.values?.fill(NaN);
    }
    const valueNumber = Number(value);
    if (valueNumber && value.length <= this.length && this.values) {
      for (let i = 0; i < value.length; i++) {
        this.values[i] = Number(value.charAt(i));
      }
    }
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
