import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoDateComponent } from './mo-date.component';

describe('MoDateComponent', () => {
  let component: MoDateComponent;
  let fixture: ComponentFixture<MoDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoDateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MoDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
