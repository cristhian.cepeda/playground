import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoDropDownComponent } from './mo-drop-down.component';

describe('MoDropDownComponent', () => {
  let component: MoDropDownComponent;
  let fixture: ComponentFixture<MoDropDownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoDropDownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
