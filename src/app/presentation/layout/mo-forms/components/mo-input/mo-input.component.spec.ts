import { DebugElement, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import {
  MatDatepicker,
  MatDatepickerModule,
} from '@angular/material/datepicker';
import { By } from '@angular/platform-browser';
import { MODatePipe } from '@app/presentation/layout/pipes/date.pipe';
import { ICON_POSITION, INPUT_TYPE } from '../../enums/fields.type';
import { MoInputComponent } from './mo-input.component';

describe('MoInputComponent', () => {
  let component: MoInputComponent;
  let fixture: ComponentFixture<MoInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDatepickerModule],
      declarations: [MoInputComponent],
      providers: [MODatePipe, DateAdapter],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoInputComponent);
    component = fixture.componentInstance;
    component.picker = TestBed.createComponent(MatDatepicker<Date>)
      .componentInstance as MatDatepicker<Date>;
    fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);
    fixture.debugElement.injector.get(NG_VALIDATORS);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnChanges', () => {
    it('should be regex be defined', () => {
      component.regex = new RegExp('[a-z]');
      component.ngOnChanges({});
      expect(component.regex).toBeDefined();
    });

    it('should be regex be defined with string', () => {
      component.regex = '[a-z]' as any;
      component.ngOnChanges({});
      expect(component.regex).toBeDefined();
      expect(component.regex).toBeInstanceOf(RegExp);
    });

    it('should be iconPosition equal to right', () => {
      component.icon = 'test' as any;
      component.iconPosition;
      component.ngOnChanges({ icon: new SimpleChange('', 'test', true) });
      expect(component.iconPosition).toEqual(ICON_POSITION.RIGHT);
    });

    it('should be setup by type has been called', () => {
      const spy = spyOn(component as any, '_setupByType');
      component.ngOnChanges({ type: new SimpleChange('', 'test', true) });
      expect(spy).toHaveBeenCalled();
    });
  });

  describe('writeValue', () => {
    it('should be value has equal to test', () => {
      component.value = '';
      component.valueUpdated = '';
      component.writeValue('test');
      expect(component.value).toEqual('test');
      expect(component.valueUpdated).toEqual('test');
    });

    it('should be value has equal to ""', () => {
      component.value = null;
      component.valueUpdated = null;
      component.writeValue(null);
      expect(component.value).toEqual('');
      expect(component.valueUpdated).toEqual('');
    });
  });

  describe('registerOnChange', () => {
    it('should be _onChange to equal fn', () => {
      const fn = () => {};
      component.registerOnChange(fn);
      expect((component as any)._onChange).toEqual(fn);
    });
  });

  describe('registerOnTouched', () => {
    it('should be _onTouch to equal fn', () => {
      const fn = () => {};
      component.registerOnTouched(fn);
      expect((component as any)._onTouch).toEqual(fn);
    });
  });

  describe('setDisabledState', () => {
    it('should be isDisabled to equal false', () => {
      component.setDisabledState(false);
      expect(component.isDisabled).toBeFalse();
    });

    it('should be isDisabled to equal true', () => {
      component.setDisabledState(true);
      expect(component.isDisabled).toBeTrue();
    });
  });

  describe('onInput', () => {
    beforeEach(() => {
      component.isCalendarComponent = false;
      component.type = INPUT_TYPE.TEXT;
      component.value = 'text';
      component.placeholder = 'placeholderText';
      component.isDisabled = false;
      component.disabled = false;
      component.required = true;
      component.inputmode = '';
      component.regex = new RegExp('');
      component.maxlength = 4;
    });

    it('should be value has equal to "" for maxLength validation', (doneFn) => {
      component.maxlength = 3;

      const spy = spyOn(component as any, '_onTouch');
      const inputDebugElement: DebugElement = fixture.debugElement.query(
        By.css('input.form-control')
      );
      const eventObj = {
        target: { value: 'text' },
        data: 'text',
      };
      component.Input.subscribe((event: Event) => {
        expect((<HTMLInputElement>event.target)?.value).toEqual('');
        doneFn();
      });

      fixture.detectChanges();

      inputDebugElement.triggerEventHandler('input', eventObj);
      expect(spy).toHaveBeenCalled();
    });

    it('should be value has equal to "text"', (doneFn) => {
      const spy = spyOn(component as any, '_onTouch');
      const inputDebugElement: DebugElement = fixture.debugElement.query(
        By.css('input.form-control')
      );
      const eventObj = {
        target: { value: 'text' },
        data: 'text',
      };
      component.Input.subscribe((event: Event) => {
        expect(event).toEqual(eventObj as any);
        doneFn();
      });

      fixture.detectChanges();

      inputDebugElement.triggerEventHandler('input', eventObj);
      expect(spy).toHaveBeenCalled();
    });

    it('should be value has equal to ""', (doneFn) => {
      const spy = spyOn(component as any, '_onTouch');
      const inputDebugElement: DebugElement = fixture.debugElement.query(
        By.css('input.form-control')
      );
      const eventObj = {
        target: { value: '' },
        data: '',
      };
      component.Input.subscribe((event: Event) => {
        expect(event).toEqual(eventObj as any);
        doneFn();
      });

      fixture.detectChanges();

      inputDebugElement.triggerEventHandler('input', eventObj);
      expect(spy).toHaveBeenCalled();
      expect(component.valueUpdated).toEqual('');
    });

    it('should be value has equal to "test"', (doneFn) => {
      const spy = spyOn(component as any, '_onTouch');
      const inputDebugElement: DebugElement = fixture.debugElement.query(
        By.css('input.form-control')
      );
      const eventObj = {
        target: { value: 'test' },
        data: '',
      };
      component.Input.subscribe((event: Event) => {
        expect(event).toEqual(eventObj as any);
        doneFn();
      });

      fixture.detectChanges();

      inputDebugElement.triggerEventHandler('input', eventObj);
      expect(spy).toHaveBeenCalled();
      expect(component.valueUpdated).toEqual('test');
    });

    it('should be value has empty for current value validation', () => {
      component.valueUpdated = '';
      component.regexInput = new RegExp('[0-9]');
      const spy = spyOn(component as any, '_onTouch');
      const inputDebugElement: DebugElement = fixture.debugElement.query(
        By.css('input.form-control')
      );
      const eventObj = {
        target: { value: 'test' },
        data: '',
      };

      fixture.detectChanges();

      inputDebugElement.triggerEventHandler('input', eventObj);
      expect(spy).toHaveBeenCalled();
      expect(component.valueUpdated).toEqual('');
    });

    it('should be value has empty for current value validation', () => {
      component.regexInput = undefined;
      const spy = spyOn(component as any, '_onTouch');
      const inputDebugElement: DebugElement = fixture.debugElement.query(
        By.css('input.form-control')
      );
      const eventObj = {
        target: { value: 'test' },
        data: 'test',
      };

      fixture.detectChanges();

      inputDebugElement.triggerEventHandler('input', eventObj);
      expect(spy).toHaveBeenCalled();
      expect(component.valueUpdated).toEqual('');
    });
  });
});
