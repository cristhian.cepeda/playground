import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MoRadioComponent } from './mo-radio.component';

describe('MoRadioComponent', () => {
  let component: MoRadioComponent;
  let fixture: ComponentFixture<MoRadioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MoRadioComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoRadioComponent);
    component = fixture.componentInstance;
    fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onChange', () => {
    it('should register the onChange function', () => {
      const inputDebugElement = fixture.debugElement.query(By.css('input'));
      fixture.detectChanges();
      var event = new Event('change', {
        target: { value: 'text' },
      } as unknown as Event);
      inputDebugElement.nativeElement.dispatchEvent(event);
      expect((component as any)._onTouch).toBeInstanceOf(Function);
    });
  });

  describe('setDisabledState', () => {
    it('should be isDisabled equal true', () => {
      component.setDisabledState(true);
      expect(component.isDisabled).toBeTruthy();
    });

    it('should be isDisabled equal false', () => {
      component.setDisabledState(false);
      expect(component.isDisabled).toBeFalsy();
    });
  });
});
