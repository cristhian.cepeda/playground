import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MoSelectComponent } from './mo-select.component';

describe('MoSelectComponent', () => {
  let component: MoSelectComponent;
  let fixture: ComponentFixture<MoSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MoSelectComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
