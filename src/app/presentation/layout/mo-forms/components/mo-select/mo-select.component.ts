import { Component, Input } from '@angular/core';
import { SelectOption } from '../../interfaces/form.interface';

@Component({
  selector: 'mo-select',
  templateUrl: './mo-select.component.html',
  styleUrls: ['./mo-select.component.scss'],
})
export class MoSelectComponent {
  @Input() label: string;
  @Input() options: SelectOption<string>[];

  constructor() {
    this.label = '';
    this.options = [];
  }
}
