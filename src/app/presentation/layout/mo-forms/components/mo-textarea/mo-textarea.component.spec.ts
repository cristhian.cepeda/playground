import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MoTextAreaComponent } from './mo-textarea.component';

describe('MoTextAreaComponent', () => {
  let component: MoTextAreaComponent;
  let fixture: ComponentFixture<MoTextAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MoTextAreaComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoTextAreaComponent);
    component = fixture.componentInstance;
    fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create the component and have default inits', () => {
    expect(component).toBeTruthy();
    (component as any)._onTouch();
    (component as any)._onChange();
  });

  describe('writeValue', () => {
    it('should be value equal to "text"', () => {
      component.writeValue('text');
      expect(component.value).toEqual('text');
    });

    it('should be value equal to ""', () => {
      component.writeValue('');
      expect(component.value).toEqual('');
    });
  });

  describe('registerOnChange', () => {
    it('should register the onChange function', () => {
      const fn = () => {};
      component.registerOnChange(fn);
      expect((component as any)._onChange).toBe(fn);
    });
  });

  describe('registerOnTouched', () => {
    it('should register the onTouch function', () => {
      const fn = () => {};
      component.registerOnTouched(fn);
      expect((component as any)._onTouch).toBe(fn);
    });
  });

  describe('setDisabledState', () => {
    it('should be disabled true', () => {
      component.setDisabledState(true);
      expect(component.disabled).toBeTruthy();
    });

    it('should be disabled false', () => {
      component.setDisabledState(false);
      expect(component.disabled).toBeFalsy();
    });
  });

  describe('onChange', () => {
    it('should register the onChange function', () => {
      const textAreaDebugElement = fixture.debugElement.query(
        By.css('textarea')
      );
      fixture.detectChanges();
      var event = new Event('change', {
        target: { value: 'text' },
      } as unknown as Event);
      textAreaDebugElement.nativeElement.dispatchEvent(event);
      expect((component as any)._onTouch).toBeInstanceOf(Function);
    });
  });

  describe('onInput', () => {
    it('should register the onInput function', () => {
      const textAreaDebugElement = fixture.debugElement.query(
        By.css('textarea')
      );
      fixture.detectChanges();
      var event = new Event('input', {
        target: { value: 'text' },
      } as unknown as Event);
      textAreaDebugElement.nativeElement.dispatchEvent(event);
      expect((component as any)._onTouch).toBeInstanceOf(Function);
    });
  });
});
