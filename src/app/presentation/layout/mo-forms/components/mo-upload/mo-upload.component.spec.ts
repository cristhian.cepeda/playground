import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MoUploadComponent } from './mo-upload.component';

describe('MoUploadComponent', () => {
  let component: MoUploadComponent;
  let fixture: ComponentFixture<MoUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MoUploadComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
