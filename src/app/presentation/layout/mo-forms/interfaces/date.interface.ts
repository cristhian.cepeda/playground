import { Dayjs } from 'dayjs';
import { LocaleConfig } from 'ngx-daterangepicker-material';
import { DateRanges } from 'ngx-daterangepicker-material/daterangepicker.component';

export interface MoDateToolip {
  date: Dayjs;
  text: string;
}
export interface MoDateConfig {
  locale: LocaleConfig;
  ranges: DateRanges;
}
export interface MoDateRange {
  startDate: string;
  endDate: string;
}
