import { TestBed } from '@angular/core/testing';

import { FormBuilder, Validators } from '@angular/forms';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { FORM_CONSTANTS } from '../constants/form.constants';
import { FormUtilService } from './form-util.service';

describe('FormUtilService', () => {
  let service: FormUtilService;
  let store: MockStore;
  const initialState: any = {};
  let formBuilder: FormBuilder;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), FormBuilder],
    });
    service = TestBed.inject(FormUtilService);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('validErrors', () => {
    it('should return "true" if form control has errors and is dirty', () => {
      formBuilder = TestBed.inject(FormBuilder);
      const form = formBuilder.group({
        test: ['', Validators.required],
      });
      form.controls.test.markAsDirty();
      const result = service.validErrors(form, 'test');
      expect(result).toBeTrue();
    });

    it('should return "true" if form control has errors and is touched', () => {
      formBuilder = TestBed.inject(FormBuilder);
      const form = formBuilder.group({
        test: ['', Validators.required],
      });
      form.controls.test.markAllAsTouched();
      const result = service.validErrors(form, 'test');
      expect(result).toBeTrue();
    });

    it('should return "null" if form control has errors and is dirty', () => {
      formBuilder = TestBed.inject(FormBuilder);
      const form = formBuilder.group({
        test: ['test', Validators.required],
      });
      form.controls.test.markAsDirty();
      const result = service.validErrors(form, 'test');
      expect(result).toBeNull();
    });

    it('should return "null" if form control has errors and is touched', () => {
      formBuilder = TestBed.inject(FormBuilder);
      const form = formBuilder.group({
        test: ['test', Validators.required],
      });
      form.controls.test.markAllAsTouched();
      const result = service.validErrors(form, 'test');
      expect(result).toBeNull();
    });
  });

  describe('renderKeyError', () => {
    it('should return error required copy', () => {
      const errors: any = {
        required: true,
      };
      const result = service.renderKeyError(errors);
      expect(result).toEqual(
        FORM_CONSTANTS.GENERIC_KEY_ERRORS.INPUT_ERROR_REQUIRED
      );
    });

    it('should return error email copy', () => {
      const errors: any = {
        email: true,
      };
      const result = service.renderKeyError(errors);
      expect(result).toEqual(
        FORM_CONSTANTS.GENERIC_KEY_ERRORS.INPUT_ERROR_EMAIL
      );
    });

    it('should return "null" with errors "null"', () => {
      const errors: any = null;
      const result = service.renderKeyError(errors);
      expect(result).toBeNull();
    });

    it('should return "null" with errors "{}"', () => {
      const errors: any = {};
      const result = service.renderKeyError(errors);
      expect(result).toBeNull();
    });

    it('should return "null" with errors "custom"', () => {
      const errors: any = {
        custom: true,
      };
      const result = service.renderKeyError(errors);
      expect(result).toBeNull();
    });
  });
});
