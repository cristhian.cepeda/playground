import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { MODatePipe } from '@app/presentation/layout/pipes/date.pipe';
import { CellComponent } from './cell.component';

describe('CellComponent', () => {
  let component: CellComponent<any>;
  let fixture: ComponentFixture<CellComponent<any>>;
  let mockHeader: TableHeader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CellComponent],
      providers: [MODatePipe, CustomCurrencyPipe],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CellComponent);
    component = fixture.componentInstance;
    mockHeader = {
      typeColumn: TYPE_COLUMN.LABEL,
    };
    component.header = mockHeader;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getTextColumn', () => {
    describe('element call getTextColumn', () => {
      it('should be label equal "test"', () => {
        const header: TableHeader = {
          dataKey: 'test',
        };
        const row: any = {
          test: 'test',
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('test');
      });

      it('should be label equal "0" with "0"', () => {
        const header: TableHeader = {
          dataKey: 'test',
        };
        const row: any = {
          test: 0,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('0');
      });

      it('should be label equal "TEST" with transform function callback', () => {
        const header: TableHeader = {
          dataKey: 'test',
          transform: (text: string) => text.toUpperCase(),
        };
        const row: any = {
          test: 'test',
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('TEST');
      });
    });

    describe('isolated call getTextColumn', () => {
      it('should be and return "test"', () => {
        const header: TableHeader = {
          dataKey: 'test',
        };
        const row: any = {
          test: 'test',
        };
        const result = component.getTextColumn(header, row);
        expect(result).toBe('test');
      });

      it('isolated should be and return "TEST" with transform function callback', () => {
        const header: TableHeader = {
          dataKey: 'test',
          transform: (text: string) => text.toUpperCase(),
        };
        const row: any = {
          test: 'test',
        };
        const result = component.getTextColumn(header, row);
        expect(result).toBe('TEST');
      });
    });
  });

  describe('getDate', () => {
    describe('element call getDate', () => {
      it('should be with "2022-10-22" and return "Sat, 22 Oct, 2022"', () => {
        const date = '2022-10-22 00:00:10';
        const resultDate = 'Sat, 22 Oct, 2022';
        const header: TableHeader = {
          dataKey: 'date',
          typeColumn: TYPE_COLUMN.DATE,
        };
        const row: any = {
          date: date,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe(resultDate);
      });

      it('should be with "" and return "-"', () => {
        const date = '';
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
          typeColumn: TYPE_COLUMN.DATE,
        };
        const row: any = {
          date: date,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe(resultDate);
      });

      it('should be with null and return "-"', () => {
        const date = null;
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
          typeColumn: TYPE_COLUMN.DATE,
        };
        const row: any = {
          date: date,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe(resultDate);
      });

      it('should be with undefined and return "-"', () => {
        const date = undefined;
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
          typeColumn: TYPE_COLUMN.DATE,
        };
        const row: any = {
          date: date,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe(resultDate);
      });

      it('should be with empty row and return "-"', () => {
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
          typeColumn: TYPE_COLUMN.DATE,
        };
        const row: any = {};
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe(resultDate);
      });
    });

    describe('isolated call getDate', () => {
      it('should be with "2022-10-22" and return "Sat, 22 Oct, 2022"', () => {
        const date = '2022-10-22 00:00:10';
        const resultDate = 'Sat, 22 Oct, 2022';
        const header: TableHeader = {
          dataKey: 'date',
        };
        const row: any = {
          date: date,
        };
        const result = component.getDate(header, row);
        expect(result).toBe(resultDate);
      });

      it('should be with "" and return "-"', () => {
        const date = '';
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
        };
        const row: any = {
          date: date,
        };
        const result = component.getDate(header, row);
        expect(result).toBe(resultDate);
      });

      it('should be with null and return "-"', () => {
        const date = null;
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
        };
        const row: any = {
          date: date,
        };
        const result = component.getDate(header, row);
        expect(result).toBe(resultDate);
      });

      it('should be with undefined and return "-"', () => {
        const date = undefined;
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
        };
        const row: any = {
          date: date,
        };
        const result = component.getDate(header, row);
        expect(result).toBe(resultDate);
      });

      it('should be with empty row and return "-"', () => {
        const resultDate = '-';
        const header: TableHeader = {
          dataKey: 'date',
        };
        const row: any = {};
        const result = component.getDate(header, row);
        expect(result).toBe(resultDate);
      });
    });
  });

  describe('getCurrencyValue', () => {
    describe('element call getCurrencyValue', () => {
      it('should be with "100" and return "$100"', () => {
        const header: TableHeader = {
          dataKey: 'number',
          typeColumn: TYPE_COLUMN.CURRENCY,
        };
        const row: any = {
          number: 100,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('$100');
      });

      it('should be with "100000" and return "$100,000"', () => {
        const header: TableHeader = {
          dataKey: 'number',
          typeColumn: TYPE_COLUMN.CURRENCY,
        };
        const row: any = {
          number: 100000,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('$100,000');
      });

      it('should be with undefined and return "-"', () => {
        const header: TableHeader = {
          dataKey: 'number',
          typeColumn: TYPE_COLUMN.CURRENCY,
        };
        const row: any = {
          number: undefined,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('-');
      });

      it('should be with null and return "-"', () => {
        const header: TableHeader = {
          dataKey: 'number',
          typeColumn: TYPE_COLUMN.CURRENCY,
        };
        const row: any = {
          number: null,
        };
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('-');
      });

      it('should be with empty row and return "-"', () => {
        const header: TableHeader = {
          dataKey: 'number',
          typeColumn: TYPE_COLUMN.CURRENCY,
        };
        const row: any = {};
        component.header = header;
        component.row = row;
        fixture.detectChanges();
        const labelDdebug = fixture.debugElement.query(By.css('label'));
        const labelElement = labelDdebug.nativeElement;
        expect(labelElement.textContent.trim()).toBe('-');
      });
    });

    describe('isolated call getCurrencyValue', () => {
      it('should be with "100" and return "$100"', () => {
        const header: TableHeader = {
          dataKey: 'number',
        };
        const row: any = {
          number: 100,
        };
        const result = component.getCurrencyValue(header, row);
        expect(result).toBe('$100');
      });

      it('should be with "100000" and return "$100,000"', () => {
        const header: TableHeader = {
          dataKey: 'number',
        };
        const row: any = {
          number: 100000,
        };
        const result = component.getCurrencyValue(header, row);
        expect(result).toBe('$100,000');
      });

      it('should be with undefined and return "-"', () => {
        const header: TableHeader = {
          dataKey: 'number',
        };
        const row: any = {
          number: undefined,
        };
        const result = component.getCurrencyValue(header, row);
        expect(result).toBe('-');
      });

      it('should be with null and return "-"', () => {
        const header: TableHeader = {
          dataKey: 'number',
        };
        const row: any = {
          number: null,
        };
        const result = component.getCurrencyValue(header, row);
        expect(result).toBe('-');
      });

      it('should be with empty row and return "-"', () => {
        const header: TableHeader = {
          dataKey: 'number',
        };
        const row: any = {};
        const result = component.getCurrencyValue(header, row);
        expect(result).toBe('-');
      });
    });
  });
});
