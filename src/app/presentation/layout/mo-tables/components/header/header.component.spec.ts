import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { TABLE_SORT } from '../../enums/table.enum';
import { ColumnSort, TableHeader } from '../../interfaces/table.interface';
import { HeaderComponent } from './header.component';

describe('HeadersComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [HeaderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.header = {
      label: 'mock label',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnChanges', () => {
    it('should be detect chanches on columnSort on call isolated', () => {
      fixture.detectChanges();
      const columnSort: ColumnSort = {
        column: null,
        sortBy: TABLE_SORT.ASC,
        typeSort: 'string',
      };
      component.columnSort = columnSort;
      component.ngOnChanges({
        sortedColumn: new SimpleChange(component.sortedColumn, 'test1', false),
      });
      expect(component.columnSort).toEqual(columnSort);
    });
  });

  describe('onHeaderClick', () => {
    it('should be return undefined', () => {
      const header: TableHeader = {
        dataKey: 'text',
        isSortable: false,
      };
      const result = component.onHeaderClick(header);
      expect(result).toBeUndefined();
    });

    it('should be return undefined and emit sort column null', (doneFn) => {
      const header: TableHeader = {
        dataKey: 'text',
        isSortable: true,
      };
      component.columnSort = {
        column: 'text',
        sortBy: TABLE_SORT.DESC,
      };
      component.sort.subscribe((sort) => {
        expect(sort).toEqual({
          column: null,
          sortBy: TABLE_SORT.ASC,
        });
        doneFn();
      });
      const result = component.onHeaderClick(header);
      expect(result).toBeUndefined();
    });

    it('should be return undefined and emit sort by "DESC"', (doneFn) => {
      const header: TableHeader = {
        dataKey: 'text',
        isSortable: true,
      };
      component.columnSort = {
        column: 'text',
        sortBy: TABLE_SORT.ASC,
      };

      const result: ColumnSort = {
        sortBy: TABLE_SORT.DESC,
        column: 'text',
        typeSort: 'string',
      };
      component.sort.subscribe((sort) => {
        expect(sort).toEqual(result);
        doneFn();
      });
      expect(component.onHeaderClick(header)).toBeUndefined();
    });

    it('should be return undefined and emit sort by "ASC"', (doneFn) => {
      const header: TableHeader = {
        dataKey: 'data',
        isSortable: true,
      };
      component.columnSort = {
        column: 'text',
        sortBy: TABLE_SORT.DESC,
      };

      const result: ColumnSort = {
        sortBy: TABLE_SORT.ASC,
        column: 'data',
        typeSort: 'string',
      };
      component.sort.subscribe((sort) => {
        expect(sort).toEqual(result);
        doneFn();
      });
      expect(component.onHeaderClick(header)).toBeUndefined();
    });
  });
});
