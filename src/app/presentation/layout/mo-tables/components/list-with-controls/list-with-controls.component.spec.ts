import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListWithControlsComponent } from './list-with-controls.component';

describe('ListWithControlsComponent', () => {
  let component: ListWithControlsComponent<any>;
  let fixture: ComponentFixture<ListWithControlsComponent<any>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListWithControlsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ListWithControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
