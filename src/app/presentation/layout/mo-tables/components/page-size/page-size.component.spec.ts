import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { PageSizeComponent } from './page-size.component';

describe('PageSizeComponent', () => {
  let component: PageSizeComponent;
  let fixture: ComponentFixture<PageSizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [PageSizeComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnChanges', () => {
    it('should be detect chanches on pageSize', () => {
      component.pageSize = 10;
      component.ngOnChanges();
      fixture.detectChanges();
      expect(component.seletedOption).toEqual(10);
    });
  });

  describe('onChangePageSize', () => {
    it('should be emit changePageSize with value 10 on call isolated', (doneFn) => {
      component.changePageSize.subscribe((value) => {
        expect(value).toEqual(10);
        doneFn();
      });
      component.onChangePageSize(10);
    });

    it('should be emit changePageSize with value "10" on call isolated', (doneFn) => {
      component.changePageSize.subscribe((value) => {
        expect(value).toEqual(10);
        doneFn();
      });
      component.onChangePageSize('10');
    });
  });
});
