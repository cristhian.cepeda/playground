import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginatorComponent } from './paginator.component';

describe('PaginatorComponent', () => {
  let component: PaginatorComponent;
  let fixture: ComponentFixture<PaginatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaginatorComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnChanges', () => {
    it('should be detect chanches on totalPages', () => {
      component.totalPages = 10;
      component.ngOnChanges({
        name: new SimpleChange(null, component.totalPages, true),
      });
      fixture.detectChanges();
      expect().nothing();
    });
  });

  describe('onSelectPage', () => {
    it('should be change emit 10', (doneFn) => {
      const page = 10;
      component.totalPages = 11;
      component.ngOnChanges({
        totalPages: new SimpleChange(null, component.totalPages, true),
      });
      fixture.detectChanges();
      component.change.subscribe((currentPage) => {
        expect(currentPage).toEqual(page);
        doneFn();
      });
      component.onSelectPage(page);
    });
  });

  describe('onNext', () => {
    it('should be change emit 10 + 1', (doneFn) => {
      const page = 10;
      component.totalPages = 11;
      component.currentPage = page;
      component.ngOnChanges({
        totalPages: new SimpleChange(null, component.totalPages, true),
        currentPage: new SimpleChange(null, component.currentPage, true),
      });
      fixture.detectChanges();
      component.change.subscribe((currentPage) => {
        expect(currentPage).toEqual(page + 1);
        doneFn();
      });
      component.onNext();
    });
  });

  describe('onPrev', () => {
    it('should be change emit 10 - 1', (doneFn) => {
      const page = 10;
      component.totalPages = 11;
      component.currentPage = page;
      component.ngOnChanges({
        totalPages: new SimpleChange(null, component.totalPages, true),
        currentPage: new SimpleChange(null, component.currentPage, true),
      });
      fixture.detectChanges();
      component.change.subscribe((currentPage) => {
        expect(currentPage).toEqual(page - 1);
        doneFn();
      });
      component.onPrev();
    });
  });
});
