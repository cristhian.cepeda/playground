import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { inOutAnimation } from '@app/core/ animations/ animations';
import { Skeleton } from '@app/core/models/skeleton.model';

import {
  TABLE_DESIGN_CLASS,
  TYPE_COLUMN,
} from '@app/presentation/layout/mo-tables/enums/table.enum';
import { TableHeader } from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import {
  SKELETON_ITEMS_TABLE_DETAILS,
  TABLE_DETAILS_LOADER_SIZE,
} from '../../constants/table.constant';

@Component({
  selector: 'table-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss'],
  animations: inOutAnimation,
})
export class RowComponent<T> implements OnInit, OnChanges {
  @Input() designClass: TABLE_DESIGN_CLASS = TABLE_DESIGN_CLASS.DEFAULT;
  @Input() rowDetailTemplate: TemplateRef<T>;
  @Input() isLoadingRowDetailTemplate: boolean;
  @Input() loadingColumnsDetails: number = TABLE_DETAILS_LOADER_SIZE.COLUMNS;
  @Input() headers: TableHeader[];
  @Input() row: T;
  @Input() urlRow: string;
  @Input() firstColumnFixed: boolean = false;
  @Input() fixedMinWidth: boolean = true;
  @Input() urlRowIdentifier: string = TYPE_COLUMN.ID;

  @Input() showDetails: boolean = false;
  @Input() multiplesDetails: boolean = false;
  @Input() detailsSelected!: T;

  @Output() toggleDetails: EventEmitter<T> = new EventEmitter<T>();

  public skeletons: Skeleton[];
  public loaderDetailsColums: any[];

  constructor(private _router: Router) {}

  ngOnInit(): void {
    this.skeletons = SKELETON_ITEMS_TABLE_DETAILS;
    this.loaderDetailsColums = new Array(this.loadingColumnsDetails);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!!changes['showDetails']?.currentValue && this.showDetails)
      setTimeout(() => this.toggleDetails.emit(this.row), 0);

    if (
      !!changes['detailsSelected']?.currentValue &&
      this.detailsSelected &&
      !this.multiplesDetails
    ) {
      this.showDetails =
        this.detailsSelected[this.urlRowIdentifier] ==
        this.row[this.urlRowIdentifier];
    }
  }

  public onToggleDetails(row: T): void {
    this.showDetails = !this.showDetails;
    if (this.showDetails) this.toggleDetails.emit(row);
  }

  public onGoToUrlRow(header: TableHeader) {
    if (this.urlRow && header?.typeColumn != TYPE_COLUMN.COMPOUND_LINK)
      this._router.navigate([this.urlRow], {
        queryParams: {
          [this.urlRowIdentifier]: this.row[this.urlRowIdentifier],
        },
      });
  }
}
