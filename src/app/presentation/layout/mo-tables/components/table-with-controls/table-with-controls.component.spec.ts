import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Filters } from '@app/core/models/filters.model';
import {
  PAGE_DEFAULT,
  PAGE_SIZE_DEFAULT,
} from '../../constants/page-size.constant';
import { PaginatorComponent } from '../paginator/paginator.component';

import { TableWithControlsComponent } from './table-with-controls.component';

describe('TableWithControlsComponent', () => {
  let component: TableWithControlsComponent<any>;
  let fixture: ComponentFixture<TableWithControlsComponent<any>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [PaginatorComponent],
      declarations: [TableWithControlsComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableWithControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be changePage emit 1', (doneFn) => {
    component.changePage.subscribe((page) => {
      expect(page).toEqual(1);
      doneFn();
    });
    component.onChangePage(1);
    expect(component.currentPage).toEqual(1);
  });

  it('should be changeTableFilters emit 1', (doneFn) => {
    const currentPage: number = PAGE_DEFAULT;
    const pageSize: number = PAGE_SIZE_DEFAULT;
    const offset: number = (currentPage - 1) * pageSize;
    const pageFilter: Filters = { offset, limit: pageSize };
    component.changeTableFilters.subscribe((pageFilter) => {
      expect(pageFilter).toEqual(pageFilter);
      doneFn();
    });
    component.onChangePage(1);
    expect(component.currentPage).toEqual(1);
  });

  it('should be onChangePageSize emit 10', (doneFn) => {
    component.changePageSize.subscribe((pageSize) => {
      expect(pageSize).toEqual(10);
      doneFn();
    });
    component.onChangePageSize(10);
    expect(component.currentPage).toEqual(PAGE_DEFAULT);
  });

  it('should changeTableFilters emit filters mock', (doneFn) => {
    const sortFilterMock: Filters = {};
    component.changeTableFilters.subscribe((sortFilter) => {
      expect(sortFilter).toEqual(sortFilterMock);
      doneFn();
    });
    component.onSort(sortFilterMock);
  });
});
