import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TYPE_COLUMN } from '@app/presentation/layout/mo-tables/enums/table.enum';
import {
  ColumnSort,
  TableHeader,
} from '@app/presentation/layout/mo-tables/interfaces/table.interface';
import { CustomCurrencyPipe } from '@app/presentation/layout/pipes/currency.pipe';
import { MODatePipe } from '@app/presentation/layout/pipes/date.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderComponent } from '../header/header.component';
import { RowComponent } from '../row/row.component';
import { TableComponent } from './table.component';

describe('TableComponent', () => {
  let component: TableComponent<any>;
  let fixture: ComponentFixture<TableComponent<any>>;
  let mockHeaders: TableHeader[];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      declarations: [TableComponent, HeaderComponent, RowComponent],
      providers: [MODatePipe, CustomCurrencyPipe],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    mockHeaders = [
      {
        typeColumn: TYPE_COLUMN.LABEL,
        dataKey: 'test',
      },
    ];
    component.headers = mockHeaders;
    component.isLoading = false;
    fixture.autoDetectChanges();
  });

  it('should create with columnSize 1', () => {
    component.columnsSize = 1;
    component.headers = mockHeaders;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should create with columnSize 5', () => {
    component.headers = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('getTextColumn', () => {
    it('should return the text of the column "dataKey"', () => {
      const header: TableHeader = {
        dataKey: 'test',
      };
      const row: any = {
        test: 'test',
      };
      const result: string = component.getTextColumn(header, row);
      expect(result).toBe('test');
    });

    it('should return the text of the column "-"', () => {
      const header: TableHeader = {
        dataKey: 'test',
      };
      const row: any = {
        test: '',
      };
      const result: string = component.getTextColumn(header, row);
      expect(result).toBe('-');
    });

    it('should return the text of the column "-" when datakey is null', () => {
      const header: TableHeader = {
        dataKey: 'test',
      };
      const row: any = {
        test: null,
      };
      const result: string = component.getTextColumn(header, row);
      expect(result).toBe('-');
    });

    it('should return the text of the column "-" when header is null', () => {
      const header: TableHeader = {
        dataKey: '',
      };
      const row: any = {
        test: null,
      };
      const result: string = component.getTextColumn(header, row);
      expect(result).toBe('-');
    });

    it('should return the text of the column "prueba" when header is null', () => {
      const header: TableHeader = {
        dataKey: '',
        transform: () => {
          return 'prueba';
        },
      };
      const row: any = {
        test: null,
      };
      const result: string = component.getTextColumn(header, row);
      expect(result).toBe('prueba');
    });
  });

  describe('onGoToUrlRow', () => {
    it('should be navigate to the url when click on element td', () => {
      const headers = [
        {
          typeColumn: TYPE_COLUMN.ID,
          dataKey: 'test',
        },
      ];
      const row = {
        test: 'test',
      };
      const identificator = 'test';
      const urlRow = 'urlTest';
      const result = `${urlRow}/${identificator}`;

      component.urlRow = urlRow;
      component.data = [row];
      component.headers = headers;

      fixture.detectChanges();
      const _router = TestBed.inject(Router);
      const spy = spyOn(_router, 'navigateByUrl');

      const tdElement = fixture.debugElement.query(
        By.css('td.table-content')
      ).nativeElement;

      tdElement.click();
      expect(spy).toHaveBeenCalledOnceWith(result);
    });

    it('should navigate to the url', () => {
      const headers: TableHeader[] = [
        {
          dataKey: 'test',
          typeColumn: TYPE_COLUMN.ID,
        },
      ];
      const identificator = 123;
      const urlRow = 'urlTest';
      const row: any = {
        test: identificator,
      };
      component.urlRow = urlRow;
      const _router = TestBed.inject(Router);
      const spy = spyOn(_router, 'navigateByUrl');
      const result = `${urlRow}/${identificator}`;
      component.onGoToUrlRow(headers, row);
      expect(spy).toHaveBeenCalledOnceWith(result);
    });
  });

  describe('onSort', () => {
    it('should be return the result when click element table-header', () => {
      const columnSort: ColumnSort = {
        column: 'test',
        sortBy: 'ASC',
      };
      fixture.detectChanges();
      const tableHeaderDebug = fixture.debugElement.query(
        By.css('table-header')
      );
      tableHeaderDebug.triggerEventHandler('sort', columnSort);
      expect(component.sortedColumn).toEqual('test');
    });

    it('should be return the result on call function onSort', () => {
      const columnSort: ColumnSort = {
        column: 'test',
        sortBy: 'ASC',
      };
      component.onSort(columnSort);
      expect(component.sortedColumn).toEqual('test');
    });
  });
});
