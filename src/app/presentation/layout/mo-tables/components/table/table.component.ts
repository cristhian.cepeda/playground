import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';
import { Filters } from '@app/core/models/filters.model';
import { CONTAINER_SIZE } from '../../constants/container.constants';
import {
  TABLE_COLUMNS_SIZE,
  TABLE_DETAILS_LOADER_SIZE,
  TABLE_LOADER_SIZE,
} from '../../constants/table.constant';
import { TABLE_DESIGN_CLASS, TYPE_COLUMN } from '../../enums/table.enum';
import { ColumnSort, TableHeader } from '../../interfaces/table.interface';

@Component({
  selector: 'mo-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent<T> implements OnInit, OnChanges {
  @Input() containerSize: string = CONTAINER_SIZE.DEFAULT;
  @Input() designClass: TABLE_DESIGN_CLASS = TABLE_DESIGN_CLASS.DEFAULT;
  @Input() headers: TableHeader[];
  @Input() data: T[];
  @Input() multiplesDetails: boolean = false;
  @Input() showDetails: boolean = false;
  @Input() rowDetailTemplate: TemplateRef<T>;
  @Input() firstColumnFixed: boolean = false;
  @Input() fixedMinWidth: boolean = true;
  @Input() urlRow: string;
  @Input() urlRowIdentifier: string = TYPE_COLUMN.ID;
  @Input() isLoading: boolean;
  @Input() isLoadingRowDetailTemplate: boolean;
  @Input() loadingColumnsDetails: number = TABLE_DETAILS_LOADER_SIZE.COLUMNS;
  @Input() loadingRows: number = TABLE_LOADER_SIZE.ROWS;
  @Input() loadingColumns: number = TABLE_LOADER_SIZE.COLUMNS;
  @Output() sortFilters: EventEmitter<Filters> = new EventEmitter<Filters>();
  @Output() toggleDetails: EventEmitter<T> = new EventEmitter<T>();

  public detailsSelected: T;
  public sortedColumn: string;
  public columnsSize: number;
  public loaderRows: any[];
  public loaderColums: any[];

  public TYPE_COLUMNS = TYPE_COLUMN;

  constructor() {}

  ngOnInit(): void {
    this.loaderRows = new Array(this.loadingRows);
    this.loaderColums = new Array(this.loadingColumns);
    this._calculateColumnSize();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data']) this.detailsSelected = null;
  }

  public onSort(columnSort: ColumnSort) {
    this.sortedColumn = columnSort.column;
    const ordering: string =
      columnSort.sortBy == 'ASC' && this.sortedColumn
        ? `-${this.sortedColumn}`
        : this.sortedColumn;

    const tableSortFilter: Filters = { ordering };
    this.sortFilters.emit(tableSortFilter);
  }

  public onToggleDetails(row: T): void {
    if (!this.multiplesDetails) this.detailsSelected = row;
    this.toggleDetails.emit(row);
  }

  private _calculateColumnSize() {
    this.columnsSize = Math.round(
      TABLE_COLUMNS_SIZE.TOTAL_COLUMNS /
        (this.headers?.length ?? TABLE_COLUMNS_SIZE.MIN_COLUMNS)
    );
    this.columnsSize =
      this.columnsSize < TABLE_COLUMNS_SIZE.MIN_COLUMNS
        ? TABLE_COLUMNS_SIZE.MIN_COLUMNS
        : this.columnsSize;
  }
}
