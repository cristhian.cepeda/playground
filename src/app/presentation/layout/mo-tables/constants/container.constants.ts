export const CONTAINER_SIZE = {
  DEFAULT: 'mo-container',
  SMALL: 'mo-container-sm',
  MEDIUM: 'mo-container-md',
  LARGE: 'mo-container-lg',
  EXTRA_LARGE: 'mo-container-xl',
};
