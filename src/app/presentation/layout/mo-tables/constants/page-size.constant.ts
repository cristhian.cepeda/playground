export const PAGE_DEFAULT = 1;
export const PAGE_SIZE_DEFAULT = 20;

export const PAGE_SIZE_OPTIONS = [
  { value: 5, key: '5' },
  { value: 10, key: '10' },
  { value: 15, key: '15' },
  { value: 20, key: '20' },
];
