import {
  getSkeleton,
  Skeleton,
  SkeletonTheme,
} from '@app/core/models/skeleton.model';
import { TableHeader } from '../interfaces/table.interface';

export interface iTableConst<T> {
  HEADERS: TableHeader[];
  DATA: T;
}

export const TABLE_COLUMNS_SIZE = {
  TOTAL_COLUMNS: 12,
  MIN_COLUMNS: 4,
};

export const TABLE_LOADER_SIZE = {
  ROWS: 5,
  COLUMNS: 5,
};

export const TABLE_DETAILS_LOADER_SIZE = {
  COLUMNS: 3,
};

const skeletonTheme: SkeletonTheme = {
  minWidth: 100,
  maxWidth: 150,
  margin: '0 5px 5px 0',
};

export const SKELETON_ITEMS_TABLE_DETAILS: Skeleton[] = [
  ...getSkeleton(1, 1, skeletonTheme),
  ...getSkeleton(2, 3, skeletonTheme),
];
