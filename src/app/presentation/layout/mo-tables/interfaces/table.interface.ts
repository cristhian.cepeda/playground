import { TemplateRef } from '@angular/core';
import { QueryParamRouting } from '@app/core/models/utils.model';
import { TYPE_COLUMN } from '../enums/table.enum';

export type TypeSort = 'string' | 'number' | 'date';

export interface TableResponse<T> {
  filtered?: number;
  count?: number;
  link?: TableResponseLinks;
  results?: T[];
}

export interface TableResponseLinks {
  next?: string;
  previous?: string;
}

export interface TableHeader {
  template?: TemplateRef<ContextColumn>;
  label?: string;
  size?: number; // number between 1 to 12 for cols
  isSortable?: boolean;
  typeSort?: TypeSort;
  isCopyable?: boolean;
  messageCopied?: string;
  typeColumn?: TYPE_COLUMN;
  tooltipTemplate?: TemplateRef<ContextColumn>;
  dataKey?: string;
  color?: string;
  customClass?: CustomClass;
  transform?: Function;
  compoundLink?: string;
  compoundLinkParams?: QueryParamRouting[]; // Name of the properties to be used as queryParams
}

export interface Column<T> {
  headers?: TableHeader[];
  rows?: T[];
  fixedRows?: { [key: string]: T }[];
}

export interface ColumnSort {
  column: string;
  sortBy: 'ASC' | 'DESC';
  typeSort?: TypeSort;
}

export interface ContextColumn {
  column: TableHeader;
  onChange?: Function;
  onClick?: Function;
  onCopyValue?: Function;
}

export interface CustomClass {
  header?: string;
  row?: string;
}

export interface TableFamilyConfig {
  [key: string]: {
    [key: string]: TableHeader[];
  };
}
