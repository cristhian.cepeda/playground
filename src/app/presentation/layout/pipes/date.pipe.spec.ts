import { MODatePipe } from './date.pipe';

describe('MODatePipe', () => {
  const pipe = new MODatePipe();

  it('transforms "2022-10-22" to "Sat, 22 Oct, 2022"', () => {
    const date = '2022-10-22 00:00:10';
    const resultDate = 'Sat, 22 Oct, 2022';
    expect(pipe.transform(new Date(date))).toBe(resultDate);
  });

  it('transforms "2022-10-22" to "Sat, 22 Oct, 2022"', () => {
    const date = '2022-10-22 00:00:10';
    const formatDate = 'dd, MMM, yyyy';
    const resultDate = '22, Oct, 2022';
    expect(pipe.transform(new Date(date), formatDate)).toBe(resultDate);
  });

  it('shoul be return "" when value is null', () => {
    expect(pipe.transform(null)).toBe('');
  });

  it('shoul be return "" when value is undefined', () => {
    expect(pipe.transform(undefined)).toBe('');
  });

  it('shoul be return "" when value is empty string', () => {
    expect(pipe.transform('')).toBe('');
  });
});
