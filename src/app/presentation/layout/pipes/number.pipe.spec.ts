import { MONumberPipe } from './number.pipe';

describe('MONumberPipe', () => {
  const pipe = new MONumberPipe();

  it('transforms "123123" to "123,123"', () => {
    expect(pipe.transform('123123')).toBe('123,123');
  });

  it('transforms "123" to "123"', () => {
    expect(pipe.transform('123')).toBe('123');
  });

  it('transforms "0" to "0"', () => {
    expect(pipe.transform('0')).toBe('0');
  });

  it('transforms "" to "0"', () => {
    expect(pipe.transform('')).toBe('0');
  });

  it('transforms "123456789" to "123,456,789"', () => {
    expect(pipe.transform('123456789')).toBe('123,456,789');
  });
});
