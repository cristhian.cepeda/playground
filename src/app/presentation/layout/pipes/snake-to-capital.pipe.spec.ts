import { SnakeToCapitalPipe } from './snake-to-capital.pipe';

describe('SnakeToCapitalPipe', () => {
  const pipe = new SnakeToCapitalPipe();

  it('transforms "snake_case" to "Snake case"', () => {
    expect(pipe.transform('snake_case')).toBe('Snake case');
  });

  it('transforms "hola" to "Hola"', () => {
    expect(pipe.transform('hola')).toBe('Hola');
  });

  it('transforms "" to ""', () => {
    expect(pipe.transform('')).toBe('');
  });

  it('transforms null to ""', () => {
    expect(pipe.transform(null)).toBe('');
  });
});
