export const environment = {
  production: true,
  testServeWait: 1000,
  isEnablePutRecord: true,
  isTestServer: false,
  api_url: '',
  ga_key: '',
  gtag_id: '',
  pixel_id: '',
  hotjar_id: '',
  hotjar_version: '',
  reportIframeUrl: '',
};
