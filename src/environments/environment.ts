export const environment = {
  production: false,
  testServeWait: 1000,
  isEnablePutRecord: true,
  isTestServer: true,
  api_url: '',
  ga_key: '',
  gtag_id: '',
  pixel_id: '',
  hotjar_id: '',
  hotjar_version: '',
  reportIframeUrl: '',
};
